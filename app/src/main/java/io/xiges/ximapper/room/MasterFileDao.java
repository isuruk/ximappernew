package io.xiges.ximapper.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MasterFileDao {
    @Query("SELECT * FROM master_file")
    List<MasterFileEntity> getAll();

    @Query("SELECT * FROM master_file WHERE mid IN (:master_file_id)")
    List<MasterFileEntity> loadAllByIds(int[] master_file_id);


    @Insert
    void insertAll(MasterFileEntity... masterFileEntities);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(MasterFileEntity masterFileEntity);

    @Delete
    void delete(MasterFileEntity receipt);
}
