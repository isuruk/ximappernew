package io.xiges.ximapper.room;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "master_file")
public class MasterFileEntity {

    @PrimaryKey
    private long mid;

    @ColumnInfo(name = "master_file_id")
    private String masterFileId;

    @ColumnInfo(name = "version")
    private String version;

    public long getMid() {
        return mid;
    }

    public void setMid(long mid) {
        this.mid = mid;
    }

    public String getMasterFileId() {
        return masterFileId;
    }

    public void setMasterFileId(String masterFileId) {
        this.masterFileId = masterFileId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
