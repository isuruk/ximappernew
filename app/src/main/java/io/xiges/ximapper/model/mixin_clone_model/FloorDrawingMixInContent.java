package io.xiges.ximapper.model.mixin_clone_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class FloorDrawingMixInContent implements Parcelable {
     private String drawingType;
     @SerializedName("fillPoints")
     private List<PointMixinContent> fillLayerPointList = new ArrayList<>();
     @SerializedName("linePoints")
     private List<PointMixinContent> lineLayerPointList = new ArrayList<>();
     @SerializedName("circlePoints")
     private List<FeatureMixinContent> circleLayerFeatureList = new ArrayList<>();
     @SerializedName("circle_drawing")
     private CircleMixinContent circle;
     private String fillLayerID;
     private String lineLayerID;
     private String circleLayerID;
     private String fillSourceID;
     private String lineSourceID;
     private String circleSourceID;
     private String aqCircleLayerID;
     private String aqCircleSourceID;
     private String aqLineSourceID;
     private String aqLineLayerID;
     private String aqFillSourceID;
     private String aqFillLayerID;
     @SerializedName("acquisitionFillPoints")
     private List<PointMixinContent> fillLayerAQPointList = new ArrayList<>();
     @SerializedName("acquisitionLinePoints")
     private List<PointMixinContent> lineLayerAQPointList = new ArrayList<>();
     @SerializedName("acquisitionCirclePoints")
     private List<FeatureMixinContent> circleLayerAQFeatureList = new ArrayList<>();
     public FloorDrawingMixInContent() {
          drawingType="";
     }


     protected FloorDrawingMixInContent(Parcel in) {
          drawingType = in.readString();
          fillLayerPointList = in.createTypedArrayList(PointMixinContent.CREATOR);
          lineLayerPointList = in.createTypedArrayList(PointMixinContent.CREATOR);
          circleLayerFeatureList = in.createTypedArrayList(FeatureMixinContent.CREATOR);
          circle = in.readParcelable(CircleMixinContent.class.getClassLoader());
          fillLayerID = in.readString();
          lineLayerID = in.readString();
          circleLayerID = in.readString();
          fillSourceID = in.readString();
          lineSourceID = in.readString();
          circleSourceID = in.readString();
          aqCircleLayerID = in.readString();
          aqCircleSourceID = in.readString();
          aqLineSourceID = in.readString();
          aqLineLayerID = in.readString();
          aqFillSourceID = in.readString();
          aqFillLayerID = in.readString();
          fillLayerAQPointList = in.createTypedArrayList(PointMixinContent.CREATOR);
          lineLayerAQPointList = in.createTypedArrayList(PointMixinContent.CREATOR);
          circleLayerAQFeatureList = in.createTypedArrayList(FeatureMixinContent.CREATOR);
     }

     @Override
     public void writeToParcel(Parcel dest, int flags) {
          dest.writeString(drawingType);
          dest.writeTypedList(fillLayerPointList);
          dest.writeTypedList(lineLayerPointList);
          dest.writeTypedList(circleLayerFeatureList);
          dest.writeParcelable(circle, flags);
          dest.writeString(fillLayerID);
          dest.writeString(lineLayerID);
          dest.writeString(circleLayerID);
          dest.writeString(fillSourceID);
          dest.writeString(lineSourceID);
          dest.writeString(circleSourceID);
          dest.writeString(aqCircleLayerID);
          dest.writeString(aqCircleSourceID);
          dest.writeString(aqLineSourceID);
          dest.writeString(aqLineLayerID);
          dest.writeString(aqFillSourceID);
          dest.writeString(aqFillLayerID);
          dest.writeTypedList(fillLayerAQPointList);
          dest.writeTypedList(lineLayerAQPointList);
          dest.writeTypedList(circleLayerAQFeatureList);
     }

     @Override
     public int describeContents() {
          return 0;
     }

     public static final Creator<FloorDrawingMixInContent> CREATOR = new Creator<FloorDrawingMixInContent>() {
          @Override
          public FloorDrawingMixInContent createFromParcel(Parcel in) {
               return new FloorDrawingMixInContent(in);
          }

          @Override
          public FloorDrawingMixInContent[] newArray(int size) {
               return new FloorDrawingMixInContent[size];
          }
     };

     public String getDrawingType() {
          return drawingType;
     }

     public void setDrawingType(String drawingType) {
          this.drawingType = drawingType;
     }

     public List<PointMixinContent> getFillLayerPointList() {
          return fillLayerPointList;
     }

     public void setFillLayerPointList(List<PointMixinContent> fillLayerPointList) {
          this.fillLayerPointList = fillLayerPointList;
     }

     public List<PointMixinContent> getLineLayerPointList() {
          return lineLayerPointList;
     }

     public void setLineLayerPointList(List<PointMixinContent> lineLayerPointList) {
          this.lineLayerPointList = lineLayerPointList;
     }

     public List<FeatureMixinContent> getCircleLayerFeatureList() {
          return circleLayerFeatureList;
     }

     public void setCircleLayerFeatureList(List<FeatureMixinContent> circleLayerFeatureList) {
          this.circleLayerFeatureList = circleLayerFeatureList;
     }

     public CircleMixinContent getCircle() {
          return circle;
     }

     public void setCircle(CircleMixinContent circle) {
          this.circle = circle;
     }

     public String getFillLayerID() {
          return fillLayerID;
     }

     public void setFillLayerID(String fillLayerID) {
          this.fillLayerID = fillLayerID;
     }

     public String getLineLayerID() {
          return lineLayerID;
     }

     public void setLineLayerID(String lineLayerID) {
          this.lineLayerID = lineLayerID;
     }

     public String getCircleLayerID() {
          return circleLayerID;
     }

     public void setCircleLayerID(String circleLayerID) {
          this.circleLayerID = circleLayerID;
     }

     public String getFillSourceID() {
          return fillSourceID;
     }

     public void setFillSourceID(String fillSourceID) {
          this.fillSourceID = fillSourceID;
     }

     public String getLineSourceID() {
          return lineSourceID;
     }

     public void setLineSourceID(String lineSourceID) {
          this.lineSourceID = lineSourceID;
     }

     public String getCircleSourceID() {
          return circleSourceID;
     }

     public void setCircleSourceID(String circleSourceID) {
          this.circleSourceID = circleSourceID;
     }

     public String getAqCircleLayerID() {
          return aqCircleLayerID;
     }

     public void setAqCircleLayerID(String aqCircleLayerID) {
          this.aqCircleLayerID = aqCircleLayerID;
     }

     public String getAqCircleSourceID() {
          return aqCircleSourceID;
     }

     public void setAqCircleSourceID(String aqCircleSourceID) {
          this.aqCircleSourceID = aqCircleSourceID;
     }

     public String getAqLineSourceID() {
          return aqLineSourceID;
     }

     public void setAqLineSourceID(String aqLineSourceID) {
          this.aqLineSourceID = aqLineSourceID;
     }

     public String getAqLineLayerID() {
          return aqLineLayerID;
     }

     public void setAqLineLayerID(String aqLineLayerID) {
          this.aqLineLayerID = aqLineLayerID;
     }

     public String getAqFillSourceID() {
          return aqFillSourceID;
     }

     public void setAqFillSourceID(String aqFillSourceID) {
          this.aqFillSourceID = aqFillSourceID;
     }

     public String getAqFillLayerID() {
          return aqFillLayerID;
     }

     public void setAqFillLayerID(String aqFillLayerID) {
          this.aqFillLayerID = aqFillLayerID;
     }

     public List<PointMixinContent> getFillLayerAQPointList() {
          return fillLayerAQPointList;
     }

     public void setFillLayerAQPointList(List<PointMixinContent> fillLayerAQPointList) {
          this.fillLayerAQPointList = fillLayerAQPointList;
     }

     public List<PointMixinContent> getLineLayerAQPointList() {
          return lineLayerAQPointList;
     }

     public void setLineLayerAQPointList(List<PointMixinContent> lineLayerAQPointList) {
          this.lineLayerAQPointList = lineLayerAQPointList;
     }

     public List<FeatureMixinContent> getCircleLayerAQFeatureList() {
          return circleLayerAQFeatureList;
     }

     public void setCircleLayerAQFeatureList(List<FeatureMixinContent> circleLayerAQFeatureList) {
          this.circleLayerAQFeatureList = circleLayerAQFeatureList;
     }
}
