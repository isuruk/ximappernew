package io.xiges.ximapper.model.api.master_data;

import android.os.Parcel;
import android.os.Parcelable;

public class LandUseType implements Parcelable
{

    private int id;
    private String description;
    public final static Parcelable.Creator<LandUseType> CREATOR = new Creator<LandUseType>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LandUseType createFromParcel(Parcel in) {
            return new LandUseType(in);
        }

        public LandUseType[] newArray(int size) {
            return (new LandUseType[size]);
        }

    }
            ;

    protected LandUseType(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public LandUseType() {
    }

    /**
     *
     * @param description
     * @param id
     */
    public LandUseType(int id, String description) {
        super();
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(description);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "LandUseType{" +
                "id=" + id +
                ", description='" + description + '\'' +
                '}';
    }
}