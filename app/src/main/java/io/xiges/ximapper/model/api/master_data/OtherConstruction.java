package io.xiges.ximapper.model.api.master_data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class OtherConstruction implements Parcelable
{

    private List<OtherConstructionsType> otherConstructionsTypes = null;
    private List<OtherConstructionsMaterial> otherConstructionsMaterials = null;
    private List<OtherConstructionUnit> otherConstructionUnits = null;
    public final static Parcelable.Creator<OtherConstruction> CREATOR = new Creator<OtherConstruction>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OtherConstruction createFromParcel(Parcel in) {
            return new OtherConstruction(in);
        }

        public OtherConstruction[] newArray(int size) {
            return (new OtherConstruction[size]);
        }

    }
            ;

    protected OtherConstruction(Parcel in) {
        in.readList(this.otherConstructionsTypes, (OtherConstructionsType.class.getClassLoader()));
        in.readList(this.otherConstructionsMaterials, (OtherConstructionsMaterial.class.getClassLoader()));
        in.readList(this.otherConstructionUnits, (OtherConstructionUnit.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public OtherConstruction() {
    }

    /**
     *
     * @param otherConstructionsMaterials
     * @param otherConstructionsTypes
     * @param otherConstructionUnits
     */
    public OtherConstruction(List<OtherConstructionsType> otherConstructionsTypes, List<OtherConstructionsMaterial> otherConstructionsMaterials, List<OtherConstructionUnit> otherConstructionUnits) {
        super();
        this.otherConstructionsTypes = otherConstructionsTypes;
        this.otherConstructionsMaterials = otherConstructionsMaterials;
        this.otherConstructionUnits = otherConstructionUnits;
    }

    public List<OtherConstructionsType> getOtherConstructionsTypes() {
        if(otherConstructionsTypes == null){
            otherConstructionsTypes =new ArrayList<>();
        }
        return otherConstructionsTypes;
    }

    public void setOtherConstructionsTypes(List<OtherConstructionsType> otherConstructionsTypes) {
        this.otherConstructionsTypes = otherConstructionsTypes;
    }

    public List<OtherConstructionsMaterial> getOtherConstructionsMaterials() {
        if(otherConstructionsMaterials == null){
            otherConstructionsMaterials =new ArrayList<>();
        }
        return otherConstructionsMaterials;
    }

    public void setOtherConstructionsMaterials(List<OtherConstructionsMaterial> otherConstructionsMaterials) {
        this.otherConstructionsMaterials = otherConstructionsMaterials;
    }

    public List<OtherConstructionUnit> getOtherConstructionUnits() {
        if(otherConstructionUnits == null){
            otherConstructionUnits =new ArrayList<>();
        }
        return otherConstructionUnits;
    }

    public void setOtherConstructionUnits(List<OtherConstructionUnit> otherConstructionUnits) {
        this.otherConstructionUnits = otherConstructionUnits;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(otherConstructionsTypes);
        dest.writeList(otherConstructionsMaterials);
        dest.writeList(otherConstructionUnits);
    }

    public int describeContents() {
        return 0;
    }

}