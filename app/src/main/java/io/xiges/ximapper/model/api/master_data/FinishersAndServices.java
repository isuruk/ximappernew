package io.xiges.ximapper.model.api.master_data;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class FinishersAndServices implements Parcelable
{

    private List<FinishersAndServicesWallFinisher> finishersAndServicesWallFinisher = null;
    private List<FinishersAndServicesFloorFinisher> finishersAndServicesFloorFinisher = null;
    private List<FinishersAndServicesBathroomAndToilet> finishersAndServicesBathroomAndToilet = null;
    private List<FinishersAndServicesService> finishersAndServicesServices = null;
    public final static Parcelable.Creator<FinishersAndServices> CREATOR = new Creator<FinishersAndServices>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FinishersAndServices createFromParcel(Parcel in) {
            return new FinishersAndServices(in);
        }

        public FinishersAndServices[] newArray(int size) {
            return (new FinishersAndServices[size]);
        }

    }
            ;

    protected FinishersAndServices(Parcel in) {
        in.readList(this.finishersAndServicesWallFinisher, (FinishersAndServicesWallFinisher.class.getClassLoader()));
        in.readList(this.finishersAndServicesFloorFinisher, (FinishersAndServicesFloorFinisher.class.getClassLoader()));
        in.readList(this.finishersAndServicesBathroomAndToilet, (FinishersAndServicesBathroomAndToilet.class.getClassLoader()));
        in.readList(this.finishersAndServicesServices, (FinishersAndServicesService.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public FinishersAndServices() {
    }

    /**
     *
     * @param finishersAndServicesBathroomAndToilet
     * @param finishersAndServicesFloorFinisher
     * @param finishersAndServicesServices
     * @param finishersAndServicesWallFinisher
     */
    public FinishersAndServices(List<FinishersAndServicesWallFinisher> finishersAndServicesWallFinisher, List<FinishersAndServicesFloorFinisher> finishersAndServicesFloorFinisher, List<FinishersAndServicesBathroomAndToilet> finishersAndServicesBathroomAndToilet, List<FinishersAndServicesService> finishersAndServicesServices) {
        super();
        this.finishersAndServicesWallFinisher = finishersAndServicesWallFinisher;
        this.finishersAndServicesFloorFinisher = finishersAndServicesFloorFinisher;
        this.finishersAndServicesBathroomAndToilet = finishersAndServicesBathroomAndToilet;
        this.finishersAndServicesServices = finishersAndServicesServices;
    }

    public List<FinishersAndServicesWallFinisher> getFinishersAndServicesWallFinisher() {
        return finishersAndServicesWallFinisher;
    }

    public void setFinishersAndServicesWallFinisher(List<FinishersAndServicesWallFinisher> finishersAndServicesWallFinisher) {
        this.finishersAndServicesWallFinisher = finishersAndServicesWallFinisher;
    }

    public List<FinishersAndServicesFloorFinisher> getFinishersAndServicesFloorFinisher() {
        return finishersAndServicesFloorFinisher;
    }

    public void setFinishersAndServicesFloorFinisher(List<FinishersAndServicesFloorFinisher> finishersAndServicesFloorFinisher) {
        this.finishersAndServicesFloorFinisher = finishersAndServicesFloorFinisher;
    }

    public List<FinishersAndServicesBathroomAndToilet> getFinishersAndServicesBathroomAndToilet() {
        return finishersAndServicesBathroomAndToilet;
    }

    public void setFinishersAndServicesBathroomAndToilet(List<FinishersAndServicesBathroomAndToilet> finishersAndServicesBathroomAndToilet) {
        this.finishersAndServicesBathroomAndToilet = finishersAndServicesBathroomAndToilet;
    }

    public List<FinishersAndServicesService> getFinishersAndServicesServices() {
        return finishersAndServicesServices;
    }

    public void setFinishersAndServicesServices(List<FinishersAndServicesService> finishersAndServicesServices) {
        this.finishersAndServicesServices = finishersAndServicesServices;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(finishersAndServicesWallFinisher);
        dest.writeList(finishersAndServicesFloorFinisher);
        dest.writeList(finishersAndServicesBathroomAndToilet);
        dest.writeList(finishersAndServicesServices);
    }

    public int describeContents() {
        return 0;
    }

}