package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BuildingFloor implements Parcelable {
    String floorNo;
    @SerializedName("floorData")
    List<FloorDrawing> floorDrawingList = new ArrayList<>();
    @SerializedName("floorLabelData")
    List<Symbol> symbols = new ArrayList<>();
    @SerializedName("floorSymbol")
    List<Symbol> floorSymbol = new ArrayList<>();
    private double floorArea;
    private double aqArea;
    private int gFloors;
    private int uFloors;

    public BuildingFloor(Parcel in) {
        floorNo = in.readString();
    }

    public BuildingFloor() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(floorNo);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BuildingFloor> CREATOR = new Creator<BuildingFloor>() {
        @Override
        public BuildingFloor createFromParcel(Parcel in) {
            return new BuildingFloor(in);
        }

        @Override
        public BuildingFloor[] newArray(int size) {
            return new BuildingFloor[size];
        }
    };

    public String getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(String floorNo) {
        this.floorNo = floorNo;
    }

    public List<FloorDrawing> getFloorDrawingList() {
        return floorDrawingList;
    }

    public void setFloorDrawingList(List<FloorDrawing> floorDrawingList) {
        this.floorDrawingList = floorDrawingList;
    }

    public List<Symbol> getFloorSymbol() {
        return floorSymbol;
    }

    public void setFloorSymbol(List<Symbol> floorSymbol) {
        this.floorSymbol = floorSymbol;
    }

    public List<Symbol> getSymbols() {
        return symbols;
    }

    public void setSymbols(List<Symbol> symbols) {
        this.symbols = symbols;
    }

    public double getFloorArea() {
        return floorArea;
    }

    public void setFloorArea(double floorArea) {
        this.floorArea = floorArea;
    }

    public double getAqArea() {
        return aqArea;
    }

    public void setAqArea(double aqArea) {
        this.aqArea = aqArea;
    }

    public int getgFloors() {
        return gFloors;
    }

    public void setgFloors(int gFloors) {
        this.gFloors = gFloors;
    }

    public int getuFloors() {
        return uFloors;
    }

    public void setuFloors(int uFloors) {
        this.uFloors = uFloors;
    }

    @Override
    public String toString() {
        return "BuildingFloor{" +
                "floorNo='" + floorNo + '\'' +
                ", floorDrawingList=" + floorDrawingList +
                ", symbols=" + symbols +
                '}';
    }
}
