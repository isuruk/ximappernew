package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FloorCopyDetails implements Parcelable {
    private String floorId;
    private int index;
    private boolean isAqLine;
    private boolean isFloor;
    private boolean isRoom;


    public FloorCopyDetails(Parcel in) {
        setFloorId(in.readString());
        setIndex(in.readInt());
        setAqLine(in.readByte() != 0);
        setFloor(in.readByte() != 0);
        setRoom(in.readByte() != 0);
    }

    public FloorCopyDetails() {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getFloorId());
        dest.writeInt(getIndex());
        dest.writeByte((byte) (isAqLine() ? 1 : 0));
        dest.writeByte((byte) (isFloor() ? 1 : 0));
        dest.writeByte((byte) (isRoom() ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FloorCopyDetails> CREATOR = new Creator<FloorCopyDetails>() {
        @Override
        public FloorCopyDetails createFromParcel(Parcel in) {
            return new FloorCopyDetails(in);
        }

        @Override
        public FloorCopyDetails[] newArray(int size) {
            return new FloorCopyDetails[size];
        }
    };

    public boolean isAqLine() {
        return isAqLine;
    }

    public void setAqLine(boolean aqLine) {
        isAqLine = aqLine;
    }

    public boolean isFloor() {
        return isFloor;
    }

    public void setFloor(boolean floor) {
        isFloor = floor;
    }

    public boolean isRoom() {
        return isRoom;
    }

    public void setRoom(boolean room) {
        isRoom = room;
    }

    public String getFloorId() {
        return floorId;
    }

    public void setFloorId(String floorId) {
        this.floorId = floorId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
