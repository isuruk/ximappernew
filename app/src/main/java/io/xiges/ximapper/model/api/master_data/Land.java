package io.xiges.ximapper.model.api.master_data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Land implements Parcelable
{

    private List<AccessCategory> accessCategory = null;
    private List<LandCategory> landCategory = null;
    private List<LandSubCategory> landSubCategory = null;
    private List<LandUseType> landUseType = null;
    public final static Parcelable.Creator<Land> CREATOR = new Creator<Land>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Land createFromParcel(Parcel in) {
            return new Land(in);
        }

        public Land[] newArray(int size) {
            return (new Land[size]);
        }

    }
            ;

    protected Land(Parcel in) {
        in.readList(this.accessCategory, (AccessCategory.class.getClassLoader()));
        in.readList(this.landCategory, (LandCategory.class.getClassLoader()));
        in.readList(this.landSubCategory, (LandSubCategory.class.getClassLoader()));
        in.readList(this.landUseType, (LandUseType.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Land() {
    }

    /**
     *
     * @param landSubCategory
     * @param landUseType
     * @param accessCategory
     * @param landCategory
     */
    public Land(List<AccessCategory> accessCategory, List<LandCategory> landCategory, List<LandSubCategory> landSubCategory, List<LandUseType> landUseType) {
        super();
        this.accessCategory = accessCategory;
        this.landCategory = landCategory;
        this.landSubCategory = landSubCategory;
        this.landUseType = landUseType;
    }

    public List<AccessCategory> getAccessCategory() {
        return accessCategory;
    }

    public void setAccessCategory(List<AccessCategory> accessCategory) {
        this.accessCategory = accessCategory;
    }

    public List<LandCategory> getLandCategory() {
        return landCategory;
    }

    public void setLandCategory(List<LandCategory> landCategory) {
        this.landCategory = landCategory;
    }

    public List<LandSubCategory> getLandSubCategory() {
        return landSubCategory;
    }

    public void setLandSubCategory(List<LandSubCategory> landSubCategory) {
        this.landSubCategory = landSubCategory;
    }

    public List<LandUseType> getLandUseType() {
        return landUseType;
    }

    public void setLandUseType(List<LandUseType> landUseType) {
        this.landUseType = landUseType;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(accessCategory);
        dest.writeList(landCategory);
        dest.writeList(landSubCategory);
        dest.writeList(landUseType);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "Land{" +
                "accessCategory=" + accessCategory +
                ", landCategory=" + landCategory +
                ", landSubCategory=" + landSubCategory +
                ", landUseType=" + landUseType +
                '}';
    }
}