package io.xiges.ximapper.model.building_details;

import android.os.Parcel;
import android.os.Parcelable;

public class WallDetails implements Parcelable {
    private boolean brick;
    private boolean cabook;
    private boolean timberPlank;
    private boolean  block;
    private boolean clay;
    private String wallRemarks;

    public WallDetails(Parcel in) {
        brick = in.readByte() != 0;
        cabook = in.readByte() != 0;
        timberPlank = in.readByte() != 0;
        block = in.readByte() != 0;
        clay = in.readByte() != 0;
        wallRemarks = in.readString();
    }

    public static final Creator<WallDetails> CREATOR = new Creator<WallDetails>() {
        @Override
        public WallDetails createFromParcel(Parcel in) {
            return new WallDetails(in);
        }

        @Override
        public WallDetails[] newArray(int size) {
            return new WallDetails[size];
        }
    };

    public WallDetails() {

    }

    public boolean isBrick() {
        return brick;
    }

    public void setBrick(boolean brick) {
        this.brick = brick;
    }

    public boolean isCabook() {
        return cabook;
    }

    public void setCabook(boolean cabook) {
        this.cabook = cabook;
    }

    public boolean isTimberPlank() {
        return timberPlank;
    }

    public void setTimberPlank(boolean timberPlank) {
        this.timberPlank = timberPlank;
    }

    public boolean isBlock() {
        return block;
    }

    public void setBlock(boolean block) {
        this.block = block;
    }

    public boolean isClay() {
        return clay;
    }

    public void setClay(boolean clay) {
        this.clay = clay;
    }

    public String getWallRemarks() {
        return wallRemarks;
    }

    public void setWallRemarks(String wallRemarks) {
        this.wallRemarks = wallRemarks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (brick ? 1 : 0));
        dest.writeByte((byte) (cabook ? 1 : 0));
        dest.writeByte((byte) (timberPlank ? 1 : 0));
        dest.writeByte((byte) (block ? 1 : 0));
        dest.writeByte((byte) (clay ? 1 : 0));
        dest.writeString(wallRemarks);
    }
}
