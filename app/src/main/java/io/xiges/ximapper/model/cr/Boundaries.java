
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Boundaries implements Parcelable
{

    private String bottom;
    private String east;
    private String north;
    private String south;
    private String top;
    private String west;
    public final static Parcelable.Creator<Boundaries> CREATOR = new Creator<Boundaries>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Boundaries createFromParcel(Parcel in) {
            return new Boundaries(in);
        }

        public Boundaries[] newArray(int size) {
            return (new Boundaries[size]);
        }

    }
    ;

    protected Boundaries(Parcel in) {
        this.bottom = ((String) in.readValue((String.class.getClassLoader())));
        this.east = ((String) in.readValue((String.class.getClassLoader())));
        this.north = ((String) in.readValue((String.class.getClassLoader())));
        this.south = ((String) in.readValue((String.class.getClassLoader())));
        this.top = ((String) in.readValue((String.class.getClassLoader())));
        this.west = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Boundaries() {
    }

    /**
     * 
     * @param east
     * @param top
     * @param south
     * @param bottom
     * @param north
     * @param west
     */
    public Boundaries(String bottom, String east, String north, String south, String top, String west) {
        super();
        this.bottom = bottom;
        this.east = east;
        this.north = north;
        this.south = south;
        this.top = top;
        this.west = west;
    }

    public String getBottom() {
        return bottom;
    }

    public void setBottom(String bottom) {
        this.bottom = bottom;
    }

    public String getEast() {
        return east;
    }

    public void setEast(String east) {
        this.east = east;
    }

    public String getNorth() {
        return north;
    }

    public void setNorth(String north) {
        this.north = north;
    }

    public String getSouth() {
        return south;
    }

    public void setSouth(String south) {
        this.south = south;
    }

    public String getTop() {
        return top;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public String getWest() {
        return west;
    }

    public void setWest(String west) {
        this.west = west;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(bottom);
        dest.writeValue(east);
        dest.writeValue(north);
        dest.writeValue(south);
        dest.writeValue(top);
        dest.writeValue(west);
    }

    public int describeContents() {
        return  0;
    }

}
