
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class GramaNiladhariDetails implements Parcelable
{

    private String date;
    private String name;
    private String sign;
    public final static Parcelable.Creator<GramaNiladhariDetails> CREATOR = new Creator<GramaNiladhariDetails>() {


        @SuppressWarnings({
            "unchecked"
        })
        public GramaNiladhariDetails createFromParcel(Parcel in) {
            return new GramaNiladhariDetails(in);
        }

        public GramaNiladhariDetails[] newArray(int size) {
            return (new GramaNiladhariDetails[size]);
        }

    }
    ;

    protected GramaNiladhariDetails(Parcel in) {
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.sign = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public GramaNiladhariDetails() {
    }

    /**
     * 
     * @param date
     * @param name
     * @param sign
     */
    public GramaNiladhariDetails(String date, String name, String sign) {
        super();
        this.date = date;
        this.name = name;
        this.sign = sign;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(date);
        dest.writeValue(name);
        dest.writeValue(sign);
    }

    public int describeContents() {
        return  0;
    }

}
