package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MultiSelectSpinner implements Parcelable {

    private int id;
    private String code;
    private String name;
    private boolean ischecked;


    protected MultiSelectSpinner(Parcel in) {
        id = in.readInt();
        name = in.readString();
        code = in.readString();
        ischecked = in.readByte() != 0;
    }

    public MultiSelectSpinner(int id, String name, boolean ischecked) {
        this.id = id;
        this.name = name;
        this.ischecked = ischecked;
    }

    public MultiSelectSpinner() {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(code);
        dest.writeByte((byte) (ischecked ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MultiSelectSpinner> CREATOR = new Creator<MultiSelectSpinner>() {
        @Override
        public MultiSelectSpinner createFromParcel(Parcel in) {
            return new MultiSelectSpinner(in);
        }

        @Override
        public MultiSelectSpinner[] newArray(int size) {
            return new MultiSelectSpinner[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIschecked() {
        return ischecked;
    }

    public void setIschecked(boolean ischecked) {
        this.ischecked = ischecked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
