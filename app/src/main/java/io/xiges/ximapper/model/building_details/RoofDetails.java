package io.xiges.ximapper.model.building_details;

import android.os.Parcel;
import android.os.Parcelable;

public class RoofDetails implements Parcelable {
    private boolean asbastors;
    private boolean giSheet;
    private boolean tile;
    private boolean rcc;
    private String roofRemarks;

    public RoofDetails(Parcel in) {
        asbastors = in.readByte() != 0;
        giSheet = in.readByte() != 0;
        tile = in.readByte() != 0;
        rcc = in.readByte() != 0;
        roofRemarks = in.readString();
    }

    public static final Creator<RoofDetails> CREATOR = new Creator<RoofDetails>() {
        @Override
        public RoofDetails createFromParcel(Parcel in) {
            return new RoofDetails(in);
        }

        @Override
        public RoofDetails[] newArray(int size) {
            return new RoofDetails[size];
        }
    };

    public RoofDetails() {

    }

    public boolean isAsbastors() {
        return asbastors;
    }

    public void setAsbastors(boolean asbastors) {
        this.asbastors = asbastors;
    }

    public boolean isGiSheet() {
        return giSheet;
    }

    public void setGiSheet(boolean giSheet) {
        this.giSheet = giSheet;
    }

    public boolean isTile() {
        return tile;
    }

    public void setTile(boolean tile) {
        this.tile = tile;
    }

    public boolean isRcc() {
        return rcc;
    }

    public void setRcc(boolean rcc) {
        this.rcc = rcc;
    }

    public String getRoofRemarks() {
        return roofRemarks;
    }

    public void setRoofRemarks(String roofRemarks) {
        this.roofRemarks = roofRemarks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (asbastors ? 1 : 0));
        dest.writeByte((byte) (giSheet ? 1 : 0));
        dest.writeByte((byte) (tile ? 1 : 0));
        dest.writeByte((byte) (rcc ? 1 : 0));
        dest.writeString(roofRemarks);
    }
}
