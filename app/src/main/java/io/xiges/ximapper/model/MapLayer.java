package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MapLayer  implements Parcelable{
    private String fileName;
    private String fileLocation;
    private LayerType layerType;
    private String color;
    private List<String> colorList = new ArrayList<>(Arrays.asList(
            "#b31217",
            "#8E2DE2",
            "#2C7744",
            "#a2ab58",
            "#4A00E0",
            "#e9d362",
            "#94716B",
            "#ef8e38",
            "#ef32d9",
            "#16A085",
            "#3a6186",
            "#89253e",
            "#292E49",
            "#414d0b",
            "#673AB7",
            "#FC354C",
            "#db36a4",
            "#4AC29A",
            "#9796f0",
            "#f8b500"));

    public MapLayer() {

    }


    protected MapLayer(Parcel in) {
        fileName = in.readString();
    }

    public static final Creator<MapLayer> CREATOR = new Creator<MapLayer>() {
        @Override
        public MapLayer createFromParcel(Parcel in) {
            return new MapLayer(in);
        }

        @Override
        public MapLayer[] newArray(int size) {
            return new MapLayer[size];
        }
    };

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public LayerType getLayerType() {
        return layerType;
    }

    public void setLayerType(LayerType layerType) {
        this.layerType = layerType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fileName);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public String getColorHex(int index){
        if(index<colorList.size()){
            return colorList.get(index);
        }else {
            Random rn = new Random();
            int rnd = rn.nextInt(20);
            return colorList.get(rnd);
        }
    }
}
