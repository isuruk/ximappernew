package io.xiges.ximapper.model.mixin_clone_model;

import android.os.Parcel;
import android.os.Parcelable;

public  class FeatureMixinContent implements Parcelable {
     private String type;
     private PointMixinContent geometry;
     private Object properties;

     protected FeatureMixinContent(Parcel in) {
          type = in.readString();
          geometry = in.readParcelable(PointMixinContent.class.getClassLoader());
     }

     @Override
     public void writeToParcel(Parcel dest, int flags) {
          dest.writeString(type);
          dest.writeParcelable(geometry, flags);
     }

     @Override
     public int describeContents() {
          return 0;
     }

     public static final Creator<FeatureMixinContent> CREATOR = new Creator<FeatureMixinContent>() {
          @Override
          public FeatureMixinContent createFromParcel(Parcel in) {
               return new FeatureMixinContent(in);
          }

          @Override
          public FeatureMixinContent[] newArray(int size) {
               return new FeatureMixinContent[size];
          }
     };

     public String getType() {
          return type;
     }

     public void setType(String type) {
          this.type = type;
     }

     public PointMixinContent getGeometry() {
          return geometry;
     }

     public void setGeometry(PointMixinContent geometry) {
          this.geometry = geometry;
     }

     public Object getProperties() {
          return properties;
     }

     public void setProperties(Object properties) {
          this.properties = properties;
     }

     @Override
     public String toString() {
          return "FeatureMixinContent{" +
                  "type='" + type + '\'' +
                  ", geometry=" + geometry +
                  ", properties=" + properties +
                  '}';
     }
}
