package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import io.xiges.ximapper.model.building_details.CRBuildingDetails;

public class ConditionalReport implements Parcelable {
    private CRLandInfo landInfo;
    private List<CRBuildingDetails> buildingDetails;
    private List<CROtherConstruction> otherConstructions;
    private List<CROwnerDetails> ownerDetails;
    private List<CRSignature> signatures;

    public ConditionalReport() {

    }


    protected ConditionalReport(Parcel in) {
        landInfo = in.readParcelable(CRLandInfo.class.getClassLoader());
        buildingDetails = in.createTypedArrayList(CRBuildingDetails.CREATOR);
        otherConstructions = in.createTypedArrayList(CROtherConstruction.CREATOR);
        ownerDetails = in.createTypedArrayList(CROwnerDetails.CREATOR);
        signatures = in.createTypedArrayList(CRSignature.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(landInfo, flags);
        dest.writeTypedList(buildingDetails);
        dest.writeTypedList(otherConstructions);
        dest.writeTypedList(ownerDetails);
        dest.writeTypedList(signatures);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConditionalReport> CREATOR = new Creator<ConditionalReport>() {
        @Override
        public ConditionalReport createFromParcel(Parcel in) {
            return new ConditionalReport(in);
        }

        @Override
        public ConditionalReport[] newArray(int size) {
            return new ConditionalReport[size];
        }
    };

    public CRLandInfo getLandInfo() {
        return landInfo;
    }

    public void setLandInfo(CRLandInfo landInfo) {
        this.landInfo = landInfo;
    }

    public List<CRBuildingDetails> getBuildingDetails() {
        if(buildingDetails== null){
            buildingDetails = new ArrayList<>();
        }
        return buildingDetails;
    }

    public List<CROwnerDetails> getOwnerDetails() {
        if(ownerDetails== null){
            ownerDetails = new ArrayList<>();
        }
        return ownerDetails;
    }

    public void setOwnerDetails(List<CROwnerDetails> ownerDetails) {
        this.ownerDetails = ownerDetails;
    }

    public void setBuildingDetails(List<CRBuildingDetails> buildingDetails) {
        this.buildingDetails = buildingDetails;
    }

    public List<CROtherConstruction> getOtherConstructions() {

        if(otherConstructions== null){
            otherConstructions = new ArrayList<>();
        }
        return otherConstructions;
    }

    public void setOtherConstructions(List<CROtherConstruction> otherConstructions) {
        this.otherConstructions = otherConstructions;
    }


    public List<CRSignature> getSignatures() {
        if(signatures== null){
            signatures = new ArrayList<>();
        }
        return signatures;
    }

    public void setSignatures(List<CRSignature> signatures) {
        this.signatures = signatures;
    }
}
