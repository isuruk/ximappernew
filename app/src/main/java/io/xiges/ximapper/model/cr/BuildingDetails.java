
package io.xiges.ximapper.model.cr;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class BuildingDetails implements Parcelable
{

    private List<Building> buildings = new ArrayList<Building>();
    private String explanation;
    public final static Parcelable.Creator<BuildingDetails> CREATOR = new Creator<BuildingDetails>() {


        @SuppressWarnings({
            "unchecked"
        })
        public BuildingDetails createFromParcel(Parcel in) {
            return new BuildingDetails(in);
        }

        public BuildingDetails[] newArray(int size) {
            return (new BuildingDetails[size]);
        }

    }
    ;

    protected BuildingDetails(Parcel in) {
        in.readList(this.buildings, (io.xiges.ximapper.model.cr.Building.class.getClassLoader()));
        this.explanation = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public BuildingDetails() {
    }

    /**
     * 
     * @param buildings
     * @param explanation
     */
    public BuildingDetails(List<Building> buildings, String explanation) {
        super();
        this.buildings = buildings;
        this.explanation = explanation;
    }

    public List<Building> getBuildings() {
        if(buildings== null){
            buildings = new ArrayList<>();
        }
        return buildings;
    }

    public void setBuildings(List<Building> buildings) {
        this.buildings = buildings;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(buildings);
        dest.writeValue(explanation);
    }

    public int describeContents() {
        return  0;
    }

}
