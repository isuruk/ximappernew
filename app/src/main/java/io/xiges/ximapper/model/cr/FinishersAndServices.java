
package io.xiges.ximapper.model.cr;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class FinishersAndServices implements Parcelable
{

    private List<Integer> finishersAndServicesBathroomAndToilet = new ArrayList<Integer>();
    private List<Integer> finishersAndServicesFloorFinisher = new ArrayList<Integer>();
    private List<Integer> finishersAndServicesServices = new ArrayList<Integer>();
    private List<Integer> finishersAndServicesWallFinisher = new ArrayList<Integer>();
    public final static Parcelable.Creator<FinishersAndServices> CREATOR = new Creator<FinishersAndServices>() {


        @SuppressWarnings({
            "unchecked"
        })
        public FinishersAndServices createFromParcel(Parcel in) {
            return new FinishersAndServices(in);
        }

        public FinishersAndServices[] newArray(int size) {
            return (new FinishersAndServices[size]);
        }

    }
    ;

    protected FinishersAndServices(Parcel in) {
        in.readList(this.finishersAndServicesBathroomAndToilet, (java.lang.Integer.class.getClassLoader()));
        in.readList(this.finishersAndServicesFloorFinisher, (java.lang.Integer.class.getClassLoader()));
        in.readList(this.finishersAndServicesServices, (java.lang.Integer.class.getClassLoader()));
        in.readList(this.finishersAndServicesWallFinisher, (java.lang.Integer.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public FinishersAndServices() {
    }

    /**
     * 
     * @param finishersAndServicesBathroomAndToilet
     * @param finishersAndServicesFloorFinisher
     * @param finishersAndServicesServices
     * @param finishersAndServicesWallFinisher
     */
    public FinishersAndServices(List<Integer> finishersAndServicesBathroomAndToilet, List<Integer> finishersAndServicesFloorFinisher, List<Integer> finishersAndServicesServices, List<Integer> finishersAndServicesWallFinisher) {
        super();
        this.finishersAndServicesBathroomAndToilet = finishersAndServicesBathroomAndToilet;
        this.finishersAndServicesFloorFinisher = finishersAndServicesFloorFinisher;
        this.finishersAndServicesServices = finishersAndServicesServices;
        this.finishersAndServicesWallFinisher = finishersAndServicesWallFinisher;
    }

    public List<Integer> getFinishersAndServicesBathroomAndToilet() {
        if(finishersAndServicesBathroomAndToilet == null){
            finishersAndServicesBathroomAndToilet = new ArrayList<>();
        }

        return finishersAndServicesBathroomAndToilet;
    }

    public void setFinishersAndServicesBathroomAndToilet(List<Integer> finishersAndServicesBathroomAndToilet) {
        this.finishersAndServicesBathroomAndToilet = finishersAndServicesBathroomAndToilet;
    }

    public List<Integer> getFinishersAndServicesFloorFinisher() {
        if(finishersAndServicesFloorFinisher == null){
            finishersAndServicesFloorFinisher = new ArrayList<>();
        }

        return finishersAndServicesFloorFinisher;
    }

    public void setFinishersAndServicesFloorFinisher(List<Integer> finishersAndServicesFloorFinisher) {
        this.finishersAndServicesFloorFinisher = finishersAndServicesFloorFinisher;
    }

    public List<Integer> getFinishersAndServicesServices() {
        if(finishersAndServicesServices == null){
            finishersAndServicesServices = new ArrayList<>();
        }

        return finishersAndServicesServices;
    }

    public void setFinishersAndServicesServices(List<Integer> finishersAndServicesServices) {
        this.finishersAndServicesServices = finishersAndServicesServices;
    }

    public List<Integer> getFinishersAndServicesWallFinisher() {
        if(finishersAndServicesWallFinisher == null){
            finishersAndServicesWallFinisher = new ArrayList<>();
        }
        return finishersAndServicesWallFinisher;
    }

    public void setFinishersAndServicesWallFinisher(List<Integer> finishersAndServicesWallFinisher) {
        this.finishersAndServicesWallFinisher = finishersAndServicesWallFinisher;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(finishersAndServicesBathroomAndToilet);
        dest.writeList(finishersAndServicesFloorFinisher);
        dest.writeList(finishersAndServicesServices);
        dest.writeList(finishersAndServicesWallFinisher);
    }

    public int describeContents() {
        return  0;
    }

}
