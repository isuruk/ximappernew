package io.xiges.ximapper.model;

public interface OnAPICallback {
    void onSuccess();
    void onFailed(String message);
}
