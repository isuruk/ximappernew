
        package io.xiges.ximapper.model;

        import java.util.List;
        import android.os.Parcel;
        import android.os.Parcelable;
        import android.os.Parcelable.Creator;


public class Geojson implements Parcelable
{

    private String type;
    private List<Feature> features = null;
    public final static Parcelable.Creator<Geojson> CREATOR = new Creator<Geojson>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Geojson createFromParcel(Parcel in) {
            return new Geojson(in);
        }

        public Geojson[] newArray(int size) {
            return (new Geojson[size]);
        }

    }
            ;

    protected Geojson(Parcel in) {
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.features, (io.xiges.ximapper.model.Feature.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Geojson() {
    }

    /**
     *
     * @param features
     * @param type
     */
    public Geojson(String type, List<Feature> features) {
        super();
        this.type = type;
        this.features = features;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(type);
        dest.writeList(features);
    }

    public int describeContents() {
        return 0;
    }

}
