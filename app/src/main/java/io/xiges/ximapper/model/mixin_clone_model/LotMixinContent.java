package io.xiges.ximapper.model.mixin_clone_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.xiges.ximapper.model.Symbol;

public class LotMixinContent  implements Parcelable {
     private String lotId;
     private String lotName;
    @SerializedName("fillPoints")
    private List<PointMixinContent> fillLayerPointList;
    @SerializedName("linePoints")
    private List<PointMixinContent> lineLayerPointList;
    @SerializedName("circlePoints")
    private List<FeatureMixinContent> circleLayerFeatureList;
     private List<ConstructionMixInContent> constructions;
    @SerializedName("lotLabels")
    private List<Symbol> symbolList;
    @SerializedName("landLabels")
    private List<Symbol> landLabels;

    public LotMixinContent() {

    }

    protected LotMixinContent(Parcel in) {
        lotId = in.readString();
        lotName = in.readString();
        fillLayerPointList = in.createTypedArrayList(PointMixinContent.CREATOR);
        lineLayerPointList = in.createTypedArrayList(PointMixinContent.CREATOR);
        circleLayerFeatureList = in.createTypedArrayList(FeatureMixinContent.CREATOR);
        constructions = in.createTypedArrayList(ConstructionMixInContent.CREATOR);
        symbolList = in.createTypedArrayList(Symbol.CREATOR);
        landLabels = in.createTypedArrayList(Symbol.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(lotId);
        dest.writeString(lotName);
        dest.writeTypedList(fillLayerPointList);
        dest.writeTypedList(lineLayerPointList);
        dest.writeTypedList(circleLayerFeatureList);
        dest.writeTypedList(constructions);
        dest.writeTypedList(symbolList);
        dest.writeTypedList(landLabels);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LotMixinContent> CREATOR = new Creator<LotMixinContent>() {
        @Override
        public LotMixinContent createFromParcel(Parcel in) {
            return new LotMixinContent(in);
        }

        @Override
        public LotMixinContent[] newArray(int size) {
            return new LotMixinContent[size];
        }
    };

    public String getLotId() {
        return lotId;
    }

    public void setLotId(String lotId) {
        this.lotId = lotId;
    }

    public String getLotName() {
        return lotName;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

    public List<PointMixinContent> getFillLayerPointList() {
        return fillLayerPointList;
    }

    public void setFillLayerPointList(List<PointMixinContent> fillLayerPointList) {
        this.fillLayerPointList = fillLayerPointList;
    }

    public List<PointMixinContent> getLineLayerPointList() {
        return lineLayerPointList;
    }

    public void setLineLayerPointList(List<PointMixinContent> lineLayerPointList) {
        this.lineLayerPointList = lineLayerPointList;
    }

    public List<FeatureMixinContent> getCircleLayerFeatureList() {
        return circleLayerFeatureList;
    }

    public void setCircleLayerFeatureList(List<FeatureMixinContent> circleLayerFeatureList) {
        this.circleLayerFeatureList = circleLayerFeatureList;
    }

    public List<ConstructionMixInContent> getConstructions() {
        return constructions;
    }

    public void setConstructions(List<ConstructionMixInContent> constructions) {
        this.constructions = constructions;
    }

    public List<Symbol> getSymbolList() {
        return symbolList;
    }

    public void setSymbolList(List<Symbol> symbolList) {
        this.symbolList = symbolList;
    }



    public List<Symbol> getFloorLabels() {
        return landLabels;
    }

    public void setFloorLabels(List<Symbol> floorLabels) {
        this.landLabels = floorLabels;
    }

    @Override
    public String toString() {
        return "LotMixinContent{" +
                "lotId='" + lotId + '\'' +
                ", lotName='" + lotName + '\'' +
                ", fillLayerPointList=" + fillLayerPointList +
                ", lineLayerPointList=" + lineLayerPointList +
                ", circleLayerFeatureList=" + circleLayerFeatureList +
                ", constructions=" + constructions +
                ", symbolList=" + symbolList +
                ", landLabels=" + landLabels +
                '}';
    }
}
