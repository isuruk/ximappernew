
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class RepresentativeDetail implements Parcelable
{

    private String date;
    private String name;
    private String nicNumber;
    private String relationShipWithOwner;
    private String sign;
    public final static Parcelable.Creator<RepresentativeDetail> CREATOR = new Creator<RepresentativeDetail>() {


        @SuppressWarnings({
            "unchecked"
        })
        public RepresentativeDetail createFromParcel(Parcel in) {
            return new RepresentativeDetail(in);
        }

        public RepresentativeDetail[] newArray(int size) {
            return (new RepresentativeDetail[size]);
        }

    }
    ;

    protected RepresentativeDetail(Parcel in) {
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.nicNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.relationShipWithOwner = ((String) in.readValue((String.class.getClassLoader())));
        this.sign = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public RepresentativeDetail() {
    }

    /**
     * 
     * @param date
     * @param name
     * @param nicNumber
     * @param sign
     * @param relationShipWithOwner
     */
    public RepresentativeDetail(String date, String name, String nicNumber, String relationShipWithOwner, String sign) {
        super();
        this.date = date;
        this.name = name;
        this.nicNumber = nicNumber;
        this.relationShipWithOwner = relationShipWithOwner;
        this.sign = sign;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNicNumber() {
        return nicNumber;
    }

    public void setNicNumber(String nicNumber) {
        this.nicNumber = nicNumber;
    }

    public String getRelationShipWithOwner() {
        return relationShipWithOwner;
    }

    public void setRelationShipWithOwner(String relationShipWithOwner) {
        this.relationShipWithOwner = relationShipWithOwner;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(date);
        dest.writeValue(name);
        dest.writeValue(nicNumber);
        dest.writeValue(relationShipWithOwner);
        dest.writeValue(sign);
    }

    public int describeContents() {
        return  0;
    }

}
