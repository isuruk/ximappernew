
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class OccupierDetails implements Parcelable
{

    private String date;
    private String name;
    private String period;
    private String rent;
    private String sign;
    private String term;
    public final static Parcelable.Creator<OccupierDetails> CREATOR = new Creator<OccupierDetails>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OccupierDetails createFromParcel(Parcel in) {
            return new OccupierDetails(in);
        }

        public OccupierDetails[] newArray(int size) {
            return (new OccupierDetails[size]);
        }

    }
    ;

    protected OccupierDetails(Parcel in) {
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.period = ((String) in.readValue((String.class.getClassLoader())));
        this.rent = ((String) in.readValue((String.class.getClassLoader())));
        this.sign = ((String) in.readValue((String.class.getClassLoader())));
        this.term = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public OccupierDetails() {
    }

    /**
     * 
     * @param date
     * @param period
     * @param name
     * @param sign
     * @param term
     * @param rent
     */
    public OccupierDetails(String date, String name, String period, String rent, String sign, String term) {
        super();
        this.date = date;
        this.name = name;
        this.period = period;
        this.rent = rent;
        this.sign = sign;
        this.term = term;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getRent() {
        return rent;
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(date);
        dest.writeValue(name);
        dest.writeValue(period);
        dest.writeValue(rent);
        dest.writeValue(sign);
        dest.writeValue(term);
    }

    public int describeContents() {
        return  0;
    }

}
