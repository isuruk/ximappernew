package io.xiges.ximapper.model.building_details;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import io.xiges.ximapper.model.api.master_data.FixtureAndFittingDoor;
import io.xiges.ximapper.model.api.master_data.FixtureAndFittingDoorsBathroomAndToiletFitting;
import io.xiges.ximapper.model.api.master_data.FixtureAndFittingDoorsHandRail;
import io.xiges.ximapper.model.api.master_data.FixtureAndFittingDoorsOther;
import io.xiges.ximapper.model.api.master_data.FixtureAndFittingDoorsPantryCupbord;
import io.xiges.ximapper.model.api.master_data.FixtureAndFittingWindow;
import io.xiges.ximapper.model.api.master_data.FixtureAndFittingWindowProtection;

public class FixtureAndFitting implements Parcelable {
    private FixtureAndFittingDoor fixtureAndFittingDoor;
    private FixtureAndFittingWindow fixtureAndFittingWindows;
    private FixtureAndFittingWindowProtection fixtureAndFittingWindowProtection;
    private FixtureAndFittingDoorsBathroomAndToiletFitting fixtureAndFittingDoorsBathroomAndToiletFittings;
    private FixtureAndFittingDoorsHandRail fixtureAndFittingDoorsHandRail;
    private FixtureAndFittingDoorsPantryCupbord fixtureAndFittingDoorsPantryCupbord;
    private FixtureAndFittingDoorsOther fixtureAndFittingDoorsOther;


    public FixtureAndFitting() {

    }

    protected FixtureAndFitting(Parcel in) {
        fixtureAndFittingDoor = in.readParcelable(FixtureAndFittingDoor.class.getClassLoader());
        fixtureAndFittingWindows = in.readParcelable(FixtureAndFittingWindow.class.getClassLoader());
        fixtureAndFittingWindowProtection = in.readParcelable(FixtureAndFittingWindowProtection.class.getClassLoader());
        fixtureAndFittingDoorsBathroomAndToiletFittings = in.readParcelable(FixtureAndFittingDoorsBathroomAndToiletFitting.class.getClassLoader());
        fixtureAndFittingDoorsHandRail = in.readParcelable(FixtureAndFittingDoorsHandRail.class.getClassLoader());
        fixtureAndFittingDoorsPantryCupbord = in.readParcelable(FixtureAndFittingDoorsPantryCupbord.class.getClassLoader());
        fixtureAndFittingDoorsOther = in.readParcelable(FixtureAndFittingDoorsOther.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(fixtureAndFittingDoor, flags);
        dest.writeParcelable(fixtureAndFittingWindows, flags);
        dest.writeParcelable(fixtureAndFittingWindowProtection, flags);
        dest.writeParcelable(fixtureAndFittingDoorsBathroomAndToiletFittings, flags);
        dest.writeParcelable(fixtureAndFittingDoorsHandRail, flags);
        dest.writeParcelable(fixtureAndFittingDoorsPantryCupbord, flags);
        dest.writeParcelable(fixtureAndFittingDoorsOther, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FixtureAndFitting> CREATOR = new Creator<FixtureAndFitting>() {
        @Override
        public FixtureAndFitting createFromParcel(Parcel in) {
            return new FixtureAndFitting(in);
        }

        @Override
        public FixtureAndFitting[] newArray(int size) {
            return new FixtureAndFitting[size];
        }
    };

    public FixtureAndFittingDoor getFixtureAndFittingDoor() {
        return fixtureAndFittingDoor;
    }

    public void setFixtureAndFittingDoor(FixtureAndFittingDoor fixtureAndFittingDoor) {
        this.fixtureAndFittingDoor = fixtureAndFittingDoor;
    }

    public FixtureAndFittingWindow getFixtureAndFittingWindows() {
        return fixtureAndFittingWindows;
    }

    public void setFixtureAndFittingWindows(FixtureAndFittingWindow fixtureAndFittingWindows) {
        this.fixtureAndFittingWindows = fixtureAndFittingWindows;
    }

    public FixtureAndFittingWindowProtection getFixtureAndFittingWindowProtection() {
        return fixtureAndFittingWindowProtection;
    }

    public void setFixtureAndFittingWindowProtection(FixtureAndFittingWindowProtection fixtureAndFittingWindowProtection) {
        this.fixtureAndFittingWindowProtection = fixtureAndFittingWindowProtection;
    }

    public FixtureAndFittingDoorsBathroomAndToiletFitting getFixtureAndFittingDoorsBathroomAndToiletFittings() {
        return fixtureAndFittingDoorsBathroomAndToiletFittings;
    }

    public void setFixtureAndFittingDoorsBathroomAndToiletFittings(FixtureAndFittingDoorsBathroomAndToiletFitting fixtureAndFittingDoorsBathroomAndToiletFittings) {
        this.fixtureAndFittingDoorsBathroomAndToiletFittings = fixtureAndFittingDoorsBathroomAndToiletFittings;
    }

    public FixtureAndFittingDoorsHandRail getFixtureAndFittingDoorsHandRail() {
        return fixtureAndFittingDoorsHandRail;
    }

    public void setFixtureAndFittingDoorsHandRail(FixtureAndFittingDoorsHandRail fixtureAndFittingDoorsHandRail) {
        this.fixtureAndFittingDoorsHandRail = fixtureAndFittingDoorsHandRail;
    }

    public FixtureAndFittingDoorsPantryCupbord getFixtureAndFittingDoorsPantryCupbord() {
        return fixtureAndFittingDoorsPantryCupbord;
    }

    public void setFixtureAndFittingDoorsPantryCupbord(FixtureAndFittingDoorsPantryCupbord fixtureAndFittingDoorsPantryCupbord) {
        this.fixtureAndFittingDoorsPantryCupbord = fixtureAndFittingDoorsPantryCupbord;
    }

    public FixtureAndFittingDoorsOther getFixtureAndFittingDoorsOther() {
        return fixtureAndFittingDoorsOther;
    }

    public void setFixtureAndFittingDoorsOther(FixtureAndFittingDoorsOther fixtureAndFittingDoorsOther) {
        this.fixtureAndFittingDoorsOther = fixtureAndFittingDoorsOther;
    }
}
