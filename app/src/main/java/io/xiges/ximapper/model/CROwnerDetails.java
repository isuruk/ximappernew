package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.xiges.ximapper.model.api.master_data.OccupierType;

public class CROwnerDetails implements Parcelable {
    private String ownerName;
    private String ownerNic;
    private String occupierName;
    private String occupierId;
    private OccupierType occupierType;
    private String tenantDetails;
    private String term;
    private String rent;
    private String period;


    public CROwnerDetails() {

    }

    protected CROwnerDetails(Parcel in) {
        ownerName = in.readString();
        ownerNic = in.readString();
        occupierName = in.readString();
        occupierId = in.readString();
        occupierType = in.readParcelable(OccupierType.class.getClassLoader());
        tenantDetails = in.readString();
        term = in.readString();
        rent = in.readString();
        period = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ownerName);
        dest.writeString(ownerNic);
        dest.writeString(occupierName);
        dest.writeString(occupierId);
        dest.writeParcelable(occupierType, flags);
        dest.writeString(tenantDetails);
        dest.writeString(term);
        dest.writeString(rent);
        dest.writeString(period);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CROwnerDetails> CREATOR = new Creator<CROwnerDetails>() {
        @Override
        public CROwnerDetails createFromParcel(Parcel in) {
            return new CROwnerDetails(in);
        }

        @Override
        public CROwnerDetails[] newArray(int size) {
            return new CROwnerDetails[size];
        }
    };

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerNic() {
        return ownerNic;
    }

    public void setOwnerNic(String ownerNic) {
        this.ownerNic = ownerNic;
    }

    public String getOccupierName() {
        return occupierName;
    }

    public void setOccupierName(String occupierName) {
        this.occupierName = occupierName;
    }

    public String getOccupierId() {
        return occupierId;
    }

    public void setOccupierId(String occupierId) {
        this.occupierId = occupierId;
    }

    public OccupierType getOccupierType() {
        return occupierType;
    }

    public void setOccupierType(OccupierType occupierType) {
        this.occupierType = occupierType;
    }

    public String getTenantDetails() {
        return tenantDetails;
    }

    public void setTenantDetails(String tenantDetails) {
        this.tenantDetails = tenantDetails;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getRent() {
        return rent;
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
