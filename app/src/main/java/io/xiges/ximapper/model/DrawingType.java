package io.xiges.ximapper.model;

public enum DrawingType {
    LINE,
    POLYGON,
    CIRCLE,
    AQLINE,
    ZFLOOR_POLYGON,
    BROOM_POLYGON,
    AQ_POLYGON,
    CNULL
}
