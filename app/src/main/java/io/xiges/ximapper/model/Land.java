package io.xiges.ximapper.model;

import java.util.List;

public class Land {
    private String name;
   private List<Construction> constructions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<Construction> getConstructions() {
        return constructions;
    }

    public void setConstructions(List<Construction> constructions) {
        this.constructions = constructions;
    }
}
