package io.xiges.ximapper.model.api.master_data;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class BuildingDetails implements Parcelable
{

    private List<BuildingCategory> buildingCategory = null;
    private List<BuildingClass> buildingClass = null;
    private List<BuildingNatureOfConstruction> buildingNatureOfConstruction = null;
    private List<TypeOfCondition> typeOfCondition = null;
    private List<BuildingAcquiredPortion> buildingAcquiredPortion = null;
    public final static Parcelable.Creator<BuildingDetails> CREATOR = new Creator<BuildingDetails>() {


        @SuppressWarnings({
                "unchecked"
        })
        public BuildingDetails createFromParcel(Parcel in) {
            return new BuildingDetails(in);
        }

        public BuildingDetails[] newArray(int size) {
            return (new BuildingDetails[size]);
        }

    }
            ;

    protected BuildingDetails(Parcel in) {
        in.readList(this.buildingCategory, (BuildingCategory.class.getClassLoader()));
        in.readList(this.buildingClass, (BuildingClass.class.getClassLoader()));
        in.readList(this.buildingNatureOfConstruction, (BuildingNatureOfConstruction.class.getClassLoader()));
        in.readList(this.typeOfCondition, (TypeOfCondition.class.getClassLoader()));
        in.readList(this.buildingAcquiredPortion, (BuildingAcquiredPortion.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public BuildingDetails() {
    }

    /**
     *
     * @param buildingNatureOfConstruction
     * @param buildingCategory
     * @param typeOfCondition
     * @param buildingAcquiredPortion
     * @param BuildingClasss
     */
    public BuildingDetails(List<BuildingCategory> buildingCategory, List<BuildingClass> BuildingClasss, List<BuildingNatureOfConstruction> buildingNatureOfConstruction, List<TypeOfCondition> typeOfCondition, List<BuildingAcquiredPortion> buildingAcquiredPortion) {
        super();
        this.buildingCategory = buildingCategory;
        this.buildingClass = buildingClass;
        this.buildingNatureOfConstruction = buildingNatureOfConstruction;
        this.typeOfCondition = typeOfCondition;
        this.buildingAcquiredPortion = buildingAcquiredPortion;
    }

    public List<BuildingCategory> getBuildingCategory() {
        return buildingCategory;
    }

    public void setBuildingCategory(List<BuildingCategory> buildingCategory) {
        this.buildingCategory = buildingCategory;
    }

    public List<BuildingClass> getBuildingClasss() {
        return buildingClass;
    }

    public void setBuildingClasss(List<BuildingClass> BuildingClasss) {
        this.buildingClass = BuildingClasss;
    }

    public List<BuildingNatureOfConstruction> getBuildingNatureOfConstruction() {
        return buildingNatureOfConstruction;
    }

    public void setBuildingNatureOfConstruction(List<BuildingNatureOfConstruction> buildingNatureOfConstruction) {
        this.buildingNatureOfConstruction = buildingNatureOfConstruction;
    }

    public List<TypeOfCondition> getTypeOfCondition() {
        return typeOfCondition;
    }

    public void setTypeOfCondition(List<TypeOfCondition> typeOfCondition) {
        this.typeOfCondition = typeOfCondition;
    }

    public List<BuildingAcquiredPortion> getBuildingAcquiredPortion() {
        return buildingAcquiredPortion;
    }

    public void setBuildingAcquiredPortion(List<BuildingAcquiredPortion> buildingAcquiredPortion) {
        this.buildingAcquiredPortion = buildingAcquiredPortion;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(buildingCategory);
        dest.writeList(buildingClass);
        dest.writeList(buildingNatureOfConstruction);
        dest.writeList(typeOfCondition);
        dest.writeList(buildingAcquiredPortion);
    }

    public int describeContents() {
        return 0;
    }

}