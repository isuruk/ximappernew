
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class AcquisitionOfficerDetails implements Parcelable
{

    private String date;
    private String designation;
    private String name;
    private String sign;
    public final static Parcelable.Creator<AcquisitionOfficerDetails> CREATOR = new Creator<AcquisitionOfficerDetails>() {


        @SuppressWarnings({
            "unchecked"
        })
        public AcquisitionOfficerDetails createFromParcel(Parcel in) {
            return new AcquisitionOfficerDetails(in);
        }

        public AcquisitionOfficerDetails[] newArray(int size) {
            return (new AcquisitionOfficerDetails[size]);
        }

    }
    ;

    protected AcquisitionOfficerDetails(Parcel in) {
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.designation = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.sign = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public AcquisitionOfficerDetails() {
    }

    /**
     * 
     * @param date
     * @param name
     * @param sign
     * @param designation
     */
    public AcquisitionOfficerDetails(String date, String designation, String name, String sign) {
        super();
        this.date = date;
        this.designation = designation;
        this.name = name;
        this.sign = sign;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(date);
        dest.writeValue(designation);
        dest.writeValue(name);
        dest.writeValue(sign);
    }

    public int describeContents() {
        return  0;
    }

}
