
package io.xiges.ximapper.model.cr;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Structure implements Parcelable
{

    private List<Integer> foundationStructure = new ArrayList<Integer>();
    private List<Integer> wallStructure = new ArrayList<Integer>();
    public final static Parcelable.Creator<Structure> CREATOR = new Creator<Structure>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Structure createFromParcel(Parcel in) {
            return new Structure(in);
        }

        public Structure[] newArray(int size) {
            return (new Structure[size]);
        }

    }
    ;

    protected Structure(Parcel in) {
        in.readList(this.foundationStructure, (java.lang.Integer.class.getClassLoader()));
        in.readList(this.wallStructure, (java.lang.Integer.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Structure() {
    }

    /**
     * 
     * @param foundationStructure
     * @param wallStructure
     */
    public Structure(List<Integer> foundationStructure, List<Integer> wallStructure) {
        super();
        this.foundationStructure = foundationStructure;
        this.wallStructure = wallStructure;
    }

    public List<Integer> getFoundationStructure() {
        if(foundationStructure == null){
            foundationStructure = new ArrayList<>();
        }

        return foundationStructure;
    }

    public void setFoundationStructure(List<Integer> foundationStructure) {
        this.foundationStructure = foundationStructure;
    }

    public List<Integer> getWallStructure() {
        if(wallStructure == null){
            wallStructure = new ArrayList<>();
        }

        return wallStructure;
    }

    public void setWallStructure(List<Integer> wallStructure) {
        this.wallStructure = wallStructure;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(foundationStructure);
        dest.writeList(wallStructure);
    }

    public int describeContents() {
        return  0;
    }

}
