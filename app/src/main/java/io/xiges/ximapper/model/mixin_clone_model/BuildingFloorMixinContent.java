package io.xiges.ximapper.model.mixin_clone_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.xiges.ximapper.model.Symbol;

public class BuildingFloorMixinContent implements Parcelable {
    private String floorNo;
    @SerializedName("floorData")
    private List<FloorDrawingMixInContent> floorDrawingList = new ArrayList<>();
    @SerializedName("floorLabelData")
    List<Symbol> symbols = new ArrayList<>();
    @SerializedName("floorSymbol")
    List<Symbol> floorSymbol = new ArrayList<>();
    private double floorArea;
    private double aqArea;
    private int gFloors;
    private int uFloors;


    protected BuildingFloorMixinContent(Parcel in) {
        floorNo = in.readString();
        floorDrawingList = in.createTypedArrayList(FloorDrawingMixInContent.CREATOR);
        symbols = in.createTypedArrayList(Symbol.CREATOR);
        floorSymbol = in.createTypedArrayList(Symbol.CREATOR);
        floorArea = in.readDouble();
        aqArea = in.readDouble();
        gFloors = in.readInt();
        uFloors = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(floorNo);
        dest.writeTypedList(floorDrawingList);
        dest.writeTypedList(symbols);
        dest.writeTypedList(floorSymbol);
        dest.writeDouble(floorArea);
        dest.writeDouble(aqArea);
        dest.writeInt(gFloors);
        dest.writeInt(uFloors);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BuildingFloorMixinContent> CREATOR = new Creator<BuildingFloorMixinContent>() {
        @Override
        public BuildingFloorMixinContent createFromParcel(Parcel in) {
            return new BuildingFloorMixinContent(in);
        }

        @Override
        public BuildingFloorMixinContent[] newArray(int size) {
            return new BuildingFloorMixinContent[size];
        }
    };

    public String getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(String floorNo) {
        this.floorNo = floorNo;
    }

    public List<FloorDrawingMixInContent> getFloorDrawingList() {
        return floorDrawingList;
    }

    public void setFloorDrawingList(List<FloorDrawingMixInContent> floorDrawingList) {
        this.floorDrawingList = floorDrawingList;
    }

    public double getFloorArea() {
        return floorArea;
    }

    public void setFloorArea(double floorArea) {
        this.floorArea = floorArea;
    }

    public double getAqArea() {
        return aqArea;
    }

    public void setAqArea(double aqArea) {
        this.aqArea = aqArea;
    }

    public int getgFloors() {
        return gFloors;
    }

    public void setgFloors(int gFloors) {
        this.gFloors = gFloors;
    }

    public int getuFloors() {
        return uFloors;
    }

    public void setuFloors(int uFloors) {
        this.uFloors = uFloors;
    }

    public List<Symbol> getSymbols() {
        return symbols;
    }

    public void setSymbols(List<Symbol> symbols) {
        this.symbols = symbols;
    }

    public List<Symbol> getFloorSymbol() {
        return floorSymbol;
    }

    public void setFloorSymbol(List<Symbol> floorSymbol) {
        this.floorSymbol = floorSymbol;
    }
}
