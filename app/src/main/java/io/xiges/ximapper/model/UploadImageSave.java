package io.xiges.ximapper.model;

import java.util.List;

public class UploadImageSave {
    private int masterFile;
    private int lotNo;
    private List<String> files;

    public int getMasterFile() {
        return masterFile;
    }

    public void setMasterFile(int masterFile) {
        this.masterFile = masterFile;
    }

    public int getLotNo() {
        return lotNo;
    }

    public void setLotNo(int lotNo) {
        this.lotNo = lotNo;
    }

    public List<String> getFiles() {
        return files;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }
}
