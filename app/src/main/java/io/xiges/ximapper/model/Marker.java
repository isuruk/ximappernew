package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.mapbox.mapboxsdk.geometry.LatLng;

import java.util.Objects;

public class Marker implements Parcelable {
    private LatLng latLng;
    private String name;
    private String type;


    public Marker() {
    }


    protected Marker(Parcel in) {
        latLng = in.readParcelable(LatLng.class.getClassLoader());
        name = in.readString();
        type = in.readString();

    }

    public static final Creator<Marker> CREATOR = new Creator<Marker>() {
        @Override
        public Marker createFromParcel(Parcel in) {
            return new Marker(in);
        }

        @Override
        public Marker[] newArray(int size) {
            return new Marker[size];
        }
    };

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Marker marker = (Marker) o;
        return Objects.equals(latLng, marker.latLng) &&
                Objects.equals(name, marker.name) &&
                Objects.equals(type, marker.type);

    }

    @Override
    public int hashCode() {
        return Objects.hash(latLng, name, type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(latLng, i);
        parcel.writeString(name);
        parcel.writeString(type);
    }
}
