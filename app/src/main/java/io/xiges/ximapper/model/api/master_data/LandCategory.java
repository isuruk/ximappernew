package io.xiges.ximapper.model.api.master_data;

import android.os.Parcel;
import android.os.Parcelable;

public class LandCategory implements Parcelable
{

    private int id;
    private String description;
    public final static Parcelable.Creator<LandCategory> CREATOR = new Creator<LandCategory>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LandCategory createFromParcel(Parcel in) {
            return new LandCategory(in);
        }

        public LandCategory[] newArray(int size) {
            return (new LandCategory[size]);
        }

    }
            ;

    protected LandCategory(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public LandCategory() {
    }

    /**
     *
     * @param description
     * @param id
     */
    public LandCategory(int id, String description) {
        super();
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(description);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "LandCategory{" +
                "id=" + id +
                ", description='" + description + '\'' +
                '}';
    }
}