package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CRSignature implements Parcelable {
    String name;
    String type;
    String signature;

    public CRSignature(Parcel in) {
        name = in.readString();
        type = in.readString();
        signature = in.readString();
    }

    public CRSignature() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(type);
        dest.writeString(signature);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CRSignature> CREATOR = new Creator<CRSignature>() {
        @Override
        public CRSignature createFromParcel(Parcel in) {
            return new CRSignature(in);
        }

        @Override
        public CRSignature[] newArray(int size) {
            return new CRSignature[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
