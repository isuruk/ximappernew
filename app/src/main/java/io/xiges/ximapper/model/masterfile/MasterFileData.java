
package io.xiges.ximapper.model.masterfile;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class MasterFileData implements Parcelable
{

    private int masterfileId;
    private String masterfileRefNumber;
    private String dsRefNumber;
    private String atMapNumber;
    private String fileType;
    private String fileNumber;
    private int districtId;
    private String districtName;
    private int dsId;
    private String dsName;
    private List<VillageList> villageList = new ArrayList<VillageList>();
    private Unit unit;
    private Region region;
    private List<LotNumber> lotNumbers = new ArrayList<LotNumber>();
    public final static Parcelable.Creator<MasterFileData> CREATOR = new Creator<MasterFileData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public MasterFileData createFromParcel(Parcel in) {
            return new MasterFileData(in);
        }

        public MasterFileData[] newArray(int size) {
            return (new MasterFileData[size]);
        }

    }
    ;

    protected MasterFileData(Parcel in) {
        this.masterfileId = ((int) in.readValue((int.class.getClassLoader())));
        this.masterfileRefNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.dsRefNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.atMapNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.fileType = ((String) in.readValue((String.class.getClassLoader())));
        this.fileNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.districtId = ((int) in.readValue((int.class.getClassLoader())));
        this.districtName = ((String) in.readValue((String.class.getClassLoader())));
        this.dsId = ((int) in.readValue((int.class.getClassLoader())));
        this.dsName = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.villageList, (io.xiges.ximapper.model.masterfile.VillageList.class.getClassLoader()));
        this.unit = ((Unit) in.readValue((Unit.class.getClassLoader())));
        this.region = ((Region) in.readValue((Region.class.getClassLoader())));
        in.readList(this.lotNumbers, (io.xiges.ximapper.model.masterfile.LotNumber.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public MasterFileData() {
    }

    /**
     * 
     * @param dsRefNumber
     * @param fileNumber
     * @param districtName
     * @param dsName
     * @param dsId
     * @param masterfileId
     * @param masterfileRefNumber
     * @param villageList
     * @param unit
     * @param lotNumbers
     * @param districtId
     * @param atMapNumber
     * @param region
     * @param fileType
     */
    public MasterFileData(int masterfileId, String masterfileRefNumber, String dsRefNumber, String atMapNumber, String fileType, String fileNumber, int districtId, String districtName, int dsId, String dsName, List<VillageList> villageList, Unit unit, Region region, List<LotNumber> lotNumbers) {
        super();
        this.masterfileId = masterfileId;
        this.masterfileRefNumber = masterfileRefNumber;
        this.dsRefNumber = dsRefNumber;
        this.atMapNumber = atMapNumber;
        this.fileType = fileType;
        this.fileNumber = fileNumber;
        this.districtId = districtId;
        this.districtName = districtName;
        this.dsId = dsId;
        this.dsName = dsName;
        this.villageList = villageList;
        this.unit = unit;
        this.region = region;
        this.lotNumbers = lotNumbers;
    }

    public int getMasterfileId() {
        return masterfileId;
    }

    public void setMasterfileId(int masterfileId) {
        this.masterfileId = masterfileId;
    }

    public String getMasterfileRefNumber() {
        return masterfileRefNumber;
    }

    public void setMasterfileRefNumber(String masterfileRefNumber) {
        this.masterfileRefNumber = masterfileRefNumber;
    }

    public String getDsRefNumber() {
        return dsRefNumber;
    }

    public void setDsRefNumber(String dsRefNumber) {
        this.dsRefNumber = dsRefNumber;
    }

    public String getAtMapNumber() {
        return atMapNumber;
    }

    public void setAtMapNumber(String atMapNumber) {
        this.atMapNumber = atMapNumber;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileNumber() {
        return fileNumber;
    }

    public void setFileNumber(String fileNumber) {
        this.fileNumber = fileNumber;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public int getDsId() {
        return dsId;
    }

    public void setDsId(int dsId) {
        this.dsId = dsId;
    }

    public String getDsName() {
        return dsName;
    }

    public void setDsName(String dsName) {
        this.dsName = dsName;
    }

    public List<VillageList> getVillageList() {
        if(villageList == null){
            villageList = new ArrayList<>();
        }
        return villageList;
    }

    public void setVillageList(List<VillageList> villageList) {
        this.villageList = villageList;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public List<LotNumber> getLotNumbers() {
        if(lotNumbers == null){
            lotNumbers = new ArrayList<>();
        }

        return lotNumbers;
    }

    public void setLotNumbers(List<LotNumber> lotNumbers) {
        this.lotNumbers = lotNumbers;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(masterfileId);
        dest.writeValue(masterfileRefNumber);
        dest.writeValue(dsRefNumber);
        dest.writeValue(atMapNumber);
        dest.writeValue(fileType);
        dest.writeValue(fileNumber);
        dest.writeValue(districtId);
        dest.writeValue(districtName);
        dest.writeValue(dsId);
        dest.writeValue(dsName);
        dest.writeList(villageList);
        dest.writeValue(unit);
        dest.writeValue(region);
        dest.writeList(lotNumbers);
    }

    public int describeContents() {
        return  0;
    }

}
