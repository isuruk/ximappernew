package io.xiges.ximapper.model.building_details;

import android.os.Parcel;
import android.os.Parcelable;

import io.xiges.ximapper.model.api.master_data.BuildingAcquiredPortion;
import io.xiges.ximapper.model.api.master_data.BuildingCategory;
import io.xiges.ximapper.model.api.master_data.BuildingClass;
import io.xiges.ximapper.model.api.master_data.BuildingNatureOfConstruction;
import io.xiges.ximapper.model.api.master_data.TypeOfCondition;

public class CRBuildingDetails implements Parcelable {
    private String id;
    private BuildingCategory buildingCategory;
    private int floors;
    private TypeOfCondition condition;
    private BuildingClass buildingClass;
    private BuildingNatureOfConstruction buildingNatureOfConstruction;
    private BuildingAcquiredPortion aquiredPotion;
    private CelingDetails celingDetails;
    private Roof roof;
    private Structure structure;
    private FixtureAndFitting fixtureAndFitting;
    private FinishersAndServices finishersAndServices;
    private String age;
    private double totalFloorArea;



    public CRBuildingDetails() {

    }

    protected CRBuildingDetails(Parcel in) {
        id = in.readString();
        buildingCategory = in.readParcelable(BuildingCategory.class.getClassLoader());
        floors = in.readInt();
        condition = in.readParcelable(TypeOfCondition.class.getClassLoader());
        buildingClass = in.readParcelable(BuildingClass.class.getClassLoader());
        buildingNatureOfConstruction = in.readParcelable(BuildingNatureOfConstruction.class.getClassLoader());
        aquiredPotion = in.readParcelable(BuildingAcquiredPortion.class.getClassLoader());
        celingDetails = in.readParcelable(CelingDetails.class.getClassLoader());
        roof = in.readParcelable(Roof.class.getClassLoader());
        structure = in.readParcelable(Structure.class.getClassLoader());
        fixtureAndFitting = in.readParcelable(FixtureAndFitting.class.getClassLoader());
        finishersAndServices = in.readParcelable(FinishersAndServices.class.getClassLoader());
        age = in.readString();
        totalFloorArea = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeParcelable(buildingCategory, flags);
        dest.writeInt(floors);
        dest.writeParcelable(condition, flags);
        dest.writeParcelable(buildingClass, flags);
        dest.writeParcelable(buildingNatureOfConstruction, flags);
        dest.writeParcelable(aquiredPotion, flags);
        dest.writeParcelable(celingDetails, flags);
        dest.writeParcelable(roof, flags);
        dest.writeParcelable(structure, flags);
        dest.writeParcelable(fixtureAndFitting, flags);
        dest.writeParcelable(finishersAndServices, flags);
        dest.writeString(age);
        dest.writeDouble(totalFloorArea);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CRBuildingDetails> CREATOR = new Creator<CRBuildingDetails>() {
        @Override
        public CRBuildingDetails createFromParcel(Parcel in) {
            return new CRBuildingDetails(in);
        }

        @Override
        public CRBuildingDetails[] newArray(int size) {
            return new CRBuildingDetails[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BuildingCategory getBuildingCategory() {
        return buildingCategory;
    }

    public void setBuildingCategory(BuildingCategory buildingCategory) {
        this.buildingCategory = buildingCategory;
    }

    public int getFloors() {
        return floors;
    }

    public void setFloors(int floors) {
        this.floors = floors;
    }

    public TypeOfCondition getCondition() {
        return condition;
    }

    public void setCondition(TypeOfCondition condition) {
        this.condition = condition;
    }

    public BuildingClass getBuildingClass() {
        return buildingClass;
    }

    public void setBuildingClass(BuildingClass buildingClass) {
        this.buildingClass = buildingClass;
    }

    public BuildingNatureOfConstruction getBuildingNatureOfConstruction() {
        return buildingNatureOfConstruction;
    }

    public void setBuildingNatureOfConstruction(BuildingNatureOfConstruction buildingNatureOfConstruction) {
        this.buildingNatureOfConstruction = buildingNatureOfConstruction;
    }

    public BuildingAcquiredPortion getAquiredPotion() {
        return aquiredPotion;
    }

    public void setAquiredPotion(BuildingAcquiredPortion aquiredPotion) {
        this.aquiredPotion = aquiredPotion;
    }

    public CelingDetails getCelingDetails() {
        return celingDetails;
    }

    public void setCelingDetails(CelingDetails celingDetails) {
        this.celingDetails = celingDetails;
    }

    public Roof getRoof() {
        return roof;
    }

    public void setRoof(Roof roof) {
        this.roof = roof;
    }

    public Structure getStructure() {
        return structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    public FixtureAndFitting getFixtureAndFitting() {
        return fixtureAndFitting;
    }

    public void setFixtureAndFitting(FixtureAndFitting fixtureAndFitting) {
        this.fixtureAndFitting = fixtureAndFitting;
    }

    public FinishersAndServices getFinishersAndServices() {
        return finishersAndServices;
    }

    public void setFinishersAndServices(FinishersAndServices finishersAndServices) {
        this.finishersAndServices = finishersAndServices;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public double getTotalFloorArea() {
        return totalFloorArea;
    }

    public void setTotalFloorArea(double totalFloorArea) {
        this.totalFloorArea = totalFloorArea;
    }


}
