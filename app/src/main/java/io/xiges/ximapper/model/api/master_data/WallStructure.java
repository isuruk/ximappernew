package io.xiges.ximapper.model.api.master_data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class WallStructure implements Parcelable
{

    private int id;
    private String description;
    public final static Parcelable.Creator<WallStructure> CREATOR = new Creator<WallStructure>() {


        @SuppressWarnings({
                "unchecked"
        })
        public WallStructure createFromParcel(Parcel in) {
            return new WallStructure(in);
        }

        public WallStructure[] newArray(int size) {
            return (new WallStructure[size]);
        }

    }
            ;

    protected WallStructure(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public WallStructure() {
    }

    /**
     *
     * @param description
     * @param id
     */
    public WallStructure(int id, String description) {
        super();
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(description);
    }

    public int describeContents() {
        return 0;
    }

}