
package io.xiges.ximapper.model.masterfile;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Region implements Parcelable
{

    private int regionId;
    private String regionName;
    public final static Parcelable.Creator<Region> CREATOR = new Creator<Region>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Region createFromParcel(Parcel in) {
            return new Region(in);
        }

        public Region[] newArray(int size) {
            return (new Region[size]);
        }

    }
    ;

    protected Region(Parcel in) {
        this.regionId = ((int) in.readValue((int.class.getClassLoader())));
        this.regionName = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Region() {
    }

    /**
     * 
     * @param regionId
     * @param regionName
     */
    public Region(int regionId, String regionName) {
        super();
        this.regionId = regionId;
        this.regionName = regionName;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(regionId);
        dest.writeValue(regionName);
    }

    public int describeContents() {
        return  0;
    }

}
