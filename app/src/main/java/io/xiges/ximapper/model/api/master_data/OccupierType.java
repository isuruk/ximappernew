package io.xiges.ximapper.model.api.master_data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class OccupierType implements Parcelable
{

    private int id;
    private String description;
    public final static Parcelable.Creator<OccupierType> CREATOR = new Creator<OccupierType>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OccupierType createFromParcel(Parcel in) {
            return new OccupierType(in);
        }

        public OccupierType[] newArray(int size) {
            return (new OccupierType[size]);
        }

    }
            ;

    protected OccupierType(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public OccupierType() {
    }

    /**
     *
     * @param description
     * @param id
     */
    public OccupierType(int id, String description) {
        super();
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(description);
    }

    public int describeContents() {
        return 0;
    }

}