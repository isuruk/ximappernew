package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;

import java.util.ArrayList;
import java.util.List;

public class Construction implements Parcelable {
    private String id;
    private String name;
    private String type;
    private String drawingType;
    @SerializedName("fillPoints")
    private List<Point> fillLayerPointList = new ArrayList<>();
    @SerializedName("linePoints")
    private List<Point> lineLayerPointList = new ArrayList<>();
    @SerializedName("circlePoints")
    private List<Feature> circleLayerFeatureList = new ArrayList<>();
    @SerializedName("circle_drawing")
    private Circle circle;
    private String fillLayerID;
    private String lineLayerID;
    private String circleLayerID;
    private String fillSourceID;
    private String lineSourceID;
    private String circleSourceID;
    @SerializedName("buildingLabels")
    private List<Symbol> buildingLabels;
    @SerializedName("floorDetails")
    private List<BuildingFloor> buildingFloorList = new ArrayList<>();

    public Construction() {

    }

    protected Construction(Parcel in) {
        id = in.readString();
        name = in.readString();
        type = in.readString();
        drawingType = in.readString();
        circle = in.readParcelable(Circle.class.getClassLoader());
        fillLayerID = in.readString();
        lineLayerID = in.readString();
        circleLayerID = in.readString();
        fillSourceID = in.readString();
        lineSourceID = in.readString();
        circleSourceID = in.readString();
        buildingFloorList = in.createTypedArrayList(BuildingFloor.CREATOR);
    }

    public static final Creator<Construction> CREATOR = new Creator<Construction>() {
        @Override
        public Construction createFromParcel(Parcel in) {
            return new Construction(in);
        }

        @Override
        public Construction[] newArray(int size) {
            return new Construction[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDrawingType() {
        return drawingType;
    }

    public void setDrawingType(String drawingType) {
        this.drawingType = drawingType;
    }

    public List<Point> getFillLayerPointList() {
        return fillLayerPointList;
    }

    public void setFillLayerPointList(List<Point> fillLayerPointList) {
        this.fillLayerPointList = fillLayerPointList;
    }

    public List<Point> getLineLayerPointList() {
        return lineLayerPointList;
    }

    public void setLineLayerPointList(List<Point> lineLayerPointList) {
        this.lineLayerPointList = lineLayerPointList;
    }

    public List<Feature> getCircleLayerFeatureList() {
        return circleLayerFeatureList;
    }

    public void setCircleLayerFeatureList(List<Feature> circleLayerFeatureList) {
        this.circleLayerFeatureList = circleLayerFeatureList;
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public String getFillLayerID() {
        return fillLayerID;
    }

    public void setFillLayerID(String fillLayerID) {
        this.fillLayerID = fillLayerID;
    }

    public String getLineLayerID() {
        return lineLayerID;
    }

    public void setLineLayerID(String lineLayerID) {
        this.lineLayerID = lineLayerID;
    }

    public String getCircleLayerID() {
        return circleLayerID;
    }

    public void setCircleLayerID(String circleLayerID) {
        this.circleLayerID = circleLayerID;
    }

    public String getFillSourceID() {
        return fillSourceID;
    }

    public void setFillSourceID(String fillSourceID) {
        this.fillSourceID = fillSourceID;
    }

    public String getLineSourceID() {
        return lineSourceID;
    }

    public void setLineSourceID(String lineSourceID) {
        this.lineSourceID = lineSourceID;
    }

    public String getCircleSourceID() {
        return circleSourceID;
    }

    public void setCircleSourceID(String circleSourceID) {
        this.circleSourceID = circleSourceID;
    }


    public List<BuildingFloor> getBuildingFloorList() {

        if(buildingFloorList== null){
            buildingFloorList = new ArrayList<>();
        }
        return buildingFloorList;
    }

    public void setBuildingFloorList(List<BuildingFloor> buildingFloorList) {
        this.buildingFloorList = buildingFloorList;
    }



    public List<Symbol> getBuildingLabels() {
        if(buildingLabels== null){
            buildingLabels = new ArrayList<>();
        }
        return buildingLabels;
    }

    public void setBuildingLabels(List<Symbol> buildingLabels) {
        this.buildingLabels = buildingLabels;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(type);
        dest.writeString(drawingType);
        dest.writeParcelable(circle, flags);
        dest.writeString(fillLayerID);
        dest.writeString(lineLayerID);
        dest.writeString(circleLayerID);
        dest.writeString(fillSourceID);
        dest.writeString(lineSourceID);
        dest.writeString(circleSourceID);
        dest.writeTypedList(buildingFloorList);
    }

    @Override
    public String toString() {
        return "Construction{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", drawingType='" + drawingType + '\'' +
                ", fillLayerPointList=" + fillLayerPointList +
                ", lineLayerPointList=" + lineLayerPointList +
                ", circleLayerFeatureList=" + circleLayerFeatureList +
                ", circle=" + circle +
                ", fillLayerID='" + fillLayerID + '\'' +
                ", lineLayerID='" + lineLayerID + '\'' +
                ", circleLayerID='" + circleLayerID + '\'' +
                ", fillSourceID='" + fillSourceID + '\'' +
                ", lineSourceID='" + lineSourceID + '\'' +
                ", circleSourceID='" + circleSourceID + '\'' +
                ", buildingFloorList=" + buildingFloorList +
                '}';
    }
}
