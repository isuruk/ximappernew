package io.xiges.ximapper.model.mixin_clone_model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class GeometryMixinContent implements Parcelable {
    private List<PointMixinContent> coordinates;


    protected GeometryMixinContent(Parcel in) {
        coordinates = in.createTypedArrayList(PointMixinContent.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(coordinates);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GeometryMixinContent> CREATOR = new Creator<GeometryMixinContent>() {
        @Override
        public GeometryMixinContent createFromParcel(Parcel in) {
            return new GeometryMixinContent(in);
        }

        @Override
        public GeometryMixinContent[] newArray(int size) {
            return new GeometryMixinContent[size];
        }
    };

    public List<PointMixinContent> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<PointMixinContent> coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "GeometryMixinContent{" +
                "coordinates=" + coordinates +
                '}';
    }
}
