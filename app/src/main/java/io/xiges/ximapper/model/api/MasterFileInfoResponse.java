package io.xiges.ximapper.model.api;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import io.xiges.ximapper.model.masterfile.LotNumber;

public class MasterFileInfoResponse implements Parcelable {
    private int masterfileId;
    private String masterfileRefNumber;
    private String dsRefNumber;
    private String atMapNumber;
    private String fileType;
    private String fileNumber;
    private int districtId;
    private String districtName;
    private int dsId;
    private String dsName;
    private List<Village> villageList;
    private Unit unit;
    Region region;
    List<GJsonFiles> geoJsonFileDtos;
    List<LotNumber> lotNumbers;

    protected MasterFileInfoResponse(Parcel in) {
        masterfileId = in.readInt();
        masterfileRefNumber = in.readString();
        dsRefNumber = in.readString();
        atMapNumber = in.readString();
        fileType = in.readString();
        fileNumber = in.readString();
        districtId = in.readInt();
        districtName = in.readString();
        dsId = in.readInt();
        dsName = in.readString();
        lotNumbers = in.createTypedArrayList(LotNumber.CREATOR);
    }

    public static final Creator<MasterFileInfoResponse> CREATOR = new Creator<MasterFileInfoResponse>() {
        @Override
        public MasterFileInfoResponse createFromParcel(Parcel in) {
            return new MasterFileInfoResponse(in);
        }

        @Override
        public MasterFileInfoResponse[] newArray(int size) {
            return new MasterFileInfoResponse[size];
        }
    };

    public int getMasterfileId() {
        return masterfileId;
    }

    public void setMasterfileId(int masterfileId) {
        this.masterfileId = masterfileId;
    }

    public String getMasterfileRefNumber() {
        return masterfileRefNumber;
    }

    public void setMasterfileRefNumber(String masterfileRefNumber) {
        this.masterfileRefNumber = masterfileRefNumber;
    }

    public String getDsRefNumber() {
        return dsRefNumber;
    }

    public void setDsRefNumber(String dsRefNumber) {
        this.dsRefNumber = dsRefNumber;
    }

    public String getAtMapNumber() {
        return atMapNumber;
    }

    public void setAtMapNumber(String atMapNumber) {
        this.atMapNumber = atMapNumber;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileNumber() {
        return fileNumber;
    }

    public void setFileNumber(String fileNumber) {
        this.fileNumber = fileNumber;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public int getDsId() {
        return dsId;
    }

    public void setDsId(int dsId) {
        this.dsId = dsId;
    }

    public String getDsName() {
        return dsName;
    }

    public void setDsName(String dsName) {
        this.dsName = dsName;
    }

    public List<Village> getVillageList() {
        return villageList;
    }

    public void setVillageList(List<Village> villageList) {
        this.villageList = villageList;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public List<GJsonFiles> getGeoJsonFileDtos() {
        return geoJsonFileDtos;
    }

    public void setGeoJsonFileDtos(List<GJsonFiles> geoJsonFileDtos) {
        this.geoJsonFileDtos = geoJsonFileDtos;
    }

    public List<LotNumber> getLotNumbers() {
        return lotNumbers;
    }

    public void setLotNumbers(List<LotNumber> lotNumbers) {
        this.lotNumbers = lotNumbers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(masterfileId);
        dest.writeString(masterfileRefNumber);
        dest.writeString(dsRefNumber);
        dest.writeString(atMapNumber);
        dest.writeString(fileType);
        dest.writeString(fileNumber);
        dest.writeInt(districtId);
        dest.writeString(districtName);
        dest.writeInt(dsId);
        dest.writeString(dsName);
        dest.writeTypedList(lotNumbers);
    }
}
