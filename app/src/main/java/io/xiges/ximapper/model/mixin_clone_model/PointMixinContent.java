package io.xiges.ximapper.model.mixin_clone_model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public  class PointMixinContent implements Parcelable {
     private ArrayList<Double> coordinates;
     private String type;


     protected PointMixinContent(Parcel in) {
          type = in.readString();
          coordinates = (ArrayList<Double>) in.readSerializable();

     }


     @Override
     public void writeToParcel(Parcel dest, int flags) {
          dest.writeString(type);
          dest.writeSerializable(coordinates);

     }

     @Override
     public int describeContents() {
          return 0;
     }

     public static final Creator<PointMixinContent> CREATOR = new Creator<PointMixinContent>() {
          @Override
          public PointMixinContent createFromParcel(Parcel in) {
               return new PointMixinContent(in);
          }

          @Override
          public PointMixinContent[] newArray(int size) {
               return new PointMixinContent[size];
          }
     };

     public ArrayList<Double> getCoordinates() {
          return coordinates;
     }

     public void setCoordinates(ArrayList<Double> coordinates) {
          this.coordinates = coordinates;
     }

     public String getType() {
          return type;
     }

     public void setType(String type) {
          this.type = type;
     }


     @Override
     public String toString() {
          return "PointMixinContent{" +
                  "coordinates=" + coordinates +
                  ", type='" + type + '\'' +
                  '}';
     }


}
