package io.xiges.ximapper.model.mixin_clone_model;

import android.os.Parcel;
import android.os.Parcelable;

public class FeaturesMixinContent implements Parcelable {
    private String type;
    private PointsMixinContent geometry;
    private Object properties;

    protected FeaturesMixinContent(Parcel in) {
        type = in.readString();
        geometry = in.readParcelable(PointsMixinContent.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeParcelable(geometry, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FeaturesMixinContent> CREATOR = new Creator<FeaturesMixinContent>() {
        @Override
        public FeaturesMixinContent createFromParcel(Parcel in) {
            return new FeaturesMixinContent(in);
        }

        @Override
        public FeaturesMixinContent[] newArray(int size) {
            return new FeaturesMixinContent[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PointsMixinContent getGeometry() {
        return geometry;
    }

    public void setGeometry(PointsMixinContent geometry) {
        this.geometry = geometry;
    }

    public Object getProperties() {
        return properties;
    }

    public void setProperties(Object properties) {
        this.properties = properties;
    }
}
