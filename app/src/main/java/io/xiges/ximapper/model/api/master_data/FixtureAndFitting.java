package io.xiges.ximapper.model.api.master_data;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class FixtureAndFitting implements Parcelable {

    private List<FixtureAndFittingDoor> fixtureAndFittingDoor = null;
    private List<FixtureAndFittingWindow> fixtureAndFittingWindows = null;
    private List<FixtureAndFittingWindowProtection> fixtureAndFittingWindowProtection = null;
    private List<FixtureAndFittingDoorsBathroomAndToiletFitting> fixtureAndFittingDoorsBathroomAndToiletFittings = null;
    private List<FixtureAndFittingDoorsHandRail> fixtureAndFittingDoorsHandRail = null;
    private List<FixtureAndFittingDoorsPantryCupbord> fixtureAndFittingDoorsPantryCupbord = null;
    private List<FixtureAndFittingDoorsOther> fixtureAndFittingDoorsOther = null;
    public final static Parcelable.Creator<FixtureAndFitting> CREATOR = new Creator<FixtureAndFitting>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FixtureAndFitting createFromParcel(Parcel in) {
            return new FixtureAndFitting(in);
        }

        public FixtureAndFitting[] newArray(int size) {
            return (new FixtureAndFitting[size]);
        }

    };

    protected FixtureAndFitting(Parcel in) {
        in.readList(this.fixtureAndFittingDoor, (FixtureAndFittingDoor.class.getClassLoader()));
        in.readList(this.fixtureAndFittingWindows, (FixtureAndFittingWindow.class.getClassLoader()));
        in.readList(this.fixtureAndFittingWindowProtection, (FixtureAndFittingWindowProtection.class.getClassLoader()));
        in.readList(this.fixtureAndFittingDoorsBathroomAndToiletFittings, (FixtureAndFittingDoorsBathroomAndToiletFitting.class.getClassLoader()));
        in.readList(this.fixtureAndFittingDoorsHandRail, (FixtureAndFittingDoorsHandRail.class.getClassLoader()));
        in.readList(this.fixtureAndFittingDoorsPantryCupbord, (FixtureAndFittingDoorsPantryCupbord.class.getClassLoader()));
        in.readList(this.fixtureAndFittingDoorsOther, (FixtureAndFittingDoorsOther.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     */
    public FixtureAndFitting() {
    }

    /**
     * @param fixtureAndFittingDoorsHandRail
     * @param fixtureAndFittingWindowProtection
     * @param fixtureAndFittingDoorsBathroomAndToiletFittings
     * @param fixtureAndFittingDoorsPantryCupbord
     * @param fixtureAndFittingDoor
     * @param fixtureAndFittingWindows
     * @param fixtureAndFittingDoorsOther
     */
    public FixtureAndFitting(List<FixtureAndFittingDoor> fixtureAndFittingDoor, List<FixtureAndFittingWindow> fixtureAndFittingWindows, List<FixtureAndFittingWindowProtection> fixtureAndFittingWindowProtection, List<FixtureAndFittingDoorsBathroomAndToiletFitting> fixtureAndFittingDoorsBathroomAndToiletFittings, List<FixtureAndFittingDoorsHandRail> fixtureAndFittingDoorsHandRail, List<FixtureAndFittingDoorsPantryCupbord> fixtureAndFittingDoorsPantryCupbord, List<FixtureAndFittingDoorsOther> fixtureAndFittingDoorsOther) {
        super();
        this.fixtureAndFittingDoor = fixtureAndFittingDoor;
        this.fixtureAndFittingWindows = fixtureAndFittingWindows;
        this.fixtureAndFittingWindowProtection = fixtureAndFittingWindowProtection;
        this.fixtureAndFittingDoorsBathroomAndToiletFittings = fixtureAndFittingDoorsBathroomAndToiletFittings;
        this.fixtureAndFittingDoorsHandRail = fixtureAndFittingDoorsHandRail;
        this.fixtureAndFittingDoorsPantryCupbord = fixtureAndFittingDoorsPantryCupbord;
        this.fixtureAndFittingDoorsOther = fixtureAndFittingDoorsOther;
    }

    public List<FixtureAndFittingDoor> getFixtureAndFittingDoor() {
        return fixtureAndFittingDoor;
    }

    public void setFixtureAndFittingDoor(List<FixtureAndFittingDoor> fixtureAndFittingDoor) {
        this.fixtureAndFittingDoor = fixtureAndFittingDoor;
    }

    public List<FixtureAndFittingWindow> getFixtureAndFittingWindows() {
        return fixtureAndFittingWindows;
    }

    public void setFixtureAndFittingWindows(List<FixtureAndFittingWindow> fixtureAndFittingWindows) {
        this.fixtureAndFittingWindows = fixtureAndFittingWindows;
    }

    public List<FixtureAndFittingWindowProtection> getFixtureAndFittingWindowProtection() {
        return fixtureAndFittingWindowProtection;
    }

    public void setFixtureAndFittingWindowProtection(List<FixtureAndFittingWindowProtection> fixtureAndFittingWindowProtection) {
        this.fixtureAndFittingWindowProtection = fixtureAndFittingWindowProtection;
    }

    public List<FixtureAndFittingDoorsBathroomAndToiletFitting> getFixtureAndFittingDoorsBathroomAndToiletFittings() {
        return fixtureAndFittingDoorsBathroomAndToiletFittings;
    }

    public void setFixtureAndFittingDoorsBathroomAndToiletFittings(List<FixtureAndFittingDoorsBathroomAndToiletFitting> fixtureAndFittingDoorsBathroomAndToiletFittings) {
        this.fixtureAndFittingDoorsBathroomAndToiletFittings = fixtureAndFittingDoorsBathroomAndToiletFittings;
    }

    public List<FixtureAndFittingDoorsHandRail> getFixtureAndFittingDoorsHandRail() {
        return fixtureAndFittingDoorsHandRail;
    }

    public void setFixtureAndFittingDoorsHandRail(List<FixtureAndFittingDoorsHandRail> fixtureAndFittingDoorsHandRail) {
        this.fixtureAndFittingDoorsHandRail = fixtureAndFittingDoorsHandRail;
    }

    public List<FixtureAndFittingDoorsPantryCupbord> getFixtureAndFittingDoorsPantryCupbord() {
        return fixtureAndFittingDoorsPantryCupbord;
    }

    public void setFixtureAndFittingDoorsPantryCupbord(List<FixtureAndFittingDoorsPantryCupbord> fixtureAndFittingDoorsPantryCupbord) {
        this.fixtureAndFittingDoorsPantryCupbord = fixtureAndFittingDoorsPantryCupbord;
    }

    public List<FixtureAndFittingDoorsOther> getFixtureAndFittingDoorsOther() {
        return fixtureAndFittingDoorsOther;
    }

    public void setFixtureAndFittingDoorsOther(List<FixtureAndFittingDoorsOther> fixtureAndFittingDoorsOther) {
        this.fixtureAndFittingDoorsOther = fixtureAndFittingDoorsOther;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(fixtureAndFittingDoor);
        dest.writeList(fixtureAndFittingWindows);
        dest.writeList(fixtureAndFittingWindowProtection);
        dest.writeList(fixtureAndFittingDoorsBathroomAndToiletFittings);
        dest.writeList(fixtureAndFittingDoorsHandRail);
        dest.writeList(fixtureAndFittingDoorsPantryCupbord);
        dest.writeList(fixtureAndFittingDoorsOther);
    }

    public int describeContents() {
        return 0;
    }

}