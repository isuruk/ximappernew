package io.xiges.ximapper.model.building_details;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import io.xiges.ximapper.model.api.master_data.FinishersAndServicesBathroomAndToilet;
import io.xiges.ximapper.model.api.master_data.FinishersAndServicesFloorFinisher;
import io.xiges.ximapper.model.api.master_data.FinishersAndServicesService;
import io.xiges.ximapper.model.api.master_data.FinishersAndServicesWallFinisher;

public class FinishersAndServices implements Parcelable {
    private FinishersAndServicesWallFinisher finishersAndServicesWallFinisher;
    private FinishersAndServicesFloorFinisher finishersAndServicesFloorFinisher;
    private FinishersAndServicesBathroomAndToilet finishersAndServicesBathroomAndToilet;
    private FinishersAndServicesService finishersAndServicesServices;



    public FinishersAndServices() {

    }

    protected FinishersAndServices(Parcel in) {
        finishersAndServicesWallFinisher = in.readParcelable(FinishersAndServicesWallFinisher.class.getClassLoader());
        finishersAndServicesFloorFinisher = in.readParcelable(FinishersAndServicesFloorFinisher.class.getClassLoader());
        finishersAndServicesBathroomAndToilet = in.readParcelable(FinishersAndServicesBathroomAndToilet.class.getClassLoader());
        finishersAndServicesServices = in.readParcelable(FinishersAndServicesService.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(finishersAndServicesWallFinisher, flags);
        dest.writeParcelable(finishersAndServicesFloorFinisher, flags);
        dest.writeParcelable(finishersAndServicesBathroomAndToilet, flags);
        dest.writeParcelable(finishersAndServicesServices, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FinishersAndServices> CREATOR = new Creator<FinishersAndServices>() {
        @Override
        public FinishersAndServices createFromParcel(Parcel in) {
            return new FinishersAndServices(in);
        }

        @Override
        public FinishersAndServices[] newArray(int size) {
            return new FinishersAndServices[size];
        }
    };

    public FinishersAndServicesWallFinisher getFinishersAndServicesWallFinisher() {
        return finishersAndServicesWallFinisher;
    }

    public void setFinishersAndServicesWallFinisher(FinishersAndServicesWallFinisher finishersAndServicesWallFinisher) {
        this.finishersAndServicesWallFinisher = finishersAndServicesWallFinisher;
    }

    public FinishersAndServicesFloorFinisher getFinishersAndServicesFloorFinisher() {
        return finishersAndServicesFloorFinisher;
    }

    public void setFinishersAndServicesFloorFinisher(FinishersAndServicesFloorFinisher finishersAndServicesFloorFinisher) {
        this.finishersAndServicesFloorFinisher = finishersAndServicesFloorFinisher;
    }

    public FinishersAndServicesBathroomAndToilet getFinishersAndServicesBathroomAndToilet() {
        return finishersAndServicesBathroomAndToilet;
    }

    public void setFinishersAndServicesBathroomAndToilet(FinishersAndServicesBathroomAndToilet finishersAndServicesBathroomAndToilet) {
        this.finishersAndServicesBathroomAndToilet = finishersAndServicesBathroomAndToilet;
    }

    public FinishersAndServicesService getFinishersAndServicesServices() {
        return finishersAndServicesServices;
    }

    public void setFinishersAndServicesServices(FinishersAndServicesService finishersAndServicesServices) {
        this.finishersAndServicesServices = finishersAndServicesServices;
    }
}
