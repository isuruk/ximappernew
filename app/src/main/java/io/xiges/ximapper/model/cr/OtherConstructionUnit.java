
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import java.util.Objects;

public class OtherConstructionUnit implements Parcelable
{

    private String code;
    private String description;
    public final static Parcelable.Creator<OtherConstructionUnit> CREATOR = new Creator<OtherConstructionUnit>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OtherConstructionUnit createFromParcel(Parcel in) {
            return new OtherConstructionUnit(in);
        }

        public OtherConstructionUnit[] newArray(int size) {
            return (new OtherConstructionUnit[size]);
        }

    }
    ;

    protected OtherConstructionUnit(Parcel in) {
        this.code = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public OtherConstructionUnit() {
    }

    /**
     * 
     * @param code
     * @param description
     */
    public OtherConstructionUnit(String code, String description) {
        super();
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(code);
        dest.writeValue(description);
    }

    public int describeContents() {
        return  0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OtherConstructionUnit that = (OtherConstructionUnit) o;
        return Objects.equals(code, that.code) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, description);
    }
}
