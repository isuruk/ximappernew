package io.xiges.ximapper.model.api.master_data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class OtherConstructionsMaterial implements Parcelable
{

    private String code;
    private String description;
    private String unit;
    private String otherConstructionType;
    public final static Parcelable.Creator<OtherConstructionsMaterial> CREATOR = new Creator<OtherConstructionsMaterial>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OtherConstructionsMaterial createFromParcel(Parcel in) {
            return new OtherConstructionsMaterial(in);
        }

        public OtherConstructionsMaterial[] newArray(int size) {
            return (new OtherConstructionsMaterial[size]);
        }

    }
            ;

    protected OtherConstructionsMaterial(Parcel in) {
        this.code = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.unit = ((String) in.readValue((String.class.getClassLoader())));
        this.otherConstructionType = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public OtherConstructionsMaterial() {
    }

    /**
     *
     * @param unit
     * @param code
     * @param otherConstructionType
     * @param description
     */
    public OtherConstructionsMaterial(String code, String description, String unit, String otherConstructionType) {
        super();
        this.code = code;
        this.description = description;
        this.unit = unit;
        this.otherConstructionType = otherConstructionType;
    }
    public OtherConstructionsMaterial(String code, String description) {
        super();
        this.code = code;
        this.description = description;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getOtherConstructionType() {
        return otherConstructionType;
    }

    public void setOtherConstructionType(String otherConstructionType) {
        this.otherConstructionType = otherConstructionType;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(code);
        dest.writeValue(description);
        dest.writeValue(unit);
        dest.writeValue(otherConstructionType);
    }

    public int describeContents() {
        return 0;
    }


    @Override
    public String toString() {
        return "OtherConstructionsMaterial{" +
                "code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", unit='" + unit + '\'' +
                ", otherConstructionType='" + otherConstructionType + '\'' +
                '}';
    }
}