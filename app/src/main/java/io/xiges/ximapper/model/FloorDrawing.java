package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FloorDrawing implements Parcelable {
    private String drawingType;
    @SerializedName("fillPoints")
    private List<Point> fillLayerPointList = new ArrayList<>();
    @SerializedName("linePoints")
    private List<Point> lineLayerPointList = new ArrayList<>();
    @SerializedName("circlePoints")
    private List<Feature> circleLayerFeatureList = new ArrayList<>();
    @SerializedName("circleFillPoints")
    private List<Feature> circleFillPoints = new ArrayList<>();
    private String fillLayerID;
    private String lineLayerID;
    private String circleLayerID;
    private String fillSourceID;
    private String lineSourceID;
    private String circleSourceID;
    @SerializedName("circle_drawing")
    private Circle circle;
    private String aqCircleLayerID;
    private String aqCircleSourceID;
    private String aqLineSourceID;
    private String aqLineLayerID;
    private String aqFillSourceID;
    private String aqFillLayerID;
    @SerializedName("acquisitionFillPoints")
    private List<Point> fillLayerAQPointList = new ArrayList<>();
    @SerializedName("acquisitionLinePoints")
    private List<Point> lineLayerAQPointList = new ArrayList<>();
    @SerializedName("acquisitionCirclePoints")
    private List<Feature> circleLayerAQFeatureList = new ArrayList<>();



    public FloorDrawing() {

    }

    protected FloorDrawing(Parcel in) {
        drawingType = in.readString();
        fillLayerID = in.readString();
        lineLayerID = in.readString();
        circleLayerID = in.readString();
        fillSourceID = in.readString();
        lineSourceID = in.readString();
        circleSourceID = in.readString();
        aqCircleLayerID = in.readString();
        aqCircleSourceID = in.readString();
        aqLineSourceID = in.readString();
        aqLineLayerID = in.readString();
        aqFillSourceID = in.readString();
        aqFillLayerID = in.readString();
        circle = in.readParcelable(Circle.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(drawingType);
        dest.writeString(fillLayerID);
        dest.writeString(lineLayerID);
        dest.writeString(circleLayerID);
        dest.writeString(fillSourceID);
        dest.writeString(lineSourceID);
        dest.writeString(circleSourceID);
        dest.writeString(aqCircleLayerID);
        dest.writeString(aqCircleSourceID);
        dest.writeString(aqLineSourceID);
        dest.writeString(aqLineLayerID);
        dest.writeString(aqFillSourceID);
        dest.writeString(aqFillLayerID);
        dest.writeParcelable(circle, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FloorDrawing> CREATOR = new Creator<FloorDrawing>() {
        @Override
        public FloorDrawing createFromParcel(Parcel in) {
            return new FloorDrawing(in);
        }

        @Override
        public FloorDrawing[] newArray(int size) {
            return new FloorDrawing[size];
        }
    };

    public String getDrawingType() {
        return drawingType;
    }

    public void setDrawingType(String drawingType) {
        this.drawingType = drawingType;
    }

    public List<Point> getFillLayerPointList() {
        return fillLayerPointList;
    }

    public void setFillLayerPointList(List<Point> fillLayerPointList) {
        this.fillLayerPointList = fillLayerPointList;
    }

    public List<Point> getLineLayerPointList() {
        return lineLayerPointList;
    }

    public void setLineLayerPointList(List<Point> lineLayerPointList) {
        this.lineLayerPointList = lineLayerPointList;
    }

    public List<Feature> getCircleLayerFeatureList() {
        return circleLayerFeatureList;
    }

    public void setCircleLayerFeatureList(List<Feature> circleLayerFeatureList) {
        this.circleLayerFeatureList = circleLayerFeatureList;
    }

    public List<Feature> getCircleFillPoints() {
        return circleFillPoints;
    }

    public void setCircleFillPoints(List<Feature> circleFillPoints) {
        this.circleFillPoints = circleFillPoints;
    }

    public String getFillLayerID() {
        return fillLayerID;
    }

    public void setFillLayerID(String fillLayerID) {
        this.fillLayerID = fillLayerID;
    }

    public String getLineLayerID() {
        return lineLayerID;
    }

    public void setLineLayerID(String lineLayerID) {
        this.lineLayerID = lineLayerID;
    }

    public String getCircleLayerID() {
        return circleLayerID;
    }

    public void setCircleLayerID(String circleLayerID) {
        this.circleLayerID = circleLayerID;
    }

    public String getFillSourceID() {
        return fillSourceID;
    }

    public void setFillSourceID(String fillSourceID) {
        this.fillSourceID = fillSourceID;
    }

    public String getLineSourceID() {
        return lineSourceID;
    }

    public void setLineSourceID(String lineSourceID) {
        this.lineSourceID = lineSourceID;
    }

    public String getCircleSourceID() {
        return circleSourceID;
    }

    public void setCircleSourceID(String circleSourceID) {
        this.circleSourceID = circleSourceID;
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public String getAqCircleLayerID() {
        return aqCircleLayerID;
    }

    public void setAqCircleLayerID(String aqCircleLayerID) {
        this.aqCircleLayerID = aqCircleLayerID;
    }

    public String getAqCircleSourceID() {
        return aqCircleSourceID;
    }

    public void setAqCircleSourceID(String aqCircleSourceID) {
        this.aqCircleSourceID = aqCircleSourceID;
    }

    public String getAqLineSourceID() {
        return aqLineSourceID;
    }

    public void setAqLineSourceID(String aqLineSourceID) {
        this.aqLineSourceID = aqLineSourceID;
    }

    public String getAqLineLayerID() {
        return aqLineLayerID;
    }

    public void setAqLineLayerID(String aqLineLayerID) {
        this.aqLineLayerID = aqLineLayerID;
    }

    public String getAqFillSourceID() {
        return aqFillSourceID;
    }

    public void setAqFillSourceID(String aqFillSourceID) {
        this.aqFillSourceID = aqFillSourceID;
    }

    public String getAqFillLayerID() {
        return aqFillLayerID;
    }

    public void setAqFillLayerID(String aqFillLayerID) {
        this.aqFillLayerID = aqFillLayerID;
    }

    public List<Point> getFillLayerAQPointList() {
        return fillLayerAQPointList;
    }

    public void setFillLayerAQPointList(List<Point> fillLayerAQPointList) {
        this.fillLayerAQPointList = fillLayerAQPointList;
    }

    public List<Point> getLineLayerAQPointList() {
        return lineLayerAQPointList;
    }

    public void setLineLayerAQPointList(List<Point> lineLayerAQPointList) {
        this.lineLayerAQPointList = lineLayerAQPointList;
    }

    public List<Feature> getCircleLayerAQFeatureList() {
        return circleLayerAQFeatureList;
    }

    public void setCircleLayerAQFeatureList(List<Feature> circleLayerAQFeatureList) {
        this.circleLayerAQFeatureList = circleLayerAQFeatureList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FloorDrawing that = (FloorDrawing) o;
        return Objects.equals(drawingType, that.drawingType) &&
                Objects.equals(circleFillPoints, that.circleFillPoints) &&
                Objects.equals(fillLayerID, that.fillLayerID) &&
                Objects.equals(lineLayerID, that.lineLayerID) &&
                Objects.equals(circleLayerID, that.circleLayerID) &&
                Objects.equals(fillSourceID, that.fillSourceID) &&
                Objects.equals(lineSourceID, that.lineSourceID) &&
                Objects.equals(circleSourceID, that.circleSourceID) &&
                Objects.equals(circle, that.circle) &&
                Objects.equals(aqCircleLayerID, that.aqCircleLayerID) &&
                Objects.equals(aqCircleSourceID, that.aqCircleSourceID) &&
                Objects.equals(aqLineSourceID, that.aqLineSourceID) &&
                Objects.equals(aqLineLayerID, that.aqLineLayerID) &&
                Objects.equals(aqFillSourceID, that.aqFillSourceID) &&
                Objects.equals(aqFillLayerID, that.aqFillLayerID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(drawingType, circleFillPoints, fillLayerID, lineLayerID, circleLayerID, fillSourceID, lineSourceID, circleSourceID, circle, aqCircleLayerID, aqCircleSourceID, aqLineSourceID, aqLineLayerID, aqFillSourceID, aqFillLayerID);
    }

    @Override
    public String toString() {
        return "FloorDrawing{" +
                "drawingType='" + drawingType + '\'' +
                ", fillLayerPointList=" + fillLayerPointList +
                ", lineLayerPointList=" + lineLayerPointList +
                ", circleLayerFeatureList=" + circleLayerFeatureList +
                ", circleFillPoints=" + circleFillPoints +
                ", fillLayerID='" + fillLayerID + '\'' +
                ", lineLayerID='" + lineLayerID + '\'' +
                ", circleLayerID='" + circleLayerID + '\'' +
                ", fillSourceID='" + fillSourceID + '\'' +
                ", lineSourceID='" + lineSourceID + '\'' +
                ", circleSourceID='" + circleSourceID + '\'' +
                ", circle=" + circle +
                ", aqCircleLayerID='" + aqCircleLayerID + '\'' +
                ", aqCircleSourceID='" + aqCircleSourceID + '\'' +
                ", aqLineSourceID='" + aqLineSourceID + '\'' +
                ", aqLineLayerID='" + aqLineLayerID + '\'' +
                ", aqFillSourceID='" + aqFillSourceID + '\'' +
                ", aqFillLayerID='" + aqFillLayerID + '\'' +
                ", fillLayerAQPointList=" + fillLayerAQPointList +
                ", lineLayerAQPointList=" + lineLayerAQPointList +
                ", circleLayerAQFeatureList=" + circleLayerAQFeatureList +
                '}';
    }
}
