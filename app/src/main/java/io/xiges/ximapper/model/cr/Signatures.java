
package io.xiges.ximapper.model.cr;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Signatures implements Parcelable
{

    private AcquisitionOfficerDetails acquisitionOfficerDetails;
    private ChiefValuerRepresentationDetails chiefValuerRepresentationDetails;
    private GramaNiladhariDetails gramaNiladhariDetails;
    private OccupierDetails occupierDetails;
    private List<RepresentativeDetail> representativeDetails = new ArrayList<RepresentativeDetail>();
    public final static Parcelable.Creator<Signatures> CREATOR = new Creator<Signatures>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Signatures createFromParcel(Parcel in) {
            return new Signatures(in);
        }

        public Signatures[] newArray(int size) {
            return (new Signatures[size]);
        }

    }
    ;

    protected Signatures(Parcel in) {
        this.acquisitionOfficerDetails = ((AcquisitionOfficerDetails) in.readValue((AcquisitionOfficerDetails.class.getClassLoader())));
        this.chiefValuerRepresentationDetails = ((ChiefValuerRepresentationDetails) in.readValue((ChiefValuerRepresentationDetails.class.getClassLoader())));
        this.gramaNiladhariDetails = ((GramaNiladhariDetails) in.readValue((GramaNiladhariDetails.class.getClassLoader())));
        this.occupierDetails = ((OccupierDetails) in.readValue((OccupierDetails.class.getClassLoader())));
        in.readList(this.representativeDetails, (io.xiges.ximapper.model.cr.RepresentativeDetail.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Signatures() {
    }

    /**
     * 
     * @param representativeDetails
     * @param acquisitionOfficerDetails
     * @param chiefValuerRepresentationDetails
     * @param gramaNiladhariDetails
     * @param occupierDetails
     */
    public Signatures(AcquisitionOfficerDetails acquisitionOfficerDetails, ChiefValuerRepresentationDetails chiefValuerRepresentationDetails, GramaNiladhariDetails gramaNiladhariDetails, OccupierDetails occupierDetails, List<RepresentativeDetail> representativeDetails) {
        super();
        this.acquisitionOfficerDetails = acquisitionOfficerDetails;
        this.chiefValuerRepresentationDetails = chiefValuerRepresentationDetails;
        this.gramaNiladhariDetails = gramaNiladhariDetails;
        this.occupierDetails = occupierDetails;
        this.representativeDetails = representativeDetails;
    }

    public AcquisitionOfficerDetails getAcquisitionOfficerDetails() {
        return acquisitionOfficerDetails;
    }

    public void setAcquisitionOfficerDetails(AcquisitionOfficerDetails acquisitionOfficerDetails) {
        this.acquisitionOfficerDetails = acquisitionOfficerDetails;
    }

    public ChiefValuerRepresentationDetails getChiefValuerRepresentationDetails() {
        return chiefValuerRepresentationDetails;
    }

    public void setChiefValuerRepresentationDetails(ChiefValuerRepresentationDetails chiefValuerRepresentationDetails) {
        this.chiefValuerRepresentationDetails = chiefValuerRepresentationDetails;
    }

    public GramaNiladhariDetails getGramaNiladhariDetails() {
        return gramaNiladhariDetails;
    }

    public void setGramaNiladhariDetails(GramaNiladhariDetails gramaNiladhariDetails) {
        this.gramaNiladhariDetails = gramaNiladhariDetails;
    }

    public OccupierDetails getOccupierDetails() {
        return occupierDetails;
    }

    public void setOccupierDetails(OccupierDetails occupierDetails) {
        this.occupierDetails = occupierDetails;
    }

    public List<RepresentativeDetail> getRepresentativeDetails() {
        return representativeDetails;
    }

    public void setRepresentativeDetails(List<RepresentativeDetail> representativeDetails) {
        this.representativeDetails = representativeDetails;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(acquisitionOfficerDetails);
        dest.writeValue(chiefValuerRepresentationDetails);
        dest.writeValue(gramaNiladhariDetails);
        dest.writeValue(occupierDetails);
        dest.writeList(representativeDetails);
    }

    public int describeContents() {
        return  0;
    }

}
