

package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class LineJson implements Parcelable
{

    private String type;
    private List<List<Double>> coordinates = null;
    public final static Parcelable.Creator<LineJson> CREATOR = new Creator<LineJson>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LineJson createFromParcel(Parcel in) {
            return new LineJson(in);
        }

        public LineJson[] newArray(int size) {
            return (new LineJson[size]);
        }

    }
            ;

    protected LineJson(Parcel in) {
        this.type = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public LineJson() {
    }

    /**
     *
     * @param coordinates
     * @param type
     */
    public LineJson(String type, List<List<Double>> coordinates) {
        super();
        this.type = type;
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<List<Double>> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<List<Double>> coordinates) {
        this.coordinates = coordinates;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(type);
        dest.writeList(coordinates);
    }

    public int describeContents() {
        return 0;
    }

}