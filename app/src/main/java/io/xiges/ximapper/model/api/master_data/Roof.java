package io.xiges.ximapper.model.api.master_data;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Roof implements Parcelable
{

    private List<RoofMaterial> roofMaterial = null;
    private List<RoofFrame> roofFrame = null;
    private List<RoofFinisher> roofFinisher = null;
    private List<Ceiling> ceiling = null;
    public final static Parcelable.Creator<Roof> CREATOR = new Creator<Roof>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Roof createFromParcel(Parcel in) {
            return new Roof(in);
        }

        public Roof[] newArray(int size) {
            return (new Roof[size]);
        }

    }
            ;

    protected Roof(Parcel in) {
        in.readList(this.roofMaterial, (RoofMaterial.class.getClassLoader()));
        in.readList(this.roofFrame, (RoofFrame.class.getClassLoader()));
        in.readList(this.roofFinisher, (RoofFinisher.class.getClassLoader()));
        in.readList(this.ceiling, (Ceiling.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Roof() {
    }

    /**
     *
     * @param roofFinisher
     * @param ceiling
     * @param roofMaterial
     * @param roofFrame
     */
    public Roof(List<RoofMaterial> roofMaterial, List<RoofFrame> roofFrame, List<RoofFinisher> roofFinisher, List<Ceiling> ceiling) {
        super();
        this.roofMaterial = roofMaterial;
        this.roofFrame = roofFrame;
        this.roofFinisher = roofFinisher;
        this.ceiling = ceiling;
    }

    public List<RoofMaterial> getRoofMaterial() {
        return roofMaterial;
    }

    public void setRoofMaterial(List<RoofMaterial> roofMaterial) {
        this.roofMaterial = roofMaterial;
    }

    public List<RoofFrame> getRoofFrame() {
        return roofFrame;
    }

    public void setRoofFrame(List<RoofFrame> roofFrame) {
        this.roofFrame = roofFrame;
    }

    public List<RoofFinisher> getRoofFinisher() {
        return roofFinisher;
    }

    public void setRoofFinisher(List<RoofFinisher> roofFinisher) {
        this.roofFinisher = roofFinisher;
    }

    public List<Ceiling> getCeiling() {
        return ceiling;
    }

    public void setCeiling(List<Ceiling> ceiling) {
        this.ceiling = ceiling;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(roofMaterial);
        dest.writeList(roofFrame);
        dest.writeList(roofFinisher);
        dest.writeList(ceiling);
    }

    public int describeContents() {
        return 0;
    }

}