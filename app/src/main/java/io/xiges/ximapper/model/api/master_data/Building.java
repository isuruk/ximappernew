package io.xiges.ximapper.model.api.master_data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Building implements Parcelable
{

    private BuildingDetails buildingDetails;
    private Roof roof;
    private Structure structure;
    private FixtureAndFitting fixtureAndFitting;
    private FinishersAndServices finishersAndServices;
    private OwnersAndOccupierDetails ownersAndOccupierDetails;
    public final static Parcelable.Creator<Building> CREATOR = new Creator<Building>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Building createFromParcel(Parcel in) {
            return new Building(in);
        }

        public Building[] newArray(int size) {
            return (new Building[size]);
        }

    }
            ;

    protected Building(Parcel in) {
        this.buildingDetails = ((BuildingDetails) in.readValue((BuildingDetails.class.getClassLoader())));
        this.roof = ((Roof) in.readValue((Roof.class.getClassLoader())));
        this.structure = ((Structure) in.readValue((Structure.class.getClassLoader())));
        this.fixtureAndFitting = ((FixtureAndFitting) in.readValue((FixtureAndFitting.class.getClassLoader())));
        this.finishersAndServices = ((FinishersAndServices) in.readValue((FinishersAndServices.class.getClassLoader())));
        this.ownersAndOccupierDetails = ((OwnersAndOccupierDetails) in.readValue((OwnersAndOccupierDetails.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Building() {
    }

    /**
     *
     * @param roof
     * @param fixtureAndFitting
     * @param finishersAndServices
     * @param buildingDetails
     * @param ownersAndOccupierDetails
     * @param structure
     */
    public Building(BuildingDetails buildingDetails, Roof roof, Structure structure, FixtureAndFitting fixtureAndFitting, FinishersAndServices finishersAndServices, OwnersAndOccupierDetails ownersAndOccupierDetails) {
        super();
        this.buildingDetails = buildingDetails;
        this.roof = roof;
        this.structure = structure;
        this.fixtureAndFitting = fixtureAndFitting;
        this.finishersAndServices = finishersAndServices;
        this.ownersAndOccupierDetails = ownersAndOccupierDetails;
    }

    public BuildingDetails getBuildingDetails() {
        return buildingDetails;
    }

    public void setBuildingDetails(BuildingDetails buildingDetails) {
        this.buildingDetails = buildingDetails;
    }

    public Roof getRoof() {
        return roof;
    }

    public void setRoof(Roof roof) {
        this.roof = roof;
    }

    public Structure getStructure() {
        return structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    public FixtureAndFitting getFixtureAndFitting() {
        return fixtureAndFitting;
    }

    public void setFixtureAndFitting(FixtureAndFitting fixtureAndFitting) {
        this.fixtureAndFitting = fixtureAndFitting;
    }

    public FinishersAndServices getFinishersAndServices() {
        return finishersAndServices;
    }

    public void setFinishersAndServices(FinishersAndServices finishersAndServices) {
        this.finishersAndServices = finishersAndServices;
    }

    public OwnersAndOccupierDetails getOwnersAndOccupierDetails() {
        return ownersAndOccupierDetails;
    }

    public void setOwnersAndOccupierDetails(OwnersAndOccupierDetails ownersAndOccupierDetails) {
        this.ownersAndOccupierDetails = ownersAndOccupierDetails;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(buildingDetails);
        dest.writeValue(roof);
        dest.writeValue(structure);
        dest.writeValue(fixtureAndFitting);
        dest.writeValue(finishersAndServices);
        dest.writeValue(ownersAndOccupierDetails);
    }

    public int describeContents() {
        return 0;
    }

}