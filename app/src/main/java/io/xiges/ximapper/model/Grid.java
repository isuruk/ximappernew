package io.xiges.ximapper.model;

import java.util.List;

public class Grid {
    private List<GridFeatures> features;
    private String type;

    public List<GridFeatures> getFeatures() {
        return features;
    }

    public void setFeatures(List<GridFeatures> features) {
        this.features = features;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
