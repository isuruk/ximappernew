package io.xiges.ximapper.model.api.master_data;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterData implements Parcelable {

    private Land land;
    private Building building;
    private OtherConstruction otherConstruction;

    public MasterData() {

    }

    public MasterData(Land land, Building building, OtherConstruction otherConstruction) {
        this.land = land;
        this.building = building;
        this.otherConstruction = otherConstruction;
    }

    protected MasterData(Parcel in) {
        land = in.readParcelable(Land.class.getClassLoader());
        building = in.readParcelable(Building.class.getClassLoader());
        otherConstruction = in.readParcelable(OtherConstruction.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(land, flags);
        dest.writeParcelable(building, flags);
        dest.writeParcelable(otherConstruction, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MasterData> CREATOR = new Creator<MasterData>() {
        @Override
        public MasterData createFromParcel(Parcel in) {
            return new MasterData(in);
        }

        @Override
        public MasterData[] newArray(int size) {
            return new MasterData[size];
        }
    };

    public Land getLand() {
        return land;
    }

    public void setLand(Land land) {
        this.land = land;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    public OtherConstruction getOtherConstruction() {
        return otherConstruction;
    }

    public void setOtherConstruction(OtherConstruction otherConstruction) {
        this.otherConstruction = otherConstruction;
    }

    @Override
    public String toString() {
        return "MasterData{" +
                "land=" + land +
                ", building=" + building +
                ", otherConstruction=" + otherConstruction +
                '}';
    }
}