package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.xiges.ximapper.model.api.master_data.AccessCategory;
import io.xiges.ximapper.model.api.master_data.LandCategory;
import io.xiges.ximapper.model.api.master_data.LandSubCategory;
import io.xiges.ximapper.model.api.master_data.LandUseType;

public class CRLandInfo implements Parcelable {
    private String nameOfTheVillage;
    private String nameOfLand;
    private String assessmentNo;
    private String roadName;
    private String north;
    private String east;
    private String west;
    private String south;
    private String access;
    private AccessCategory accessCategory;
    private String description;
    private LandCategory landCategory;
    private LandSubCategory landSubCategory;
    private String remarks;
    private LandUseType landUseType;
    private String depthOfTheLand;
    private String planNo;
    private String lotId;




    public CRLandInfo() {

    }

    protected CRLandInfo(Parcel in) {
        nameOfTheVillage = in.readString();
        nameOfLand = in.readString();
        assessmentNo = in.readString();
        roadName = in.readString();
        north = in.readString();
        east = in.readString();
        west = in.readString();
        south = in.readString();
        access = in.readString();
        accessCategory = in.readParcelable(AccessCategory.class.getClassLoader());
        description = in.readString();
        landCategory = in.readParcelable(LandCategory.class.getClassLoader());
        landSubCategory = in.readParcelable(LandSubCategory.class.getClassLoader());
        remarks = in.readString();
        landUseType = in.readParcelable(LandUseType.class.getClassLoader());
        depthOfTheLand = in.readString();
        planNo = in.readString();
        lotId = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameOfTheVillage);
        dest.writeString(nameOfLand);
        dest.writeString(assessmentNo);
        dest.writeString(roadName);
        dest.writeString(north);
        dest.writeString(east);
        dest.writeString(west);
        dest.writeString(south);
        dest.writeString(access);
        dest.writeParcelable(accessCategory, flags);
        dest.writeString(description);
        dest.writeParcelable(landCategory, flags);
        dest.writeParcelable(landSubCategory, flags);
        dest.writeString(remarks);
        dest.writeParcelable(landUseType, flags);
        dest.writeString(depthOfTheLand);
        dest.writeString(planNo);
        dest.writeString(lotId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CRLandInfo> CREATOR = new Creator<CRLandInfo>() {
        @Override
        public CRLandInfo createFromParcel(Parcel in) {
            return new CRLandInfo(in);
        }

        @Override
        public CRLandInfo[] newArray(int size) {
            return new CRLandInfo[size];
        }
    };

    public String getNameOfTheVillage() {
        return nameOfTheVillage;
    }

    public void setNameOfTheVillage(String nameOfTheVillage) {
        this.nameOfTheVillage = nameOfTheVillage;
    }

    public String getNameOfLand() {
        return nameOfLand;
    }

    public void setNameOfLand(String nameOfLand) {
        this.nameOfLand = nameOfLand;
    }

    public String getAssessmentNo() {
        return assessmentNo;
    }

    public void setAssessmentNo(String assessmentNo) {
        this.assessmentNo = assessmentNo;
    }

    public String getRoadName() {
        return roadName;
    }

    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }

    public String getNorth() {
        return north;
    }

    public void setNorth(String north) {
        this.north = north;
    }

    public String getEast() {
        return east;
    }

    public void setEast(String east) {
        this.east = east;
    }

    public String getWest() {
        return west;
    }

    public void setWest(String west) {
        this.west = west;
    }

    public String getSouth() {
        return south;
    }

    public void setSouth(String south) {
        this.south = south;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public AccessCategory getAccessCategory() {
        return accessCategory;
    }

    public void setAccessCategory(AccessCategory accessCategory) {
        this.accessCategory = accessCategory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LandCategory getLandCategory() {
        return landCategory;
    }

    public void setLandCategory(LandCategory landCategory) {
        this.landCategory = landCategory;
    }

    public LandSubCategory getLandSubCategory() {
        return landSubCategory;
    }

    public void setLandSubCategory(LandSubCategory landSubCategory) {
        this.landSubCategory = landSubCategory;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public LandUseType getLandUseType() {
        return landUseType;
    }

    public void setLandUseType(LandUseType landUseType) {
        this.landUseType = landUseType;
    }

    public String getDepthOfTheLand() {
        return depthOfTheLand;
    }

    public void setDepthOfTheLand(String depthOfTheLand) {
        this.depthOfTheLand = depthOfTheLand;
    }

    public String getPlanNo() {
        return planNo;
    }

    public void setPlanNo(String planNo) {
        this.planNo = planNo;
    }

    public String getLotId() {
        return lotId;
    }

    public void setLotId(String lotId) {
        this.lotId = lotId;
    }
}
