package io.xiges.ximapper.model.api.master_data;

import android.os.Parcel;
import android.os.Parcelable;

public class LandSubCategory implements Parcelable
{

    private int id;
    private int categoryId;
    private String description;
    public final static Parcelable.Creator<LandSubCategory> CREATOR = new Creator<LandSubCategory>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LandSubCategory createFromParcel(Parcel in) {
            return new LandSubCategory(in);
        }

        public LandSubCategory[] newArray(int size) {
            return (new LandSubCategory[size]);
        }

    }
            ;

    protected LandSubCategory(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.categoryId = ((int) in.readValue((int.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public LandSubCategory() {
    }

    /**
     *
     * @param description
     * @param id
     * @param categoryId
     */
    public LandSubCategory(int id, int categoryId, String description) {
        super();
        this.id = id;
        this.categoryId = categoryId;
        this.description = description;
    }

    public LandSubCategory(int id, String description) {
        super();
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(categoryId);
        dest.writeValue(description);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "LandSubCategory{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", description='" + description + '\'' +
                '}';
    }
}