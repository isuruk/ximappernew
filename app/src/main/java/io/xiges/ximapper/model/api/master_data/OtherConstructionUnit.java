package io.xiges.ximapper.model.api.master_data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class OtherConstructionUnit implements Parcelable
{

    private String code;
    private String typeDescription;
    public final static Parcelable.Creator<OtherConstructionUnit> CREATOR = new Creator<OtherConstructionUnit>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OtherConstructionUnit createFromParcel(Parcel in) {
            return new OtherConstructionUnit(in);
        }

        public OtherConstructionUnit[] newArray(int size) {
            return (new OtherConstructionUnit[size]);
        }

    }
            ;

    protected OtherConstructionUnit(Parcel in) {
        this.code = ((String) in.readValue((String.class.getClassLoader())));
        this.typeDescription = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public OtherConstructionUnit() {
    }

    /**
     *
     * @param code
     * @param typeDescription
     */
    public OtherConstructionUnit(String code, String typeDescription) {
        super();
        this.code = code;
        this.typeDescription = typeDescription;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTypeDescription() {
        return typeDescription;
    }

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(code);
        dest.writeValue(typeDescription);
    }

    public int describeContents() {
        return 0;
    }

}