package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Lot implements Parcelable {
    private String lotId;
    private String lotName;
    @SerializedName("fillPoints")
    private List<Point> fillLayerPointList;
    @SerializedName("linePoints")
    private List<Point> lineLayerPointList;
    @SerializedName("circlePoints")
    private List<Feature> circleLayerFeatureList;
    private List<Construction> constructions;
    @SerializedName("lotLabels")
    private List<Symbol> symbolList;
    @SerializedName("landLabels")
    private List<Symbol> landLabels;


    public Lot(Parcel in) {
        lotId = in.readString();
        lotName = in.readString();
    }

    public static final Creator<Lot> CREATOR = new Creator<Lot>() {
        @Override
        public Lot createFromParcel(Parcel in) {
            return new Lot(in);
        }

        @Override
        public Lot[] newArray(int size) {
            return new Lot[size];
        }
    };

    public Lot() {

    }

    public String getLotId() {
        return  lotId.replaceAll("\\s+","");
    }

    public void setLotId(String lotId) {
        this.lotId = lotId.replaceAll("\\s+","");
    }

    public String getLotName() {
        return lotName.replaceAll("\\s+","");
    }

    public void setLotName(String lotName) {
        this.lotName =  lotName.replaceAll("\\s+","");
    }

    public List<Point> getFillLayerPointList() {
        return fillLayerPointList;
    }

    public void setFillLayerPointList(List<Point> fillLayerPointList) {
        this.fillLayerPointList = fillLayerPointList;
    }

    public List<Point> getLineLayerPointList() {
        return lineLayerPointList;
    }

    public void setLineLayerPointList(List<Point> lineLayerPointList) {
        this.lineLayerPointList = lineLayerPointList;
    }

    public List<Feature> getCircleLayerFeatureList() {
        return circleLayerFeatureList;
    }

    public void setCircleLayerFeatureList(List<Feature> circleLayerFeatureList) {
        this.circleLayerFeatureList = circleLayerFeatureList;
    }

    public List<Symbol> getSymbolList()
    {
        if(symbolList== null){
            symbolList = new ArrayList<>();
        }
        return symbolList;
    }

    public void setSymbolList(List<Symbol> symbolList) {
        this.symbolList = symbolList;
    }

    public List<Construction> getConstructions() {
        return constructions;
    }

    public void setConstructions(List<Construction> constructions) {
        this.constructions = constructions;
    }

    public static Creator<Lot> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(lotId);
        dest.writeString(lotName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lot lot = (Lot) o;
        return Objects.equals(lotId, lot.lotId);
    }

    public List<Symbol> getFloorLabels() {
        if(landLabels== null){
            landLabels = new ArrayList<>();
        }
        return landLabels;
    }

    public void setFloorLabels(List<Symbol> floorLabels) {
        this.landLabels = floorLabels;
    }

    @Override
    public int hashCode() {
        return Objects.hash(lotId);
    }

    @Override
    public String toString() {
        return "Lot{" +
                "lotId='" + lotId + '\'' +
                ", lotName='" + lotName + '\'' +
                ", fillLayerPointList=" + fillLayerPointList +
                ", lineLayerPointList=" + lineLayerPointList +
                ", circleLayerFeatureList=" + circleLayerFeatureList +
                ", constructions=" + constructions +
                ", symbolList=" + symbolList +
                ", landLabels=" + landLabels +
                '}';
    }
}
