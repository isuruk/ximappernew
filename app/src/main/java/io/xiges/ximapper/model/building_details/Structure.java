package io.xiges.ximapper.model.building_details;

import android.os.Parcel;
import android.os.Parcelable;

import io.xiges.ximapper.model.api.master_data.FloorStructure;
import io.xiges.ximapper.model.api.master_data.FoundationStructure;
import io.xiges.ximapper.model.api.master_data.WallStructure;

public class Structure implements Parcelable {
    private FoundationStructure foundationStructure;
    private WallStructure wallStructure;
    private FloorStructure floorStructure;



    public Structure() {

    }

    protected Structure(Parcel in) {
        foundationStructure = in.readParcelable(FoundationStructure.class.getClassLoader());
        wallStructure = in.readParcelable(WallStructure.class.getClassLoader());
        floorStructure = in.readParcelable(FloorStructure.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(foundationStructure, flags);
        dest.writeParcelable(wallStructure, flags);
        dest.writeParcelable(floorStructure, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Structure> CREATOR = new Creator<Structure>() {
        @Override
        public Structure createFromParcel(Parcel in) {
            return new Structure(in);
        }

        @Override
        public Structure[] newArray(int size) {
            return new Structure[size];
        }
    };

    public FoundationStructure getFoundationStructure() {
        return foundationStructure;
    }

    public void setFoundationStructure(FoundationStructure foundationStructure) {
        this.foundationStructure = foundationStructure;
    }

    public WallStructure getWallStructure() {
        return wallStructure;
    }

    public void setWallStructure(WallStructure wallStructure) {
        this.wallStructure = wallStructure;
    }

    public FloorStructure getFloorStructure() {
        return floorStructure;
    }

    public void setFloorStructure(FloorStructure floorStructure) {
        this.floorStructure = floorStructure;
    }
}
