package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import java.util.Objects;

public class MasterFile implements Parcelable{

    private String masterFileId;
    private String masterFileName;
    private String atNo;
    private String documentReferenceType;
    private String referenceNo;
    private String url;
    private int version;
    private List<MapLayer> layers;
    private boolean availability;
    private double bbox[];

    public MasterFile() {

    }


    protected MasterFile(Parcel in) {
        masterFileId = in.readString();
        masterFileName = in.readString();
        atNo = in.readString();
        documentReferenceType = in.readString();
        referenceNo = in.readString();
        url = in.readString();
        version = in.readInt();
        layers = in.createTypedArrayList(MapLayer.CREATOR);
        availability = in.readByte() != 0;
        bbox = in.createDoubleArray();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(masterFileId);
        dest.writeString(masterFileName);
        dest.writeString(atNo);
        dest.writeString(documentReferenceType);
        dest.writeString(referenceNo);
        dest.writeString(url);
        dest.writeInt(version);
        dest.writeTypedList(layers);
        dest.writeByte((byte) (availability ? 1 : 0));
        dest.writeDoubleArray(bbox);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MasterFile> CREATOR = new Creator<MasterFile>() {
        @Override
        public MasterFile createFromParcel(Parcel in) {
            return new MasterFile(in);
        }

        @Override
        public MasterFile[] newArray(int size) {
            return new MasterFile[size];
        }
    };

    public String getMasterFileName() {
        return masterFileName;
    }

    public void setMasterFileName(String masterFileName) {
        this.masterFileName = masterFileName;
    }

    public String getAtNo() {
        return atNo;
    }

    public void setAtNo(String atNo) {
        this.atNo = atNo;
    }

    public String getDocumentReferenceType() {
        return documentReferenceType;
    }

    public void setDocumentReferenceType(String documentReferenceType) {
        this.documentReferenceType = documentReferenceType;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<MapLayer> getLayers() {
        return layers;
    }

    public void setLayers(List<MapLayer> layers) {
        this.layers = layers;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MasterFile that = (MasterFile) o;
        return Objects.equals(referenceNo, that.referenceNo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(referenceNo);
    }

    public double[] getBbox() {
        return bbox;
    }

    public void setBbox(double[] bbox) {
        this.bbox = bbox;
    }

    public String bBoxToString(){
        if(bbox.length == 4){
            return bbox[0]+","+bbox[1]+","+bbox[2]+","+bbox[3];
        }else {
            return null;
        }
    }

    public String getMasterFileId() {
        return masterFileId;
    }

    public void setMasterFileId(String masterFileId) {
        this.masterFileId = masterFileId;
    }
}
