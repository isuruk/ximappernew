package io.xiges.ximapper.model.building_details;

import android.os.Parcel;
import android.os.Parcelable;

import io.xiges.ximapper.model.api.master_data.Ceiling;
import io.xiges.ximapper.model.api.master_data.RoofFinisher;
import io.xiges.ximapper.model.api.master_data.RoofFrame;
import io.xiges.ximapper.model.api.master_data.RoofMaterial;

public class Roof implements Parcelable {
    private RoofMaterial roofMaterial;
    private RoofFrame roofFrame;
    private RoofFinisher roofFinisher;
    private Ceiling ceiling;


    public Roof(Parcel in) {
        roofMaterial = in.readParcelable(RoofMaterial.class.getClassLoader());
        roofFrame = in.readParcelable(RoofFrame.class.getClassLoader());
        roofFinisher = in.readParcelable(RoofFinisher.class.getClassLoader());
        ceiling = in.readParcelable(Ceiling.class.getClassLoader());
    }

    public Roof() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(roofMaterial, flags);
        dest.writeParcelable(roofFrame, flags);
        dest.writeParcelable(roofFinisher, flags);
        dest.writeParcelable(ceiling, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Roof> CREATOR = new Creator<Roof>() {
        @Override
        public Roof createFromParcel(Parcel in) {
            return new Roof(in);
        }

        @Override
        public Roof[] newArray(int size) {
            return new Roof[size];
        }
    };

    public RoofMaterial getRoofMaterial() {
        return roofMaterial;
    }

    public void setRoofMaterial(RoofMaterial roofMaterial) {
        this.roofMaterial = roofMaterial;
    }

    public RoofFrame getRoofFrame() {
        return roofFrame;
    }

    public void setRoofFrame(RoofFrame roofFrame) {
        this.roofFrame = roofFrame;
    }

    public RoofFinisher getRoofFinisher() {
        return roofFinisher;
    }

    public void setRoofFinisher(RoofFinisher roofFinisher) {
        this.roofFinisher = roofFinisher;
    }

    public Ceiling getCeiling() {
        return ceiling;
    }

    public void setCeiling(Ceiling ceiling) {
        this.ceiling = ceiling;
    }
}
