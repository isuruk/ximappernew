
package io.xiges.ximapper.model.masterfile;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class VillageList implements Parcelable
{

    private String villageName;
    private int gnId;
    private String gnName;
    public final static Parcelable.Creator<VillageList> CREATOR = new Creator<VillageList>() {


        @SuppressWarnings({
            "unchecked"
        })
        public VillageList createFromParcel(Parcel in) {
            return new VillageList(in);
        }

        public VillageList[] newArray(int size) {
            return (new VillageList[size]);
        }

    }
    ;

    protected VillageList(Parcel in) {
        this.villageName = ((String) in.readValue((String.class.getClassLoader())));
        this.gnId = ((int) in.readValue((int.class.getClassLoader())));
        this.gnName = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public VillageList() {
    }

    /**
     * 
     * @param gnId
     * @param gnName
     * @param villageName
     */
    public VillageList(String villageName, int gnId, String gnName) {
        super();
        this.villageName = villageName;
        this.gnId = gnId;
        this.gnName = gnName;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    public int getGnId() {
        return gnId;
    }

    public void setGnId(int gnId) {
        this.gnId = gnId;
    }

    public String getGnName() {
        return gnName;
    }

    public void setGnName(String gnName) {
        this.gnName = gnName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(villageName);
        dest.writeValue(gnId);
        dest.writeValue(gnName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VillageList that = (VillageList) o;
        return Objects.equals(villageName, that.villageName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(villageName);
    }

    public int describeContents() {
        return  0;
    }

    @Override
    public String toString() {
        return "VillageList{" +
                "villageName='" + villageName + '\'' +
                ", gnId=" + gnId +
                ", gnName='" + gnName + '\'' +
                '}';
    }
}
