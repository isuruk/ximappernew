
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Access implements Parcelable
{

    private List<Integer> accessCategory = new ArrayList<Integer>();
    private String accessDetails;
    private String depthOfLength;
    private String descriptionOfLand;
    private String frontAge;
    private List<Integer> landUse = new ArrayList<Integer>();
    private String levelWithAccess;
    private String plantationDetails;
    private List<ProposedRate> proposedRates = new ArrayList<ProposedRate>();
    private String detailsOfBusiness;





    public Access() {
    }

    /**
     * 
     * @param plantationDetails
     * @param depthOfLength
     * @param levelWithAccess
     * @param proposedRates
     * @param accessDetails
     * @param descriptionOfLand
     * @param accessCategory
     * @param frontAge
     * @param landUse
     */
    public Access(List<Integer> accessCategory, String accessDetails, String depthOfLength, String descriptionOfLand, String frontAge, List<Integer> landUse, String levelWithAccess, String plantationDetails, List<ProposedRate> proposedRates) {
        super();
        this.accessCategory = accessCategory;
        this.accessDetails = accessDetails;
        this.depthOfLength = depthOfLength;
        this.descriptionOfLand = descriptionOfLand;
        this.frontAge = frontAge;
        this.landUse = landUse;
        this.levelWithAccess = levelWithAccess;
        this.plantationDetails = plantationDetails;
        this.proposedRates = proposedRates;
    }

    protected Access(Parcel in) {
        accessDetails = in.readString();
        depthOfLength = in.readString();
        descriptionOfLand = in.readString();
        frontAge = in.readString();
        levelWithAccess = in.readString();
        plantationDetails = in.readString();
        proposedRates = in.createTypedArrayList(ProposedRate.CREATOR);
        detailsOfBusiness = in.readString();
    }

    public static final Creator<Access> CREATOR = new Creator<Access>() {
        @Override
        public Access createFromParcel(Parcel in) {
            return new Access(in);
        }

        @Override
        public Access[] newArray(int size) {
            return new Access[size];
        }
    };

    public List<Integer> getAccessCategory() {
        if(accessCategory == null){
            accessCategory = new ArrayList<>();
        }
        return accessCategory;
    }

    public void setAccessCategory(List<Integer> accessCategory) {
        this.accessCategory = accessCategory;
    }

    public String getAccessDetails() {
        return accessDetails;
    }

    public void setAccessDetails(String accessDetails) {
        this.accessDetails = accessDetails;
    }

    public String getDepthOfLength() {
        return depthOfLength;
    }

    public void setDepthOfLength(String depthOfLength) {
        this.depthOfLength = depthOfLength;
    }

    public String getDescriptionOfLand() {
        return descriptionOfLand;
    }

    public void setDescriptionOfLand(String descriptionOfLand) {
        this.descriptionOfLand = descriptionOfLand;
    }

    public String getFrontAge() {
        return frontAge;
    }

    public void setFrontAge(String frontAge) {
        this.frontAge = frontAge;
    }

    public List<Integer> getLandUse() {
        if(landUse == null){
            landUse = new ArrayList<>();
        }
        return landUse;
    }

    public void setLandUse(List<Integer> landUse) {
        this.landUse = landUse;
    }

    public String getLevelWithAccess() {
        return levelWithAccess;
    }

    public void setLevelWithAccess(String levelWithAccess) {
        this.levelWithAccess = levelWithAccess;
    }

    public String getPlantationDetails() {
        return plantationDetails;
    }

    public void setPlantationDetails(String plantationDetails) {
        this.plantationDetails = plantationDetails;
    }

    public List<ProposedRate> getProposedRates() {
        if(proposedRates == null){
            proposedRates = new ArrayList<>();
        }
        return proposedRates;
    }

    public void setProposedRates(List<ProposedRate> proposedRates) {
        this.proposedRates = proposedRates;
    }

    public String getDetailsOfBusiness() {
        return detailsOfBusiness;
    }

    public void setDetailsOfBusiness(String detailsOfBusiness) {
        this.detailsOfBusiness = detailsOfBusiness;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(accessDetails);
        parcel.writeString(depthOfLength);
        parcel.writeString(descriptionOfLand);
        parcel.writeString(frontAge);
        parcel.writeString(levelWithAccess);
        parcel.writeString(plantationDetails);
        parcel.writeTypedList(proposedRates);
        parcel.writeString(detailsOfBusiness);
    }
}
