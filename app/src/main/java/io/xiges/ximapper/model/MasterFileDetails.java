package io.xiges.ximapper.model;

public class MasterFileDetails {
    private String masterFileId;

    public String getMasterFileId() {
        return masterFileId;
    }

    public void setMasterFileId(String masterFileId) {
        this.masterFileId = masterFileId;
    }
}
