
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class OwnerDetail implements Parcelable
{

    private String occupierId;
    private String occupierName;
    private int occupierType;
    private String ownerName;
    private String ownerNic;
    private String period;
    private String rent;
    private String tenantDetails;
    private String term;
    private String remark;
    private String sign;
    private String date;
    private String buildingNo;
    public final static Parcelable.Creator<OwnerDetail> CREATOR = new Creator<OwnerDetail>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OwnerDetail createFromParcel(Parcel in) {
            return new OwnerDetail(in);
        }

        public OwnerDetail[] newArray(int size) {
            return (new OwnerDetail[size]);
        }

    }
    ;

    protected OwnerDetail(Parcel in) {
        this.occupierId = ((String) in.readValue((String.class.getClassLoader())));
        this.occupierName = ((String) in.readValue((String.class.getClassLoader())));
        this.occupierType = ((int) in.readValue((int.class.getClassLoader())));
        this.ownerName = ((String) in.readValue((String.class.getClassLoader())));
        this.ownerNic = ((String) in.readValue((String.class.getClassLoader())));
        this.period = ((String) in.readValue((String.class.getClassLoader())));
        this.rent = ((String) in.readValue((String.class.getClassLoader())));
        this.tenantDetails = ((String) in.readValue((String.class.getClassLoader())));
        this.term = ((String) in.readValue((String.class.getClassLoader())));
        this.remark = ((String) in.readValue((String.class.getClassLoader())));
        this.sign = ((String) in.readValue((String.class.getClassLoader())));
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.buildingNo = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public OwnerDetail() {
    }

    public OwnerDetail(String occupierId, String occupierName, int occupierType, String ownerName, String ownerNic, String period, String rent, String tenantDetails, String term, String remark, String sign, String date, String buildingNo) {
        this.occupierId = occupierId;
        this.occupierName = occupierName;
        this.occupierType = occupierType;
        this.ownerName = ownerName;
        this.ownerNic = ownerNic;
        this.period = period;
        this.rent = rent;
        this.tenantDetails = tenantDetails;
        this.term = term;
        this.remark = remark;
        this.sign = sign;
        this.date = date;
        this.buildingNo = buildingNo;

    }

    public String getOccupierId() {
        return occupierId;
    }

    public void setOccupierId(String occupierId) {
        this.occupierId = occupierId;
    }

    public String getOccupierName() {
        return occupierName;
    }

    public void setOccupierName(String occupierName) {
        this.occupierName = occupierName;
    }

    public int getOccupierType() {
        return occupierType;
    }

    public void setOccupierType(int occupierType) {
        this.occupierType = occupierType;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerNic() {
        return ownerNic;
    }

    public void setOwnerNic(String ownerNic) {
        this.ownerNic = ownerNic;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getRent() {
        return rent;
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    public String getTenantDetails() {
        return tenantDetails;
    }

    public void setTenantDetails(String tenantDetails) {
        this.tenantDetails = tenantDetails;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getDate()
    {
        if(TextUtils.isEmpty(date) || date.equals("")) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            String d = sf.format(cal.getTime());
            date = d;
        }

        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(occupierId);
        dest.writeValue(occupierName);
        dest.writeValue(occupierType);
        dest.writeValue(ownerName);
        dest.writeValue(ownerNic);
        dest.writeValue(period);
        dest.writeValue(rent);
        dest.writeValue(tenantDetails);
        dest.writeValue(term);
        dest.writeValue(remark);
        dest.writeValue(sign);
        dest.writeValue(date);
        dest.writeValue(buildingNo);
    }

    public int describeContents() {
        return  0;
    }

    public String getBuildingNo() {
        return buildingNo;
    }

    public void setBuildingNo(String buildingNo) {
        this.buildingNo = buildingNo;
    }

    @Override
    public String toString() {
        return "OwnerDetail{" +
                "occupierId='" + occupierId + '\'' +
                ", occupierName='" + occupierName + '\'' +
                ", occupierType=" + occupierType +
                ", ownerName='" + ownerName + '\'' +
                ", ownerNic='" + ownerNic + '\'' +
                ", period='" + period + '\'' +
                ", rent='" + rent + '\'' +
                ", tenantDetails='" + tenantDetails + '\'' +
                ", term='" + term + '\'' +
                ", remark='" + remark + '\'' +
                ", sign='" + sign + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
