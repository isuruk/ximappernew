package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.xiges.ximapper.model.api.master_data.OtherConstructionUnit;
import io.xiges.ximapper.model.api.master_data.OtherConstructionsMaterial;
import io.xiges.ximapper.model.api.master_data.OtherConstructionsType;

public class CROtherConstruction implements Parcelable {
    private OtherConstructionsType constructionType;
    private String description;
    private OtherConstructionsMaterial otherConstructionsMaterial;
    private OtherConstructionUnit unit;
    private String remarks;
    private String extent;


    public CROtherConstruction() {

    }

    protected CROtherConstruction(Parcel in) {
        constructionType = in.readParcelable(OtherConstructionsType.class.getClassLoader());
        description = in.readString();
        otherConstructionsMaterial = in.readParcelable(OtherConstructionsMaterial.class.getClassLoader());
        unit = in.readParcelable(OtherConstructionUnit.class.getClassLoader());
        remarks = in.readString();
        extent = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(constructionType, flags);
        dest.writeString(description);
        dest.writeParcelable(otherConstructionsMaterial, flags);
        dest.writeParcelable(unit, flags);
        dest.writeString(remarks);
        dest.writeString(extent);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CROtherConstruction> CREATOR = new Creator<CROtherConstruction>() {
        @Override
        public CROtherConstruction createFromParcel(Parcel in) {
            return new CROtherConstruction(in);
        }

        @Override
        public CROtherConstruction[] newArray(int size) {
            return new CROtherConstruction[size];
        }
    };

    public OtherConstructionsType getConstructionType() {
        return constructionType;
    }

    public void setConstructionType(OtherConstructionsType constructionType) {
        this.constructionType = constructionType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OtherConstructionsMaterial getOtherConstructionsMaterial() {
        return otherConstructionsMaterial;
    }

    public void setOtherConstructionsMaterial(OtherConstructionsMaterial otherConstructionsMaterial) {
        this.otherConstructionsMaterial = otherConstructionsMaterial;
    }

    public OtherConstructionUnit getUnit() {
        return unit;
    }

    public void setUnit(OtherConstructionUnit unit) {
        this.unit = unit;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getExtent() {
        return extent;
    }

    public void setExtent(String extent) {
        this.extent = extent;
    }
}
