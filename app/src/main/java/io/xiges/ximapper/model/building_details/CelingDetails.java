package io.xiges.ximapper.model.building_details;

import android.os.Parcel;
import android.os.Parcelable;

public class CelingDetails implements Parcelable {
    private boolean flatAsbastors;
    private boolean plastic;
    private boolean learnToRoofTimber;
    private boolean learnToRoofAsbastors;
    private boolean flatTimber;
    private boolean gypsumBoard;
    private String celingRemarks;


    public CelingDetails(Parcel in) {
        flatAsbastors = in.readByte() != 0;
        plastic = in.readByte() != 0;
        learnToRoofTimber = in.readByte() != 0;
        learnToRoofAsbastors = in.readByte() != 0;
        flatTimber = in.readByte() != 0;
        gypsumBoard = in.readByte() != 0;
        celingRemarks = in.readString();
    }

    public static final Creator<CelingDetails> CREATOR = new Creator<CelingDetails>() {
        @Override
        public CelingDetails createFromParcel(Parcel in) {
            return new CelingDetails(in);
        }

        @Override
        public CelingDetails[] newArray(int size) {
            return new CelingDetails[size];
        }
    };

    public CelingDetails() {

    }

    public boolean isFlatAsbastors() {
        return flatAsbastors;
    }

    public void setFlatAsbastors(boolean flatAsbastors) {
        this.flatAsbastors = flatAsbastors;
    }

    public boolean isPlastic() {
        return plastic;
    }

    public void setPlastic(boolean plastic) {
        this.plastic = plastic;
    }

    public boolean isLearnToRoofTimber() {
        return learnToRoofTimber;
    }

    public void setLearnToRoofTimber(boolean learnToRoofTimber) {
        this.learnToRoofTimber = learnToRoofTimber;
    }

    public boolean isLearnToRoofAsbastors() {
        return learnToRoofAsbastors;
    }

    public void setLearnToRoofAsbastors(boolean learnToRoofAsbastors) {
        this.learnToRoofAsbastors = learnToRoofAsbastors;
    }

    public boolean isFlatTimber() {
        return flatTimber;
    }

    public void setFlatTimber(boolean flatTimber) {
        this.flatTimber = flatTimber;
    }

    public boolean isGypsumBoard() {
        return gypsumBoard;
    }

    public void setGypsumBoard(boolean gypsumBoard) {
        this.gypsumBoard = gypsumBoard;
    }

    public String getCelingRemarks() {
        return celingRemarks;
    }

    public void setCelingRemarks(String celingRemarks) {
        this.celingRemarks = celingRemarks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (flatAsbastors ? 1 : 0));
        dest.writeByte((byte) (plastic ? 1 : 0));
        dest.writeByte((byte) (learnToRoofTimber ? 1 : 0));
        dest.writeByte((byte) (learnToRoofAsbastors ? 1 : 0));
        dest.writeByte((byte) (flatTimber ? 1 : 0));
        dest.writeByte((byte) (gypsumBoard ? 1 : 0));
        dest.writeString(celingRemarks);
    }
}
