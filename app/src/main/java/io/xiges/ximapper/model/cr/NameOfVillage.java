
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class NameOfVillage implements Parcelable
{

    private int gnDivision;
    private String villageName;
    public final static Parcelable.Creator<NameOfVillage> CREATOR = new Creator<NameOfVillage>() {


        @SuppressWarnings({
            "unchecked"
        })
        public NameOfVillage createFromParcel(Parcel in) {
            return new NameOfVillage(in);
        }

        public NameOfVillage[] newArray(int size) {
            return (new NameOfVillage[size]);
        }

    }
    ;

    protected NameOfVillage(Parcel in) {
        this.gnDivision = ((int) in.readValue((int.class.getClassLoader())));
        this.villageName = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public NameOfVillage() {
    }

    /**
     * 
     * @param villageName
     * @param gnDivision
     */
    public NameOfVillage(int gnDivision, String villageName) {
        super();
        this.gnDivision = gnDivision;
        this.villageName = villageName;
    }

    public int getGnDivision() {
        return gnDivision;
    }

    public void setGnDivision(int gnDivision) {
        this.gnDivision = gnDivision;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(gnDivision);
        dest.writeValue(villageName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NameOfVillage that = (NameOfVillage) o;
        return Objects.equals(villageName, that.villageName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(villageName);
    }

    public int describeContents() {
        return  0;
    }

    @Override
    public String toString() {
        return "NameOfVillage{" +
                "gnDivision=" + gnDivision +
                ", villageName='" + villageName + '\'' +
                '}';
    }
}
