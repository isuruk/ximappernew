package io.xiges.ximapper.model.api.master_data;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Structure implements Parcelable
{

    private List<FoundationStructure> foundationStructure = null;
    private List<WallStructure> wallStructure = null;
    private List<FloorStructure> floorStructure = null;
    public final static Parcelable.Creator<Structure> CREATOR = new Creator<Structure>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Structure createFromParcel(Parcel in) {
            return new Structure(in);
        }

        public Structure[] newArray(int size) {
            return (new Structure[size]);
        }

    }
            ;

    protected Structure(Parcel in) {
        in.readList(this.foundationStructure, (FoundationStructure.class.getClassLoader()));
        in.readList(this.wallStructure, (WallStructure.class.getClassLoader()));
        in.readList(this.floorStructure, (FloorStructure.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Structure() {
    }

    /**
     *
     * @param foundationStructure
     * @param wallStructure
     * @param floorStructure
     */
    public Structure(List<FoundationStructure> foundationStructure, List<WallStructure> wallStructure, List<FloorStructure> floorStructure) {
        super();
        this.foundationStructure = foundationStructure;
        this.wallStructure = wallStructure;
        this.floorStructure = floorStructure;
    }

    public List<FoundationStructure> getFoundationStructure() {
        return foundationStructure;
    }

    public void setFoundationStructure(List<FoundationStructure> foundationStructure) {
        this.foundationStructure = foundationStructure;
    }

    public List<WallStructure> getWallStructure() {
        return wallStructure;
    }

    public void setWallStructure(List<WallStructure> wallStructure) {
        this.wallStructure = wallStructure;
    }

    public List<FloorStructure> getFloorStructure() {
        return floorStructure;
    }

    public void setFloorStructure(List<FloorStructure> floorStructure) {
        this.floorStructure = floorStructure;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(foundationStructure);
        dest.writeList(wallStructure);
        dest.writeList(floorStructure);
    }

    public int describeContents() {
        return 0;
    }

}