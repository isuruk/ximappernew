
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;

public class ProposedRate implements Parcelable
{

    private String extent;
    private int landCategory;
    private int landSubCategory;
    private String remark;
    public final static Parcelable.Creator<ProposedRate> CREATOR = new Creator<ProposedRate>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ProposedRate createFromParcel(Parcel in) {
            return new ProposedRate(in);
        }

        public ProposedRate[] newArray(int size) {
            return (new ProposedRate[size]);
        }

    }
    ;

    protected ProposedRate(Parcel in) {
        this.extent = ((String) in.readValue((String.class.getClassLoader())));
        this.landCategory = ((int) in.readValue((int.class.getClassLoader())));
        this.landSubCategory = ((int) in.readValue((int.class.getClassLoader())));
        this.remark = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public ProposedRate() {
    }

    /**
     * 
     * @param extent
     * @param landSubCategory
     * @param landCategory
     * @param remark
     */
    public ProposedRate(String extent, int landCategory, int landSubCategory, String remark) {
        super();
        this.extent = extent;
        this.landCategory = landCategory;
        this.landSubCategory = landSubCategory;
        this.remark = remark;
    }

    public String getExtent() {
        return extent;
    }

    public void setExtent(String extent) {
        this.extent = extent;
    }

    public int getLandCategory() {
        return landCategory;
    }

    public void setLandCategory(int landCategory) {
        this.landCategory = landCategory;
    }

    public int getLandSubCategory() {
        return landSubCategory;
    }

    public void setLandSubCategory(int landSubCategory) {
        this.landSubCategory = landSubCategory;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(extent);
        dest.writeValue(landCategory);
        dest.writeValue(landSubCategory);
        dest.writeValue(remark);
    }

    @Override
    public String toString() {
        return "ProposedRate{" +
                "extent='" + extent + '\'' +
                ", landCategory=" + landCategory +
                ", landSubCategory=" + landSubCategory +
                ", remark='" + remark + '\'' +
                '}';
    }

    public int describeContents() {
        return  0;
    }

}
