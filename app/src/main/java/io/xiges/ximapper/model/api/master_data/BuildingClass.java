package io.xiges.ximapper.model.api.master_data;

import android.os.Parcel;
import android.os.Parcelable;

public class BuildingClass implements Parcelable {

    private int id;
    private String buildingClass;

    public BuildingClass(){

    }


    public BuildingClass(int id, String buildingClass) {
        this.id = id;
        this.buildingClass = buildingClass;
    }

    public BuildingClass(Parcel in) {
        id = in.readInt();
        buildingClass = in.readString();
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(buildingClass);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BuildingClass> CREATOR = new Creator<BuildingClass>() {
        @Override
        public BuildingClass createFromParcel(Parcel in) {
            return new BuildingClass(in);
        }

        @Override
        public BuildingClass[] newArray(int size) {
            return new BuildingClass[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBuildingClass() {
        return buildingClass;
    }

    public void setBuildingClass(String buildingClass) {
        this.buildingClass = buildingClass;
    }
}