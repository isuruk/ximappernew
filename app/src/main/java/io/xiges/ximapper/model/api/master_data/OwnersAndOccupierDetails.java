package io.xiges.ximapper.model.api.master_data;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class OwnersAndOccupierDetails implements Parcelable
{

    private List<OccupierType> occupierType = null;
    public final static Parcelable.Creator<OwnersAndOccupierDetails> CREATOR = new Creator<OwnersAndOccupierDetails>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OwnersAndOccupierDetails createFromParcel(Parcel in) {
            return new OwnersAndOccupierDetails(in);
        }

        public OwnersAndOccupierDetails[] newArray(int size) {
            return (new OwnersAndOccupierDetails[size]);
        }

    }
            ;

    protected OwnersAndOccupierDetails(Parcel in) {
        in.readList(this.occupierType, (OccupierType.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public OwnersAndOccupierDetails() {
    }

    /**
     *
     * @param occupierType
     */
    public OwnersAndOccupierDetails(List<OccupierType> occupierType) {
        super();
        this.occupierType = occupierType;
    }

    public List<OccupierType> getOccupierType() {
        return occupierType;
    }

    public void setOccupierType(List<OccupierType> occupierType) {
        this.occupierType = occupierType;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(occupierType);
    }

    public int describeContents() {
        return 0;
    }

}