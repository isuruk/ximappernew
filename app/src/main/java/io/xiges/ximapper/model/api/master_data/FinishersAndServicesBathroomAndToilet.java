package io.xiges.ximapper.model.api.master_data;

import java.util.HashMap;
import java.util.Map;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class FinishersAndServicesBathroomAndToilet implements Parcelable
{

    private int id;
    private String description;
    public final static Parcelable.Creator<FinishersAndServicesBathroomAndToilet> CREATOR = new Creator<FinishersAndServicesBathroomAndToilet>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FinishersAndServicesBathroomAndToilet createFromParcel(Parcel in) {
            return new FinishersAndServicesBathroomAndToilet(in);
        }

        public FinishersAndServicesBathroomAndToilet[] newArray(int size) {
            return (new FinishersAndServicesBathroomAndToilet[size]);
        }

    }
            ;

    protected FinishersAndServicesBathroomAndToilet(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public FinishersAndServicesBathroomAndToilet() {
    }

    /**
     *
     * @param description
     * @param id
     */
    public FinishersAndServicesBathroomAndToilet(int id, String description) {
        super();
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(description);
    }

    public int describeContents() {
        return 0;
    }

}