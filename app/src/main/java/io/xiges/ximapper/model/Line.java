package io.xiges.ximapper.model;

import android.graphics.Point;

import java.util.List;

public class Line {
    private List<Point> lineList;

    public List<Point> getLineList() {
        return lineList;
    }

    public void setLineList(List<Point> lineList) {
        this.lineList = lineList;
    }
}
