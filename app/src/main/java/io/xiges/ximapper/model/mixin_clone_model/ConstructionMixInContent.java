package io.xiges.ximapper.model.mixin_clone_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.xiges.ximapper.model.Symbol;

public class ConstructionMixInContent implements Parcelable {
    private String id  ;
    private String name;
    private String type;
    private String drawingType;
    @SerializedName("fillPoints")
    private List<PointMixinContent> fillLayerPointList = new ArrayList<>();
    @SerializedName("linePoints")
    private List<PointMixinContent> lineLayerPointList = new ArrayList<>();
    @SerializedName("circlePoints")
    private List<FeatureMixinContent> circleLayerFeatureList = new ArrayList<>();
    private String fillLayerID;
    private String lineLayerID;
    private String circleLayerID;
    private String fillSourceID;
    private String lineSourceID;
    private String circleSourceID;
    @SerializedName("floorDetails")
    private List<BuildingFloorMixinContent> buildingFloorList = new ArrayList<>();
    @SerializedName("circle_drawing")
    private CircleMixinContent circle;
    @SerializedName("buildingLabels")
    private List<Symbol> buildingLabels;
    public ConstructionMixInContent() {
    }


    protected ConstructionMixInContent(Parcel in) {
        id = in.readString();
        name = in.readString();
        type = in.readString();
        drawingType = in.readString();
        fillLayerPointList = in.createTypedArrayList(PointMixinContent.CREATOR);
        lineLayerPointList = in.createTypedArrayList(PointMixinContent.CREATOR);
        circleLayerFeatureList = in.createTypedArrayList(FeatureMixinContent.CREATOR);
        fillLayerID = in.readString();
        lineLayerID = in.readString();
        circleLayerID = in.readString();
        fillSourceID = in.readString();
        lineSourceID = in.readString();
        circleSourceID = in.readString();
        buildingFloorList = in.createTypedArrayList(BuildingFloorMixinContent.CREATOR);
        circle = in.readParcelable(CircleMixinContent.class.getClassLoader());
        buildingLabels = in.createTypedArrayList(Symbol.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(type);
        dest.writeString(drawingType);
        dest.writeTypedList(fillLayerPointList);
        dest.writeTypedList(lineLayerPointList);
        dest.writeTypedList(circleLayerFeatureList);
        dest.writeString(fillLayerID);
        dest.writeString(lineLayerID);
        dest.writeString(circleLayerID);
        dest.writeString(fillSourceID);
        dest.writeString(lineSourceID);
        dest.writeString(circleSourceID);
        dest.writeTypedList(buildingFloorList);
        dest.writeParcelable(circle, flags);
        dest.writeTypedList(buildingLabels);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConstructionMixInContent> CREATOR = new Creator<ConstructionMixInContent>() {
        @Override
        public ConstructionMixInContent createFromParcel(Parcel in) {
            return new ConstructionMixInContent(in);
        }

        @Override
        public ConstructionMixInContent[] newArray(int size) {
            return new ConstructionMixInContent[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDrawingType() {
        return drawingType;
    }

    public void setDrawingType(String drawingType) {
        this.drawingType = drawingType;
    }

    public List<PointMixinContent> getFillLayerPointList() {
        return fillLayerPointList;
    }

    public void setFillLayerPointList(List<PointMixinContent> fillLayerPointList) {
        this.fillLayerPointList = fillLayerPointList;
    }

    public List<PointMixinContent> getLineLayerPointList() {
        return lineLayerPointList;
    }

    public void setLineLayerPointList(List<PointMixinContent> lineLayerPointList) {
        this.lineLayerPointList = lineLayerPointList;
    }

    public List<FeatureMixinContent> getCircleLayerFeatureList() {
        return circleLayerFeatureList;
    }

    public void setCircleLayerFeatureList(List<FeatureMixinContent> circleLayerFeatureList) {
        this.circleLayerFeatureList = circleLayerFeatureList;
    }

    public String getFillLayerID() {
        return fillLayerID;
    }

    public void setFillLayerID(String fillLayerID) {
        this.fillLayerID = fillLayerID;
    }

    public String getLineLayerID() {
        return lineLayerID;
    }

    public void setLineLayerID(String lineLayerID) {
        this.lineLayerID = lineLayerID;
    }

    public String getCircleLayerID() {
        return circleLayerID;
    }

    public void setCircleLayerID(String circleLayerID) {
        this.circleLayerID = circleLayerID;
    }

    public String getFillSourceID() {
        return fillSourceID;
    }

    public void setFillSourceID(String fillSourceID) {
        this.fillSourceID = fillSourceID;
    }

    public String getLineSourceID() {
        return lineSourceID;
    }

    public void setLineSourceID(String lineSourceID) {
        this.lineSourceID = lineSourceID;
    }

    public String getCircleSourceID() {
        return circleSourceID;
    }

    public void setCircleSourceID(String circleSourceID) {
        this.circleSourceID = circleSourceID;
    }

    public List<BuildingFloorMixinContent> getBuildingFloorList() {
        return buildingFloorList;
    }

    public void setBuildingFloorList(List<BuildingFloorMixinContent> buildingFloorList) {
        this.buildingFloorList = buildingFloorList;
    }
    public CircleMixinContent getCircle() {
        return circle;
    }

    public void setCircle(CircleMixinContent circle) {
        this.circle = circle;
    }

    public List<Symbol> getBuildingLabels() {
        return buildingLabels;
    }


    public void setBuildingLabels(List<Symbol> buildingLabels) {
        this.buildingLabels = buildingLabels;
    }
}
