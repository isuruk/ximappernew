package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BundleInfo implements Parcelable {
    private String fileName;
    private String Directory;
    private boolean isMasterFile;
    private String masterFilePath;
    private boolean isATFile;
    private String atFilePath;
    private boolean isCadesterFile;
    private String cadesterFilePath;
    private boolean isPPFile;
    private String ppFilePath;


    public BundleInfo(Parcel in) {
        fileName = in.readString();
        Directory = in.readString();
        isMasterFile = in.readByte() != 0;
        masterFilePath = in.readString();
        isATFile = in.readByte() != 0;
        atFilePath = in.readString();
        isCadesterFile = in.readByte() != 0;
        cadesterFilePath = in.readString();
        isPPFile = in.readByte() != 0;
        ppFilePath = in.readString();
    }

    public static final Creator<BundleInfo> CREATOR = new Creator<BundleInfo>() {
        @Override
        public BundleInfo createFromParcel(Parcel in) {
            return new BundleInfo(in);
        }

        @Override
        public BundleInfo[] newArray(int size) {
            return new BundleInfo[size];
        }
    };

    public BundleInfo() {

    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDirectory() {
        return Directory;
    }

    public void setDirectory(String directory) {
        Directory = directory;
    }

    public boolean isMasterFile() {
        return isMasterFile;
    }

    public void setMasterFile(boolean masterFile) {
        isMasterFile = masterFile;
    }

    public boolean isATFile() {
        return isATFile;
    }

    public void setATFile(boolean ATFile) {
        isATFile = ATFile;
    }

    public boolean isCadesterFile() {
        return isCadesterFile;
    }

    public void setCadesterFile(boolean cadesterFile) {
        isCadesterFile = cadesterFile;
    }

    public boolean isPPFile() {
        return isPPFile;
    }

    public void setPPFile(boolean PPFile) {
        isPPFile = PPFile;
    }

    public String getMasterFilePath() {
        return masterFilePath;
    }

    public void setMasterFilePath(String masterFilePath) {
        this.masterFilePath = masterFilePath;
    }

    public String getAtFilePath() {
        return atFilePath;
    }

    public void setAtFilePath(String atFilePath) {
        this.atFilePath = atFilePath;
    }

    public String getCadesterFilePath() {
        return cadesterFilePath;
    }

    public void setCadesterFilePath(String cadesterFilePath) {
        this.cadesterFilePath = cadesterFilePath;
    }

    public String getPpFilePath() {
        return ppFilePath;
    }

    public void setPpFilePath(String ppFilePath) {
        this.ppFilePath = ppFilePath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fileName);
        dest.writeString(Directory);
        dest.writeByte((byte) (isMasterFile ? 1 : 0));
        dest.writeString(masterFilePath);
        dest.writeByte((byte) (isATFile ? 1 : 0));
        dest.writeString(atFilePath);
        dest.writeByte((byte) (isCadesterFile ? 1 : 0));
        dest.writeString(cadesterFilePath);
        dest.writeByte((byte) (isPPFile ? 1 : 0));
        dest.writeString(ppFilePath);
    }
}
