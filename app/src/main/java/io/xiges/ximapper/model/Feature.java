package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Feature implements Parcelable
{

    private String type;
    private Geometry geometry;
    public final static Parcelable.Creator<Feature> CREATOR = new Creator<Feature>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Feature createFromParcel(Parcel in) {
            return new Feature(in);
        }

        public Feature[] newArray(int size) {
            return (new Feature[size]);
        }

    }
            ;

    protected Feature(Parcel in) {
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.geometry = ((Geometry) in.readValue((Geometry.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Feature() {
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(type);
        dest.writeValue(geometry);
    }

    public int describeContents() {
        return 0;
    }

}