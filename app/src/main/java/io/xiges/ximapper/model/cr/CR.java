
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class CR implements Parcelable
{

    private Access access;
    private BuildingDetails buildingDetails;
    private General general;
    private List<OtherConstruction> otherConstructions = new ArrayList<OtherConstruction>();
    private Signatures signatures;
    public final static Parcelable.Creator<CR> CREATOR = new Creator<CR>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CR createFromParcel(Parcel in) {
            return new CR(in);
        }

        public CR[] newArray(int size) {
            return (new CR[size]);
        }

    }
    ;

    protected CR(Parcel in) {
        this.access = ((Access) in.readValue((Access.class.getClassLoader())));
        this.buildingDetails = ((BuildingDetails) in.readValue((BuildingDetails.class.getClassLoader())));
        this.general = ((General) in.readValue((General.class.getClassLoader())));
        in.readList(this.otherConstructions, (io.xiges.ximapper.model.cr.OtherConstruction.class.getClassLoader()));
        this.signatures = ((Signatures) in.readValue((Signatures.class.getClassLoader())));

    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public CR() {
    }

    /**
     * 
     * @param general
     * @param access
     * @param otherConstructions
     * @param buildingDetails
     * @param signatures
     */
    public CR(Access access, BuildingDetails buildingDetails, General general, List<OtherConstruction> otherConstructions, Signatures signatures,List<OwnerDetail> ownerDetails) {
        super();
        this.access = access;
        this.buildingDetails = buildingDetails;
        this.general = general;
        this.otherConstructions = otherConstructions;
        this.signatures = signatures;
    }

    public Access getAccess() {
        if(access==null){
            access = new Access();
        }
        return access;
    }

    public void setAccess(Access access) {
        this.access = access;
    }

    public BuildingDetails getBuildingDetails() {
        if(buildingDetails == null){
            buildingDetails = new BuildingDetails();
        }
        return buildingDetails;
    }

    public void setBuildingDetails(BuildingDetails buildingDetails) {
        this.buildingDetails = buildingDetails;
    }

    public General getGeneral() {
        if(general==null){
            general = new General();
        }
        return general;
    }

    public void setGeneral(General general) {
        this.general = general;
    }

    public List<OtherConstruction> getOtherConstructions() {
        if(otherConstructions == null){
            otherConstructions =new ArrayList<>();
        }
        return otherConstructions;
    }

    public void setOtherConstructions(List<OtherConstruction> otherConstructions) {
        this.otherConstructions = otherConstructions;
    }



    public Signatures getSignatures() {
        if(signatures == null){
            signatures = new Signatures();
        }
        return signatures;
    }

    public void setSignatures(Signatures signatures) {
        this.signatures = signatures;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(access);
        dest.writeValue(buildingDetails);
        dest.writeValue(general);
        dest.writeList(otherConstructions);
        dest.writeValue(signatures);
    }

    public int describeContents() {
        return  0;
    }


}
