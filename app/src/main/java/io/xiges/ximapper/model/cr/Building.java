package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Building implements Parcelable {

    private int acquiredPotion = -1;
    private String age;
    private int buildingAcquiredPortion = -1;
    private int buildingCategory = -1;
    private int buildingClass = -1;
    private int buildingId =-1;
    private int buildingNatureOfConstruction = -1;
    private FinishersAndServices finishersAndServices;
    private FixtureAndFitting fixtureAndFitting;
    private int floors;
    private int minusFloors;
    private String expectedLifePeriod;
    private List<Integer> floorStructure = new ArrayList<>();
    private String no;
    private String name;
    private Roof roof;
    private Structure structure;
    private String totalFloorArea;
    private String type;
    private int typeOfCondition;
    private List<OwnerDetail> ownerDetails;


    public Building() {
    }



    public int getAcquiredPotion() {
        return acquiredPotion;
    }

    public void setAcquiredPotion(int acquiredPotion) {
        this.acquiredPotion = acquiredPotion;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public int getBuildingAcquiredPortion() {
        return buildingAcquiredPortion;
    }

    public void setBuildingAcquiredPortion(int buildingAcquiredPortion) {
        this.buildingAcquiredPortion = buildingAcquiredPortion;
    }

    public int getBuildingCategory() {
        return buildingCategory;
    }

    public void setBuildingCategory(int buildingCategory) {
        this.buildingCategory = buildingCategory;
    }

    public int getBuildingClass() {
        return buildingClass;
    }

    public void setBuildingClass(int buildingClass) {
        this.buildingClass = buildingClass;
    }

    public int getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(int buildingId) {
        this.buildingId = buildingId;
    }

    public int getBuildingNatureOfConstruction() {
        return buildingNatureOfConstruction;
    }

    public void setBuildingNatureOfConstruction(int buildingNatureOfConstruction) {
        this.buildingNatureOfConstruction = buildingNatureOfConstruction;
    }

    public FinishersAndServices getFinishersAndServices()

    {
        if (finishersAndServices == null){
            finishersAndServices = new FinishersAndServices();
        }
        return finishersAndServices;
    }

    public void setFinishersAndServices(FinishersAndServices finishersAndServices) {
        this.finishersAndServices = finishersAndServices;
    }

    public FixtureAndFitting getFixtureAndFitting()

    {
        if (fixtureAndFitting == null){
            fixtureAndFitting = new FixtureAndFitting();
        }
        return fixtureAndFitting;
    }

    public void setFixtureAndFitting(FixtureAndFitting fixtureAndFitting) {
        this.fixtureAndFitting = fixtureAndFitting;
    }

    public int getFloors() {
        return floors;
    }

    public void setFloors(int floors) {
        this.floors = floors;
    }

    public List<Integer> getFloorStructure() {
        if(floorStructure == null){
            floorStructure = new ArrayList<>();
        }
        return floorStructure;
    }

    public void setFloorStructure(List<Integer> floorStructure) {
        this.floorStructure = floorStructure;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Roof getRoof() {
        if(roof == null){
            roof = new Roof();
        }

        return roof;
    }

    public void setRoof(Roof roof) {
        this.roof = roof;
    }

    public Structure getStructure() {
        if (structure == null){
            structure = new Structure();
        }
        return structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    public String getTotalFloorArea() {
        return totalFloorArea;
    }

    public void setTotalFloorArea(String totalFloorArea) {
        this.totalFloorArea = totalFloorArea;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTypeOfCondition() {
        return typeOfCondition;
    }

    public void setTypeOfCondition(int typeOfCondition) {
        this.typeOfCondition = typeOfCondition;
    }



    public int getMinusFloors() {
        return minusFloors;
    }

    public void setMinusFloors(int minusFloors) {
        this.minusFloors = minusFloors;
    }

    public String getExpectedLifePeriod() {
        return expectedLifePeriod;
    }

    public void setExpectedLifePeriod(String expectedLifePeriod) {
        this.expectedLifePeriod = expectedLifePeriod;
    }

    public int describeContents() {
        return  0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Building building = (Building) o;
        return no.equals(building.no);
    }

    @Override
    public int hashCode() {
        return Objects.hash(no);
    }
    public List<OwnerDetail> getOwnerDetails() {
        if(ownerDetails == null){
            ownerDetails =new ArrayList<>();
        }
        return ownerDetails;
    }

    public void setOwnerDetails(List<OwnerDetail> ownerDetails) {
        this.ownerDetails = ownerDetails;
    }

    @Override
    public String toString() {
        return "Building{" +
                "acquiredPotion=" + acquiredPotion +
                ", age='" + age + '\'' +
                ", buildingAcquiredPortion=" + buildingAcquiredPortion +
                ", buildingCategory=" + buildingCategory +
                ", buildingClass=" + buildingClass +
                ", buildingId=" + buildingId +
                ", buildingNatureOfConstruction=" + buildingNatureOfConstruction +
                ", finishersAndServices=" + finishersAndServices +
                ", fixtureAndFitting=" + fixtureAndFitting +
                ", floors=" + floors +
                ", minusFloors=" + minusFloors +
                ", expectedLifePeriod='" + expectedLifePeriod + '\'' +
                ", floorStructure=" + floorStructure +
                ", no='" + no + '\'' +
                ", roof=" + roof +
                ", structure=" + structure +
                ", totalFloorArea='" + totalFloorArea + '\'' +
                ", type='" + type + '\'' +
                ", typeOfCondition=" + typeOfCondition +
                ", ownerDetails=" + ownerDetails +
                '}';
    }

    protected Building(Parcel in) {
        acquiredPotion = in.readInt();
        age = in.readString();
        buildingAcquiredPortion = in.readInt();
        buildingCategory = in.readInt();
        buildingClass = in.readInt();
        buildingId = in.readInt();
        buildingNatureOfConstruction = in.readInt();
        finishersAndServices = (FinishersAndServices) in.readValue(FinishersAndServices.class.getClassLoader());
        fixtureAndFitting = (FixtureAndFitting) in.readValue(FixtureAndFitting.class.getClassLoader());
        floors = in.readInt();
        minusFloors = in.readInt();
        expectedLifePeriod = in.readString();
        if (in.readByte() == 0x01) {
            floorStructure = new ArrayList<Integer>();
            in.readList(floorStructure, Integer.class.getClassLoader());
        } else {
            floorStructure = null;
        }
        no = in.readString();
        roof = (Roof) in.readValue(Roof.class.getClassLoader());
        structure = (Structure) in.readValue(Structure.class.getClassLoader());
        totalFloorArea = in.readString();
        type = in.readString();
        typeOfCondition = in.readInt();
        if (in.readByte() == 0x01) {
            ownerDetails = new ArrayList<OwnerDetail>();
            in.readList(ownerDetails, OwnerDetail.class.getClassLoader());
        } else {
            ownerDetails = null;
        }
        name = in.readString();

    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(acquiredPotion);
        dest.writeString(age);
        dest.writeInt(buildingAcquiredPortion);
        dest.writeInt(buildingCategory);
        dest.writeInt(buildingClass);
        dest.writeInt(buildingId);
        dest.writeInt(buildingNatureOfConstruction);
        dest.writeValue(finishersAndServices);
        dest.writeValue(fixtureAndFitting);
        dest.writeInt(floors);
        dest.writeInt(minusFloors);
        dest.writeString(expectedLifePeriod);
        if (floorStructure == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(floorStructure);
        }
        dest.writeString(no);
        dest.writeValue(roof);
        dest.writeValue(structure);
        dest.writeString(totalFloorArea);
        dest.writeString(type);
        dest.writeInt(typeOfCondition);
        if (ownerDetails == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(ownerDetails);
        }
        dest.writeString(name);

    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Building> CREATOR = new Parcelable.Creator<Building>() {
        @Override
        public Building createFromParcel(Parcel in) {
            return new Building(in);
        }

        @Override
        public Building[] newArray(int size) {
            return new Building[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}