package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;

import java.util.ArrayList;
import java.util.List;

public class Circle implements Parcelable {
    @SerializedName("circleFillPoints")
    private List<Feature> circleFillPoints = new ArrayList<>();
    @SerializedName("circleMidPoint")
    private Point midPoint;
    @SerializedName("circleRadius")
    private double radius;
    @SerializedName("circleUnit")
    private String unit;

    public Circle(Parcel in) {
        radius = in.readDouble();
        unit = in.readString();
    }

    public static final Creator<Circle> CREATOR = new Creator<Circle>() {
        @Override
        public Circle createFromParcel(Parcel in) {
            return new Circle(in);
        }

        @Override
        public Circle[] newArray(int size) {
            return new Circle[size];
        }
    };

    public Circle() {

    }

    public List<Feature> getCircleFillPoints() {
        return circleFillPoints;
    }

    public void setCircleFillPoints(List<Feature> circleFillPoints) {
        this.circleFillPoints = circleFillPoints;
    }

    public Point getMidPoint() {
        return midPoint;
    }

    public void setMidPoint(Point midPoint) {
        this.midPoint = midPoint;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(radius);
        dest.writeString(unit);
    }
}
