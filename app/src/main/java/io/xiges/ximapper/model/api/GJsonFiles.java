package io.xiges.ximapper.model.api;

public class GJsonFiles {
    private String fileType;
    private String fileData;

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileData() {
        return fileData;
    }

    public void setFileData(String fileData) {
        this.fileData = fileData;
    }


}
