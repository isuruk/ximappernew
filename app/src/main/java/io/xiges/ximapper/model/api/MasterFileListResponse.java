package io.xiges.ximapper.model.api;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.xiges.ximapper.model.LayerType;
import io.xiges.ximapper.model.MapLayer;

public class MasterFileListResponse implements Parcelable {
    private int masterFileId;
    private String masterFileRefNumber;
    private String fileType;
    private String fileNumber;
    private String atNumber;
    private boolean availability;
    private String masterFileName;
    private List<MapLayer> layers;
    private  String checksum;


    public MasterFileListResponse(Parcel in) {
        masterFileId = in.readInt();
        masterFileRefNumber = in.readString();
        fileType = in.readString();
        fileNumber = in.readString();
        atNumber = in.readString();
        checksum = in.readString();
        layers = in.createTypedArrayList(MapLayer.CREATOR);
        availability = in.readByte() != 0;
    }

    public static final Creator<MasterFileListResponse> CREATOR = new Creator<MasterFileListResponse>() {
        @Override
        public MasterFileListResponse createFromParcel(Parcel in) {
            return new MasterFileListResponse(in);
        }

        @Override
        public MasterFileListResponse[] newArray(int size) {
            return new MasterFileListResponse[size];
        }
    };

    public MasterFileListResponse() {

    }

    public int getMasterFileId() {
        return masterFileId;
    }

    public void setMasterFileId(int masterFileId) {
        this.masterFileId = masterFileId;
    }

    public String getMasterFileRefNumber() {
        return masterFileRefNumber;
        // return "GAMLA10085";

    }

    public void setMasterFileRefNumber(String masterFileRefNumber) {
        this.masterFileRefNumber = masterFileRefNumber;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileNumber() {
        return fileNumber;
    }

    public void setFileNumber(String fileNumber) {
        this.fileNumber = fileNumber;
    }

    public String getAtNumber() {
        return atNumber;
    }

    public void setAtNumber(String atNumber) {
        this.atNumber = atNumber;
    }



    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(masterFileId);
        parcel.writeString(masterFileRefNumber);
        parcel.writeString(fileType);
        parcel.writeString(fileNumber);
        parcel.writeString(atNumber);
        parcel.writeString(checksum);
        parcel.writeTypedList(layers);
        parcel.writeByte((byte) (availability ? 1 : 0));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MasterFileListResponse that = (MasterFileListResponse) o;
        return masterFileId == that.masterFileId &&
                checksum.equals(that.checksum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(masterFileId, checksum);
    }

    public String getMasterFileName() {
        return getMasterFileRefNumber().replaceAll("/","");
       // return "GAMLA10085";
    }

    public List<MapLayer> getLayers() {
        List<MapLayer>  l = new ArrayList<>();
        MapLayer mapLayer1 = new MapLayer();
        mapLayer1.setFileName("linekml.geojson");
        mapLayer1.setLayerType(LayerType.AT);

        MapLayer mapLayer2 = new MapLayer();
        mapLayer2.setFileName("labels.geojson");
        mapLayer2.setLayerType(LayerType.AT_LABELS);
        MapLayer mapLayer3 = null;

        MapLayer mapLayer4 = null;

        MapLayer mapLayer5 = null;
        l.add(mapLayer1);
        l.add(mapLayer2);
        l.add(mapLayer3);
        l.add(mapLayer4);
        l.add(mapLayer5);
        return l;
    }

    public void setMasterFileName(String masterFileName) {
        this.masterFileName = masterFileName;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public void setLayers(List<MapLayer> layers) {
        this.layers = layers;
    }
}
