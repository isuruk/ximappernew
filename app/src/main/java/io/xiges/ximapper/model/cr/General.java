
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class General implements Parcelable {

    private String acquiredExtent;
    private String acquisitionName;
    private String assesmentNumber;
    private Boundaries boundaries;
    private String dateOfPrepared;
    private String dateOfSection38A;
    private String lotId;
    private String nameOfLand;
    private List<NameOfVillage> nameOfVillage = new ArrayList<NameOfVillage>();
    private String planNumber;
    private String privatePlanNumber;
    private String roadName;
    private String totalExtent;
    private String landUseDetail;


    /**
     * No args constructor for use in serialization
     * 
     */
    public General() {
    }

    public General(String acquiredExtent, String acquisitionName, String assesmentNumber, Boundaries boundaries, String dateOfPrepared, String dateOfSection38A, String lotId, String nameOfLand, List<NameOfVillage> nameOfVillage, String planNumber, String privatePlanNumber, String roadName, String totalExtent, String landUseDetail) {
        this.acquiredExtent = acquiredExtent;
        this.acquisitionName = acquisitionName;
        this.assesmentNumber = assesmentNumber;
        this.boundaries = boundaries;
        this.dateOfPrepared = dateOfPrepared;
        this.dateOfSection38A = dateOfSection38A;
        this.lotId = lotId;
        this.nameOfLand = nameOfLand;
        this.nameOfVillage = nameOfVillage;
        this.planNumber = planNumber;
        this.privatePlanNumber = privatePlanNumber;
        this.roadName = roadName;
        this.totalExtent = totalExtent;
        this.landUseDetail = landUseDetail;
    }

    protected General(Parcel in) {
        acquiredExtent = in.readString();
        acquisitionName = in.readString();
        assesmentNumber = in.readString();
        boundaries = in.readParcelable(Boundaries.class.getClassLoader());
        dateOfPrepared = in.readString();
        dateOfSection38A = in.readString();
        lotId = in.readString();
        nameOfLand = in.readString();
        nameOfVillage = in.createTypedArrayList(NameOfVillage.CREATOR);
        planNumber = in.readString();
        privatePlanNumber = in.readString();
        roadName = in.readString();
        totalExtent = in.readString();
        landUseDetail = in.readString();
    }

    public static final Creator<General> CREATOR = new Creator<General>() {
        @Override
        public General createFromParcel(Parcel in) {
            return new General(in);
        }

        @Override
        public General[] newArray(int size) {
            return new General[size];
        }
    };

    public String getAcquiredExtent() {
        return acquiredExtent;
    }

    public void setAcquiredExtent(String acquiredExtent) {
        this.acquiredExtent = acquiredExtent;
    }

    public String getAcquisitionName() {
        return acquisitionName;
    }

    public void setAcquisitionName(String acquisitionName) {
        this.acquisitionName = acquisitionName;
    }

    public String getAssessmentNumber() {
        return assesmentNumber;
    }

    public void setAssesmentNumber(String assesmentNumber) {
        this.assesmentNumber = assesmentNumber;
    }

    public Boundaries getBoundaries() {
        return boundaries;
    }

    public void setBoundaries(Boundaries boundaries) {
        this.boundaries = boundaries;
    }

    public String getDateOfPrepared() {

        if(TextUtils.isEmpty(dateOfPrepared) || dateOfPrepared.equals("")) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sf.format(cal.getTime());
            dateOfPrepared = date;
        }


        return dateOfPrepared;
    }

    public void setDateOfPrepared(String dateOfPrepared) {
        this.dateOfPrepared = dateOfPrepared;
    }

    public String getDateOfSection38A() {
        if(TextUtils.isEmpty(dateOfSection38A) || dateOfSection38A.equals("")) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sf.format(cal.getTime());
            dateOfSection38A = date;
        }

        return dateOfSection38A;
    }

    public void setDateOfSection38A(String dateOfSection38A) {
        this.dateOfSection38A = dateOfSection38A;
    }

    public String getLotId() {
        return lotId;
    }

    public void setLotId(String lotId) {
        this.lotId = lotId;
    }

    public String getNameOfLand() {
        return nameOfLand;
    }

    public void setNameOfLand(String nameOfLand) {
        this.nameOfLand = nameOfLand;
    }

    public List<NameOfVillage> getNameOfVillage() {
        return nameOfVillage;
    }

    public void setNameOfVillage(List<NameOfVillage> nameOfVillage) {
        this.nameOfVillage = nameOfVillage;
    }

    public String getPlanNumber() {
        return planNumber;
    }

    public void setPlanNumber(String planNumber) {
        this.planNumber = planNumber;
    }

    public String getPrivatePlanNumber() {
        return privatePlanNumber;
    }

    public void setPrivatePlanNumber(String privatePlanNumber) {
        this.privatePlanNumber = privatePlanNumber;
    }

    public String getRoadName() {
        return roadName;
    }

    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }

    public String getTotalExtent() {
        return totalExtent;
    }

    public void setTotalExtent(String totalExtent) {
        this.totalExtent = totalExtent;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public String getLandUseDetail() {
        return landUseDetail;
    }

    public void setLandUseDetail(String landUseDetail) {
        this.landUseDetail = landUseDetail;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(acquiredExtent);
        parcel.writeString(acquisitionName);
        parcel.writeString(assesmentNumber);
        parcel.writeParcelable(boundaries, i);
        parcel.writeString(dateOfPrepared);
        parcel.writeString(dateOfSection38A);
        parcel.writeString(lotId);
        parcel.writeString(nameOfLand);
        parcel.writeTypedList(nameOfVillage);
        parcel.writeString(planNumber);
        parcel.writeString(privatePlanNumber);
        parcel.writeString(roadName);
        parcel.writeString(totalExtent);
        parcel.writeString(landUseDetail);
    }

}
