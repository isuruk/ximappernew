package io.xiges.ximapper.model.mixin_clone_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CircleMixinContent implements Parcelable {
    @SerializedName("circleFillPoints")
    private List<FeaturesMixinContent> circleFillPoints = new ArrayList<>();
    @SerializedName("circleMidPoint")
    private PointMixinContent midPoint;
    @SerializedName("circleRadius")
    private double radius;
    @SerializedName("circleUnit")
    private String unit;

    public CircleMixinContent() {
    }

    protected CircleMixinContent(Parcel in) {
        circleFillPoints = in.createTypedArrayList(FeaturesMixinContent.CREATOR);
        midPoint = in.readParcelable(PointMixinContent.class.getClassLoader());
        radius = in.readDouble();
        unit = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(circleFillPoints);
        dest.writeParcelable(midPoint, flags);
        dest.writeDouble(radius);
        dest.writeString(unit);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CircleMixinContent> CREATOR = new Creator<CircleMixinContent>() {
        @Override
        public CircleMixinContent createFromParcel(Parcel in) {
            return new CircleMixinContent(in);
        }

        @Override
        public CircleMixinContent[] newArray(int size) {
            return new CircleMixinContent[size];
        }
    };

    public List<FeaturesMixinContent> getCircleFillPoints() {
        return circleFillPoints;
    }

    public void setCircleFillPoints(List<FeaturesMixinContent> circleFillPoints) {
        this.circleFillPoints = circleFillPoints;
    }

    public PointMixinContent getMidPoint() {
        return midPoint;
    }

    public void setMidPoint(PointMixinContent midPoint) {
        this.midPoint = midPoint;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
