package io.xiges.ximapper.model.mixin_clone_model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class CoordinatesMixinContent extends ArrayList<PointMixinContent> implements Parcelable {

    public CoordinatesMixinContent(){
        super();
    }

    protected CoordinatesMixinContent(Parcel in) {
        in.readTypedList(this, PointMixinContent.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CoordinatesMixinContent> CREATOR = new Creator<CoordinatesMixinContent>() {
        @Override
        public CoordinatesMixinContent createFromParcel(Parcel in) {
            return new CoordinatesMixinContent(in);
        }

        @Override
        public CoordinatesMixinContent[] newArray(int size) {
            return new CoordinatesMixinContent[size];
        }
    };


}
