package io.xiges.ximapper.model.api.master_data;


import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class OtherConstructionsType implements Parcelable
{

    private String code;
    private String description;
    public final static Parcelable.Creator<OtherConstructionsType> CREATOR = new Creator<OtherConstructionsType>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OtherConstructionsType createFromParcel(Parcel in) {
            return new OtherConstructionsType(in);
        }

        public OtherConstructionsType[] newArray(int size) {
            return (new OtherConstructionsType[size]);
        }

    }
            ;

    protected OtherConstructionsType(Parcel in) {
        this.code = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public OtherConstructionsType() {
    }

    /**
     *
     * @param code
     * @param description
     */
    public OtherConstructionsType(String code, String description) {
        super();
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(code);
        dest.writeValue(description);
    }

    public int describeContents() {
        return 0;
    }

}