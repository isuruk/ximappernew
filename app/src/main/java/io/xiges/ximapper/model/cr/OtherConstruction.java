
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class OtherConstruction implements Parcelable
{

    private String no;
    private String name;
    private String extent;
    private OtherConstructionsMaterials otherConstructionsMaterials;
    private int otherConstructionsType;
    private List<OtherConstructionUnit> otherConstructionUnits = new ArrayList<OtherConstructionUnit>();
    private String remark;
    public final static Parcelable.Creator<OtherConstruction> CREATOR = new Creator<OtherConstruction>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OtherConstruction createFromParcel(Parcel in) {
            return new OtherConstruction(in);
        }

        public OtherConstruction[] newArray(int size) {
            return (new OtherConstruction[size]);
        }

    }
    ;

    protected OtherConstruction(Parcel in) {
        this.extent = ((String) in.readValue((String.class.getClassLoader())));
        this.otherConstructionsMaterials = ((OtherConstructionsMaterials) in.readValue((OtherConstructionsMaterials.class.getClassLoader())));
        this.otherConstructionsType = ((int) in.readValue((int.class.getClassLoader())));
        in.readList(this.otherConstructionUnits, (io.xiges.ximapper.model.cr.OtherConstructionUnit.class.getClassLoader()));
        this.remark = ((String) in.readValue((String.class.getClassLoader())));
        this.no = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));

    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public OtherConstruction() {
    }

    /**
     * 
     * @param extent
     * @param otherConstructionsMaterials
     * @param remark
     * @param otherConstructionUnits
     * @param otherConstructionsType
     */
    public OtherConstruction(String no,String extent, OtherConstructionsMaterials otherConstructionsMaterials, int otherConstructionsType, List<OtherConstructionUnit> otherConstructionUnits, String remark,String name) {
        super();
        this.extent = extent;
        this.name = name;
        this.otherConstructionsMaterials = otherConstructionsMaterials;
        this.otherConstructionsType = otherConstructionsType;
        this.otherConstructionUnits = otherConstructionUnits;
        this.remark = remark;
        this.no = no;

    }

    public String getExtent() {
        return extent;
    }

    public void setExtent(String extent) {
        this.extent = extent;
    }

    public OtherConstructionsMaterials getOtherConstructionsMaterials() {
        return otherConstructionsMaterials;
    }

    public void setOtherConstructionsMaterials(OtherConstructionsMaterials otherConstructionsMaterials) {
        this.otherConstructionsMaterials = otherConstructionsMaterials;
    }

    public int getOtherConstructionsType() {
        return otherConstructionsType;
    }

    public void setOtherConstructionsType(int otherConstructionsType) {
        this.otherConstructionsType = otherConstructionsType;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public List<OtherConstructionUnit> getOtherConstructionUnits() {
        if(otherConstructionUnits == null){
            otherConstructionUnits =new ArrayList<>();
        }

        return otherConstructionUnits;
    }

    public void setOtherConstructionUnits(List<OtherConstructionUnit> otherConstructionUnits) {
        this.otherConstructionUnits = otherConstructionUnits;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(extent);
        dest.writeValue(otherConstructionsMaterials);
        dest.writeValue(otherConstructionsType);
        dest.writeList(otherConstructionUnits);
        dest.writeValue(remark);
        dest.writeValue(no);
        dest.writeValue(name);
    }

    public int describeContents() {
        return  0;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
