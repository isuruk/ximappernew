
package io.xiges.ximapper.model.masterfile;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Unit implements Parcelable
{

    private int unitId;
    private String unitName;
    public final static Parcelable.Creator<Unit> CREATOR = new Creator<Unit>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Unit createFromParcel(Parcel in) {
            return new Unit(in);
        }

        public Unit[] newArray(int size) {
            return (new Unit[size]);
        }

    }
    ;

    protected Unit(Parcel in) {
        this.unitId = ((int) in.readValue((int.class.getClassLoader())));
        this.unitName = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Unit() {
    }

    /**
     * 
     * @param unitName
     * @param unitId
     */
    public Unit(int unitId, String unitName) {
        super();
        this.unitId = unitId;
        this.unitName = unitName;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(unitId);
        dest.writeValue(unitName);
    }

    public int describeContents() {
        return  0;
    }

}
