package io.xiges.ximapper.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.xiges.ximapper.model.cr.CR;
import io.xiges.ximapper.model.mixin_clone_model.ConstructionMixInContent;
import io.xiges.ximapper.model.mixin_clone_model.FeatureMixinContent;
import io.xiges.ximapper.model.mixin_clone_model.PointMixinContent;

public class Output implements Parcelable {
    private String lotId;
    private String lotName;
    @SerializedName("fillPoints")
    private List<PointMixinContent> fillLayerPointList;
    @SerializedName("linePoints")
    private List<PointMixinContent> lineLayerPointList;
    @SerializedName("circlePoints")
    private List<FeatureMixinContent> circleLayerFeatureList;
    private List<ConstructionMixInContent> constructions;
    @SerializedName("lotLabels")
    private List<Symbol> symbolList;
    @SerializedName("buildingLabels")
    private List<Symbol> buildingLabels;
    @SerializedName("floorLabels")
    private List<Symbol> floorLabels;
    private List<Marker> markers;
    private CR conditional_report;


    public Output() {

    }

    protected Output(Parcel in) {
        lotId = in.readString();
        lotName = in.readString();
        fillLayerPointList = in.createTypedArrayList(PointMixinContent.CREATOR);
        lineLayerPointList = in.createTypedArrayList(PointMixinContent.CREATOR);
        circleLayerFeatureList = in.createTypedArrayList(FeatureMixinContent.CREATOR);
        constructions = in.createTypedArrayList(ConstructionMixInContent.CREATOR);
        symbolList = in.createTypedArrayList(Symbol.CREATOR);
        buildingLabels = in.createTypedArrayList(Symbol.CREATOR);
        floorLabels = in.createTypedArrayList(Symbol.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(lotId);
        dest.writeString(lotName);
        dest.writeTypedList(fillLayerPointList);
        dest.writeTypedList(lineLayerPointList);
        dest.writeTypedList(circleLayerFeatureList);
        dest.writeTypedList(constructions);
        dest.writeTypedList(symbolList);
        dest.writeTypedList(buildingLabels);
        dest.writeTypedList(floorLabels);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Output> CREATOR = new Creator<Output>() {
        @Override
        public Output createFromParcel(Parcel in) {
            return new Output(in);
        }

        @Override
        public Output[] newArray(int size) {
            return new Output[size];
        }
    };

    public String getLotId() {
        return lotId;
    }

    public void setLotId(String lotId) {
        this.lotId = lotId;
    }

    public String getLotName() {
        return lotName;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

    public List<PointMixinContent> getFillLayerPointList() {
        return fillLayerPointList;
    }

    public void setFillLayerPointList(List<PointMixinContent> fillLayerPointList) {
        this.fillLayerPointList = fillLayerPointList;
    }

    public List<PointMixinContent> getLineLayerPointList() {
        return lineLayerPointList;
    }

    public void setLineLayerPointList(List<PointMixinContent> lineLayerPointList) {
        this.lineLayerPointList = lineLayerPointList;
    }

    public List<FeatureMixinContent> getCircleLayerFeatureList() {
        return circleLayerFeatureList;
    }

    public void setCircleLayerFeatureList(List<FeatureMixinContent> circleLayerFeatureList) {
        this.circleLayerFeatureList = circleLayerFeatureList;
    }

    public List<ConstructionMixInContent> getConstructions() {
        return constructions;
    }

    public void setConstructions(List<ConstructionMixInContent> constructions) {
        this.constructions = constructions;
    }

    public List<Symbol> getSymbolList() {
        return symbolList;
    }

    public void setSymbolList(List<Symbol> symbolList) {
        this.symbolList = symbolList;
    }

    public List<Symbol> getBuildingLabels() {
        return buildingLabels;
    }

    public void setBuildingLabels(List<Symbol> buildingLabels) {
        this.buildingLabels = buildingLabels;
    }

    public List<Symbol> getFloorLabels() {
        return floorLabels;
    }

    public void setFloorLabels(List<Symbol> floorLabels) {
        this.floorLabels = floorLabels;
    }

    public void setMarkers(List<Marker> markers) {
        this.markers = markers;
    }

    public List<Marker> getMarkers() {
        return markers;
    }

    public CR getConditional_report() {
        return conditional_report;
    }

    public void setConditional_report(CR conditional_report) {
        this.conditional_report = conditional_report;
    }
}
