
package io.xiges.ximapper.model.cr;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import java.util.Objects;

public class OtherConstructionsMaterials implements Parcelable
{

    private String code;
    private String description;
    private String otherConstructionType;
    private String unit;
    public final static Parcelable.Creator<OtherConstructionsMaterials> CREATOR = new Creator<OtherConstructionsMaterials>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OtherConstructionsMaterials createFromParcel(Parcel in) {
            return new OtherConstructionsMaterials(in);
        }

        public OtherConstructionsMaterials[] newArray(int size) {
            return (new OtherConstructionsMaterials[size]);
        }

    }
    ;

    protected OtherConstructionsMaterials(Parcel in) {
        this.code = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.otherConstructionType = ((String) in.readValue((String.class.getClassLoader())));
        this.unit = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public OtherConstructionsMaterials() {
    }

    /**
     * 
     * @param unit
     * @param code
     * @param otherConstructionType
     * @param description
     */
    public OtherConstructionsMaterials(String code, String description, String otherConstructionType, String unit) {
        super();
        this.code = code;
        this.description = description;
        this.otherConstructionType = otherConstructionType;
        this.unit = unit;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOtherConstructionType() {
        return otherConstructionType;
    }

    public void setOtherConstructionType(String otherConstructionType) {
        this.otherConstructionType = otherConstructionType;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(code);
        dest.writeValue(description);
        dest.writeValue(otherConstructionType);
        dest.writeValue(unit);
    }

    public int describeContents() {
        return  0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OtherConstructionsMaterials that = (OtherConstructionsMaterials) o;
        return Objects.equals(code, that.code) &&
                Objects.equals(description, that.description) &&
                Objects.equals(otherConstructionType, that.otherConstructionType) &&
                Objects.equals(unit, that.unit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, description, otherConstructionType, unit);
    }
}
