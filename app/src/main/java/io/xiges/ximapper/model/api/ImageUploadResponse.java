package io.xiges.ximapper.model.api;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class ImageUploadResponse implements Parcelable {

    private String message;
    private Integer fileCount;
    private List<String> files = null;
    private Object error;
    public final static Parcelable.Creator<ImageUploadResponse> CREATOR = new Creator<ImageUploadResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ImageUploadResponse createFromParcel(Parcel in) {
            return new ImageUploadResponse(in);
        }

        public ImageUploadResponse[] newArray(int size) {
            return (new ImageUploadResponse[size]);
        }

    };

    protected ImageUploadResponse(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.fileCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.files, (java.lang.String.class.getClassLoader()));
        this.error = ((Object) in.readValue((Object.class.getClassLoader())));
    }

    public ImageUploadResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getFileCount() {
        return fileCount;
    }

    public void setFileCount(Integer fileCount) {
        this.fileCount = fileCount;
    }

    public List<String> getFiles() {
        return files;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(fileCount);
        dest.writeList(files);
        dest.writeValue(error);
    }

    public int describeContents() {
        return 0;
    }

}