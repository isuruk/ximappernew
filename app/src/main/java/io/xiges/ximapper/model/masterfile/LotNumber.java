
package io.xiges.ximapper.model.masterfile;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class LotNumber implements Parcelable
{

    private int id;
    private String lotId;
    public final static Parcelable.Creator<LotNumber> CREATOR = new Creator<LotNumber>() {


        @SuppressWarnings({
            "unchecked"
        })
        public LotNumber createFromParcel(Parcel in) {
            return new LotNumber(in);
        }

        public LotNumber[] newArray(int size) {
            return (new LotNumber[size]);
        }

    }
    ;

    protected LotNumber(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.lotId = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public LotNumber() {
    }

    /**
     * 
     * @param lotId
     * @param id
     */
    public LotNumber(int id, String lotId) {
        super();
        this.id = id;
        this.lotId = lotId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLotId() {
        return lotId;
    }

    public void setLotId(String lotId) {
        this.lotId = lotId;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(lotId);
    }

    public int describeContents() {
        return  0;
    }

}
