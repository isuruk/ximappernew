package io.xiges.ximapper.model;

public class DataSave {
    DataMasterFileDetails masterFileDetails;
    Output building;

    public DataMasterFileDetails getMasterFileDetails() {
        return masterFileDetails;
    }

    public void setMasterFileDetails(DataMasterFileDetails masterFileDetails) {
        this.masterFileDetails = masterFileDetails;
    }

    public Output getBuilding() {
        return building;
    }

    public void setBuilding(Output building) {
        this.building = building;
    }
}
