
package io.xiges.ximapper.model.cr;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class FixtureAndFitting implements Parcelable
{

    private List<Integer> fixtureAndFittingDoor = new ArrayList<Integer>();
    private List<Integer> fixtureAndFittingDoorsBathroomAndToiletFittings = new ArrayList<Integer>();
    private List<Integer> fixtureAndFittingDoorsHandRail = new ArrayList<Integer>();
    private List<Integer> fixtureAndFittingDoorsOther = new ArrayList<Integer>();
    private List<Integer> fixtureAndFittingDoorsPantryCupbord = new ArrayList<Integer>();
    private List<Integer> fixtureAndFittingWindowProtection = new ArrayList<Integer>();
    private List<Integer> fixtureAndFittingWindows = new ArrayList<Integer>();
    public final static Parcelable.Creator<FixtureAndFitting> CREATOR = new Creator<FixtureAndFitting>() {


        @SuppressWarnings({
            "unchecked"
        })
        public FixtureAndFitting createFromParcel(Parcel in) {
            return new FixtureAndFitting(in);
        }

        public FixtureAndFitting[] newArray(int size) {
            return (new FixtureAndFitting[size]);
        }

    }
    ;

    protected FixtureAndFitting(Parcel in) {
        in.readList(this.fixtureAndFittingDoor, (java.lang.Integer.class.getClassLoader()));
        in.readList(this.fixtureAndFittingDoorsBathroomAndToiletFittings, (java.lang.Integer.class.getClassLoader()));
        in.readList(this.fixtureAndFittingDoorsHandRail, (java.lang.Integer.class.getClassLoader()));
        in.readList(this.fixtureAndFittingDoorsOther, (java.lang.Integer.class.getClassLoader()));
        in.readList(this.fixtureAndFittingDoorsPantryCupbord, (java.lang.Integer.class.getClassLoader()));
        in.readList(this.fixtureAndFittingWindowProtection, (java.lang.Integer.class.getClassLoader()));
        in.readList(this.fixtureAndFittingWindows, (java.lang.Integer.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public FixtureAndFitting() {
    }

    /**
     * 
     * @param fixtureAndFittingDoorsHandRail
     * @param fixtureAndFittingWindowProtection
     * @param fixtureAndFittingDoorsBathroomAndToiletFittings
     * @param fixtureAndFittingDoorsPantryCupbord
     * @param fixtureAndFittingDoor
     * @param fixtureAndFittingWindows
     * @param fixtureAndFittingDoorsOther
     */
    public FixtureAndFitting(List<Integer> fixtureAndFittingDoor, List<Integer> fixtureAndFittingDoorsBathroomAndToiletFittings, List<Integer> fixtureAndFittingDoorsHandRail, List<Integer> fixtureAndFittingDoorsOther, List<Integer> fixtureAndFittingDoorsPantryCupbord, List<Integer> fixtureAndFittingWindowProtection, List<Integer> fixtureAndFittingWindows) {
        super();
        this.fixtureAndFittingDoor = fixtureAndFittingDoor;
        this.fixtureAndFittingDoorsBathroomAndToiletFittings = fixtureAndFittingDoorsBathroomAndToiletFittings;
        this.fixtureAndFittingDoorsHandRail = fixtureAndFittingDoorsHandRail;
        this.fixtureAndFittingDoorsOther = fixtureAndFittingDoorsOther;
        this.fixtureAndFittingDoorsPantryCupbord = fixtureAndFittingDoorsPantryCupbord;
        this.fixtureAndFittingWindowProtection = fixtureAndFittingWindowProtection;
        this.fixtureAndFittingWindows = fixtureAndFittingWindows;
    }

    public List<Integer> getFixtureAndFittingDoor() {
        if(fixtureAndFittingDoor == null){
            fixtureAndFittingDoor = new ArrayList<>();
        }


        return fixtureAndFittingDoor;
    }

    public void setFixtureAndFittingDoor(List<Integer> fixtureAndFittingDoor) {
        this.fixtureAndFittingDoor = fixtureAndFittingDoor;
    }

    public List<Integer> getFixtureAndFittingDoorsBathroomAndToiletFittings() {
        if(fixtureAndFittingDoorsBathroomAndToiletFittings == null){
            fixtureAndFittingDoorsBathroomAndToiletFittings = new ArrayList<>();
        }

        return fixtureAndFittingDoorsBathroomAndToiletFittings;
    }

    public void setFixtureAndFittingDoorsBathroomAndToiletFittings(List<Integer> fixtureAndFittingDoorsBathroomAndToiletFittings) {
        this.fixtureAndFittingDoorsBathroomAndToiletFittings = fixtureAndFittingDoorsBathroomAndToiletFittings;
    }

    public List<Integer> getFixtureAndFittingDoorsHandRail() {
        if(fixtureAndFittingDoorsHandRail == null){
            fixtureAndFittingDoorsHandRail = new ArrayList<>();
        }

        return fixtureAndFittingDoorsHandRail;
    }

    public void setFixtureAndFittingDoorsHandRail(List<Integer> fixtureAndFittingDoorsHandRail) {
        this.fixtureAndFittingDoorsHandRail = fixtureAndFittingDoorsHandRail;
    }

    public List<Integer> getFixtureAndFittingDoorsOther(){
        if(fixtureAndFittingDoorsOther == null){
            fixtureAndFittingDoorsOther = new ArrayList<>();
        }

        return fixtureAndFittingDoorsOther;
    }

    public void setFixtureAndFittingDoorsOther(List<Integer> fixtureAndFittingDoorsOther) {
        this.fixtureAndFittingDoorsOther = fixtureAndFittingDoorsOther;
    }

    public List<Integer> getFixtureAndFittingDoorsPantryCupbord() {
        if(fixtureAndFittingDoorsPantryCupbord == null){
            fixtureAndFittingDoorsPantryCupbord = new ArrayList<>();
        }

        return fixtureAndFittingDoorsPantryCupbord;
    }

    public void setFixtureAndFittingDoorsPantryCupbord(List<Integer> fixtureAndFittingDoorsPantryCupbord) {
        this.fixtureAndFittingDoorsPantryCupbord = fixtureAndFittingDoorsPantryCupbord;
    }

    public List<Integer> getFixtureAndFittingWindowProtection() {
        if(fixtureAndFittingWindowProtection == null){
            fixtureAndFittingWindowProtection = new ArrayList<>();
        }


        return fixtureAndFittingWindowProtection;
    }

    public void setFixtureAndFittingWindowProtection(List<Integer> fixtureAndFittingWindowProtection) {
        this.fixtureAndFittingWindowProtection = fixtureAndFittingWindowProtection;
    }

    public List<Integer> getFixtureAndFittingWindows() {
        if(fixtureAndFittingWindows == null){
            fixtureAndFittingWindows = new ArrayList<>();
        }

        return fixtureAndFittingWindows;
    }

    public void setFixtureAndFittingWindows(List<Integer> fixtureAndFittingWindows) {
        this.fixtureAndFittingWindows = fixtureAndFittingWindows;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(fixtureAndFittingDoor);
        dest.writeList(fixtureAndFittingDoorsBathroomAndToiletFittings);
        dest.writeList(fixtureAndFittingDoorsHandRail);
        dest.writeList(fixtureAndFittingDoorsOther);
        dest.writeList(fixtureAndFittingDoorsPantryCupbord);
        dest.writeList(fixtureAndFittingWindowProtection);
        dest.writeList(fixtureAndFittingWindows);
    }

    public int describeContents() {
        return  0;
    }

}
