package io.xiges.ximapper.model.building_details;

public class BuildingDetailsPlaceholders {
    private String foundationStructure;
    private String wallStructure;
    private String finishersAndServicesBathroomAndToilet;
    private String finishersAndServicesFloorFinisher;
    private String finishersAndServicesServices;
    private String finishersAndServicesWallFinisher;
    private String fixtureAndFittingDoor;
    private String fixtureAndFittingDoorsBathroomAndToiletFittings;
    private String fixtureAndFittingDoorsHandRail;
    private String fixtureAndFittingDoorsOther;
    private String fixtureAndFittingDoorsPantryCupbord;
    private String fixtureAndFittingWindowProtection;
    private String  fixtureAndFittingWindows;
    private String ceiling;
    private String  roofFinisher;
    private String roofFrame;
    private String roofMaterial;

    public String getFoundationStructure() {
        return foundationStructure;
    }

    public void setFoundationStructure(String foundationStructure) {
        this.foundationStructure = foundationStructure;
    }

    public String getWallStructure() {
        return wallStructure;
    }

    public void setWallStructure(String wallStructure) {
        this.wallStructure = wallStructure;
    }

    public String getFinishersAndServicesBathroomAndToilet() {
        return finishersAndServicesBathroomAndToilet;
    }

    public void setFinishersAndServicesBathroomAndToilet(String finishersAndServicesBathroomAndToilet) {
        this.finishersAndServicesBathroomAndToilet = finishersAndServicesBathroomAndToilet;
    }

    public String getFinishersAndServicesFloorFinisher() {
        return finishersAndServicesFloorFinisher;
    }

    public void setFinishersAndServicesFloorFinisher(String finishersAndServicesFloorFinisher) {
        this.finishersAndServicesFloorFinisher = finishersAndServicesFloorFinisher;
    }

    public String getFinishersAndServicesServices() {
        return finishersAndServicesServices;
    }

    public void setFinishersAndServicesServices(String finishersAndServicesServices) {
        this.finishersAndServicesServices = finishersAndServicesServices;
    }

    public String getFinishersAndServicesWallFinisher() {
        return finishersAndServicesWallFinisher;
    }

    public void setFinishersAndServicesWallFinisher(String finishersAndServicesWallFinisher) {
        this.finishersAndServicesWallFinisher = finishersAndServicesWallFinisher;
    }

    public String getFixtureAndFittingDoor() {
        return fixtureAndFittingDoor;
    }

    public void setFixtureAndFittingDoor(String fixtureAndFittingDoor) {
        this.fixtureAndFittingDoor = fixtureAndFittingDoor;
    }

    public String getFixtureAndFittingDoorsBathroomAndToiletFittings() {
        return fixtureAndFittingDoorsBathroomAndToiletFittings;
    }

    public void setFixtureAndFittingDoorsBathroomAndToiletFittings(String fixtureAndFittingDoorsBathroomAndToiletFittings) {
        this.fixtureAndFittingDoorsBathroomAndToiletFittings = fixtureAndFittingDoorsBathroomAndToiletFittings;
    }

    public String getFixtureAndFittingDoorsHandRail() {
        return fixtureAndFittingDoorsHandRail;
    }

    public void setFixtureAndFittingDoorsHandRail(String fixtureAndFittingDoorsHandRail) {
        this.fixtureAndFittingDoorsHandRail = fixtureAndFittingDoorsHandRail;
    }

    public String getFixtureAndFittingDoorsOther() {
        return fixtureAndFittingDoorsOther;
    }

    public void setFixtureAndFittingDoorsOther(String fixtureAndFittingDoorsOther) {
        this.fixtureAndFittingDoorsOther = fixtureAndFittingDoorsOther;
    }

    public String getFixtureAndFittingDoorsPantryCupbord() {
        return fixtureAndFittingDoorsPantryCupbord;
    }

    public void setFixtureAndFittingDoorsPantryCupbord(String fixtureAndFittingDoorsPantryCupbord) {
        this.fixtureAndFittingDoorsPantryCupbord = fixtureAndFittingDoorsPantryCupbord;
    }

    public String getFixtureAndFittingWindowProtection() {
        return fixtureAndFittingWindowProtection;
    }

    public void setFixtureAndFittingWindowProtection(String fixtureAndFittingWindowProtection) {
        this.fixtureAndFittingWindowProtection = fixtureAndFittingWindowProtection;
    }

    public String getFixtureAndFittingWindows() {
        return fixtureAndFittingWindows;
    }

    public void setFixtureAndFittingWindows(String fixtureAndFittingWindows) {
        this.fixtureAndFittingWindows = fixtureAndFittingWindows;
    }

    public String getCeiling() {
        return ceiling;
    }

    public void setCeiling(String ceiling) {
        this.ceiling = ceiling;
    }

    public String getRoofFinisher() {
        return roofFinisher;
    }

    public void setRoofFinisher(String roofFinisher) {
        this.roofFinisher = roofFinisher;
    }

    public String getRoofFrame() {
        return roofFrame;
    }

    public void setRoofFrame(String roofFrame) {
        this.roofFrame = roofFrame;
    }

    public String getRoofMaterial() {
        return roofMaterial;
    }

    public void setRoofMaterial(String roofMaterial) {
        this.roofMaterial = roofMaterial;
    }
}
