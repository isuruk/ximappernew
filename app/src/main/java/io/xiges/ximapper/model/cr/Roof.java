
package io.xiges.ximapper.model.cr;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Roof implements Parcelable
{

    private List<Integer> ceiling = new ArrayList<Integer>();
    private List<Integer> roofFinisher = new ArrayList<Integer>();
    private List<Integer> roofFrame = new ArrayList<Integer>();
    private List<Integer> roofMaterial = new ArrayList<Integer>();
    public final static Parcelable.Creator<Roof> CREATOR = new Creator<Roof>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Roof createFromParcel(Parcel in) {
            return new Roof(in);
        }

        public Roof[] newArray(int size) {
            return (new Roof[size]);
        }

    }
    ;

    protected Roof(Parcel in) {
        in.readList(this.ceiling, (java.lang.Integer.class.getClassLoader()));
        in.readList(this.roofFinisher, (java.lang.Integer.class.getClassLoader()));
        in.readList(this.roofFrame, (java.lang.Integer.class.getClassLoader()));
        in.readList(this.roofMaterial, (java.lang.Integer.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Roof() {
    }

    /**
     * 
     * @param roofFinisher
     * @param ceiling
     * @param roofFrame
     * @param roofMaterial
     */
    public Roof(List<Integer> ceiling, List<Integer> roofFinisher, List<Integer> roofFrame, List<Integer> roofMaterial) {
        super();
        this.ceiling = ceiling;
        this.roofFinisher = roofFinisher;
        this.roofFrame = roofFrame;
        this.roofMaterial = roofMaterial;
    }

    public List<Integer> getCeiling() {
        if(ceiling == null){
            ceiling = new ArrayList<>();
        }
        return ceiling;
    }

    public void setCeiling(List<Integer> ceiling) {
        this.ceiling = ceiling;
    }

    public List<Integer> getRoofFinisher() {
        if(roofFinisher == null){
            roofFinisher = new ArrayList<>();
        }
        return roofFinisher;
    }

    public void setRoofFinisher(List<Integer> roofFinisher) {
        this.roofFinisher = roofFinisher;
    }

    public List<Integer> getRoofFrame() {

        if(roofFrame == null){
            roofFrame = new ArrayList<>();
        }
        return roofFrame;
    }

    public void setRoofFrame(List<Integer> roofFrame) {


        this.roofFrame = roofFrame;
    }

    public List<Integer> getRoofMaterial() {
        if(roofMaterial == null){
            roofMaterial = new ArrayList<>();
        }

        return roofMaterial;
    }

    public void setRoofMaterial(List<Integer> roofMaterial) {
        this.roofMaterial = roofMaterial;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(ceiling);
        dest.writeList(roofFinisher);
        dest.writeList(roofFrame);
        dest.writeList(roofMaterial);
    }

    public int describeContents() {
        return  0;
    }

}
