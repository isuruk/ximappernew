package io.xiges.ximapper.model;

import java.util.List;

public class GridFeatures {
    private GridGeometry geometry;
    private String type;
    private GridProperties properties;

    public GridGeometry getGeometry() {
        return geometry;
    }

    public void setGeometry(GridGeometry geometry) {
        this.geometry = geometry;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public GridProperties getProperties() {
        return properties;
    }

    public void setProperties(GridProperties properties) {
        this.properties = properties;
    }
}
