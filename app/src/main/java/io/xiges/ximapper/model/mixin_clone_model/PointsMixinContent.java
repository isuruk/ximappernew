package io.xiges.ximapper.model.mixin_clone_model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class PointsMixinContent implements Parcelable {
    private List<CoordinatesMixinContent> coordinates;
    private String type;


    protected PointsMixinContent(Parcel in) {
        coordinates = in.createTypedArrayList(CoordinatesMixinContent.CREATOR);
        type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(coordinates);
        dest.writeString(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PointsMixinContent> CREATOR = new Creator<PointsMixinContent>() {
        @Override
        public PointsMixinContent createFromParcel(Parcel in) {
            return new PointsMixinContent(in);
        }

        @Override
        public PointsMixinContent[] newArray(int size) {
            return new PointsMixinContent[size];
        }
    };

    public List<CoordinatesMixinContent> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<CoordinatesMixinContent> coordinates) {
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "PointsMixinContent{" +
                "coordinates=" + coordinates +
                ", type='" + type + '\'' +
                '}';
    }
}
