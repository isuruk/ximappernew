package io.xiges.ximapper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.OtherConstructionListItemBinding;
import io.xiges.ximapper.model.CROtherConstruction;
import io.xiges.ximapper.model.cr.OtherConstruction;
import io.xiges.ximapper.ui.documents.RecyclerViewClickListener;

public class OtherConstructionDetailAdapter extends RecyclerView.Adapter<OtherConstructionDetailAdapter.OCDetailViewHolder> {

    private List<OtherConstruction> details;
    private Context context;
    private RecyclerViewClickListener listener;


    public OtherConstructionDetailAdapter(List<OtherConstruction> details, Context context, RecyclerViewClickListener listener) {
        this.details = details;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public OtherConstructionDetailAdapter.OCDetailViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                                int viewType) {
        OtherConstructionListItemBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.other_construction_list_item, viewGroup, false);
        return new OtherConstructionDetailAdapter.OCDetailViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OtherConstructionDetailAdapter.OCDetailViewHolder holder, int position) {
        holder.binding.setData(details.get(position));
    }


    public void setDetails(List<OtherConstruction> details) {
        this.details = details;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return this.details.size();
    }

    class OCDetailViewHolder extends RecyclerView.ViewHolder {

        OtherConstructionListItemBinding binding;

        OCDetailViewHolder(OtherConstructionListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v -> listener.onItemClick(getAdapterPosition()));
        }
    }
}
