package io.xiges.ximapper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.MultiselectItemBinding;
import io.xiges.ximapper.model.MultiSelectSpinner;

public class MultiSelectSpinnerAdapter extends RecyclerView.Adapter<io.xiges.ximapper.adapter.MultiSelectSpinnerAdapter.MultiSelectSpinnerViewHolder> {


    private ArrayList<MultiSelectSpinner> spinner;
    private Context context;

    public MultiSelectSpinnerAdapter(ArrayList<MultiSelectSpinner> spinner, Context context) {
        this.spinner = spinner;
        this.context = context;

    }

    @Override
    public MultiSelectSpinnerAdapter.MultiSelectSpinnerViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                 int viewType) {
        MultiselectItemBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.multiselect_item, viewGroup, false);
        return new MultiSelectSpinnerAdapter.MultiSelectSpinnerViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MultiSelectSpinnerAdapter.MultiSelectSpinnerViewHolder holder, int position) {
        holder.binding.setData(spinner.get(position));
    }


    public void setDetails(ArrayList<MultiSelectSpinner> spinner) {
        this.spinner = spinner;
        notifyDataSetChanged();
    }


    public ArrayList<MultiSelectSpinner> getSpinner() {
        return spinner;
    }


    @Override
    public int getItemCount() {
        return this.spinner.size();
    }

    class MultiSelectSpinnerViewHolder extends RecyclerView.ViewHolder {

        MultiselectItemBinding binding;

        MultiSelectSpinnerViewHolder(MultiselectItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }

}
