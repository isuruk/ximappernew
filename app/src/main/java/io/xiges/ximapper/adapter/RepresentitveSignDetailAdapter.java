package io.xiges.ximapper.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.RepresentativeListItemBinding;
import io.xiges.ximapper.model.cr.RepresentativeDetail;

public class RepresentitveSignDetailAdapter extends RecyclerView.Adapter<RepresentitveSignDetailAdapter.RepresentitveSignDetailHolder>{

    private List<RepresentativeDetail> details;
    private Context context;

    public RepresentitveSignDetailAdapter(List<RepresentativeDetail> details, Context context) {
        this.details = details;
        this.context = context;

    }

    @Override
    public RepresentitveSignDetailAdapter.RepresentitveSignDetailHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                 int viewType) {
        RepresentativeListItemBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.representative_list_item, viewGroup, false);
        return new RepresentitveSignDetailAdapter.RepresentitveSignDetailHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RepresentitveSignDetailAdapter.RepresentitveSignDetailHolder holder, int position) {
        holder.binding.setData(details.get(position));
        if (details.get(position).getSign() != null) {
            byte[] decodedString = Base64.decode(details.get(position).getSign(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            holder.binding.rpSign.setImageBitmap(decodedByte);
        }
    }


    public void setDetails(List<RepresentativeDetail> details) {
        this.details = details;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return this.details.size();
    }

    class RepresentitveSignDetailHolder extends RecyclerView.ViewHolder {

        RepresentativeListItemBinding binding;

        RepresentitveSignDetailHolder(RepresentativeListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}
