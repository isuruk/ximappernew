package io.xiges.ximapper.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.BuildingAreaListItemBinding;
import io.xiges.ximapper.model.BuildingArea;

public class LandBuildingAreaListAdapter extends RecyclerView.Adapter<LandBuildingAreaListAdapter.LandBuildingAreaViewHolder> {

    private List<BuildingArea> buildingAreas;
    private Context context;


    public LandBuildingAreaListAdapter(List<BuildingArea> buildingAreas, Context context) {
        this.buildingAreas = buildingAreas;
        this.context = context;
    }



    @Override
    public LandBuildingAreaListAdapter.LandBuildingAreaViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                                     int viewType) {
        BuildingAreaListItemBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.building_area_list_item, viewGroup, false);
        return new LandBuildingAreaListAdapter.LandBuildingAreaViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull LandBuildingAreaViewHolder holder, int position) {
        BuildingArea buildingArea = buildingAreas.get(position);
        holder.binding.setData(buildingArea);
        double inchtot = (buildingArea.getArea() *  1550.0031);
        int feet = (int) Math.round(inchtot / (144));
        int inches = (int) Math.round(inchtot % (144));
        holder.binding.tvArea.setText(Html.fromHtml(feet+" ft<sup>2</sup> "+inches+" inch<sup>2</sup>"));
    }

    public void setLandBuildingAreaList(List<BuildingArea> buildingArea) {
        this.buildingAreas = buildingArea;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return buildingAreas.size();
    }


    class LandBuildingAreaViewHolder extends RecyclerView.ViewHolder {

        BuildingAreaListItemBinding binding;

        LandBuildingAreaViewHolder(BuildingAreaListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
