package io.xiges.ximapper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mapbox.mapboxsdk.plugins.annotation.Symbol;

import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.LabelListItemBinding;
import io.xiges.ximapper.ui.map.MapViewActivity;

public class LabelListAdapter extends RecyclerView.Adapter<io.xiges.ximapper.adapter.LabelListAdapter.LabelListHolder> {


    private List<Symbol> labels;
    private Context context;


    public LabelListAdapter(List<Symbol> labels, Context context) {
        this.labels = labels;
        this.context = context;
    }


    @Override
    public io.xiges.ximapper.adapter.LabelListAdapter.LabelListHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                                         int viewType) {
        LabelListItemBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.label_list_item, viewGroup, false);
        return new io.xiges.ximapper.adapter.LabelListAdapter.LabelListHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull io.xiges.ximapper.adapter.LabelListAdapter.LabelListHolder holder, int position) {
        Symbol label = labels.get(position);
        holder.binding.setLabel(label);
    }


    public void setLabel(List<Symbol> labels) {
        this.labels = labels;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return labels.size();
    }


    class LabelListHolder extends RecyclerView.ViewHolder {

        LabelListItemBinding binding;

        LabelListHolder(LabelListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.txtLabel.setOnClickListener(v -> {
                ((MapViewActivity) context).onListItemClicked(getAdapterPosition(),labels.get(getAdapterPosition()).getTextField());
            });


        }


    }
}
