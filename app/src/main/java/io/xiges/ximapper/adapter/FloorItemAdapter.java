package io.xiges.ximapper.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.FloorDrawingItemBinding;
import io.xiges.ximapper.model.DrawingType;
import io.xiges.ximapper.model.FloorDrawing;

public class FloorItemAdapter extends RecyclerView.Adapter<FloorItemAdapter.FloorItemAViewHolder> {

    private List<FloorDrawing> drawing;
    private Context context;

    public FloorItemAdapter(List<FloorDrawing> drawing, Context context) {
        this.drawing = drawing;
        this.context = context;
    }

    @Override
    public FloorItemAdapter.FloorItemAViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                           int viewType) {
        FloorDrawingItemBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.floor_drawing_item, viewGroup, false);
        return new FloorItemAdapter.FloorItemAViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FloorItemAViewHolder holder, int position) {
        FloorDrawing floors = this.drawing.get(position);
        if (floors.getDrawingType().equals(DrawingType.CIRCLE.name())) {
            holder.binding.txtLabel.setText("Circle Drawing " + position);
        } else if (floors.getDrawingType().equals(DrawingType.LINE.name())) {
            holder.binding.txtLabel.setText("Drawing " +  position);
        } else if (floors.getDrawingType().equals(DrawingType.POLYGON.name())) {
            holder.binding.txtLabel.setText("Polygon Drawing " +  position);
        } else  if (floors.getDrawingType().equals(DrawingType.AQLINE.name())){
            holder.binding.txtLabel.setText("AQ Line Drawing- " + position);
        } else  if (floors.getDrawingType().equals(DrawingType.AQ_POLYGON.name())){
            holder.binding.txtLabel.setText("AQ Polygon Drawing- " + position);
        } else  if (floors.getDrawingType().equals(DrawingType.BROOM_POLYGON.name())){
            holder.binding.txtLabel.setText("Room Drawing- " +  position);
        } else  if (floors.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name())){
            holder.binding.txtLabel.setText("Floor Drawing- " +  position);
        }else if (floors.getDrawingType().equals(DrawingType.CNULL.name())){
            //holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
            //holder.binding.txtLabel.setText("<DELETED>");
        }else{
            holder.binding.txtLabel.setText("Drawing - " +  position);
        }


        holder.binding.executePendingBindings();
    }


    public void setFloors(List<FloorDrawing> drawing) {
        drawing.removeIf((FloorDrawing a) -> a.getDrawingType().equals(DrawingType.CNULL.name()));
        this.drawing = drawing;
        notifyDataSetChanged();
    }

    public FloorDrawing getItem(int pos) {
        if (pos != -1) {
            return this.drawing.get(pos);
        }else{
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return this.drawing.size();
    }

    class FloorItemAViewHolder extends RecyclerView.ViewHolder {

        FloorDrawingItemBinding binding;

        FloorItemAViewHolder(FloorDrawingItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
