package io.xiges.ximapper.adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.ui.documents.BuildingDetailListFragment;
import io.xiges.ximapper.ui.documents.LandInfoFragment;
import io.xiges.ximapper.ui.documents.OtherConstructionListFragment;
import io.xiges.ximapper.ui.documents.SignaturesFragment;

public class DocumentFragmentAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 4;
    private List<String> tabTitles;
    private List<Fragment> fragments;
    private Context context;

    public DocumentFragmentAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        tabTitles = new ArrayList<>();
        tabTitles.add(context.getResources().getString(R.string.tab_label_landInfo));
        tabTitles.add(context.getResources().getString(R.string.tab_label_buildingInfo));
        tabTitles.add(context.getResources().getString(R.string.tab_label_otherConstruction));
      //  tabTitles.add(context.getResources().getString(R.string.tab_label_ownerDetails));
        tabTitles.add(context.getResources().getString(R.string.tab_label_signature));

        fragments = new ArrayList<>();
        fragments.add(new LandInfoFragment());
        fragments.add(new BuildingDetailListFragment());
        fragments.add(new OtherConstructionListFragment());
        //fragments.add(new OwnerDetailListFragment());
        fragments.add(new SignaturesFragment());


    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles.get(position);
    }

}
