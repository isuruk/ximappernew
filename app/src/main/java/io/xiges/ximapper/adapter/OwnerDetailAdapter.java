package io.xiges.ximapper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.OwnerDetailListItemBinding;
import io.xiges.ximapper.model.cr.OwnerDetail;
import io.xiges.ximapper.ui.documents.RecyclerViewClickListener;

public class OwnerDetailAdapter  extends RecyclerView.Adapter<OwnerDetailAdapter.ODetailViewHolder>{

    private List<OwnerDetail> details;
    private Context context;
    private RecyclerViewClickListener listener;

    public OwnerDetailAdapter(List<OwnerDetail> details, Context context, RecyclerViewClickListener listener) {
        this.details = details;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public OwnerDetailAdapter.ODetailViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                                int viewType) {
        OwnerDetailListItemBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.owner_detail_list_item, viewGroup, false);
        return new OwnerDetailAdapter.ODetailViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OwnerDetailAdapter.ODetailViewHolder holder, int position) {
        holder.binding.setData(details.get(position));
    }


    public void setDetails(List<OwnerDetail> details) {
        this.details = details;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return this.details.size();
    }

    class ODetailViewHolder extends RecyclerView.ViewHolder {

        OwnerDetailListItemBinding binding;

        ODetailViewHolder(OwnerDetailListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v -> listener.onItemClick(getAdapterPosition()));

            binding.getRoot().setOnLongClickListener(v -> {
                listener.onLongItemClick(getAdapterPosition());
                return true;
                    }
);
        }
    }
}
