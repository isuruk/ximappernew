package io.xiges.ximapper.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.ImageListItemBinding;
import io.xiges.ximapper.ui.images.ImageGalleryActivity;

public class ImageGalleryViewAdapter extends RecyclerView.Adapter<ImageGalleryViewAdapter.ImageGalleryViewHolder>{
    private List<String> images;
    private Context context;

    public ImageGalleryViewAdapter(List<String> images, Context context) {
        this.images = images;
        this.context = context;
    }

    @Override
    public ImageGalleryViewAdapter.ImageGalleryViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                           int viewType) {
        ImageListItemBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.image_list_item, viewGroup, false);
        return new ImageGalleryViewAdapter.ImageGalleryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageGalleryViewAdapter.ImageGalleryViewHolder holder, int position) {
        String imagePath = this.images.get(position);

        Glide.with(context).load(imagePath)
                .into(holder.binding.imgGalleryItem);


    }



    public void setImages(List<String> images) {
        this.images = images;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return this.images.size();
    }

    class ImageGalleryViewHolder extends RecyclerView.ViewHolder {

        ImageListItemBinding binding;

        ImageGalleryViewHolder(ImageListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.imgGalleryItem.setOnClickListener(v -> ((ImageGalleryActivity) context).onImageSelected(images.get(getAdapterPosition()),getAdapterPosition()));
            binding.imgGalleryItem.setOnLongClickListener(v -> ((ImageGalleryActivity) context).onImageLongClicked(images.get(getAdapterPosition()),getAdapterPosition()));

        }
    }

}
