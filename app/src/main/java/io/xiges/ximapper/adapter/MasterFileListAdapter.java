package io.xiges.ximapper.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.List;

import io.xiges.ximapper.BuildConfig;
import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.MasterFileListViewBinding;
import io.xiges.ximapper.model.Grid;
import io.xiges.ximapper.model.api.MasterFileListResponse;
import io.xiges.ximapper.network.ApiInterface;
import io.xiges.ximapper.ui.master_file.MasterFileGetActivity;
import io.xiges.ximapper.util.Decompress;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MasterFileListAdapter extends RecyclerView.Adapter<MasterFileListAdapter.MasterFileListViewHolder> {

    private List<MasterFileListResponse> masterFiles;
    private Context context;
    private ImportClickedClickedListener importClickedClickedListener;
    private ApiInterface apiInterface;


    public MasterFileListAdapter(List<MasterFileListResponse> masterFiles, Context context, ImportClickedClickedListener importClickedClickedListener, ApiInterface apiInterface) {
        this.masterFiles = masterFiles;
        this.context = context;
        this.importClickedClickedListener = importClickedClickedListener;
        this.apiInterface = apiInterface;
    }


    @Override
    public MasterFileListAdapter.MasterFileListViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                             int viewType) {
        MasterFileListViewBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.master_file_list_view, viewGroup, false);
        return new MasterFileListAdapter.MasterFileListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MasterFileListViewHolder holder, int position) {
        MasterFileListResponse masterFile = masterFiles.get(position);


        if(masterFile.isAvailability()){
            holder.binding.btnDownload.setVisibility(View.INVISIBLE);
            holder.binding.btnImport.setVisibility(View.VISIBLE);
        }

        holder.binding.setData(masterFile);

        holder.binding.executePendingBindings();

    }

    @Override
    public void onViewRecycled(MasterFileListAdapter.MasterFileListViewHolder viewHolder) {
        Glide.with(viewHolder.itemView.getContext()).clear(viewHolder.itemView);
    }

    public void setMasterFiles(List<MasterFileListResponse> masterFiles) {
        this.masterFiles = masterFiles;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return masterFiles.size();
    }


    class MasterFileListViewHolder extends RecyclerView.ViewHolder {

        MasterFileListViewBinding binding;

        public MasterFileListViewHolder(MasterFileListViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.btnDownload.setOnClickListener(v -> {
                //    getGrid(binding.getData().getMasterFileName(),binding.getData().bBoxToString());
                 PRDownloader.download(BuildConfig.API_URL+"lamasterfile/details?mfid="+masterFiles.get(getAdapterPosition()).getMasterFileId()+"", Environment.getExternalStorageDirectory() +
                        File.separator + "Maps" + File.separator+ "Downloads" + File.separator, binding.getData().getMasterFileRefNumber().replaceAll("/","")+".zip")
                        .setHeader("Authorization","Bearer "+((MasterFileGetActivity) context).getPrefs().getString(Constant.ACCESS_TOKEN, ""))
                        .build()
                        .setOnStartOrResumeListener(() -> {

                        })
                        .setOnPauseListener(() -> {

                        })
                        .setOnCancelListener(() -> {

                        })
                        .setOnProgressListener(progress -> {
                            binding.progressBar.setVisibility(View.VISIBLE);
                            binding.btnDownload.setVisibility(View.INVISIBLE);
                            int p = (int) ((progress.currentBytes/ progress.totalBytes) * 100);
                            binding.progressBar.setProgress(p);
                        })
                        .start(new OnDownloadListener() {
                            @Override
                            public void onDownloadComplete() {

                                binding.tvExtracting.setVisibility(View.VISIBLE);

                                binding.progressBar.setVisibility(View.GONE);
                                String zipFile =Environment.getExternalStorageDirectory() +
                                        File.separator + "Maps" + File.separator+ "Downloads" + File.separator+binding.getData().getMasterFileRefNumber().replaceAll("/","")+".zip";
                                String unzipLocation = Environment.getExternalStorageDirectory() +
                                        File.separator + "Maps" + File.separator+ "Files" + File.separator + binding.getData().getMasterFileRefNumber().replaceAll("/","")+ File.separator;


                                 new AsyncTask<File, Void, Void>() {

                                    protected Void doInBackground(File... files) {
                                        Decompress d = new Decompress(zipFile, unzipLocation);
                                        d.unzip();
                                        return null;
                                    }

                                    protected void onPostExecute(Void result) {
                                        MasterFileListResponse mp = binding.getData();
                                           ((MasterFileGetActivity) context).addMasterFile(mp);
                                        binding.tvExtracting.setVisibility(View.GONE);
                                        binding.btnImport.setVisibility(View.VISIBLE);
                                    }
                                }.execute();
                            }

                            @Override
                            public void onError(Error error) {

                            }

                        });
            });

            binding.btnImport.setOnClickListener(v ->{
                importClickedClickedListener.onImportClicked(binding.getData());
            });
        }



        }


    public interface ImportClickedClickedListener{
        void onImportClicked(MasterFileListResponse masterFile);
    }

    private void getGrid(String masterFileName,String bBox){
        apiInterface.getGrid(BuildConfig.W3_KEY, bBox,"geojson").enqueue(new Callback<Grid>() {
            @Override
            public void onResponse(Call<Grid> call, Response<Grid> response) {
                if (response.isSuccessful()) {

                    File folder = new File( Environment.getExternalStorageDirectory() +
                            File.separator + "Maps" + File.separator+ "Files" + File.separator+masterFileName );

                    if (!folder.exists()) {
                        folder.mkdirs();
                    }

                    try {
                        Writer writer = new FileWriter(Environment.getExternalStorageDirectory() +
                                File.separator + "Maps" + File.separator+ "Files" + File.separator+masterFileName + File.separator+ "grid.geojson");
                        Gson gson = new GsonBuilder().create();

                        gson.toJson(response.body(), writer);
                        writer.flush();
                        writer.close();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Grid> call, Throwable t) {
                Log.e("SDA",t.getMessage());
            }
        });
    }
}
