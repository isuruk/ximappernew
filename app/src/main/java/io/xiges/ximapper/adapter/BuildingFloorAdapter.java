package io.xiges.ximapper.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.BuildingFloorItemBinding;
import io.xiges.ximapper.model.BuildingFloor;
import io.xiges.ximapper.ui.map.MapViewActivity;

public class BuildingFloorAdapter extends RecyclerView.Adapter<BuildingFloorAdapter.BuildingFloorViewHolder> {

    private List<BuildingFloor> floors;
    private Context context;

    public BuildingFloorAdapter(List<BuildingFloor> floors, Context context) {
        this.floors = floors;
        this.context = context;
    }

    @Override
    public BuildingFloorAdapter.BuildingFloorViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                               int viewType) {
        BuildingFloorItemBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.building_floor_item, viewGroup, false);
        return new BuildingFloorAdapter.BuildingFloorViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BuildingFloorAdapter.BuildingFloorViewHolder holder, int position) {
        BuildingFloor floors = this.floors.get(position);
        holder.binding.setFloor(floors);
    }


    public void setFloors(List<BuildingFloor> floors) {
        this.floors = floors;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return this.floors.size();
    }

    class BuildingFloorViewHolder extends RecyclerView.ViewHolder {

        BuildingFloorItemBinding binding;

        BuildingFloorViewHolder(BuildingFloorItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;


            BuildingFloor bottomFloor = null;

            if(getAdapterPosition()>0){
                bottomFloor = new BuildingFloor();
                bottomFloor = floors.get(getAdapterPosition()-1);
            }

          binding.btnFloor.setOnClickListener(v -> ((MapViewActivity) context).onFloorSelected(floors.get(getAdapterPosition()),getAdapterPosition(),(getAdapterPosition()>0) ? floors.get(getAdapterPosition()-1) : null,floors ));
        }
    }
}
