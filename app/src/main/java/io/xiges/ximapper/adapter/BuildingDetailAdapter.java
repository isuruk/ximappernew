package io.xiges.ximapper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.BuildingDetailListItemBinding;
import io.xiges.ximapper.model.cr.Building;
import io.xiges.ximapper.ui.documents.RecyclerViewClickListener;

public class BuildingDetailAdapter extends RecyclerView.Adapter<BuildingDetailAdapter.BuildingDetailViewHolder> {

    private List<Building> details;
    private Context context;
    private RecyclerViewClickListener listener;

    public BuildingDetailAdapter( List<Building> details,Context context,RecyclerViewClickListener listener) {
        this.details = details;
        this.context = context;
        this.listener = listener;

    }

    @Override
    public BuildingDetailAdapter.BuildingDetailViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                           int viewType) {
        BuildingDetailListItemBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.building_detail_list_item, viewGroup, false);
        return new BuildingDetailAdapter.BuildingDetailViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BuildingDetailAdapter.BuildingDetailViewHolder holder, int position) {
        holder.binding.setData(details.get(position));
    }


    public void setDetails(List<Building> details) {
        this.details = details;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return this.details.size();
    }

    class BuildingDetailViewHolder extends RecyclerView.ViewHolder {

        BuildingDetailListItemBinding binding;

        BuildingDetailViewHolder(BuildingDetailListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v -> listener.onItemClick(getAdapterPosition()));
        }
    }
}

