package io.xiges.ximapper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.LotListItemBinding;
import io.xiges.ximapper.model.mixin_clone_model.LotMixinContent;
import io.xiges.ximapper.ui.lot.LotListActivity;

public class LotListAdapter extends RecyclerView.Adapter<LotListAdapter.LotListViewHolder> {

    private List<LotMixinContent> lots;
    private Context context;


    public LotListAdapter(List<LotMixinContent> lots, Context context) {
        this.lots = lots;
        this.context = context;
    }


    @Override
    public LotListAdapter.LotListViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                               int viewType) {
        LotListItemBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.lot_list_item, viewGroup, false);
        return new LotListAdapter.LotListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull LotListViewHolder holder, int position) {
        LotMixinContent lot = lots.get(position);
        holder.binding.setLot(lot);
    }


    public void setLots(List<LotMixinContent> lots) {
        this.lots = lots;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return lots.size();
    }


    class LotListViewHolder extends RecyclerView.ViewHolder {

        LotListItemBinding binding;

        LotListViewHolder(LotListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.clLotListItem.setOnClickListener(v -> ((LotListActivity) context).onLotClicked(lots.get(getAdapterPosition())));
        }
    }

}

