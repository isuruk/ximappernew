package io.xiges.ximapper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.PrListItemBinding;
import io.xiges.ximapper.model.cr.OwnerDetail;
import io.xiges.ximapper.model.cr.ProposedRate;
import io.xiges.ximapper.ui.documents.RecyclerViewClickListener;


public class ProposedRatesAdapter  extends RecyclerView.Adapter<ProposedRatesAdapter.ProposedRatesHolder>{

    private List<ProposedRate> details;
    private Context context;
    private RecyclerViewClickListener listener;

    public ProposedRatesAdapter(List<ProposedRate> details, Context context, RecyclerViewClickListener listener) {
        this.details = details;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ProposedRatesAdapter.ProposedRatesHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                   int viewType) {
        PrListItemBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.pr_list_item, viewGroup, false);
        return new ProposedRatesAdapter.ProposedRatesHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProposedRatesAdapter.ProposedRatesHolder holder, int position) {
        holder.binding.setData(details.get(position));
    }


    public void setDetails(List<ProposedRate> details) {
        this.details = details;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return this.details.size();
    }

    class ProposedRatesHolder extends RecyclerView.ViewHolder {

       PrListItemBinding binding;

        ProposedRatesHolder(PrListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v -> listener.onItemClick(getAdapterPosition()));
        }
    }
}
