package io.xiges.ximapper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.LayerControllerListItemViewBinding;
import io.xiges.ximapper.model.MapLayer;

public class MapLayerAdapter extends RecyclerView.Adapter<MapLayerAdapter.MapLayerViewHolder> {

    private List<MapLayer> details;
    private Context context;

    public MapLayerAdapter(List<MapLayer> details, Context context) {
        this.details = details;
        this.context = context;

    }

    @Override
    public MapLayerAdapter.MapLayerViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                             int viewType) {
        LayerControllerListItemViewBinding binding = DataBindingUtil.inflate(LayoutInflater
                .from(viewGroup.getContext()), R.layout.layer_controller_list_item_view, viewGroup, false);
        return new MapLayerAdapter.MapLayerViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MapLayerAdapter.MapLayerViewHolder holder, int position) {
        holder.binding.setData(details.get(position));
    }


    public void setDetails(List<MapLayer> details) {
        this.details = details;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return this.details.size();
    }

    class MapLayerViewHolder extends RecyclerView.ViewHolder {

        LayerControllerListItemViewBinding binding;

        MapLayerViewHolder(LayerControllerListItemViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}