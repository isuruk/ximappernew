package io.xiges.ximapper.util.gson_instance;

import com.google.gson.InstanceCreator;
import com.mapbox.geojson.Feature;

import java.lang.reflect.Type;

public class FeatureInstanceCreator implements InstanceCreator<Feature> {
    String json;

    public FeatureInstanceCreator(String json) {
        this.json = json;
    }

    @Override
    public Feature createInstance(Type type) {
        return Feature.fromJson(json);
    }
}
