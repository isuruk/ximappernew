package io.xiges.ximapper.util;

import com.mapbox.mapboxsdk.geometry.LatLng;

public class DistanceToLocationCalculator {
double r = 250827125.9843; //Radius of the Earth

    public LatLng calculateLatLangFromDistance(double brng, double distance, LatLng latLng){

        double lat1 = Math.toRadians(latLng.getLatitude());
        double long1 = Math.toRadians(latLng.getLongitude());
        double br = Math.toRadians(brng);


       double lat2 = Math.asin(Math.sin(lat1)*Math.cos(distance/r) +
               Math.cos(lat1)* Math.sin(distance/r)* Math.cos(br));

       double long2 = long1 + Math.atan2(Math.sin(br)*Math.sin(distance/r)*Math.cos(lat1),
               Math.cos(distance/r)-Math.sin(lat1)*Math.sin(lat2));

        lat2 = Math.toDegrees(lat2);
        long2 = Math.toDegrees(long2);

       LatLng latLng1 = new LatLng();
       latLng1.setLongitude(long2);
       latLng1.setLatitude(lat2);

       return latLng1;
    }
}
