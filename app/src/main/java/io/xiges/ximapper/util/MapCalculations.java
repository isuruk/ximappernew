package io.xiges.ximapper.util;

import com.google.maps.android.SphericalUtil;
import com.mapbox.geojson.Point;

import java.math.BigDecimal;

public class MapCalculations {
    public static double calculateDistance(com.mapbox.geojson.Point firstPoint, Point secondPoint){
       double distance = SphericalUtil.computeDistanceBetween(new com.google.android.gms.maps.model.LatLng(firstPoint.latitude(),firstPoint.longitude()),new com.google.android.gms.maps.model.LatLng(secondPoint.latitude(),secondPoint.longitude()));
        return distance;
    }

    /*public static String calculateFeetArea(BigDecimal area){
        M
    }*/
}
