package io.xiges.ximapper.util;

import java.util.Random;

public class ColorManager {

    public String getRandomColor(){
        Random random = new Random(0xffffff + 1);
        String colorCode = String.format("#%06x", random);
        return colorCode;
    }
}
