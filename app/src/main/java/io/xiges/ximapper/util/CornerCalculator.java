package io.xiges.ximapper.util;

public class CornerCalculator {

    private double sin(int y, double theta){
        double calc = (1.0/theta)*y;
        double out = Math.toDegrees(Math.asin(calc));
        return out;
    }
    public double  lineLength(int x, int y){
        double p = Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
        return p;

    }
    public double getBearing(int x, int y, int side){
        double p = Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
        double d =sin(y,p);

        double bearing = 0.0;
        switch (side){
            case 1:
                bearing = 360 - d;
                break;
            case 2:
                bearing = 90 - d;
                break;
            case 3:
                bearing = 180 - d;
                break;
            case 4:
                bearing = 270 - d;
                break;
        }

        return bearing;

    }
}
