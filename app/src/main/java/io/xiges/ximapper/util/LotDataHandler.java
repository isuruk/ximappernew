package io.xiges.ximapper.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;

import java.util.ArrayList;
import java.util.List;

import io.xiges.ximapper.model.BuildingFloor;
import io.xiges.ximapper.model.Circle;
import io.xiges.ximapper.model.Construction;
import io.xiges.ximapper.model.Construction_Type;
import io.xiges.ximapper.model.DrawingType;
import io.xiges.ximapper.model.FloorDrawing;
import io.xiges.ximapper.model.Lot;
import io.xiges.ximapper.model.mixin_clone_model.BuildingFloorMixinContent;
import io.xiges.ximapper.model.mixin_clone_model.ConstructionMixInContent;
import io.xiges.ximapper.model.mixin_clone_model.FeatureMixinContent;
import io.xiges.ximapper.model.mixin_clone_model.FloorDrawingMixInContent;
import io.xiges.ximapper.model.mixin_clone_model.LotMixinContent;
import io.xiges.ximapper.model.mixin_clone_model.PointMixinContent;

public class LotDataHandler {
    public Lot getLotData(LotMixinContent data){


        Gson gson = new GsonBuilder()
                .create();

        Lot lot = new Lot();
        lot.setLotName(data.getLotName());
        lot.setLotId(data.getLotId());
        lot.setSymbolList(data.getSymbolList());
        lot.setFloorLabels(data.getFloorLabels());
        List<Point> lineLayerPointList = new ArrayList<>();
        for (PointMixinContent pointMixinContent :data.getLineLayerPointList()){
            String json = gson.toJson(pointMixinContent);
            lineLayerPointList.add(Point.fromJson(json));
        }
        lot.setLineLayerPointList(lineLayerPointList);


        List<Point>  fillLayerPointList= new ArrayList<>();
        for (PointMixinContent pointMixinContent :data.getFillLayerPointList()){
            String json = gson.toJson(pointMixinContent);
            fillLayerPointList.add(Point.fromJson(json));
        }
        lot.setFillLayerPointList(fillLayerPointList);

        List<Feature>  circleLayerFeatureList= new ArrayList<>();
        for (FeatureMixinContent featureMixinContent :data.getCircleLayerFeatureList()){
            String json = gson.toJson(featureMixinContent);
            circleLayerFeatureList.add(Feature.fromJson(json));
        }
        lot.setCircleLayerFeatureList(circleLayerFeatureList);


                    List<Construction> constructions = new ArrayList<>();
                    for (ConstructionMixInContent construction :data.getConstructions()) {
                        if (construction.getType() != null) {
                            if (construction.getType().equals(Construction_Type.Building.name())) {
                                Construction building = new Construction();
                                building.setId(construction.getId());
                                building.setName(construction.getName());
                                building.setDrawingType(construction.getDrawingType());
                                building.setCircleSourceID(construction.getCircleSourceID());
                                building.setCircleLayerID(construction.getCircleLayerID());
                                building.setLineSourceID(construction.getLineSourceID());
                                building.setLineLayerID(construction.getLineLayerID());
                                building.setFillSourceID(construction.getFillSourceID());
                                building.setFillLayerID(construction.getFillLayerID());
                                building.setType(construction.getType());
                                building.setBuildingLabels(construction.getBuildingLabels());
                                List<Point> lineLayerConstructPointList = new ArrayList<>();
                                for (PointMixinContent pointMixinContent : construction.getLineLayerPointList()) {
                                    String json = gson.toJson(pointMixinContent);
                                    lineLayerConstructPointList.add(Point.fromJson(json));
                                }
                                building.setLineLayerPointList(lineLayerConstructPointList);


                                List<Point> fillLayerConstructPointList = new ArrayList<>();
                                for (PointMixinContent pointMixinContent : construction.getFillLayerPointList()) {
                                    String json = gson.toJson(pointMixinContent);
                                    fillLayerConstructPointList.add(Point.fromJson(json));
                                }
                                building.setFillLayerPointList(fillLayerConstructPointList);

                                List<Feature> circleLayerConstructFeatureList = new ArrayList<>();
                                for (FeatureMixinContent featureMixinContent : construction.getCircleLayerFeatureList()) {
                                    String json = gson.toJson(featureMixinContent);
                                    circleLayerConstructFeatureList.add(Feature.fromJson(json));
                                }
                                building.setCircleLayerFeatureList(circleLayerConstructFeatureList);





                                if(construction.getDrawingType().equals(DrawingType.CIRCLE.name())){
                                    if(construction.getCircle()!=null){
                                        Circle circle = new Circle();
                                        circle.setRadius(construction.getCircle().getRadius());
                                        circle.setUnit(construction.getCircle().getUnit());

                                        if(construction.getCircle().getMidPoint()!=null){
                                            String json = gson.toJson(construction.getCircle().getMidPoint());
                                            circle.setMidPoint(Point.fromJson(json));
                                        }
                                        building.setCircle(circle);

                                    }
                                }
                                List<BuildingFloor> buildingFloors = new ArrayList<>();
                                for (BuildingFloorMixinContent floorMixinContent : construction.getBuildingFloorList()){
                                    BuildingFloor floor = new BuildingFloor();
                                    floor.setFloorNo(floorMixinContent.getFloorNo());
                                    floor.setSymbols(floorMixinContent.getSymbols());
                                    List<FloorDrawing> floorDrawings = new ArrayList<>();
                                    for (FloorDrawingMixInContent drawingMixInContent : floorMixinContent.getFloorDrawingList()){
                                        FloorDrawing drawing = new FloorDrawing();
                                        drawing.setDrawingType(drawingMixInContent.getDrawingType());

                                        if(drawing.getDrawingType().equals(DrawingType.AQLINE.name())){

                                            drawing.setAqCircleLayerID(drawingMixInContent.getAqCircleLayerID());
                                            drawing.setAqCircleSourceID(drawingMixInContent.getAqCircleSourceID());
                                            drawing.setAqFillLayerID(drawingMixInContent.getAqFillLayerID());
                                            drawing.setAqFillSourceID(drawingMixInContent.getAqFillSourceID());
                                            drawing.setAqLineLayerID(drawingMixInContent.getAqLineLayerID());
                                            drawing.setAqLineSourceID(drawingMixInContent.getAqLineSourceID());

                                            List<Point> lineLayerAQPointList = new ArrayList<>();
                                            for (PointMixinContent pointMixinContent : drawingMixInContent.getLineLayerAQPointList()) {
                                                String json = gson.toJson(pointMixinContent);
                                                lineLayerAQPointList.add(Point.fromJson(json));
                                            }
                                            drawing.setLineLayerAQPointList(lineLayerAQPointList);



                                            List<Point> fillLayerAQPointList = new ArrayList<>();
                                            for (PointMixinContent pointMixinContent : drawingMixInContent.getFillLayerAQPointList()) {
                                                String json = gson.toJson(pointMixinContent);
                                                fillLayerAQPointList.add(Point.fromJson(json));
                                            }
                                            drawing.setFillLayerAQPointList(fillLayerAQPointList);

                                            List<Feature> circleLayerAQFeatureList = new ArrayList<>();
                                            for (FeatureMixinContent featureMixinContent : drawingMixInContent.getCircleLayerAQFeatureList()) {
                                                String json = gson.toJson(featureMixinContent);
                                                circleLayerAQFeatureList.add(Feature.fromJson(json));
                                            }
                                            drawing.setCircleLayerAQFeatureList(circleLayerAQFeatureList);
                                        }else {


                                            drawing.setCircleLayerID(drawingMixInContent.getCircleLayerID());
                                            drawing.setCircleSourceID(drawingMixInContent.getCircleSourceID());

                                            drawing.setLineLayerID(drawingMixInContent.getLineLayerID());
                                            drawing.setLineSourceID(drawingMixInContent.getLineSourceID());

                                            drawing.setFillLayerID(drawingMixInContent.getFillLayerID());
                                            drawing.setFillSourceID(drawingMixInContent.getFillSourceID());

                                            List<Point> lineLayerFloorPointList = new ArrayList<>();
                                            for (PointMixinContent pointMixinContent : drawingMixInContent.getLineLayerPointList()) {
                                                String json = gson.toJson(pointMixinContent);
                                                lineLayerFloorPointList.add(Point.fromJson(json));
                                            }
                                            drawing.setLineLayerPointList(lineLayerFloorPointList);


                                            List<Point> fillLayerFloorPointList = new ArrayList<>();
                                            for (PointMixinContent pointMixinContent : drawingMixInContent.getFillLayerPointList()) {
                                                String json = gson.toJson(pointMixinContent);
                                                fillLayerFloorPointList.add(Point.fromJson(json));
                                            }
                                            drawing.setFillLayerPointList(fillLayerFloorPointList);

                                            List<Feature> circleLayerFloorFeatureList = new ArrayList<>();
                                            for (FeatureMixinContent featureMixinContent : drawingMixInContent.getCircleLayerFeatureList()) {
                                                String json = gson.toJson(featureMixinContent);
                                                circleLayerFloorFeatureList.add(Feature.fromJson(json));
                                            }
                                            drawing.setCircleLayerFeatureList(circleLayerFloorFeatureList);


                                            if (drawingMixInContent.getDrawingType().equals(DrawingType.CIRCLE.name())) {
                                                if (drawingMixInContent.getCircle() != null) {
                                                    Circle circle = new Circle();
                                                    circle.setRadius(drawingMixInContent.getCircle().getRadius());
                                                    circle.setUnit(drawingMixInContent.getCircle().getUnit());

                                                    if (drawingMixInContent.getCircle().getMidPoint() != null) {
                                                        String json = gson.toJson(drawingMixInContent.getCircle().getMidPoint());
                                                        circle.setMidPoint(Point.fromJson(json));
                                                    }
                                                    drawing.setCircle(circle);

                                                }
                                            }

                                        }
                                        floorDrawings.add(drawing);
                                    }
                                    floor.setFloorDrawingList(floorDrawings);
                                    buildingFloors.add(floor);
                                }
                                building.setBuildingFloorList(buildingFloors);
                                constructions.add(building);
                            } else if (construction.getType().equals(Construction_Type.Other.name())) {
                                Construction other_construction = new Construction();
                                other_construction.setType(construction.getType());
                                other_construction.setName(construction.getName());
                                other_construction.setDrawingType(construction.getDrawingType());
                                other_construction.setFillLayerID(construction.getFillLayerID());
                                other_construction.setFillSourceID(construction.getFillSourceID());
                                other_construction.setCircleLayerID(construction.getCircleLayerID());
                                other_construction.setCircleSourceID(construction.getCircleSourceID());
                                other_construction.setLineLayerID(construction.getLineLayerID());
                                other_construction.setLineSourceID(construction.getLineSourceID());
                                other_construction.setBuildingLabels(construction.getBuildingLabels());


                                List<Point> lineLayerConstructPointList = new ArrayList<>();
                                for (PointMixinContent pointMixinContent : construction.getLineLayerPointList()) {
                                    String json = gson.toJson(pointMixinContent);
                                    lineLayerConstructPointList.add(Point.fromJson(json));
                                }
                                other_construction.setLineLayerPointList(lineLayerConstructPointList);


                                List<Point> fillLayerConstructPointList = new ArrayList<>();
                                for (PointMixinContent pointMixinContent : construction.getFillLayerPointList()) {
                                    String json = gson.toJson(pointMixinContent);
                                    fillLayerConstructPointList.add(Point.fromJson(json));
                                }
                                other_construction.setFillLayerPointList(fillLayerConstructPointList);

                                List<Feature> circleLayerConstructFeatureList = new ArrayList<>();
                                for (FeatureMixinContent featureMixinContent : construction.getCircleLayerFeatureList()) {
                                    String json = gson.toJson(featureMixinContent);
                                    circleLayerConstructFeatureList.add(Feature.fromJson(json));
                                }
                                other_construction.setCircleLayerFeatureList(circleLayerConstructFeatureList);


                               if(construction.getDrawingType().equals(DrawingType.CIRCLE.name())){
                                   if(construction.getCircle()!=null){
                                       Circle circle = new Circle();
                                       circle.setRadius(construction.getCircle().getRadius());
                                       circle.setUnit(construction.getCircle().getUnit());

                                       if(construction.getCircle().getMidPoint()!=null){
                                           String json = gson.toJson(construction.getCircle().getMidPoint());
                                           circle.setMidPoint(Point.fromJson(json));
                                       }
                                       other_construction.setCircle(circle);

                                   }
                               }

                                constructions.add(other_construction);

                            } else {
                            }
                        }
                        }

                    lot.setConstructions(constructions);

        return lot;

    }

}
