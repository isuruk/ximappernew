package io.xiges.ximapper.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.model.api.MasterFileListResponse;
import io.xiges.ximapper.model.api.master_data.MasterData;

public class SharedPreferencesHandler {
    @Inject
    SharedPreferences.Editor spEditor;



    //save SharedPreferences
    public void saveIntoSharedPreferences(Context context, String token, String refresh){
        ((XimapperApplication) context.getApplicationContext()).getAppComponent().inject(this);

        spEditor.putString(Constant.ACCESS_TOKEN, token);
        spEditor.putString(Constant.REFRESH_TOKEN, refresh);
        spEditor.apply();
    }

    //save SharedPreferences
    public void saveMasterSharedPreferences(Context context, MasterData masterData){
        ((XimapperApplication) context.getApplicationContext()).getAppComponent().inject(this);

        Gson gson = new Gson();
        String json = gson.toJson(masterData);
        spEditor.putString(Constant.MASTER_DATA, json);
        spEditor.apply();
    }

    //save SharedPreferences
    public void saveMasterFileSharedPreferences(Context context, List<MasterFileListResponse> masterData){
        ((XimapperApplication) context.getApplicationContext()).getAppComponent().inject(this);

        Gson gson = new Gson();
        String json = gson.toJson(masterData);
        spEditor.putString(Constant.MASTER_FILE, json);
        spEditor.apply();
    }

    public void saveConfigSharedPreferences(Context context, String config){
        ((XimapperApplication) context.getApplicationContext()).getAppComponent().inject(this);
        spEditor.putString(Constant.CONFIG, config);
        spEditor.apply();
    }

    //clear SharedPreferences
    public void clearSharedPreferences(Context context){
        ((XimapperApplication) context.getApplicationContext()).getAppComponent().inject(this);
        spEditor.clear();
        spEditor.commit();
    }

    public void logoutClearSharedPref(Context context){
        ((XimapperApplication) context.getApplicationContext()).getAppComponent().inject(this);
        spEditor.remove(Constant.ACCESS_TOKEN);
        spEditor.remove(Constant.REFRESH_TOKEN);
        spEditor.commit();
    }

}