package io.xiges.ximapper.util;


import java.io.IOException;
import java.lang.annotation.Annotation;

import io.xiges.ximapper.model.APIError;
import io.xiges.ximapper.model.ErrorBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {

    /**
     * convert HTTP Error responce into APIError
     *
     * @param response
     * @return
     */
    public static ErrorBody parseAPIError(Response<?> response) {
        Converter<ResponseBody, ErrorBody> converter =
                ServiceGenerator.getClient()
                        .responseBodyConverter(ErrorBody.class, new Annotation[0]);

        ErrorBody error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ErrorBody();
        }

        return error;
    }


    public static APIError parseError(Response<?> response) {
        Converter<ResponseBody, APIError> converter =
                ServiceGenerator.getClient()
                        .responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIError();
        }

        return error;
    }

}
