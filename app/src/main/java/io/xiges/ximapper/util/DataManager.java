package io.xiges.ximapper.util;

import android.os.Environment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.xiges.ximapper.model.Lot;
import io.xiges.ximapper.model.Marker;
import io.xiges.ximapper.model.cr.CR;

public class DataManager {
    public void writeToFile(Lot lot, String name) throws IOException {
        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "Maps" + File.separator + "Land" + File.separator + name + File.separator + lot.getLotId() + File.separator);

        if (!folder.exists()) {
            folder.mkdirs();
        }

        Date date = new Date();
        long time = date.getTime();

        Writer writer = new FileWriter(Environment.getExternalStorageDirectory() +
                File.separator + "Maps" + File.separator + "Land" + File.separator + name + File.separator + lot.getLotId()+".json");
        Gson gson = new GsonBuilder().create();
        gson.toJson(lot, writer);
        writer.flush();
        writer.close();
    }


    public void writeSymbolToFile(List<Marker> markers, String masterfile, Lot lot) throws IOException {
        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "Maps" + File.separator + "Files" + File.separator + masterfile + File.separator + lot.getLotId() + File.separator);

        if (!folder.exists()) {
            folder.mkdirs();
        }

        Writer writer = new FileWriter(Environment.getExternalStorageDirectory() +
                File.separator + "Maps" + File.separator + "Files" + File.separator + masterfile + File.separator + lot.getLotId() + File.separator + "markers.json");
        Gson gson = new GsonBuilder().create();
        gson.toJson(markers, writer);
        writer.flush();
        writer.close();
    }

    public String loadJson(String path, String filename) throws IOException {
// Using this method to load in GeoJSON files from the assets folder.
        File file = new File(Environment.getExternalStorageDirectory() +
                File.separator + "Maps" + File.separator + "Files" + File.separator + path, filename);

        InputStream is = new FileInputStream(file);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        return new String(buffer, "UTF-8");
    }

    public void getLotData() {

    }

    public void saveCRData(CR cr, String landName, String lotName) throws IOException {
        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "Maps" + File.separator + "Land" + File.separator + landName + File.separator + lotName + File.separator  + "cr");

        if (!folder.exists()) {
            folder.getParentFile().mkdirs();
        }

        Writer writer = new FileWriter(Environment.getExternalStorageDirectory() +
                File.separator + "Maps" + File.separator + "Land" + File.separator + landName + File.separator + lotName + File.separator  + "cr"+ File.separator + "cr.json");
        Gson gson = new GsonBuilder().create();
        gson.toJson(cr, writer);
        writer.flush();
        writer.close();
    }

    public CR getCRData(String landName, String lotName) throws IOException {
// Using this method to load in GeoJSON files from the assets folder.
        CR m = new CR();
        try {
            File file = new File(Environment.getExternalStorageDirectory() +
                    File.separator + "Maps" + File.separator + "Land" + File.separator + landName + File.separator + lotName  + File.separator  + "cr" + File.separator + "cr.json");

            file.getParentFile().mkdirs();

            InputStream is = new FileInputStream(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            Gson gson = new Gson();
            m = gson.fromJson(new String(buffer, "UTF-8"), CR.class);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return m;
    }

    public Map<String, String> getLayerFiles(String masterFileName) {
        File f = new File(Environment.getExternalStorageDirectory() +
                File.separator + "Maps" + File.separator + "Files" + File.separator + masterFileName);
        Map<String, String> layerMap = new HashMap<>();
        if (f.isDirectory()) {
            File ff = new File(f.getAbsolutePath());
            for (File files : ff.listFiles()) {
                if (files.isDirectory()) {
                    File f1 = new File(files.getAbsolutePath());
                    for (File f2 : f1.listFiles()) {
                        if (f2.isFile()) {
                            layerMap.put(f2.getName(), f2.getAbsolutePath());
                        }
                    }
                }
            }
        }
        return layerMap;
    }

    public String loadGeoJsonFromAsset(String path) {
// Using this method to load in GeoJSON files from the assets folder.
        try {
            File file = new File(path);

            InputStream is = new FileInputStream(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private void readMasterFile(String path){

    }

    public File getLatestLotFile(String masterFileName,String lotName) {
        String d = Environment.getExternalStorageDirectory() +
                File.separator + "Maps" + File.separator + "Land" + File.separator;
        String path = d + masterFileName;
        File directory = new File(path);
        File[] files = directory.listFiles();
        File f = null;
        long max = 0;
        if (files != null) {
            if (files.length > 0) {
                for (File file : Arrays.asList(files)) {
                    if (!file.isDirectory()) {
                        f = file;
                    }
                }
            }
            return f;
        }else {
            return f;
        }

    }

}
