package io.xiges.ximapper.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import javax.inject.Inject;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.model.OnAPICallback;
import io.xiges.ximapper.model.api.LoginResponse;
import io.xiges.ximapper.network.ApiInterface;
import io.xiges.ximapper.ui.login.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RefreshToken {

    @Inject
    ApiInterface apiInterface;

    @Inject
    SharedPreferences prefs;


    /**
     * This method refresh current access token
     * @param context
     * @param onAPICallback
     */
    public void refreshAccessToken(final Context context, final OnAPICallback onAPICallback){
        ((XimapperApplication) context.getApplicationContext()).getAppComponent().inject(this);
        String refreshToken = prefs.getString(Constant.REFRESH_TOKEN,"");
        if(!refreshToken.equals("")) {
            apiInterface.refreshToken(Constant.GRANT_TYPE_TOKEN,prefs.getString(Constant.REFRESH_TOKEN, ""),"refresh_token").enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                    if (response.isSuccessful()) {
                        String accessToken = response.body().getAccess_token();
                        String refreshToken = response.body().getRefresh_token();

                        new SharedPreferencesHandler().saveIntoSharedPreferences(context, accessToken, refreshToken);


                        onAPICallback.onSuccess();

                    } else {
                        if (response.code()==400){
                            onAPICallback.onFailed(context.getResources().getString(R.string.error_msg_refreshTokenFailed));
                            new SharedPreferencesHandler().logoutClearSharedPref(context);
                            Intent i = new Intent(context, LoginActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK  | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            context.startActivity(i);

                        }else {
                            onAPICallback.onFailed("Something went wrong..!");
                            new SharedPreferencesHandler().logoutClearSharedPref(context);
                            Intent i = new Intent(context, LoginActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK  | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            context.startActivity(i);
                        }
                    }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    onAPICallback.onFailed(t.getMessage());
                    new SharedPreferencesHandler().logoutClearSharedPref(context);
                    Intent i = new Intent(context, LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK  | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(i);

                }
            });
        }else{
            Intent i = new Intent(context, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK  | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(i);
            onAPICallback.onFailed("Invalid user");
        }
    }

}