package io.xiges.ximapper.util;

import com.mapbox.mapboxsdk.geometry.LatLng;

import java.util.ArrayList;
import java.util.List;

public class GridManager {

    private LatLng currentPointOne;
    private LatLng currentPointTwo;

    public List<List<LatLng>> drawGrid(double bbox[]){
        List<List<LatLng>> list;
        LatLng lt1 = new LatLng(bbox[1], bbox[0]);
        LatLng lt2 = new LatLng(bbox[1], bbox[2]);
        LatLng lt3 = new LatLng(bbox[3], bbox[0]);

        list= drawVerticleLines(lt1, lt2, lt3);

        LatLng lt4 = new LatLng(bbox[1], bbox[0]);
        LatLng lt5 = new LatLng(bbox[3], bbox[0]);
        LatLng lt6 = new LatLng(bbox[1], bbox[2]);

        list.addAll((drawHorizontalLines(lt4, lt5, lt6)));

        return  list;
    }

    private List<List<LatLng>> drawVerticleLines(LatLng firstPoint,LatLng secondPoint,LatLng lastPoint){
        List<List<LatLng>> list = new ArrayList<>();
        List<LatLng> latLngs = new ArrayList<>();
        latLngs.add(firstPoint);
        latLngs.add(secondPoint);
        list.add(latLngs);

        currentPointOne = firstPoint;
        currentPointTwo = secondPoint;

        DistanceToLocationCalculator distanceToLocationCalculator = new DistanceToLocationCalculator();
        while(currentPointOne.getLatitude()<=lastPoint.getLatitude()){
            List<LatLng> lat = new ArrayList<>();
            LatLng pointOne =  distanceToLocationCalculator.calculateLatLangFromDistance(0,12,currentPointOne);
            LatLng pointTwo =  distanceToLocationCalculator.calculateLatLangFromDistance(0,12,currentPointTwo);
            currentPointOne = pointOne;
            currentPointTwo= pointTwo;
            lat.add(pointOne);
            lat.add(pointTwo);
            list.add(lat);
        }
        return list;
    }

    private List<List<LatLng>> drawHorizontalLines(LatLng firstPoint,LatLng secondPoint,LatLng lastPoint){
        List<List<LatLng>> list = new ArrayList<>();
        List<LatLng> latLngs = new ArrayList<>();
        latLngs.add(firstPoint);
        latLngs.add(secondPoint);
        list.add(latLngs);

        currentPointOne = firstPoint;
        currentPointTwo = secondPoint;

        DistanceToLocationCalculator distanceToLocationCalculator = new DistanceToLocationCalculator();
        while(currentPointOne.getLongitude()<=lastPoint.getLongitude()){
            List<LatLng> lat = new ArrayList<>();
            LatLng pointOne =  distanceToLocationCalculator.calculateLatLangFromDistance(90,12,currentPointOne);
            LatLng pointTwo =  distanceToLocationCalculator.calculateLatLangFromDistance(90,12,currentPointTwo);
            currentPointOne = pointOne;
            currentPointTwo= pointTwo;
            lat.add(pointOne);
            lat.add(pointTwo);
            list.add(lat);
        }
        return list;
    }
}
