package io.xiges.ximapper.util;

import android.app.AlertDialog;
import android.content.Context;

import io.xiges.ximapper.R;

public class AlertDialogView {
    public void showAlertDialog(Context context, String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(context.getResources().getString(R.string.ad_button_ok), (dialog, id) -> dialog.cancel());


        builder.setTitle(title);
        builder.setMessage(message);

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
