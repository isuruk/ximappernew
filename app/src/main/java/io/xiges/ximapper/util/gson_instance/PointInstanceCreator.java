package io.xiges.ximapper.util.gson_instance;

import com.google.gson.InstanceCreator;
import com.mapbox.geojson.Point;

import java.lang.reflect.Type;

public class PointInstanceCreator implements InstanceCreator<Point> {
   private String json;

    public PointInstanceCreator(String json) {
        this.json = json;
    }

    @Override
    public Point createInstance(Type type) {
     return Point.fromJson(json);
    }
}
