package io.xiges.ximapper.util;

import android.os.Environment;
import android.util.Log;

import java.io.File;

import io.xiges.ximapper.model.BundleInfo;
import io.xiges.ximapper.model.MasterFile;

public class MasterFileManager {
    public BundleInfo getMasterFileData(String fileName){
        String path = Environment.getExternalStorageDirectory() +
                File.separator + "Maps" + File.separator+ "Files" + File.separator + fileName.replaceAll("/","")+ File.separator;
        File directory = new File(path);
        File[] files = directory.listFiles();

        BundleInfo bundleInfo = new BundleInfo();
        if(files.length>0){
            path+=files[0].getName()+ File.separator;
            File data_directory = new File(path);
            File[] data_files = data_directory.listFiles();

            for(File data : data_files){
                if(data.getName().contains("AT")){
                    bundleInfo.setATFile(true);
                    bundleInfo.setAtFilePath(data.getPath());

                }else if(data.getName().contains("PP")){
                    bundleInfo.setPPFile(true);
                    bundleInfo.setPpFilePath(data.getPath());


                }else if(data.getName().contains("CADASTER")){
                    bundleInfo.setCadesterFile(true);
                    bundleInfo.setCadesterFilePath(data.getPath());



                }else if(data.getName().contains("Maste")){
                    bundleInfo.setMasterFile(true);
                    bundleInfo.setMasterFilePath(data.getPath());



                }
            }

        }


        return bundleInfo;
    }


}
