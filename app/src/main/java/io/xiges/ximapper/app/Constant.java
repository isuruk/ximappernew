package io.xiges.ximapper.app;

public interface Constant {
    String MASTER_FILE = "master_file";
    String BUNDLE = "bundle";
    String LOT = "Lot";
    String STYLE_URL = "Style";
    String JSON_FIELD_REGION_NAME = "Json_Field_Region_Name";
    String  REGION_NAME = "Region_Name";
    String NAME = "Name";
    String TYPE = "Type";
    String ID = "id";
    String MARKER = "Marker";
    String MODE = "Mode";
    String BEARING = "Bearing";
    String DISTANCE = "Distance";
    String LINE1 = "LINE1";
    String LINE2 = "LINE2";
    String IMAGE = "Image";
    String DETAILS = "Details";
    String ACCESS_TOKEN = "ACCESS_TOKEN";
    String REFRESH_TOKEN = "REFRESH_TOKEN";
    String GRANT_TYPE_TOKEN = "refresh_token";

    String AT_LAYER = "AT_LAYER";
    String PP_LAYER = "PP_LAYER";
    String CADESTER_LAYER = "CADESTER_LAYER";

    String MAP_REF = "Map_Ref";

    String AT_DATA = "AT_DATA";
    String PP_DATA = "PP_DATA";
    String CADESTER_DATA = "CADESTER_DATA";
    String SHADOW = "SHADOW";

    String MASTER_DATA = "Master_DATA";
    String CONFIG = "Config";

    String AT_TEXT_LAYER = "AT_TEXT_LAYER";
    String AT_TEXT_DATA = "AT_TEXT_DATA";

    String PP_TEXT_LAYER = "PP_TEXT_LAYER";
    String PP_TEXT_DATA = "PP_TEXT_DATA";


    String FLOOR_TYPE = "FLOOR_TYPE";
    String FLOOR_Label = "FLOOR_Label";

}
