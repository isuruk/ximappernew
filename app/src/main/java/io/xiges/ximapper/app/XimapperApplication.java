package io.xiges.ximapper.app;

import android.app.Application;

import io.xiges.ximapper.dagger.AppComponent;
import io.xiges.ximapper.dagger.AppModule;
import io.xiges.ximapper.dagger.DaggerAppComponent;

public class XimapperApplication extends Application {

    private AppComponent appComponent;

    public AppComponent getAppComponent() {

        return appComponent;
    }

    protected AppComponent initDagger(XimapperApplication application){
        return DaggerAppComponent.builder()
                .appModule(new AppModule(application))
                .build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = initDagger(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

    }
}
