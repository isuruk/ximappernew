package io.xiges.ximapper;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import javax.inject.Inject;

import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.dagger.AppComponent;

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity {

    @Inject
    protected T mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initDependencies(((XimapperApplication)getApplication()).getAppComponent());
    }

    protected abstract void initDependencies(AppComponent appComponent);


    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        if (mPresenter != null) {
            mPresenter.attachView(this);
        }

        return super.onCreateView(parent, name, context, attrs);
    }

    @Override
    protected void onDestroy() {
        if (mPresenter != null) {
            mPresenter.detachView();
        }

        super.onDestroy();
    }
}
