package io.xiges.ximapper;

public class BasePresenter<T>
{

    public T mView;

    public void attachView(T view)
    {
        this.mView = view;
    }

    public void detachView()
    {
        if (mView != null)
        {
            mView = null;
        }
    }

    public boolean isViewAttached()
    {
        return mView != null;
    }
}

