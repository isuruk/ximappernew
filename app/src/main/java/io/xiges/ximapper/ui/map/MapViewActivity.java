package io.xiges.ximapper.ui.map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.TooltipCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.MenuCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.google.maps.android.SphericalUtil;
import com.mapbox.android.gestures.MoveGestureDetector;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Geometry;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.geojson.Polygon;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.PolygonOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.geometry.LatLngQuad;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.offline.OfflineManager;
import com.mapbox.mapboxsdk.offline.OfflineRegion;
import com.mapbox.mapboxsdk.plugins.annotation.CircleManager;
import com.mapbox.mapboxsdk.plugins.annotation.CircleOptions;
import com.mapbox.mapboxsdk.plugins.annotation.LineManager;
import com.mapbox.mapboxsdk.plugins.annotation.LineOptions;
import com.mapbox.mapboxsdk.plugins.annotation.Symbol;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerViewManager;
import com.mapbox.mapboxsdk.storage.FileSource;
import com.mapbox.mapboxsdk.style.expressions.Expression;
import com.mapbox.mapboxsdk.style.layers.CircleLayer;
import com.mapbox.mapboxsdk.style.layers.FillLayer;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.RasterLayer;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.style.sources.ImageSource;
import com.mapbox.mapboxsdk.style.sources.Source;
import com.mapbox.turf.TurfConstants;
import com.mapbox.turf.TurfJoins;
import com.mapbox.turf.TurfMeasurement;
import com.mapbox.turf.TurfTransformation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.xiges.ximapper.BaseActivity;
import io.xiges.ximapper.BuildConfig;
import io.xiges.ximapper.R;
import io.xiges.ximapper.adapter.BuildingFloorAdapter;
import io.xiges.ximapper.adapter.FloorItemAdapter;
import io.xiges.ximapper.adapter.FloorLabelListAdapter;
import io.xiges.ximapper.adapter.LabelListAdapter;
import io.xiges.ximapper.adapter.LandBuildingAreaListAdapter;
import io.xiges.ximapper.adapter.MapLayerAdapter;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.dagger.AppComponent;
import io.xiges.ximapper.databinding.ActivityMapViewBinding;
import io.xiges.ximapper.model.BuildingArea;
import io.xiges.ximapper.model.BuildingFloor;
import io.xiges.ximapper.model.BundleInfo;
import io.xiges.ximapper.model.Circle;
import io.xiges.ximapper.model.Construction;
import io.xiges.ximapper.model.Construction_Type;
import io.xiges.ximapper.model.DataMasterFileDetails;
import io.xiges.ximapper.model.DataSave;
import io.xiges.ximapper.model.DrawingType;
import io.xiges.ximapper.model.ErrorBody;
import io.xiges.ximapper.model.FloorCopyDetails;
import io.xiges.ximapper.model.FloorDrawing;
import io.xiges.ximapper.model.LineJson;
import io.xiges.ximapper.model.Lot;
import io.xiges.ximapper.model.MapLayer;
import io.xiges.ximapper.model.Marker;
import io.xiges.ximapper.model.Marker_Type;
import io.xiges.ximapper.model.OnAPICallback;
import io.xiges.ximapper.model.Output;
import io.xiges.ximapper.model.api.MasterFileListResponse;
import io.xiges.ximapper.model.cr.Building;
import io.xiges.ximapper.model.cr.BuildingDetails;
import io.xiges.ximapper.model.cr.CR;
import io.xiges.ximapper.model.cr.OtherConstruction;
import io.xiges.ximapper.model.masterfile.MasterFileData;
import io.xiges.ximapper.model.mixin_clone_model.LotMixinContent;
import io.xiges.ximapper.network.ApiInterface;
import io.xiges.ximapper.ui.alertboxes.ConfirmationDialogActivity;
import io.xiges.ximapper.ui.alertboxes.ErrorBoxActivity;
import io.xiges.ximapper.ui.drawing.CopyLayerActivity;
import io.xiges.ximapper.ui.drawing.FloorAddActivity;
import io.xiges.ximapper.ui.drawing.MeasuredCornerDrawingActivity;
import io.xiges.ximapper.ui.drawing.MeasuredLineDrawingActivity;
import io.xiges.ximapper.ui.drawing.PointMarkerActivity;
import io.xiges.ximapper.ui.images.ImageGalleryActivity;
import io.xiges.ximapper.ui.login.LoginActivity;
import io.xiges.ximapper.ui.lot.ConstructionDetailActivity;
import io.xiges.ximapper.ui.lot.CustomLabelAddActivity;
import io.xiges.ximapper.ui.lot.LotListActivity;
import io.xiges.ximapper.ui.lot.LotSaveActivity;
import io.xiges.ximapper.ui.marker.AddMarkerActivity;
import io.xiges.ximapper.ui.marker.MarkerMenuActivity;
import io.xiges.ximapper.ui.master_file.MasterFileGetActivity;
import io.xiges.ximapper.ui.menu.MenuActivity;
import io.xiges.ximapper.ui.symbol.SymbolController;
import io.xiges.ximapper.util.CornerCalculator;
import io.xiges.ximapper.util.DataManager;
import io.xiges.ximapper.util.DistanceToLocationCalculator;
import io.xiges.ximapper.util.ErrorUtils;
import io.xiges.ximapper.util.FileUtils;
import io.xiges.ximapper.util.GridManager;
import io.xiges.ximapper.util.ImageUtil;
import io.xiges.ximapper.util.LotDataHandler;
import io.xiges.ximapper.util.MapCalculations;
import io.xiges.ximapper.util.NetworkStatus;
import io.xiges.ximapper.util.RefreshToken;
import io.xiges.ximapper.util.SharedPreferencesHandler;
import io.xiges.ximapper.util.SwipeHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mapbox.mapboxsdk.style.layers.Property.ICON_ROTATION_ALIGNMENT_VIEWPORT;
import static com.mapbox.mapboxsdk.style.layers.Property.LINE_JOIN_ROUND;
import static com.mapbox.mapboxsdk.style.layers.Property.NONE;
import static com.mapbox.mapboxsdk.style.layers.Property.VISIBLE;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleRadius;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineJoin;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.visibility;

public class MapViewActivity extends BaseActivity<MapViewPresenter> implements MapViewView, AdapterView.OnItemSelectedListener, OnMapReadyCallback {

    private static final String FREEHAND_DRAW_LINE_LAYER_SOURCE_ID = "FREEHAND_DRAW_LINE_LAYER_SOURCE_ID";
    private static final String MARKER_SYMBOL_LAYER_SOURCE_ID = "MARKER_SYMBOL_LAYER_SOURCE_ID";
    private static final String FREEHAND_DRAW_FILL_LAYER_SOURCE_ID = "FREEHAND_DRAW_FILL_LAYER_SOURCE_ID";
    private static final String FREEHAND_DRAW_LINE_LAYER_ID = "FREEHAND_DRAW_LINE_LAYER_ID";
    private static final String FREEHAND_DRAW_FILL_LAYER_ID = "FREEHAND_DRAW_FILL_LAYER_ID";
    private static final String SEARCH_DATA_SYMBOL_LAYER_ID = "SEARCH_DATA_SYMBOL_LAYER_ID";
    //  private static final String GRID_LAYER = "GridLayer";
    private static final String LINE_COLOR = "#F37335";
    private static final float LINE_WIDTH = 5f;
    private static final float LINE_OPACITY = 1f;
    private static final float FILL_OPACITY = .6f;
    private static final String CIRCLE_SOURCE_ID = "circle-source-id";
    private static final String FILL_SOURCE_ID = "fill-source-id";
    private static final String LINE_SOURCE_ID = "line-source-id";
    private static final String CIRCLE_LAYER_ID = "circle-layer-id";
    private static final String FILL_LAYER_ID = "fill-layer-polygon-id";
    private static final String LINE_LAYER_ID = "line-layer-id";
    private static final LatLng BOUND_CORNER_NW = new LatLng(10.541060, 79.104095);
    private static final LatLng BOUND_CORNER_SE = new LatLng(5.612482, 82.735377);
    private static final String TEST_DB_FILE_NAME = "offline_test.db";
    private static final String TEST_STYLE = Style.MAPBOX_STREETS;
    private int position = -1;

    private static final LatLngBounds RESTRICTED_BOUNDS_AREA = new LatLngBounds.Builder()
            .include(BOUND_CORNER_NW)
            .include(BOUND_CORNER_SE)
            .build();
    private static double zoomLevel = -1;
    private static List<Construction> constructions;
    private final int ACTIVITY_RESULT_CODE = 213;
    private final int ACTIVITY_LAND_RESULT_CODE = 235;
    private final int ACTIVITY_MIDDLE_POINT_RESULT_CODE = 4568;
    private final int ACTIVITY_COPY_LAYER_RESULT_CODE = 4768;
    private final int ACTIVITY_LOT_RESULT_CODE = 234;
    private final int ACTIVITY_LOT_DATA_RESULT_CODE = 478;
    private final int ACTIVITY_MARKER_RESULT_CODE = 654;
    private final int ACTIVITY_FLOOR_ADD_RESULT_CODE = 6874;
    private final int ACTIVITY_LABEL_RESULT_CODE = 786;
    private final int ACTIVITY_MEASURE_LINE_RESULT_CODE = 78;
    private final int ACTIVITY_MEASURE_Building_RESULT_CODE = 79;
    private final int ACTIVITY_MEASURE_Building_CORNER_RESULT_CODE = 80;
    private CircleManager circleManager;
    private String lotName = "Test";
    private BundleInfo bundleInfo;
    public static List<Lot> lots;
    private static final String ID_IMAGE_SOURCE = "map_sheet_source";
    private static final String ID_IMAGE_LAYER = "map_sheet_id";

    public static final int ACTIVITY_MARKER_MENU_RESULT_CODE = 755;

    private SymbolManager symbolManager;
    private SymbolManager lotLabelManager;
    private List<Symbol> labelSymbolBuildingList;
    private SymbolManager landSymbolManager;
    private SymbolManager buildingDetailManager;
    private SymbolManager floorDetailManager;
    private Map<String, List<SymbolManager>> buildingLabelManagerMap;
    private SymbolManager buildingManager;
    private LineManager lineManager;
    private Point point1 = null;
    private Point point2 = null;
    private Point point3 = null;
    private Point point4 = null;
    private Point tmpPoint1 = null;
    private Point tmpPoint2 = null;
    private Point tmpPoint3 = null;
    private Point tmpPoint4 = null;
    private LatLng lOne;
    private LatLng lTwo;

    private List<Symbol> floorSymbols = new ArrayList<>();
    private List<Symbol> landLabelSymbols = new ArrayList<>();
    private List<Symbol> floorLabelSymbols = new ArrayList<>();

    private MarkerViewManager markerViewManager;
    boolean isLineDraw = false;
    boolean isPolygonDraw = false;
    boolean isCircleDraw = false;
    boolean isFloorPolygonDraw = false;
    boolean isRoomPolygonDraw = false;
    boolean isAQPolygonDraw = false;
    boolean isDrawingMode = false;
    boolean isBuildingSketchMode = false;
    int currentFloor = -1;
    RectF rectF = null;
    List<BuildingFloor> buildingFloors;
    List<FloorDrawing> floorDrawingList;
    List<Symbol> symbolList;
    List<Symbol> symbolBuildingList;
    BuildingFloorAdapter floorAdapter;
    FloorItemAdapter floorDrawing;
    private ActivityMapViewBinding binding;
    private MapboxMap mapboxMap;
    private List<String> styleType;
    private String activeStyle = Style.MAPBOX_STREETS;
    private int index = 0;
    private MasterFileListResponse masterFile;
    private GeoJsonSource freeHandCircleSource;
    private GeoJsonSource freeHandFillSource;
    private GeoJsonSource freeHandLineSource;
    private boolean isSaved = false;
    private List<Point> freehandTouchPointListForPolygon = new ArrayList<>();
    private List<Point> freehandTouchPointListForLine = new ArrayList<>();
    private boolean drawSingleLineOnly = false;
    private List<Point> fillLayerPointList = new ArrayList<>();
    private List<Point> lineLayerPointList = new ArrayList<>();
    private List<Feature> circleLayerFeatureList = new ArrayList<>();
    private List<Point> aqFillLayerPointList = new ArrayList<>();
    private List<Point> aqLineLayerPointList = new ArrayList<>();
    private List<Feature> aqCircleLayerFeatureList = new ArrayList<>();
    private List<Point> tmpAqFillLayerPointList = new ArrayList<>();
    private List<Point> tmpAqLineLayerPointList = new ArrayList<>();
    private List<Feature> tmpAqCircleLayerFeatureList = new ArrayList<>();
    private List<Feature> circlePointList = new ArrayList<>();
    private List<List<Point>> listOfList;
    private GeoJsonSource circleSource;
    private GeoJsonSource fillSource;
    private GeoJsonSource lineSource;
    private GeoJsonSource aqCircleSource;
    private GeoJsonSource aqFillSource;
    private GeoJsonSource aqLineSource;
    private Point firstPointOfPolygon;
    private Point lastPointMarked;
    private Point lotLastPointMarked;
    private List<Point> fillLayerTempPointList;
    private List<Point> lineLayerTempPointList;
    private List<Feature> circleTempLayerList;
    private GeoJsonSource circleShadowSource;
    private GeoJsonSource lineShadowSource;
    private List<Point> fillLayerDrawingPointList = new ArrayList<>();
    private List<Point> lineLayerDrawingPointList = new ArrayList<>();
    private List<Feature> circleLayerDrawingFeatureList = new ArrayList<>();
    private List<Marker> markers = new ArrayList<>();
    private Lot lot;
    private Construction building;
    private GeoJsonSource circleLineSource;
    private GeoJsonSource lineLineSource;
    private GeoJsonSource fillLineSource;
    private LandBuildingAreaListAdapter landAreaBuildingAdapter;
    private List<BuildingArea> landArea;
    private double radius = 0;
    private boolean isAqLineDraw = false;
    private LotMixinContent lotMixinContent;
    private String layerStyle;
    private MapLayerAdapter mapLayerAdapter;
    private List<MapLayer> mapLayers;
    private LatLng zoomPoint;
    private DataManager dataManager = new DataManager();

    private LabelListAdapter labelListAdapter;
    private FloorLabelListAdapter floorLabelListAdapter;


    private OfflineManager offlineManager;

    private static final String LS_ICON_ID = "LS_ICON_ID";
    private static final String PV_ICON_ID = "PV_ICON_ID";
    private static final String RE_ICON_ID = "RE_ICON_ID";
    private static final String BR_ICON_ID = "BR_ICON_ID";
    private static final String SELECT_ICON_ID = "SELECT_ICON_ID";
    private Symbol symbolTemp = null;
    private Symbol symbolReserve = null;
    private final static int INTERVAL = 1000 * 10; //10 sec
    Handler mHandler = new Handler();
    private final int RESULT_CODE = 896;
    private final int DRAWING_RESULT_CODE = 592;

    final double meters2inches = 0.0254;
    final double inches2feet = (int) 12;

    private static final int CAPTURE_IMAGE = 450;
    private File file;
    private int mode = -1;
    private String currentStructure = "";
    private CR conditionalReport;
    private List<FloorDrawing> fdfloorDrawingList = new ArrayList<>();

    @Inject
    ApiInterface apiInterface;

    @Inject
    SharedPreferences prefs;

    private FileUtils.OnFileCopiedFromAssetsListener onFileCopiedListener = new FileUtils.OnFileCopiedFromAssetsListener() {
        @Override
        public void onError() {
           /* Toast.makeText(
                    MapViewActivity.this,
            String.format("Error copying DB file."),
                    Toast.LENGTH_LONG).show();*/
        }

        @Override
        public void onFileCopiedFromAssets() {
            mergeDb();
            /*Toast.makeText(
                    MapViewActivity.this,
            String.format("OnFileCopied."),
                    Toast.LENGTH_LONG).show();*/
        }

    };


    private OfflineManager.MergeOfflineRegionsCallback onRegionMergedListener = new OfflineManager.MergeOfflineRegionsCallback() {
        @Override
        public void onMerge(OfflineRegion[] offlineRegions) {
            binding.map.getMapAsync(MapViewActivity.this::onMapReady);
/*
            Toast.makeText(
                    MapViewActivity.this,
            String.format("Merged %d regions.", offlineRegions.length),
                    Toast.LENGTH_LONG).show();
*/
        }

        @Override
        public void onError(String error) {
           /* Toast.makeText(
                    MapViewActivity.this,
            String.format("Offline DB merge error."),
                    Toast.LENGTH_LONG).show();
            Logger.e("Ad", error);*/
        }
    };

    private MergeCallback mergeCallback = new MergeCallback(onRegionMergedListener);


    List<Feature> symbolLayerIconFeatureList = new ArrayList<>();


    private View.OnTouchListener customOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {

            LatLng latLngTouchCoordinate = mapboxMap.getProjection().fromScreenLocation(
                    new PointF(motionEvent.getX(), motionEvent.getY()));

            Point screenTouchPoint = Point.fromLngLat(latLngTouchCoordinate.getLongitude(),
                    latLngTouchCoordinate.getLatitude());

// Draw the line on the map as the finger is dragged along the map
            freehandTouchPointListForLine.add(screenTouchPoint);
            mapboxMap.getStyle(style -> {
                GeoJsonSource drawLineSource = style.getSourceAs(FREEHAND_DRAW_LINE_LAYER_SOURCE_ID);
                if (drawLineSource != null) {
                    drawLineSource.setGeoJson(LineString.fromLngLats(freehandTouchPointListForLine));
                }


// Draw a polygon area if drawSingleLineOnly == false
                if (!drawSingleLineOnly) {
                    if (freehandTouchPointListForPolygon.size() < 2) {
                        freehandTouchPointListForPolygon.add(screenTouchPoint);
                    } else if (freehandTouchPointListForPolygon.size() == 2) {
                        freehandTouchPointListForPolygon.add(screenTouchPoint);
                        freehandTouchPointListForPolygon.add(freehandTouchPointListForPolygon.get(0));
                    } else {
                        freehandTouchPointListForPolygon.remove(freehandTouchPointListForPolygon.size() - 1);
                        freehandTouchPointListForPolygon.add(screenTouchPoint);
                        freehandTouchPointListForPolygon.add(freehandTouchPointListForPolygon.get(0));
                    }
                }

// Create and show a FillLayer polygon where the search area is
                GeoJsonSource fillPolygonSource = style.getSourceAs(FREEHAND_DRAW_FILL_LAYER_SOURCE_ID);
                List<List<Point>> polygonList = new ArrayList<>();
                polygonList.add(freehandTouchPointListForPolygon);
                Polygon drawnPolygon = Polygon.fromLngLats(polygonList);
                if (fillPolygonSource != null) {
                    fillPolygonSource.setGeoJson(drawnPolygon);
                }

                // Take certain actions when the drawing is done
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {

                    if (!drawSingleLineOnly) {
                        freehandTouchPointListForLine.add(freehandTouchPointListForPolygon.get(0));
                    }
                    enableMapMovement();
                }
            });

            return true;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, BuildConfig.SDK_KEY);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_map_view);
        ((XimapperApplication) getApplication()).getAppComponent().inject(this);

        setSupportActionBar(binding.mapToolBar);

        file = new File("");
        lots = new ArrayList<>();

        binding.map.onCreate(savedInstanceState);

        conditionalReport = new CR();


        zoomPoint = null;


        try {
            PackageInfo pInfo = MapViewActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName + " | " + BuildConfig.BuildTime;
            binding.tvDebug.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        NetworkStatus nt = new NetworkStatus();
        boolean isConnected = nt.isConnected(MapViewActivity.this);
        if (!isConnected) {
            copyAsset();
        }
        TooltipCompat.setTooltipText(binding.layoutDrawingTool.imgBtnDrawLine
                , "Draw Line");
        TooltipCompat.setTooltipText(binding.layoutDrawingTool.imgBtnDrawPolygon
                , "Draw Polygon");
        TooltipCompat.setTooltipText(binding.layoutDrawingTool.imgBtnDrawCircle
                , "Draw Circle");
        TooltipCompat.setTooltipText(binding.layoutDrawingTool.imgBtnMarkCornedPoint
                , "Mark Corned Point");
        TooltipCompat.setTooltipText(binding.layoutDrawingTool.imgGallery
                , "Open Image gallery");
        TooltipCompat.setTooltipText(binding.layoutDrawingTool.imgCamera
                , "Camera");
        TooltipCompat.setTooltipText(binding.layoutBuildingDrawingTool.imgBtnMarkCornedPointBuilding
                , "Mark Corned Point");
        TooltipCompat.setTooltipText(binding.layoutBuildingDrawingTool.imgGalleryBuilding
                , "Open Image gallery");
        TooltipCompat.setTooltipText(binding.layoutBuildingDrawingTool.imgCameraBuilding
                , "Camera");

        TooltipCompat.setTooltipText(binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingLine
                , "Draw Line");
        TooltipCompat.setTooltipText(binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingCircle
                , "Draw Circle");
        TooltipCompat.setTooltipText(binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingRoomPolygon
                , "Draw Rooms");
        TooltipCompat.setTooltipText(binding.layoutBuildingDrawingTool.imgbtnDrawBuildingFloorPolygon
                , "Draw Floor Area");
        TooltipCompat.setTooltipText(binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingPolygon
                , "Draw Rooms");
        TooltipCompat.setTooltipText(binding.layoutBuildingDrawingTool.imgAqDrawing
                , "Draw Acquisition Line");
        TooltipCompat.setTooltipText(binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingAQPolygon
                , "Draw Acquisition Polygon");

        binding.layoutDrawingTool.setHandler(this);
        binding.setHandler(this);

        binding.layoutDrawingTool.btnSaveDraw.setEnabled(false);
        binding.layoutDrawingTool.btnDraw.setEnabled(false);

        hideShowLandComponents(false);
        hideShowBuildingComponents(true);
        hideShowLotComponents(true);


        constructions = new ArrayList<>();
        buildingFloors = new ArrayList<>();

        floorAdapter = new BuildingFloorAdapter(buildingFloors, this);
        binding.rcBuildingFloors.setAdapter(floorAdapter);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        binding.rcBuildingFloors.setLayoutManager(layoutManager);

        binding.layoutBuildingDrawingTool.setHandler(this);

        fdfloorDrawingList = new ArrayList<>();
        floorDrawing = new FloorItemAdapter(fdfloorDrawingList, this);

        binding.layoutBuildingDrawingTool.rcFloorDrawings.setAdapter(floorDrawing);
        LinearLayoutManager lt
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.layoutBuildingDrawingTool.rcFloorDrawings.setLayoutManager(lt);


        landArea = new ArrayList<>();
        binding.rcBuildingArea.setLayoutManager(new LinearLayoutManager(this));

        landAreaBuildingAdapter = new LandBuildingAreaListAdapter(landArea, MapViewActivity.this);
        binding.rcBuildingArea.setAdapter(landAreaBuildingAdapter);


        symbolList = new ArrayList<>();
        labelSymbolBuildingList = new ArrayList<>();
        landLabelSymbols = new ArrayList<>();
        floorLabelSymbols = new ArrayList<>();
        if (circleLayerFeatureList.size() > 0) {
            binding.imgBtnRedo.setEnabled(false);
            binding.imgBtnUndo.setEnabled(false);

        }
        this.lotMixinContent = new LotMixinContent();
        this.mode = getIntent().getExtras().getInt(Constant.MODE, -1);
        if (mode == 1) {
            masterFile = getIntent().getExtras().getParcelable(Constant.MASTER_FILE);
            bundleInfo = getIntent().getExtras().getParcelable(Constant.BUNDLE);
        } else if (mode == 2) {
            masterFile = getIntent().getExtras().getParcelable(Constant.MASTER_FILE);
            this.lotMixinContent = getIntent().getExtras().getParcelable(Constant.DETAILS);
            bundleInfo = getIntent().getExtras().getParcelable(Constant.BUNDLE);

        }


        mapLayers = new ArrayList<>();


        binding.rcMapLayers.setLayoutManager(new LinearLayoutManager(this));
        mapLayerAdapter = new MapLayerAdapter(mapLayers, MapViewActivity.this);
        binding.rcMapLayers.setAdapter(mapLayerAdapter);
        binding.rcMapLayers.setNestedScrollingEnabled(false);

        List<Symbol> labels = new ArrayList<>();
        binding.rcLabels.setLayoutManager(new LinearLayoutManager(this));
        labelListAdapter = new LabelListAdapter(labels, MapViewActivity.this);
        binding.rcLabels.setAdapter(labelListAdapter);
        binding.rcLabels.setNestedScrollingEnabled(false);

        List<Symbol> l = new ArrayList<>();
        floorLabelListAdapter = new FloorLabelListAdapter(l, MapViewActivity.this);
        binding.layoutBuildingDrawingTool.rcBuildingLabels.setAdapter(labelListAdapter);

        styleType = new ArrayList<>();
        styleType.add("Light");
        styleType.add("Dark");
        styleType.add("Street");
        styleType.add("Outdoors");
        styleType.add("Satellite");
        styleType.add("Satellite Street");


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, styleType);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spinner.setAdapter(dataAdapter);


        binding.btnViewMasterFile.setOnClickListener(v -> {
            Intent i = new Intent(MapViewActivity.this, MasterFileGetActivity.class);
            startActivity(i);
            finish();
        });

      /*  binding.layerFreeHandDrawFab.setOnClickListener(v -> {
            enableMapDrawing();
        });*/

        binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingLine.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingCircle.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingRoomPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutBuildingDrawingTool.imgbtnDrawBuildingFloorPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);

        binding.layoutDrawingTool.imgBtnDrawLine.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutDrawingTool.imgBtnDrawPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutDrawingTool.imgBtnDrawCircle.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);


        binding.addMarkerFab.setOnClickListener(v -> {
            if (isSaved) {
                Point mapTargetPoint = Point.fromLngLat(mapboxMap.getCameraPosition().target.getLongitude(),
                        mapboxMap.getCameraPosition().target.getLatitude());

                Marker marker = new Marker();
                LatLng latLng = new LatLng();
                latLng.setLongitude(mapTargetPoint.longitude());
                latLng.setLatitude(mapTargetPoint.latitude());
                marker.setLatLng(latLng);

                Intent i = new Intent(MapViewActivity.this, AddMarkerActivity.class);
                i.putExtra(Constant.MARKER, marker);
                i.putExtra(Constant.MODE, 2);
                startActivityForResult(i, ACTIVITY_MARKER_RESULT_CODE);
                CameraPosition position = new CameraPosition.Builder()
                        .target(new LatLng(marker.getLatLng().getLatitude(), marker.getLatLng().getLongitude()))
                        .zoom(20).build();
                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 1000);
            } else {
                Toast.makeText(MapViewActivity.this, "Please save the selected lot", Toast.LENGTH_LONG).show();
            }
        });


        binding.imgBtnUndo.setOnClickListener(v -> {
            undo();
        });


        SwipeHelper swipeHelper = new SwipeHelper(MapViewActivity.this, binding.rcLabels, 100) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new SwipeHelper.UnderlayButton(MapViewActivity.this,
                        getResources().getString(R.string.tv_label_delete),
                        0,
                        ContextCompat.getColor(MapViewActivity.this, R.color.colorRed),
                        pos -> {
                            position = pos;
                            Intent i = new Intent(MapViewActivity.this, ConfirmationDialogActivity.class);
                            i.putExtra(Constant.NAME, "Are you sure you want to delete this item?");
                            startActivityForResult(i, RESULT_CODE);
                        }
                ));
            }
        };

        swipeHelper.attachSwipe();


        SwipeHelper sw = new SwipeHelper(MapViewActivity.this, binding.layoutBuildingDrawingTool.rcFloorDrawings, 100) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new SwipeHelper.UnderlayButton(MapViewActivity.this,
                        getResources().getString(R.string.tv_label_delete),
                        0,
                        ContextCompat.getColor(MapViewActivity.this, R.color.colorRed),
                        pos -> {
                            position = pos;
                            Intent i = new Intent(MapViewActivity.this, ConfirmationDialogActivity.class);
                            i.putExtra(Constant.NAME, "Are you sure you want to delete this item?");
                            startActivityForResult(i, DRAWING_RESULT_CODE);
                        }
                ));
            }
        };

        sw.attachSwipe();

        setIsEnabledBuildingDraw(true);


        binding.map.getMapAsync(this);

        if (bundleInfo.isATFile()) {
            binding.swATMap.setChecked(true);
        } else {
            binding.swATMap.setEnabled(false);
        }

        if (bundleInfo.isATFile()) {
            binding.swATDetail.setChecked(true);
        } else {
            binding.swATDetail.setEnabled(false);
        }

        if (bundleInfo.isPPFile()) {
            binding.swPPMap.setChecked(true);
        } else {
            binding.swPPMap.setEnabled(false);
        }

        if (bundleInfo.isPPFile()) {
            binding.swPPDetail.setChecked(true);
        } else {
            binding.swPPDetail.setEnabled(false);
        }

        binding.swDataLayer.setChecked(true);


        binding.swATMap.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (bundleInfo.isATFile()) {
                if (isChecked) {
                    mapboxMap.getStyle().getLayer(Constant.AT_LAYER).setProperties(visibility(VISIBLE));
                } else {
                    mapboxMap.getStyle().getLayer(Constant.AT_LAYER).setProperties(visibility(NONE));
                }
            }
        });


        binding.swATDetail.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (bundleInfo.isATFile()) {
                try {
                    if (isChecked) {
                        mapboxMap.getStyle().getLayer(Constant.AT_TEXT_LAYER).setProperties(visibility(VISIBLE));
                    } else {
                        mapboxMap.getStyle().getLayer(Constant.AT_TEXT_LAYER).setProperties(visibility(NONE));
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        binding.swPPMap.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (bundleInfo.isPPFile()) {
                if (isChecked) {
                    mapboxMap.getStyle().getLayer(Constant.PP_LAYER).setProperties(visibility(VISIBLE));
                } else {
                    mapboxMap.getStyle().getLayer(Constant.PP_LAYER).setProperties(visibility(NONE));
                }
            }
        });

        binding.swPPDetail.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (bundleInfo.isPPFile()) {
                try {

                    if (isChecked) {
                        mapboxMap.getStyle().getLayer(Constant.PP_TEXT_LAYER).setProperties(visibility(VISIBLE));
                    } else {
                        mapboxMap.getStyle().getLayer(Constant.PP_TEXT_LAYER).setProperties(visibility(NONE));
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });


        binding.swDataLayer.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (circleSource != null) {
                if (isChecked) {
                    mapboxMap.getStyle().getLayer(CIRCLE_LAYER_ID).setProperties(visibility(VISIBLE));
                } else {
                    mapboxMap.getStyle().getLayer(CIRCLE_LAYER_ID).setProperties(visibility(NONE));
                }
            }

            if (lineSource != null) {
                if (isChecked) {
                    mapboxMap.getStyle().getLayer(LINE_LAYER_ID).setProperties(visibility(VISIBLE));
                } else {
                    mapboxMap.getStyle().getLayer(LINE_LAYER_ID).setProperties(visibility(NONE));
                }
            }

            if (fillSource != null) {
                if (isChecked) {
                    mapboxMap.getStyle().getLayer(FILL_LAYER_ID).setProperties(visibility(VISIBLE));
                } else {
                    mapboxMap.getStyle().getLayer(FILL_LAYER_ID).setProperties(visibility(NONE));
                }
            }

            if (freeHandFillSource != null) {
                if (isChecked) {
                    mapboxMap.getStyle().getLayer(FREEHAND_DRAW_FILL_LAYER_ID).setProperties(visibility(VISIBLE));
                } else {
                    mapboxMap.getStyle().getLayer(FREEHAND_DRAW_FILL_LAYER_ID).setProperties(visibility(NONE));
                }
            }

            if (freeHandLineSource != null) {
                if (isChecked) {
                    mapboxMap.getStyle().getLayer(FREEHAND_DRAW_LINE_LAYER_ID).setProperties(visibility(VISIBLE));
                } else {
                    mapboxMap.getStyle().getLayer(FREEHAND_DRAW_LINE_LAYER_ID).setProperties(visibility(NONE));
                }
            }


            if (isChecked) {
                showControllers();
            } else {
                hideControllers();
            }
        });

        binding.layoutDrawingTool.btnLandText.setOnClickListener(v -> {
            Intent i = new Intent(MapViewActivity.this, CustomLabelAddActivity.class);
            i.putExtra(Constant.TYPE, 1);
            i.putExtra(Constant.MODE, 1);
            startActivityForResult(i, ACTIVITY_LABEL_RESULT_CODE);
        });
        binding.layoutBuildingDrawingTool.btnBuildingText.setOnClickListener(v -> {
            Intent i = new Intent(MapViewActivity.this, CustomLabelAddActivity.class);
            i.putExtra(Constant.TYPE, 2);
            i.putExtra(Constant.MODE, 1);
            startActivityForResult(i, ACTIVITY_LABEL_RESULT_CODE);
        });

        binding.layerDrawFab.setOnClickListener(v -> {
                    // Use the map click location to create a Point object
                    RectF rectF = null;
                    Point mapTargetPoint = Point.fromLngLat(mapboxMap.getCameraPosition().target.getLongitude(),
                            mapboxMap.getCameraPosition().target.getLatitude());

// Make note of the first map click location so that it can be used to create a closed polygon later on
                    if (circleLayerFeatureList.size() == 0) {
                        firstPointOfPolygon = mapTargetPoint;
                    } else if (circleLayerFeatureList.size() == 1) {
                        binding.imgBtnUndo.setEnabled(true);
                    }

                    lineLayerTempPointList = null;
                    fillLayerTempPointList = null;
                    circleTempLayerList = null;

// Add the click point to the circle layer and update the display of the circle layer data
                    circleLayerFeatureList.add(Feature.fromGeometry(mapTargetPoint));
                    if (circleSource != null) {
                        circleSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList));
                    }


// Add the click point to the line layer and update the display of the line layer data
                    if (circleLayerFeatureList.size() < 3) {
                        lineLayerPointList.add(mapTargetPoint);
                        lotLastPointMarked = mapTargetPoint;
                        if (lineLayerPointList.size() >= 2) {
                            showDistance();
                        }

                    } else if (circleLayerFeatureList.size() == 3) {
                        lineLayerPointList.add(mapTargetPoint);
                        showDistance();
                        lineLayerPointList.add(firstPointOfPolygon);
                        if (lineLayerPointList.size() >= 2) {
                            showDistance();
                        }
                        lotLastPointMarked = mapTargetPoint;


                    } else {
                        lineLayerPointList.remove(lineLayerPointList.size() - 1);
                        lotLabelManager.delete(symbolList.get(symbolList.size() - 1));
                        lineLayerPointList.add(mapTargetPoint);
                        if (lineLayerPointList.size() >= 2) {
                            showDistance();
                        }
                        lineLayerPointList.add(firstPointOfPolygon);
                        if (lineLayerPointList.size() >= 2) {
                            showDistance();
                        }

                        lotLastPointMarked = mapTargetPoint;


                    }
                    if (lineSource != null) {
                        lineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                {Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList))}));
                    }

// Add the click point to the fill layer and update the display of the fill layer data
                    if (circleLayerFeatureList.size() < 3) {
                        fillLayerPointList.add(mapTargetPoint);
                    } else if (circleLayerFeatureList.size() == 3) {
                        fillLayerPointList.add(mapTargetPoint);
                        fillLayerPointList.add(firstPointOfPolygon);
                    } else {
                        fillLayerPointList.remove(fillLayerPointList.size() - 1);
                        fillLayerPointList.add(mapTargetPoint);
                        fillLayerPointList.add(firstPointOfPolygon);
                    }
                    listOfList = new ArrayList<>();
                    listOfList.add(fillLayerPointList);
                    List<Feature> finalFeatureList = new ArrayList<>();
                    finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                    FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
                    if (fillSource != null) {
                        fillSource.setGeoJson(newFeatureCollection);
                    }
                    LatLng latLng = new LatLng();
                    latLng.setLatitude(mapTargetPoint.latitude());
                    latLng.setLongitude(mapTargetPoint.longitude());
                    lastPointMarked = Point.fromLngLat(latLng.getLongitude(),
                            latLng.getLatitude());

                }


        );

        binding.clearMapForLayerFab.setOnClickListener(v -> {
            new AlertDialog.Builder(this)
                    .setTitle("Are you sure?")
                    .setMessage("Do you clear the lot?")
                    .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                        clearEntireMap();
                        binding.imgBtnUndo.setEnabled(false);
                        binding.imgBtnRedo.setEnabled(false);
                    })
                    .setNegativeButton(android.R.string.no, null).show();

        });

        binding.layerDrawMeasuredFab.setOnClickListener(v -> {


            // Use the map click location to create a Point object
            RectF rectF = null;
            Point mapTargetPoint = Point.fromLngLat(mapboxMap.getCameraPosition().target.getLongitude(),
                    mapboxMap.getCameraPosition().target.getLatitude());

// Make note of the first map click location so that it can be used to create a closed polygon later on
            if (circleLayerFeatureList.size() == 0) {
                firstPointOfPolygon = mapTargetPoint;
                lineLayerPointList.add(mapTargetPoint);
                lotLastPointMarked = mapTargetPoint;
                fillLayerPointList.add(mapTargetPoint);
                lastPointMarked = mapTargetPoint;
            } else if (circleLayerFeatureList.size() == 1) {
                binding.imgBtnUndo.setEnabled(true);
            }

            lineLayerTempPointList = null;
            fillLayerTempPointList = null;
            circleTempLayerList = null;

// Add the click point to the circle layer and update the display of the circle layer data
            if (circleLayerFeatureList.size() == 0) {
                circleLayerFeatureList.add(Feature.fromGeometry(mapTargetPoint));
                if (circleSource != null) {
                    circleSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList));
                }
            } else {
                Intent i = new Intent(MapViewActivity.this, MeasuredLineDrawingActivity.class);
                startActivityForResult(i, ACTIVITY_MEASURE_LINE_RESULT_CODE);
            }


        });


    }

    private void undo() {
        if (circleLayerFeatureList.size() == 0) {
            binding.imgBtnUndo.setEnabled(false);
        } else if (circleLayerFeatureList.size() == 1) {
            binding.imgBtnRedo.setEnabled(true);

            circleTempLayerList = new ArrayList<>(circleLayerFeatureList);

            circleLayerFeatureList.remove(circleLayerFeatureList.size() - 1);

            lineLayerTempPointList = new ArrayList<>(lineLayerPointList);
            lineLayerPointList.clear();

            fillLayerTempPointList = new ArrayList<>(fillLayerPointList);
            fillLayerPointList.clear();

        } else if (circleLayerFeatureList.size() == 2) {
            binding.imgBtnRedo.setEnabled(true);

            circleTempLayerList = new ArrayList<>(circleLayerFeatureList);
            circleLayerFeatureList.remove(circleLayerFeatureList.size() - 1);

            lineLayerTempPointList = new ArrayList<>(lineLayerPointList);
            lineLayerPointList.remove(lineLayerPointList.size() - 1);

            fillLayerTempPointList = new ArrayList<>(fillLayerPointList);
            fillLayerPointList.remove(fillLayerPointList.size() - 1);


        } else if (circleLayerFeatureList.size() == 3) {
            binding.imgBtnRedo.setEnabled(true);

            circleTempLayerList = new ArrayList<>(circleLayerFeatureList);
            circleLayerFeatureList.remove(circleLayerFeatureList.size() - 1);

            lineLayerTempPointList = new ArrayList<>(lineLayerPointList);
            lineLayerPointList.remove(lineLayerPointList.size() - 1);
            lineLayerPointList.remove(lineLayerPointList.size() - 1);

            fillLayerTempPointList = new ArrayList<>(fillLayerPointList);
            fillLayerPointList.remove(fillLayerPointList.size() - 1);
            fillLayerPointList.remove(fillLayerPointList.size() - 1);


        } else {
            binding.imgBtnRedo.setEnabled(true);

            circleTempLayerList = new ArrayList<>(circleLayerFeatureList);
            circleLayerFeatureList.remove(circleLayerFeatureList.size() - 1);

            lineLayerTempPointList = new ArrayList<>(lineLayerPointList);
            lineLayerPointList.remove(lineLayerPointList.size() - 1);
            lineLayerPointList.remove(lineLayerPointList.size() - 1);
            lineLayerPointList.add(firstPointOfPolygon);

            fillLayerTempPointList = new ArrayList<>(fillLayerPointList);
            fillLayerPointList.remove(fillLayerPointList.size() - 1);
            fillLayerPointList.remove(fillLayerPointList.size() - 1);
            fillLayerPointList.add(firstPointOfPolygon);

        }

        if (circleSource != null) {
            circleSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList));
        }

        if (lineSource != null) {
            lineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                    {Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList))}));
        }

        listOfList = new ArrayList<>();
        listOfList.add(fillLayerPointList);
        List<Feature> finalFeatureList = new ArrayList<>();
        finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
        FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
        if (fillSource != null) {
            fillSource.setGeoJson(newFeatureCollection);
        }

        if (isBuildingSketchMode && isDrawingMode) {
            saveToFile();
        }
    }


    private void showControllers() {
        if (!isDrawingMode && !isBuildingSketchMode) {
            binding.multipleActionsParentFab.setVisibility(View.VISIBLE);
            binding.imgBtnRedo.setVisibility(View.VISIBLE);
            binding.imgBtnUndo.setVisibility(View.VISIBLE);
            binding.imgPointer.setVisibility(View.VISIBLE);
        }
    }

    private void hideControllers() {
        binding.multipleActionsParentFab.setVisibility(View.GONE);
        binding.imgBtnRedo.setVisibility(View.INVISIBLE);
        binding.imgBtnUndo.setVisibility(View.INVISIBLE);
        binding.imgPointer.setVisibility(View.GONE);
    }


    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        mapboxMap.setStyle(activeStyle, this::initStyle);


        binding.spinner.setOnItemSelectedListener(this);


        mapboxMap.addOnMoveListener(new MapboxMap.OnMoveListener() {
            @Override
            public void onMoveBegin(MoveGestureDetector detector) {
            }

            @Override
            public void onMove(MoveGestureDetector detector) {
                Point mapTargetPoint = Point.fromLngLat(mapboxMap.getCameraPosition().target.getLongitude(),
                        mapboxMap.getCameraPosition().target.getLatitude());

                if (isDrawingMode) {
                    if (isLineDraw | isCircleDraw | isPolygonDraw | isAqLineDraw) {
                        if (circleLayerFeatureList.size() > 0 && lastPointMarked != null) {

                            double distance = MapCalculations.calculateDistance(mapTargetPoint, lastPointMarked);
                            double inchtot = Math.round(distance / meters2inches);
                            int feet = (int) inchtot / 12;
                            int inches = (int) inchtot % 12;
                            String dist = feet + "ft " + inches + " inch";
                            double bearing = TurfMeasurement.bearing(lastPointMarked, mapTargetPoint);
                            binding.tvLandDistance.setText(dist);
                            binding.tvLandAngle.setText(round(bearing, 2) + "");

                        }
                    }

                } else {
                    if (circleLayerFeatureList.size() > 0 && lotLastPointMarked != null) {
                        double distance = MapCalculations.calculateDistance(mapTargetPoint, lotLastPointMarked);

                        double inchtot = Math.round(distance / meters2inches);
                        int feet = (int) inchtot / 12;
                        int inches = (int) inchtot % 12;
                        String dist = feet + "ft " + inches + " inch";


                        double bearing = TurfMeasurement.bearing(lotLastPointMarked, mapTargetPoint);
                        binding.tvLandDistance.setText(dist);
                        binding.tvLandAngle.setText(round(bearing, 2) + "");
                    }
                }
            }

            @Override
            public void onMoveEnd(MoveGestureDetector detector) {
            }
        });

        markerViewManager = new MarkerViewManager(binding.map, mapboxMap);


        if (zoomPoint != null) {

            CameraPosition position = new CameraPosition.Builder()
                    .target(new LatLng(zoomPoint.getLatitude(), zoomPoint.getLongitude())) // Sets the new camera position
                    .zoom(15) // Sets the zoom
                    .bearing(180) // Rotate the camera
                    .tilt(30) // Set the camera tilt
                    .build(); // Creates a CameraPosition from the builder

            mapboxMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(position), 3000);
        }
    }

    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                activeStyle = Style.LIGHT;
                break;
            case 1:
                activeStyle = Style.DARK;
                break;
            case 2:
                activeStyle = Style.MAPBOX_STREETS;
                break;
            case 3:
                activeStyle = Style.OUTDOORS;
                break;
            case 4:
                activeStyle = Style.SATELLITE;
                break;
            case 5:
                activeStyle = Style.SATELLITE_STREETS;
                break;
            default:
                activeStyle = Style.LIGHT;
        }

        mapboxMap.setStyle(activeStyle, this::initStyle);


    }

    private void addMapLayerToMap(@NonNull Style loadedMapStyle, String color, String id) {
// Add FillExtrusion layer to map using GeoJSON data
        loadedMapStyle.addLayer(new LineLayer(id, id).withProperties(
                lineColor(color)
                , visibility(VISIBLE)));
    }

    public void onNothingSelected(AdapterView<?> arg0) {
    }


    private void addATLayerToMap(@NonNull Style loadedMapStyle) {
// Add FillExtrusion layer to map using GeoJSON data
        loadedMapStyle.addLayer(new LineLayer(Constant.AT_LAYER, Constant.AT_DATA).withProperties(
                lineColor(Color.BLUE)
                , visibility(VISIBLE)));
    }


    private void addPPLayerToMap(@NonNull Style loadedMapStyle) {
// Add FillExtrusion layer to map using GeoJSON data
        loadedMapStyle.addLayerAbove(new LineLayer(Constant.PP_LAYER, Constant.PP_DATA).withProperties(
                lineColor(Color.GREEN)
                , visibility(VISIBLE)), ID_IMAGE_LAYER);
    }


    private void addCadesterLayerToMap(@NonNull Style loadedMapStyle) {
// Add FillExtrusion layer to map using GeoJSON data
        loadedMapStyle.addLayerAbove(new LineLayer(Constant.CADESTER_LAYER, Constant.CADESTER_DATA).withProperties(
                lineColor(Color.RED)
                , visibility(VISIBLE)), ID_IMAGE_LAYER);
    }


    private void addATLabelLayerToMap(@NonNull Style loadedMapStyle) {
// Add FillExtrusion layer to map using GeoJSON data
        loadedMapStyle.addLayer(new SymbolLayer(Constant.AT_TEXT_LAYER, Constant.AT_TEXT_DATA)
                .withProperties(PropertyFactory.textField(Expression.get("Text")),
                        PropertyFactory.textSize(10f)));

    }

    private void addPPLabelLayerToMap(@NonNull Style loadedMapStyle) {
// Add FillExtrusion layer to map using GeoJSON data
        loadedMapStyle.addLayer(new SymbolLayer(Constant.PP_TEXT_LAYER, Constant.PP_TEXT_DATA)
                .withProperties(PropertyFactory.textField(Expression.get("Text")),
                        PropertyFactory.textSize(10f)));

    }

    private void addPPLabelDarkLayerToMap(@NonNull Style loadedMapStyle) {
// Add FillExtrusion layer to map using GeoJSON data
        loadedMapStyle.addLayer(new SymbolLayer("pptext", "pplabel")
                .withProperties(PropertyFactory.textField(Expression.get("Text")),
                        PropertyFactory.textSize(10f)
                        , PropertyFactory.textColor(Color.WHITE)));
    }

    private void addCadastralLayerToMap(@NonNull Style loadedMapStyle) {
// Add FillExtrusion layer to map using GeoJSON data
        loadedMapStyle.addLayer(new LineLayer("cadastral", "cadastraldata").withProperties(
                lineColor(Color.BLUE)
                , visibility(VISIBLE)));
    }

    private void addGridlLayerToMap(@NonNull Style loadedMapStyle) {
// Add FillExtrusion layer to map using GeoJSON data
        loadedMapStyle.addLayer(new LineLayer("grid", "gridData").withProperties(
                lineColor(Color.BLACK),
                lineOpacity(0.5f),
                lineWidth(1f)
                , visibility(VISIBLE)));
    }

    private void removeExtrusionsLayerToMap(@NonNull Style loadedMapStyle) {
// Add FillExtrusion layer to map using GeoJSON data
        loadedMapStyle.addLayer(new LineLayer("course", "coursedata").withProperties(
                lineColor(Color.BLUE)
                , visibility(VISIBLE)));
    }

    private void addSymbolicLayerToMap(@NonNull Style loadedMapStyle) {
// Add FillExtrusion layer to map using GeoJSON data
        loadedMapStyle.addLayer(new SymbolLayer("labels", "labeldata")
                .withProperties(PropertyFactory.textField(Expression.get("Text")),
                        PropertyFactory.textSize(10f)));

    }

    private void addSymbolicNightLayerToMap(@NonNull Style loadedMapStyle) {
// Add FillExtrusion layer to map using GeoJSON data
        loadedMapStyle.addLayer(new SymbolLayer("labels", "labeldata")
                .withProperties(PropertyFactory.textField(Expression.get("Text")),
                        PropertyFactory.textSize(10f)
                        , PropertyFactory.textColor(Color.WHITE)));

    }


    @Override
    protected void initDependencies(AppComponent appComponent) {
        appComponent.inject(this);
    }


    @Override
    public void onStart() {
        super.onStart();
        binding.map.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.map.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.map.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        binding.map.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.map.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (markerViewManager != null) {
            markerViewManager.onDestroy();
        }
        if (mergeCallback != null) {
            mergeCallback.onActivityDestroy();
        }
        binding.map.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        binding.map.onSaveInstanceState(outState);
    }

    private String loadJsonFromAsset(String path) {
// Using this method to load in GeoJSON files from the assets folder.
        try {
            File file = new File(path);

            InputStream is = new FileInputStream(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }


    private void enableMapDrawing() {
        binding.map.setOnTouchListener(customOnTouchListener);
    }

    private void enableMapMovement() {
        binding.map.setOnTouchListener(null);
    }


    private void initDrawing() {

        mapboxMap.getStyle(loadedStyle -> {


            freeHandLineSource = new GeoJsonSource(FREEHAND_DRAW_LINE_LAYER_SOURCE_ID);
            freeHandCircleSource = new GeoJsonSource(MARKER_SYMBOL_LAYER_SOURCE_ID);
            freeHandFillSource = new GeoJsonSource(FREEHAND_DRAW_FILL_LAYER_SOURCE_ID);

            loadedStyle.addSource(freeHandLineSource);
            loadedStyle.addSource(freeHandCircleSource);
            loadedStyle.addSource(freeHandFillSource);


// Add freehand draw LineLayer to the map
            loadedStyle.addLayerBelow(new LineLayer(FREEHAND_DRAW_LINE_LAYER_ID,
                    FREEHAND_DRAW_LINE_LAYER_SOURCE_ID).withProperties(
                    lineWidth(LINE_WIDTH),
                    lineJoin(LINE_JOIN_ROUND),
                    lineOpacity(LINE_OPACITY),
                    lineColor(Color.parseColor(LINE_COLOR))), SEARCH_DATA_SYMBOL_LAYER_ID
            );

// Add freehand draw polygon FillLayer to the map
            loadedStyle.addLayerBelow(new FillLayer(FREEHAND_DRAW_FILL_LAYER_ID,
                    FREEHAND_DRAW_FILL_LAYER_SOURCE_ID).withProperties(
                    fillColor(Color.parseColor("#00e9ff")),
                    fillOpacity(FILL_OPACITY)), FREEHAND_DRAW_LINE_LAYER_ID
            );

            //  enableMapDrawing();
        });
    }

    /**
     * Remove the drawn area from the map by resetting the FeatureCollections used by the layers' sources
     */
    private void clearEntireMap() {


        List<Source> sources = mapboxMap.getStyle().getSources();
        for (Source s : sources) {
            mapboxMap.getStyle().removeSource(s);
        }

        List<Layer> layers = mapboxMap.getStyle().getLayers();
        for (Layer l : layers) {
            //mapboxMap.getStyle().removeLayer(l);
        }

        fillLayerPointList = new ArrayList<>();
        circleLayerFeatureList = new ArrayList<>();
        lineLayerPointList = new ArrayList<>();
        if (circleSource != null) {
            circleSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]{}));
        }
        if (lineSource != null) {
            lineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]{}));
        }
        if (fillSource != null) {
            fillSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]{}));
        }


        symbolList.clear();
        lotLabelManager.deleteAll();


        // Reset ArrayLists
        freehandTouchPointListForPolygon = new ArrayList<>();
        freehandTouchPointListForLine = new ArrayList<>();

// Add empty Feature array to the sources
        GeoJsonSource drawLineSource = mapboxMap.getStyle().getSourceAs(FREEHAND_DRAW_LINE_LAYER_SOURCE_ID);
        if (drawLineSource != null) {
            drawLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]{}));
        }

        GeoJsonSource fillPolygonSource = mapboxMap.getStyle().getSourceAs(FREEHAND_DRAW_FILL_LAYER_SOURCE_ID);
        if (fillPolygonSource != null) {
            fillPolygonSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]{}));
        }
    }

    /**
     * Set up the CircleLayer source for showing map click points
     */
    private GeoJsonSource initCircleSource(@NonNull Style loadedMapStyle) {
        FeatureCollection circleFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource circleGeoJsonSource = new GeoJsonSource(CIRCLE_SOURCE_ID, circleFeatureCollection);
        loadedMapStyle.addSource(circleGeoJsonSource);
        return circleGeoJsonSource;
    }

    /**
     * Set up the CircleLayer for showing polygon click points
     */
    private void initCircleLayer(@NonNull Style loadedMapStyle) {
        CircleLayer circleLayer = new CircleLayer(CIRCLE_LAYER_ID,
                CIRCLE_SOURCE_ID);
        circleLayer.setProperties(
                circleRadius(7f),
                circleColor(Color.parseColor("#d004d3"))
        );
        loadedMapStyle.addLayerAbove(circleLayer, ID_IMAGE_LAYER);
    }

    /**
     * Set up the FillLayer source for showing map click points
     */
    private GeoJsonSource initFillSource(@NonNull Style loadedMapStyle) {
        FeatureCollection fillFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource fillGeoJsonSource = new GeoJsonSource(FILL_SOURCE_ID, fillFeatureCollection);
        loadedMapStyle.addSource(fillGeoJsonSource);
        return fillGeoJsonSource;
    }

    /**
     * Set up the FillLayer for showing the set boundaries' polygons
     */
    private void initFillLayer(@NonNull Style loadedMapStyle) {
        FillLayer fillLayer = new FillLayer(FILL_LAYER_ID,
                FILL_SOURCE_ID);
        fillLayer.setProperties(
                fillOpacity(.6f),
                fillColor(Color.parseColor("#FFFFFF"))
        );
        loadedMapStyle.addLayerBelow(fillLayer, LINE_LAYER_ID);
    }

    /**
     * Set up the LineLayer source for showing map click points
     */
    private GeoJsonSource initLineSource(@NonNull Style loadedMapStyle) {
        FeatureCollection lineFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource lineGeoJsonSource = new GeoJsonSource(LINE_SOURCE_ID, lineFeatureCollection);
        loadedMapStyle.addSource(lineGeoJsonSource);
        return lineGeoJsonSource;
    }

    /**
     * Set up the LineLayer for showing the set boundaries' polygons
     */
    private void initLineLayer(@NonNull Style loadedMapStyle) {
        LineLayer lineLayer = new LineLayer(LINE_LAYER_ID,
                LINE_SOURCE_ID);
        lineLayer.setProperties(
                lineColor(Color.parseColor("#F37335")),
                lineWidth(5f)
        );
        loadedMapStyle.addLayerBelow(lineLayer, CIRCLE_LAYER_ID);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.lot_menu, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_lot:

                new AlertDialog.Builder(this)
                        .setTitle("Are you sure?")
                        .setMessage("Do you want to perform this action?")
                        .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                            Intent intent = getIntent();
                            intent.putExtra(Constant.MASTER_FILE, masterFile);
                            intent.putExtra(Constant.BUNDLE, bundleInfo);
                            intent.putExtra(Constant.MODE, 1);
                            finish();
                            startActivity(intent);
                        })
                        .setNegativeButton(android.R.string.no, null).show();

                return true;

            case R.id.open_lot:
                Intent intent1 = new Intent(MapViewActivity.this, LotListActivity.class);
                intent1.putExtra(Constant.MASTER_FILE, masterFile);
                startActivityForResult(intent1, ACTIVITY_LOT_DATA_RESULT_CODE);

                return true;
            case R.id.save_lot:
                Intent i = new Intent(MapViewActivity.this, LotSaveActivity.class);
                MasterFileData masterData = new MasterFileData();
                if (bundleInfo.isMasterFile()) {
                    String data = new DataManager().loadGeoJsonFromAsset(bundleInfo.getMasterFilePath());
                    masterData = new Gson().fromJson(data, MasterFileData.class);
                }

                if (lot != null) {
                    if (lots.contains(lot)) {
                        int index = lots.indexOf(lot);
                        lot.setCircleLayerFeatureList(new ArrayList<>(circleLayerFeatureList));
                        lot.setFillLayerPointList(new ArrayList<>(fillLayerPointList));
                        lot.setLineLayerPointList(new ArrayList<>(lineLayerPointList));

                        lots.set(index, lot);

                    } else {
                        lot.setCircleLayerFeatureList(circleLayerFeatureList);
                        lot.setFillLayerPointList(new ArrayList<>(fillLayerPointList));
                        lot.setLineLayerPointList(new ArrayList<>(lineLayerPointList));

                        lots.add(lot);
                        i.putExtra(Constant.LOT, lot);
                        i.putExtra(Constant.MASTER_FILE, masterData);
                        startActivityForResult(i, ACTIVITY_LOT_RESULT_CODE);
                    }
                } else {
                    lot = new Lot();
                    lot.setCircleLayerFeatureList(new ArrayList<>(circleLayerFeatureList));
                    lot.setFillLayerPointList(new ArrayList<>(fillLayerPointList));
                    lot.setLineLayerPointList(new ArrayList<>(lineLayerPointList));


                    lots.add(lot);
                    i.putExtra(Constant.LOT, lot);
                    i.putExtra(Constant.MASTER_FILE, masterData);
                    startActivityForResult(i, ACTIVITY_LOT_RESULT_CODE);
                }
                return true;
            case R.id.save_as_lot:
                Intent in = new Intent(MapViewActivity.this, LotSaveActivity.class);
                MasterFileData md = new MasterFileData();
                if (bundleInfo.isMasterFile()) {
                    String data = new DataManager().loadGeoJsonFromAsset(bundleInfo.getMasterFilePath());
                    md = new Gson().fromJson(data, MasterFileData.class);
                }
                lot = new Lot();
                lot.setCircleLayerFeatureList(new ArrayList<>(circleLayerFeatureList));
                lot.setFillLayerPointList(new ArrayList<>(fillLayerPointList));
                lot.setLineLayerPointList(new ArrayList<>(lineLayerPointList));


                lots.add(lot);
                in.putExtra(Constant.LOT, lot);
                in.putExtra(Constant.MASTER_FILE, md);
                startActivityForResult(in, ACTIVITY_LOT_RESULT_CODE);
                return true;

            case R.id.log_out:
                showAlertDialog();
                return true;

/*

            case R.id.send:

                try {
                    LotMixinContent data = new LotMixinContent();
                    String directory = Environment.getExternalStorageDirectory() +
                            File.separator + "Maps" + File.separator + "Land" + File.separator + masterFile.getMasterFileRefNumber().replaceAll("/", "") + File.separator + lot.getLotId() + ".json";
                    Gson gson = new GsonBuilder()
                            .create();
                    JsonReader reader = null;
                    try {

                        File file1 = new DataManager().getLatestLotFile(masterFile.getMasterFileRefNumber().replaceAll("/", ""), lot.getLotId());

                        reader = new JsonReader(new FileReader(file1));

                        data = gson.fromJson(reader, LotMixinContent.class);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    CR conditionalReport = new CR();
                    DataManager dataManager = new DataManager();
                    try {
                        conditionalReport = dataManager.getCRData(masterFile.getMasterFileRefNumber().replaceAll("/", ""), lot.getLotId());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    saveCRData(data, conditionalReport, this.markers);

                } catch (NullPointerException e) {
                    showErrorMessage("Please save the lot");
                } catch (Exception e) {
                    showErrorMessage("Something went wrong");
                }
                return true;
*/

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void initStyle(com.mapbox.mapboxsdk.maps.Style style) {

        // ScaleBarPlugin scaleBarPlugin = new ScaleBarPlugin(binding.map, mapboxMap);

        // Create a ScaleBarOptions object to use the Plugin's default styling
        //   scaleBarPlugin.create(new ScaleBarOptions(this));


        mapboxMap.setLatLngBoundsForCameraTarget(RESTRICTED_BOUNDS_AREA);

       /* if (masterFile.getBbox().length == 4) {
            LatLng locationOne = new LatLng(masterFile.getBbox()[0], masterFile.getBbox()[1]);
            LatLng locationTwo = new LatLng(masterFile.getBbox()[2], masterFile.getBbox()[3]);


            LatLngBounds latLngBounds = new LatLngBounds.Builder()
                    .include(locationOne) // Northeast
                    .include(locationTwo) // Southwest
                    .build();


            mapboxMap.easeCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 50), 4000);

            // Set up the OfflineManager
            offlineManager = OfflineManager.getInstance(MapViewActivity.this);


// Define the offline region
            OfflineTilePyramidRegionDefinition definition = new OfflineTilePyramidRegionDefinition(
                    mapboxMap.getStyle().getUri(),
                    latLngBounds,
                    10,
                    20,
                    MapViewActivity.this.getResources().getDisplayMetrics().density);

// Set the metadata
            byte[] metadata;
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(JSON_FIELD_REGION_NAME, masterFile.getMasterFileName());
                String json = jsonObject.toString();
                metadata = json.getBytes(JSON_CHARSET);
            } catch (Exception exception) {
                Timber.e("Failed to encode metadata: %s", exception.getMessage());
                metadata = null;
            }

// Create the region asynchronously
            if (metadata != null) {
                offlineManager.createOfflineRegion(
                        definition,
                        metadata,
                        new OfflineManager.CreateOfflineRegionCallback() {
                            @Override
                            public void onCreate(OfflineRegion offlineRegion) {
                                offlineRegion.setDownloadState(OfflineRegion.STATE_ACTIVE);

// Display the download progress bar


// Monitor the download progress using setObserver
                                offlineRegion.setObserver(new OfflineRegion.OfflineRegionObserver() {
                                    @Override
                                    public void onStatusChanged(OfflineRegionStatus status) {

// Calculate the download percentage and update the progress bar
                                        double percentage = status.getRequiredResourceCount() >= 0
                                                ? (100.0 * status.getCompletedResourceCount() / status.getRequiredResourceCount()) :
                                                0.0;

                                        if (status.isComplete()) {
// Download complete
                                        } else if (status.isRequiredResourceCountPrecise()) {
// Switch to determinate state
                                            // setPercentage((int) Math.round(percentage));

                                        }
                                    }

                                    @Override
                                    public void onError(OfflineRegionError error) {
// If an error occurs, print to logcat
                                        Timber.e("onError reason: %s", error.getReason());
                                        Timber.e("onError message: %s", error.getMessage());
                                    }

                                    @Override
                                    public void mapboxTileCountLimitExceeded(long limit) {
// Notify if offline region exceeds maximum tile count
                                        Timber.e("Mapbox tile count limit exceeded: %s", limit);

                                    }
                                });
                            }

                            @Override
                            public void onError(String error) {
                                Timber.e("Error: %s", error);
                            }
                        });
            }

        } else {*/
        mapboxMap.animateCamera(
                CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                        .zoom(5)
                        .build()), 4000);


//        }


// Add sources to the map

        LatLngQuad quad = new LatLngQuad(
                new LatLng(14.689853, 69.438848),
                new LatLng(-2.372397, 69.438848),
                new LatLng(-2.372397, 90.972051),
                new LatLng(14.689853, 90.972051)
        );

        style.addSource(new ImageSource(ID_IMAGE_SOURCE, quad, R.drawable.map_sheet));
        style.addLayer(new RasterLayer(ID_IMAGE_LAYER, ID_IMAGE_SOURCE));
        style.getLayer(ID_IMAGE_LAYER).setProperties(visibility(NONE));


        mapboxMap.getUiSettings().setRotateGesturesEnabled(false);

        mapboxMap.setMinZoomPreference(8f);

        circleSource = initCircleSource(style);
        fillSource = initFillSource(style);
        lineSource = initLineSource(style);

        landSymbolManager = new SymbolManager(binding.map, mapboxMap, style);
        buildingLabelManagerMap = new HashMap<>();
        buildingManager = new SymbolManager(binding.map, mapboxMap, style);

// Add layers to the map
        initCircleLayer(style);
        initLineLayer(style);
        initFillLayer(style);

        if (lot != null) {
            try {
                lot = lots.get(lots.indexOf(lot));


                circleLayerFeatureList = lot.getCircleLayerFeatureList();
                fillLayerPointList = lot.getFillLayerPointList();
                lineLayerPointList = lot.getLineLayerPointList();


                if (circleSource != null) {
                    circleSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList));
                }

                if (lineSource != null) {
                    lineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                            {Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList))}));
                }

                listOfList = new ArrayList<>();
                listOfList.add(fillLayerPointList);
                List<Feature> finalFeatureList = new ArrayList<>();
                finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
                if (fillSource != null) {
                    fillSource.setGeoJson(newFeatureCollection);
                }
            } catch (Exception e) {

            }
        }

        if (bundleInfo.isATFile()) {


            String geojson = loadJsonFromAsset(bundleInfo.getAtFilePath());

            GeoJsonSource atGeoJson = new GeoJsonSource(
                    Constant.AT_DATA, geojson);
            GeoJsonSource atTextGeoJson = new GeoJsonSource(
                    Constant.AT_TEXT_DATA, geojson);
            style.addSource(atGeoJson);
            style.addSource(atTextGeoJson);
            addATLayerToMap(style);
            addATLabelLayerToMap(style);
            try {
                Geometry g = FeatureCollection.fromJson(geojson).features().get(0).geometry();
                String s = g.toJson();


                if (s.contains("LineString")) {
                    LineJson lj = new Gson().fromJson(s, LineJson.class);
                    if (lj.getCoordinates().size() >= 1) {
                        if (lj.getCoordinates().get(0).size() >= 2) {
                            zoomPoint = new LatLng();
                            zoomPoint.setLatitude(lj.getCoordinates().get(0).get(0));
                            zoomPoint.setLongitude(lj.getCoordinates().get(0).get(1));

                            CameraPosition position = new CameraPosition.Builder()
                                    .target(new LatLng(zoomPoint.getLongitude(), zoomPoint.getLatitude())) // Sets the new camera position
                                    .zoom(15) // Sets the zoom
                                    .build(); // Creates a CameraPosition from the builder

                            mapboxMap.animateCamera(CameraUpdateFactory
                                    .newCameraPosition(position), 3000);

                        }
                    }
                }
            } catch (Exception e) {
            }
        }

        if (bundleInfo.isPPFile()) {

            String geojson = loadJsonFromAsset(bundleInfo.getPpFilePath());


            GeoJsonSource ppGeoJson = new GeoJsonSource(
                    Constant.PP_DATA, geojson);

            GeoJsonSource ppTextGeoJson = new GeoJsonSource(
                    Constant.PP_TEXT_DATA, geojson);

            try {
                Geometry g = FeatureCollection.fromJson(geojson).features().get(0).geometry();
                String s = g.toJson();

                if (s.contains("LineString")) {
                    LineJson lj = new Gson().fromJson(s, LineJson.class);
                    if (lj.getCoordinates().size() >= 1) {
                        if (lj.getCoordinates().get(0).size() >= 2) {
                            zoomPoint = new LatLng();
                            zoomPoint.setLatitude(lj.getCoordinates().get(0).get(0));
                            zoomPoint.setLongitude(lj.getCoordinates().get(0).get(1));

                            CameraPosition position = new CameraPosition.Builder()
                                    .target(new LatLng(zoomPoint.getLongitude(), zoomPoint.getLatitude())) // Sets the new camera position
                                    .zoom(15) // Sets the zoom
                                    .build(); // Creates a CameraPosition from the builder

                            mapboxMap.animateCamera(CameraUpdateFactory
                                    .newCameraPosition(position), 3000);

                        }
                    }
                }


                style.addSource(ppTextGeoJson);
                style.addSource(ppGeoJson);
                addPPLayerToMap(style);
                addPPLabelLayerToMap(style);

            } catch (Exception e) {
            }
        }

        if (bundleInfo.isCadesterFile()) {

            GeoJsonSource cadesterGeoJson = new GeoJsonSource(
                    Constant.CADESTER_DATA, loadJsonFromAsset(bundleInfo.getCadesterFilePath()));

            style.addSource(cadesterGeoJson);
            addCadesterLayerToMap(style);
        }


        initDrawing();


        Bitmap bitmapls = BitmapFactory.decodeResource(getResources(), R.drawable.marker_ls);
        mapboxMap.getStyle().addImage(LS_ICON_ID, bitmapls);

        Bitmap bitmapmulti = BitmapFactory.decodeResource(getResources(), R.drawable.marker_multi);
        mapboxMap.getStyle().addImage(BR_ICON_ID, bitmapmulti);

        Bitmap bitmapre = BitmapFactory.decodeResource(getResources(), R.drawable.marker_re);
        mapboxMap.getStyle().addImage(RE_ICON_ID, bitmapre);

        Bitmap bitmappv = BitmapFactory.decodeResource(getResources(), R.drawable.marker_pv);
        mapboxMap.getStyle().addImage(PV_ICON_ID, bitmappv);

        Bitmap bitmapselected = BitmapFactory.decodeResource(getResources(), R.drawable.marker_selected);
        mapboxMap.getStyle().addImage(SELECT_ICON_ID, bitmapselected);

        // create symbol manager object
        symbolManager = new SymbolManager(binding.map, mapboxMap, style);


        // add click listeners if desired
        symbolManager.addClickListener(symbol -> {
                    symbolReserve = symbol;
                    Intent i = new Intent(MapViewActivity.this, MarkerMenuActivity.class);
                    JsonElement je = symbol.getData();
                    Marker m = new Gson().fromJson(je, Marker.class);
                    i.putExtra(Constant.MARKER, m);
                    i.putExtra(Constant.LOT, lot.getLotId());
                    i.putExtra(Constant.MASTER_FILE, masterFile);
                    startActivityForResult(i, ACTIVITY_MARKER_MENU_RESULT_CODE);

                }
        );

        lotLabelManager = new SymbolManager(binding.map, mapboxMap, style);
        buildingDetailManager = new SymbolManager(binding.map, mapboxMap, style);
        floorDetailManager = new SymbolManager(binding.map, mapboxMap, style);
        lineManager = new LineManager(binding.map, mapboxMap, style);
        circleManager = new CircleManager(binding.map, mapboxMap, style);


        symbolManager.addLongClickListener(symbol -> {
            if (symbolTemp == null) {
                symbolTemp = symbol;
                symbol.setIconImage(SELECT_ICON_ID);
                symbolManager.update(symbol);
            } else {
                Toast.makeText(MapViewActivity.this, "Please place previously selected marker", Toast.LENGTH_LONG).show();
            }
        });


        GeoJsonSource drawLineSource = style.getSourceAs(FREEHAND_DRAW_LINE_LAYER_SOURCE_ID);
        if (drawLineSource != null) {
            drawLineSource.setGeoJson(LineString.fromLngLats(freehandTouchPointListForLine));
        }

// Create and show a FillLayer polygon where the search area is
        GeoJsonSource fillPolygonSource = style.getSourceAs(FREEHAND_DRAW_FILL_LAYER_SOURCE_ID);
        List<List<Point>> polygonList = new ArrayList<>();
        polygonList.add(freehandTouchPointListForPolygon);
        Polygon drawnPolygon = Polygon.fromLngLats(polygonList);
        if (fillPolygonSource != null) {
            fillPolygonSource.setGeoJson(drawnPolygon);
        }

        if (mode == 2) {
            loadDataLayer(lotMixinContent);
        }


        symbolManager.setIconAllowOverlap(true);
        symbolManager.setIconRotationAlignment(ICON_ROTATION_ALIGNMENT_VIEWPORT);
        if (lot != null) {
            try {
                String json = new DataManager().loadJson(masterFile.getMasterFileRefNumber().replaceAll("/", "") + File.separator + this.lot.getLotId(), "markers.json");
                List<Marker> mList = new Gson().fromJson(json, new TypeToken<List<Marker>>() {
                }.getType());
                this.markers = mList;
                for (Marker m : this.markers) {
                    String icon = "";

                    if (m.getType().equals(Marker_Type.LS.name())) {
                        icon = LS_ICON_ID;
                    } else if (m.getType().equals(Marker_Type.PV.name())) {
                        icon = PV_ICON_ID;
                    } else if (m.getType().equals(Marker_Type.BR.name())) {
                        icon = BR_ICON_ID;
                    } else {
                        icon = RE_ICON_ID;
                    }


                    JsonElement js = new Gson().toJsonTree(m, Marker.class);

                    symbolManager.create(new SymbolOptions()
                            .withLatLng(new LatLng(m.getLatLng().getLatitude(), m.getLatLng().getLongitude()))
                            .withIconImage(icon)
                            .withDraggable(false)
                            .withData(js));

                }
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }

        }

        mapboxMap.addOnMapClickListener(v -> {

            PointF pointf = mapboxMap.getProjection().toScreenLocation(v);
            RectF rectF = new RectF(pointf.x - 20, pointf.y - 20, pointf.x + 20, pointf.y + 20);


            //move marker
            if (symbolTemp != null) {
                if (isSaved) {
                    Point mapTargetPoint = Point.fromLngLat(mapboxMap.getCameraPosition().target.getLongitude(),
                            mapboxMap.getCameraPosition().target.getLatitude());
                    symbolTemp.setGeometry(mapTargetPoint);
                    JsonElement je = symbolTemp.getData();
                    Marker m = new Gson().fromJson(je, Marker.class);
                    String icon = "";
                    if (m.getType().equals(Marker_Type.LS.name())) {
                        icon = LS_ICON_ID;
                    } else if (m.getType().equals(Marker_Type.PV.name())) {
                        icon = PV_ICON_ID;
                    } else if (m.getType().equals(Marker_Type.BR.name())) {
                        icon = BR_ICON_ID;
                    } else {
                        icon = RE_ICON_ID;
                    }


                    try {
                        int index = markers.indexOf(m);
                        m.setLatLng(new LatLng(mapTargetPoint.latitude(), mapTargetPoint.longitude()));
                        markers.set(index, m);
                        new DataManager().writeSymbolToFile(markers, masterFile.getMasterFileRefNumber().replaceAll("/", ""), lot);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    JsonElement json = new Gson().toJsonTree(m, Marker.class);


                    symbolTemp.setData(json);
                    symbolTemp.setIconImage(icon);
                    symbolManager.update(symbolTemp);
                    symbolTemp = null;

                } else {
                    Toast.makeText(MapViewActivity.this, "Please save the selected lot", Toast.LENGTH_LONG).show();
                }
            }

            if (isSaved) {
                if (!isDrawingMode && !isBuildingSketchMode) {
                    List<Feature> featureList = mapboxMap.queryRenderedFeatures(rectF, FILL_LAYER_ID);
                    for (Feature feature : featureList) {
                        MasterFileData masterData = new MasterFileData();
                        if (bundleInfo.isMasterFile()) {
                            String data = new DataManager().loadGeoJsonFromAsset(bundleInfo.getMasterFilePath());
                            masterData = new Gson().fromJson(data, MasterFileData.class);

                            Intent i = new Intent(MapViewActivity.this, MenuActivity.class);
                            i.putExtra(Constant.LOT, rectF);
                            i.putExtra(Constant.MASTER_DATA, masterData);
                            i.putExtra(Constant.MASTER_FILE, masterFile.getMasterFileRefNumber().replaceAll("/", ""));
                            i.putExtra(Constant.DETAILS, lot.getLotId());
                            startActivityForResult(i, ACTIVITY_RESULT_CODE);
                        }
                    }
                } else {
                    try {


                        if (constructions.size() > 0) {
                            for (Construction construction : constructions) {
                                // Log.e("fd",building.getType().equals(Construction_Type.Building.name())+"");
                                if (!isBuildingSketchMode) {
                                    if (!isAqLineDraw) {
                                        if (construction.getType().equals(Construction_Type.Building.name())) {
                                            switch (construction.getDrawingType()) {
                                                case "LINE":
                                                    List<Feature> featureLineList = mapboxMap.queryRenderedFeatures(rectF, construction.getLineLayerID());
                                                    for (Feature feature : featureLineList) {
                                                        enableBuildingDrawingTool(rectF, construction.getLineLayerID(), construction);
                                                    }
                                                    break;
                                                case "CIRCLE":
                                                    List<Feature> featureCircleList = mapboxMap.queryRenderedFeatures(rectF, construction.getFillLayerID());
                                                    for (Feature feature : featureCircleList) {
                                                        enableBuildingDrawingTool(rectF, construction.getFillLayerID(), construction);
                                                    }
                                                    break;
                                                case "POLYGON":
                                                    List<Feature> featurePolygonList = mapboxMap.queryRenderedFeatures(rectF, construction.getFillLayerID());
                                                    for (Feature feature : featurePolygonList) {
                                                        enableBuildingDrawingTool(rectF, construction.getFillLayerID(), construction);
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }

                                if (construction.getType().equals(Construction_Type.Building.name())) {
                                    if (isAqLineDraw) {


                                        if (currentFloor == 0) {
                                            List<Feature> featureLineList = mapboxMap.queryRenderedFeatures(rectF, construction.getCircleLayerID());

                                            for (Feature feature : featureLineList) {
                                                double minDistance = Double.MAX_VALUE;
                                                Feature point = feature;

                                                for (Feature f : construction.getCircleLayerFeatureList()) {
                                                    double distance = MapCalculations.calculateDistance((Point) feature.geometry(), (Point) f.geometry());
                                                    if (distance <= minDistance) {
                                                        minDistance = distance;

                                                        point = f;
                                                    }

                                                }


                                                drawAcqLineSelect(point);
                                            }
                                        } else if (currentFloor > 0) {

                                            for (FloorDrawing fd : building.getBuildingFloorList().get(currentFloor).getFloorDrawingList()) {
                                                if (fd.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name())) {
                                                    if (fd.getCircleLayerFeatureList().equals(construction.getCircleLayerFeatureList())) {
                                                        List<Feature> featureLineList = mapboxMap.queryRenderedFeatures(rectF, construction.getCircleLayerID());
                                                        for (Feature feature : featureLineList) {
                                                            double minDistance = Double.MAX_VALUE;
                                                            Feature point = feature;

                                                            for (Feature f : construction.getCircleLayerFeatureList()) {
                                                                double distance = MapCalculations.calculateDistance((Point) feature.geometry(), (Point) f.geometry());
                                                                if (distance <= minDistance) {
                                                                    minDistance = distance;
                                                                    point = f;

                                                                }

                                                            }
                                                            drawAcqLineSelect(point);
                                                        }
                                                    } else {
                                                        List<Feature> featureCircleList = mapboxMap.queryRenderedFeatures(rectF, fd.getCircleLayerID());
                                                        boolean isChecked = false;
                                                        for (Feature feature : featureCircleList) {
                                                            List<Feature> points = fd.getCircleLayerFeatureList();
                                                            double minDistance = Double.MAX_VALUE;
                                                            Feature point = feature;
                                                            for (Feature f : points) {
                                                                double distance = MapCalculations.calculateDistance((Point) feature.geometry(), (Point) f.geometry());
                                                                if (distance <= minDistance) {
                                                                    minDistance = distance;
                                                                    point = f;
                                                                }
                                                            }
                                                            drawAcqLineSelect(point);
                                                            isChecked = true;
                                                        }
                                                        if (!isChecked) {
                                                            if (featureCircleList.size() == 0) {
                                                                int i = 0;
                                                                for (BuildingFloor floor : building.getBuildingFloorList()) {
                                                                    if (currentFloor != i) {
                                                                        for (FloorDrawing fld : floor.getFloorDrawingList()) {
                                                                            if (fld.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name())) {
                                                                                if (fd.getCircleLayerFeatureList().equals(fld.getCircleLayerFeatureList())) {
                                                                                    List<Feature> featureLineList = mapboxMap.queryRenderedFeatures(rectF, fld.getCircleLayerID(), fd.getCircleLayerID());

                                                                                    for (Feature feature : featureLineList) {
                                                                                        double minDitance = Double.MAX_VALUE;
                                                                                        Feature point = feature;
                                                                                        for (Feature f : construction.getCircleLayerFeatureList()) {
                                                                                            double distance = MapCalculations.calculateDistance((Point) feature.geometry(), (Point) f.geometry());
                                                                                            if (distance <= minDitance) {
                                                                                                minDitance = distance;
                                                                                                point = f;
                                                                                            }

                                                                                        }
                                                                                        drawAcqLineSelect(point);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                i++;
                                                            }
                                                        }
                                                    }


                                                    break;
                                                }
                                            }


                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                showErrorMessage("Please save drawn lot.");
            }
            return true;
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTIVITY_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                boolean result = data.getBooleanExtra("sketch", false);
                RectF rectF = data.getParcelableExtra(Constant.LOT);
                if (result) {
                    if (isSaved) {
                        enableSketchMode(rectF);
                    } else {
                        Toast.makeText(MapViewActivity.this, "Please save the selected lot", Toast.LENGTH_LONG).show();
                    }
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        } else if (requestCode == ACTIVITY_FLOOR_ADD_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                String floorLabel = data.getStringExtra(Constant.FLOOR_Label);
                int floorType = data.getIntExtra(Constant.FLOOR_TYPE, -1);
                BuildingFloor floor = new BuildingFloor();
                floor.setFloorNo(floorLabel);
                floor.setFloorDrawingList(new ArrayList<>());
                if (floorType == 1) {
                    floor.setuFloors(1);
                    floor.setgFloors(0);
                } else if (floorType == 2) {
                    floor.setgFloors(1);
                    floor.setuFloors(0);
                }
                buildingFloors.add(floor);
                floorAdapter.setFloors(buildingFloors);
                building.setBuildingFloorList(buildingFloors);
            }

        } else if (requestCode == ACTIVITY_COPY_LAYER_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                FloorCopyDetails current = data.getParcelableExtra(Constant.FLOOR_Label);
                FloorCopyDetails copyFrom = data.getParcelableExtra(Constant.DETAILS);
                FloorCopyDetails copy = data.getParcelableExtra(Constant.CONFIG);

                copyLayers(current, copyFrom, copy);
            }
        } else if (requestCode == RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                landSymbolManager.delete(landLabelSymbols.get(position));
                landLabelSymbols.remove(position);
                labelListAdapter.setLabel(new ArrayList<>(landLabelSymbols));
                buildingSaveDrawing();
            }
        } else if (requestCode == DRAWING_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    FloorDrawing fd = floorDrawing.getItem(position);
                    if (fd.getDrawingType().equals(DrawingType.CIRCLE.name())) {
                        try {

                            if (mapboxMap.getStyle().getLayer(fd.getCircleLayerID()) != null) {
                                mapboxMap.getStyle().removeLayer(fd.getCircleLayerID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {

                            if (mapboxMap.getStyle().getLayer(fd.getFillLayerID()) != null) {
                                mapboxMap.getStyle().removeLayer(fd.getFillLayerID());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            if (mapboxMap.getStyle().getSource(fd.getCircleSourceID()) != null) {
                                mapboxMap.getStyle().removeSource(fd.getCircleSourceID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            if (mapboxMap.getStyle().getSource(fd.getFillSourceID()) != null) {
                                mapboxMap.getStyle().removeSource(fd.getFillSourceID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    if (fd.getDrawingType().equals(DrawingType.LINE.name())) {


                        try {
                            if (mapboxMap.getStyle().getLayer(fd.getCircleLayerID()) != null) {
                                mapboxMap.getStyle().removeLayer(fd.getCircleLayerID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            if (mapboxMap.getStyle().getSource(fd.getCircleSourceID()) != null) {
                                mapboxMap.getStyle().removeSource(fd.getCircleSourceID());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            if (mapboxMap.getStyle().getLayer(fd.getLineLayerID()) != null) {
                                mapboxMap.getStyle().removeLayer(fd.getLineLayerID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            if (mapboxMap.getStyle().getSource(fd.getLineSourceID()) != null) {
                                mapboxMap.getStyle().removeSource(fd.getLineSourceID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    if ((fd.getDrawingType().equals(DrawingType.BROOM_POLYGON.name())) || (fd.getDrawingType().equals(DrawingType.POLYGON.name())) || (fd.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name()))) {
                        try {

                            if (mapboxMap.getStyle().getLayer(fd.getCircleLayerID()) != null) {
                                mapboxMap.getStyle().removeLayer(fd.getCircleLayerID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            if (mapboxMap.getStyle().getSource(fd.getCircleSourceID()) != null) {
                                mapboxMap.getStyle().removeSource(fd.getCircleSourceID());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            if (mapboxMap.getStyle().getLayer(fd.getLineLayerID()) != null) {
                                mapboxMap.getStyle().removeLayer(fd.getLineLayerID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            if (mapboxMap.getStyle().getSource(fd.getLineSourceID()) != null) {
                                mapboxMap.getStyle().removeSource(fd.getLineSourceID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            if (mapboxMap.getStyle().getLayer(fd.getFillLayerID()) != null) {
                                mapboxMap.getStyle().removeLayer(fd.getFillLayerID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            if (mapboxMap.getStyle().getSource(fd.getFillSourceID()) != null) {
                                mapboxMap.getStyle().removeSource(fd.getFillSourceID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    if ((fd.getDrawingType().equals(DrawingType.AQ_POLYGON.name())) || (fd.getDrawingType().equals(DrawingType.AQLINE.name())) || (fd.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name()))) {
                        try {
                            if (mapboxMap.getStyle().getLayer(fd.getAqCircleLayerID()) != null) {
                                mapboxMap.getStyle().removeLayer(fd.getAqCircleLayerID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {

                            if (mapboxMap.getStyle().getSource(fd.getAqCircleSourceID()) != null) {
                                mapboxMap.getStyle().removeSource(fd.getAqCircleSourceID());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            if (mapboxMap.getStyle().getLayer(fd.getAqLineLayerID()) != null) {
                                mapboxMap.getStyle().removeLayer(fd.getAqLineLayerID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            if (mapboxMap.getStyle().getSource(fd.getAqLineSourceID()) != null) {
                                mapboxMap.getStyle().removeSource(fd.getAqLineSourceID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            if (mapboxMap.getStyle().getLayer(fd.getAqFillLayerID()) != null) {
                                mapboxMap.getStyle().removeLayer(fd.getAqFillLayerID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            if (mapboxMap.getStyle().getSource(fd.getAqFillSourceID()) != null) {
                                mapboxMap.getStyle().removeSource(fd.getAqFillSourceID());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        }

                    }

                    int fdd = floorDrawingList.indexOf(fd);


                    if (fdd != -1) {
                        floorDrawingList.get(fdd).setCircleFillPoints(new ArrayList<>());
                        floorDrawingList.get(fdd).setLineLayerPointList(new ArrayList<>());
                        floorDrawingList.get(fdd).setCircleLayerFeatureList(new ArrayList<>());
                        floorDrawingList.get(fdd).setLineLayerPointList(new ArrayList<>());
                        floorDrawingList.get(fdd).setFillLayerPointList(new ArrayList<>());
                        floorDrawingList.get(fdd).setCircleLayerAQFeatureList(new ArrayList<>());
                        floorDrawingList.get(fdd).setFillLayerAQPointList(new ArrayList<>());
                        floorDrawingList.get(fdd).setLineLayerAQPointList(new ArrayList<>());
                        floorDrawingList.get(fdd).setDrawingType(DrawingType.CNULL.name());
                    }

                    floorDetailManager.deleteAll();
                    floorSymbols.clear();


                    floorDrawing.setFloors(new ArrayList<>(floorDrawingList));

                    double area = calculateArea(building.getLineLayerPointList());
                    double inchtot = (area * 1550.0031);
                    int feet = (int) (inchtot / (144));
                    int inches = (int) Math.round(inchtot % (144));

                    if (inches == 144) {
                        inches = 0;
                        feet += 1;
                    }
                    binding.layoutBuildingDrawingTool.tvTotalBuildingArea.setText(Html.fromHtml(feet + " ft<sup>2</sup> " + inches + " inch<sup>2</sup>"));


                    List<List<Point>> points = new ArrayList<>();


                    try {
                        for (FloorDrawing ff : building.getBuildingFloorList().get(currentFloor).getFloorDrawingList()) {
                            if ((ff.getDrawingType().equals(DrawingType.AQLINE.name())) || (ff.getDrawingType().equals(DrawingType.AQ_POLYGON.name()))) {
                               points.add(ff.getLineLayerAQPointList());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    double aqArea = calculateTotalAquiredArea(points);


                        double inchtotAQ = (aqArea * 1550.0031);
                        int feetAQ = (int) (inchtotAQ / (144));
                        int inchesAQ = (int) Math.round(inchtotAQ % (144));

                        if (inchesAQ == 144) {
                            inchesAQ = 0;
                            feetAQ += 1;
                        }
                        binding.layoutBuildingDrawingTool.tvAcquiredArea.setText(Html.fromHtml(feetAQ + " ft<sup>2</sup> " + inchesAQ + " inch<sup>2</sup>"));


                    for (FloorDrawing f : floorDrawingList) {
                        showBuildingLineDistances(f.getLineLayerAQPointList());
                        showBuildingLineDistances(f.getLineLayerPointList());

                    }


                    List<io.xiges.ximapper.model.Symbol> symbols = new ArrayList<>();
                    for (Symbol s : floorSymbols) {
                        io.xiges.ximapper.model.Symbol symbol = new io.xiges.ximapper.model.Symbol();
                        symbol.setLatLng(s.getLatLng());
                        symbol.setName(s.getTextField());
                        symbols.add(symbol);
                    }


                    buildingFloors.get(currentFloor).setSymbols(symbols);
                    buildingFloors.get(currentFloor).setFloorDrawingList(new ArrayList<>(floorDrawingList));
                    this.building.setBuildingFloorList(new ArrayList<>(buildingFloors));
                    constructions.set(constructions.indexOf(building), this.building);

                    DataManager dataManager = new DataManager();
                    binding.layoutDrawingTool.btnSaveDraw.setEnabled(false);
                    binding.layoutDrawingTool.btnDraw.setEnabled(false);
                    lot.setConstructions(constructions);


                    try {
                        dataManager.writeToFile(lot, masterFile.getMasterFileName());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } else if (requestCode == ACTIVITY_LAND_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                String name = data.getStringExtra(Constant.NAME);
                int type = data.getIntExtra(Constant.TYPE, 0);
                lastPointMarked = null;
                binding.tvLandDistance.setText("0 m");
                int id = constructions.size() + 1;

                try {
                    conditionalReport = dataManager.getCRData(masterFile.getMasterFileRefNumber().replaceAll("/", ""), lot.getLotId());
                } catch (IOException e) {
                    e.printStackTrace();
                }


                if (isPolygonDraw) {
                    circleLayerDrawingFeatureList.clear();
                    lineLayerDrawingPointList.clear();
                    fillLayerDrawingPointList.clear();
                    circleLineSource = initDrawingCircleSource(mapboxMap.getStyle(), "Circle" + id);
                    lineLineSource = initDrawingLineSource(mapboxMap.getStyle(), "Line" + id);
                    fillLineSource = initDrawingFillSource(mapboxMap.getStyle(), "Fill" + id);
                    initDrawingFillLayer(mapboxMap.getStyle(), "LayerFill" + id, "Fill" + id);
                    initDrawingLineLayer(mapboxMap.getStyle(), "LayerLine" + id, "Line" + id, "LayerFill" + id);
                    initDrawingCircleLayer(mapboxMap.getStyle(), "LayerCircle" + id, "Circle" + id, "LayerLine" + id);
                } else if (isCircleDraw) {
                    circleLayerDrawingFeatureList.clear();
                    circleLineSource = initDrawingCircleSource(mapboxMap.getStyle(), "Circle" + id);
                    initDrawingCircleLayer(mapboxMap.getStyle(), "LayerCircle" + id, "Circle" + id, "");
                    fillLineSource = initDrawingFillSource(mapboxMap.getStyle(), "Fill" + id);
                    initDrawingFillLayer(mapboxMap.getStyle()
                            , "LayerFill" + id, "Fill" + id);
                } else if (isLineDraw) {
                    circleLayerDrawingFeatureList.clear();
                    lineLayerDrawingPointList.clear();
                    circleLineSource = initDrawingCircleSource(mapboxMap.getStyle(), "Circle" + id);
                    lineLineSource = initDrawingLineSource(mapboxMap.getStyle(), "Line" + id);
                    initDrawingLineLayer(mapboxMap.getStyle(), "LayerLine" + id, "Line" + id, "");
                    initDrawingCircleLayer(mapboxMap.getStyle(), "LayerCircle" + id, "Circle" + id, "LayerLine" + id);
                }


                if (isPolygonDraw) {
                    BuildingArea buildingArea = new BuildingArea();
                    buildingArea.setName(name);
                    buildingArea.setArea(calculateArea(lineLayerDrawingPointList));
                    landArea.add(buildingArea);
                    landAreaBuildingAdapter.setLandBuildingAreaList(landArea);
                } else if (isCircleDraw) {
                    BuildingArea buildingArea = new BuildingArea();
                    buildingArea.setName(name);
                    buildingArea.setArea(calculateCircleArea());
                    landArea.add(buildingArea);
                    landAreaBuildingAdapter.setLandBuildingAreaList(landArea);
                }


                if (type == 0) {
                    Construction building = new Construction();
                    building.setName(name);
                    building.setType(Construction_Type.Building.name());
                    building.setId(id + "" + name);
                    currentStructure = building.getId();


                    Building b = new Building();
                    b.setNo(building.getId());
                    b.setName(building.getName());


                    BuildingDetails buildingDetails = conditionalReport.getBuildingDetails();
                    buildingDetails.getBuildings().add(b);

                    conditionalReport.setBuildingDetails(buildingDetails);
                    try {
                        dataManager.saveCRData(conditionalReport, masterFile.getMasterFileRefNumber().replaceAll("/", ""), lot.getLotId());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (isLineDraw) {
                        building.setDrawingType(DrawingType.LINE.name());
                        building.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                        building.setLineLayerPointList(new ArrayList<>(lineLayerDrawingPointList));
                        building.setLineLayerID("LayerLine" + id);
                        building.setCircleLayerID("LayerCircle" + id);
                        building.setLineSourceID("Line" + id);
                        building.setCircleSourceID("Circle" + id);
                        // isLineDraw = false;
                        binding.layoutDrawingTool.imgBtnDrawLine.setEnabled(true);

                    } else if (isCircleDraw) {
                        building.setDrawingType(DrawingType.CIRCLE.name());
                        building.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                        Circle circle = new Circle();
                        circle.setUnit("meters");
                        circle.setRadius(radius);
                        circle.setCircleFillPoints(new ArrayList<>(circlePointList));
                        circle.setMidPoint(firstPointOfPolygon);
                        building.setCircle(circle);
                        building.setFillLayerID("LayerFill" + id);
                        building.setCircleLayerID("LayerCircle" + id);
                        building.setFillSourceID("Fill" + id);
                        building.setCircleSourceID("Circle" + id);


                        //  isCircleDraw = false;
                        binding.layoutDrawingTool.imgBtnDrawCircle.setEnabled(true);

                    } else if (isPolygonDraw) {
                        building.setDrawingType(DrawingType.POLYGON.name());
                        building.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                        building.setLineLayerPointList(new ArrayList<>(lineLayerDrawingPointList));
                        building.setFillLayerPointList(new ArrayList<>(fillLayerDrawingPointList));
                        building.setLineLayerID("LayerLine" + id);
                        building.setCircleLayerID("LayerCircle" + id);
                        building.setFillLayerID("LayerFill" + id);
                        building.setFillSourceID("Fill" + id);
                        building.setCircleSourceID("Circle" + id);
                        building.setLineSourceID("Line" + id);
                        //   isPolygonDraw = false;
                        binding.layoutDrawingTool.imgBtnDrawLine.setEnabled(true);

                    }

                    List<io.xiges.ximapper.model.Symbol> consSymbol = new ArrayList<>();
                    for (Symbol symbol : labelSymbolBuildingList) {
                        io.xiges.ximapper.model.Symbol s = new io.xiges.ximapper.model.Symbol();
                        s.setLatLng(symbol.getLatLng());
                        s.setName(symbol.getTextField());
                        consSymbol.add(s);
                    }
                    building.setBuildingLabels(consSymbol);

                    this.building = building;
                    constructions.add(building);
                } else {
                    Construction other = new Construction();
                    other.setName(name);
                    other.setId(id + "" + name);
                    currentStructure = other.getId();
                    other.setType(Construction_Type.Other.name());


                    OtherConstruction otherConstruction = new OtherConstruction();
                    otherConstruction.setNo(other.getId());
                    otherConstruction.setName(other.getName());
                    List<OtherConstruction> otherConstructionList = conditionalReport.getOtherConstructions();
                    otherConstructionList.add(otherConstruction);
                    conditionalReport.setOtherConstructions(otherConstructionList);
                    try {
                        dataManager.saveCRData(conditionalReport, masterFile.getMasterFileRefNumber().replaceAll("/", ""), lot.getLotId());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (isLineDraw) {
                        other.setDrawingType(DrawingType.LINE.name());
                        other.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                        other.setLineLayerPointList(new ArrayList<>(lineLayerDrawingPointList));
                        other.setLineLayerID("LayerLine" + id);
                        other.setCircleLayerID("LayerCircle" + id);
                        other.setLineSourceID("Line" + id);
                        other.setCircleSourceID("Circle" + id);
                        // isLineDraw = false;
                        binding.layoutDrawingTool.imgBtnDrawLine.setEnabled(true);

                    } else if (isCircleDraw) {
                        other.setDrawingType(DrawingType.CIRCLE.name());
                        other.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                        Circle circle = new Circle();
                        circle.setUnit("meters");
                        circle.setRadius(radius);
                        circle.setCircleFillPoints(new ArrayList<>(circlePointList));
                        circle.setMidPoint(firstPointOfPolygon);
                        other.setCircle(circle);
                        other.setFillLayerID("LayerFill" + id);
                        other.setCircleLayerID("LayerCircle" + id);
                        other.setFillSourceID("Fill" + id);
                        other.setCircleSourceID("Circle" + id);
                        //   isCircleDraw = false;
                        binding.layoutDrawingTool.imgBtnDrawCircle.setEnabled(true);

                    } else if (isPolygonDraw) {
                        other.setDrawingType(DrawingType.POLYGON.name());
                        other.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                        other.setLineLayerPointList(new ArrayList<>(lineLayerDrawingPointList));
                        other.setFillLayerPointList(new ArrayList<>(fillLayerDrawingPointList));
                        other.setLineLayerID("LayerLine" + id);
                        other.setCircleLayerID("LayerCircle" + id);
                        other.setFillLayerID("LayerFill" + id);
                        other.setFillSourceID("Fill" + id);
                        other.setCircleSourceID("Circle" + id);
                        other.setLineSourceID("Line" + id);
                        //  isPolygonDraw = false;
                        binding.layoutDrawingTool.imgBtnDrawPolygon.setEnabled(true);

                    }

                    List<io.xiges.ximapper.model.Symbol> consSymbol = new ArrayList<>();
                    for (Symbol symbol : labelSymbolBuildingList) {
                        io.xiges.ximapper.model.Symbol s = new io.xiges.ximapper.model.Symbol();
                        s.setLatLng(symbol.getLatLng());
                        s.setName(symbol.getTextField());
                        consSymbol.add(s);
                    }
                    other.setBuildingLabels(consSymbol);

                    constructions.add(other);
                }
                DataManager dataManager = new DataManager();
                // binding.layoutDrawingTool.btnSaveDraw.setEnabled(false);
                // binding.layoutDrawingTool.btnDraw.setEnabled(true);


                List<io.xiges.ximapper.model.Symbol> symbols = new ArrayList<>();
                for (Symbol symbol : symbolList) {
                    io.xiges.ximapper.model.Symbol s = new io.xiges.ximapper.model.Symbol();
                    s.setLatLng(symbol.getLatLng());
                    s.setName(symbol.getTextField());
                    symbols.add(s);
                }
                this.lot.setSymbolList(symbols);
                this.lot.setConstructions(constructions);
                try {
                    dataManager.writeToFile(this.lot, masterFile.getMasterFileName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        } else if (requestCode == ACTIVITY_LOT_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                DataManager dataManager = new DataManager();
                lot = new Lot();
                lot.setCircleLayerFeatureList(new ArrayList<>(circleLayerFeatureList));
                lot.setFillLayerPointList(new ArrayList<>(fillLayerPointList));
                lot.setLineLayerPointList(new ArrayList<>(lineLayerPointList));
                String id = data.getStringExtra(Constant.ID);
                String name = data.getStringExtra(Constant.NAME);
                lot.setLotId(id);
                lot.setLotName(name);
                List<io.xiges.ximapper.model.Symbol> symbols = new ArrayList<>();
                for (Symbol symbol : symbolList) {
                    io.xiges.ximapper.model.Symbol s = new io.xiges.ximapper.model.Symbol();
                    s.setLatLng(symbol.getLatLng());
                    s.setName(symbol.getTextField());
                    symbols.add(s);
                }

                CR conditionalReport = new CR();

                try {

                    dataManager.saveCRData(conditionalReport, masterFile.getMasterFileRefNumber().replaceAll("/", ""), lot.getLotId());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File storageDir = getApplication().getExternalFilesDir(Environment.DIRECTORY_PICTURES + "" + File.separator + "" + lotName);


                if (storageDir.isDirectory()) {
                    String[] children = storageDir.list();
                    for (String child : children) {
                        new File(storageDir, child).delete();
                    }
                }
                this.lot.setSymbolList(symbols);
                lot.setConstructions(constructions);
                try {
                    dataManager.writeToFile(lot, masterFile.getMasterFileName());
                    isSaved = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == ACTIVITY_MARKER_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (isSaved) {

                    Marker m = data.getParcelableExtra(Constant.MARKER);
                   /* View customView = LayoutInflater.from(MapViewActivity.this).inflate(
                            R.layout.marker_view, null);
                    customView.setLayoutParams(new FrameLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT));
                   */ //   MarkerView markerView = new MarkerView(new LatLng(m.getLatLng().getLatitude(),m.getLatLng().getLongitude()), customView);
                    //   markerViewManager.addMarker(markerView);
                    //  markerList.add(markerView);
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(m.getLatLng().getLongitude(), m.getLatLng().getLatitude())));

                    String icon = "";


                    if (m.getType().equals(Marker_Type.LS.name())) {
                        icon = LS_ICON_ID;
                    } else if (m.getType().equals(Marker_Type.PV.name())) {
                        icon = PV_ICON_ID;
                    } else if (m.getType().equals(Marker_Type.BR.name())) {
                        icon = BR_ICON_ID;
                    } else {
                        icon = RE_ICON_ID;
                    }
                    JsonElement json = new Gson().toJsonTree(m, Marker.class);

                    symbolManager.create(new SymbolOptions()
                            .withLatLng(new LatLng(m.getLatLng().getLatitude(), m.getLatLng().getLongitude()))
                            .withIconImage(icon)
                            .withDraggable(false)
                            .withData(json));
                    markers.add(m);

                    try {
                        new DataManager().writeSymbolToFile(markers, masterFile.getMasterFileName(), lot);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(MapViewActivity.this, "Please save the selected lot", Toast.LENGTH_LONG).show();
                }
            }

        } else if (requestCode == ACTIVITY_MARKER_MENU_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (isSaved) {

                    Marker m = data.getParcelableExtra(Constant.MARKER);

                    JsonElement je = symbolReserve.getData();
                    Marker pm = new Gson().fromJson(je, Marker.class);
                    int index = markers.indexOf(pm);
                    markers.set(index, m);


                    String icon = "";


                    if (m.getType().equals(Marker_Type.LS.name())) {
                        icon = LS_ICON_ID;
                    } else if (m.getType().equals(Marker_Type.PV.name())) {
                        icon = PV_ICON_ID;
                    } else if (m.getType().equals(Marker_Type.BR.name())) {
                        icon = BR_ICON_ID;
                    } else {
                        icon = RE_ICON_ID;
                    }


                    JsonElement json = new Gson().toJsonTree(m, Marker.class);
                    symbolReserve.setData(json);
                    symbolReserve.setIconImage(icon);
                    symbolManager.update(symbolReserve);
                    symbolReserve = null;
                    try {
                        new DataManager().writeSymbolToFile(markers, masterFile.getMasterFileName(), lot);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(MapViewActivity.this, "Please save the selected lot", Toast.LENGTH_LONG).show();
                }

            }


        } else if (requestCode == ACTIVITY_LABEL_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                int i = data.getIntExtra(Constant.TYPE, -1);
                int mode = data.getIntExtra(Constant.MODE, -1);
                if (i == 1) {
                    if (mode == 1) {
                        Point mapTargetPoint = Point.fromLngLat(mapboxMap.getCameraPosition().target.getLongitude(),
                                mapboxMap.getCameraPosition().target.getLatitude());

                        String text = data.getStringExtra(Constant.NAME);

                        Symbol symbol = landSymbolManager.create(new SymbolOptions()
                                .withGeometry(mapTargetPoint)
                                .withDraggable(false)
                                .withTextSize(18f)
                                .withTextHaloColor("WHITE")
                                .withTextColor("PURPLE")
                                .withTextFont(new String[]{"Ubuntu Bold"})
                                .withTextField(text)
                        );

                        landLabelSymbols.add(symbol);
                        landSymbolManager.update(landLabelSymbols);
                        labelListAdapter.setLabel(landLabelSymbols);
                    } else if (mode == 2) {
                        int position = data.getIntExtra(Constant.ID, -1);
                        String text = data.getStringExtra(Constant.NAME);
                        Symbol symbol = landSymbolManager.create(new SymbolOptions()
                                .withGeometry(landLabelSymbols.get(position).getGeometry())
                                .withDraggable(false)
                                .withTextSize(18f)
                                .withTextHaloColor("WHITE")
                                .withTextColor("PURPLE")
                                .withTextFont(new String[]{"Ubuntu Bold"})
                                .withTextField(text)
                        );


                        landSymbolManager.delete(landLabelSymbols.get(position));
                        landLabelSymbols.set(position, symbol);
                        labelListAdapter.setLabel(new ArrayList<>(landLabelSymbols));
                        buildingSaveDrawing();

                    }
                } else if (i == 2) {
                    if (mode == 1) {
                        Point mapTargetPoint = Point.fromLngLat(mapboxMap.getCameraPosition().target.getLongitude(),
                                mapboxMap.getCameraPosition().target.getLatitude());

                        String text = data.getStringExtra(Constant.NAME);

                        Symbol symbol = buildingManager.create(new SymbolOptions()
                                .withGeometry(mapTargetPoint)
                                .withDraggable(false)
                                .withTextSize(16f)
                                .withTextColor("PURPLE")
                                .withTextField(text)
                        );

                        floorLabelSymbols.add(symbol);
                        buildingManager.update(floorLabelSymbols);
                        floorLabelListAdapter.setLabel(floorLabelSymbols);
                        buildingSaveDrawing();
                    } else if (mode == 2) {
                        int position = data.getIntExtra(Constant.ID, -1);
                        String text = data.getStringExtra(Constant.NAME);
                        Symbol symbol = buildingManager.create(new SymbolOptions()
                                .withGeometry(floorLabelSymbols.get(position).getGeometry())
                                .withDraggable(false)
                                .withTextSize(16f)
                                .withTextColor("PURPLE")
                                .withTextField(text)
                        );

                        buildingManager.delete(floorLabelSymbols.get(position));
                        floorLabelSymbols.set(position, symbol);
                        floorLabelListAdapter.setLabel(new ArrayList<>(floorLabelSymbols));
                        buildingSaveDrawing();
                    }
                }
            }
        } else if (requestCode == ACTIVITY_MEASURE_LINE_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                DistanceToLocationCalculator d = new DistanceToLocationCalculator();
                double distance = data.getDoubleExtra(Constant.DISTANCE, -1);
                double bearing = data.getDoubleExtra(Constant.BEARING, -1);
                LatLng latLng = new LatLng();
                latLng.setLatitude(lastPointMarked.latitude());
                latLng.setLongitude(lastPointMarked.longitude());
                LatLng l = d.calculateLatLangFromDistance(bearing, distance, latLng);
                drawMeasuredPolygon(l);

                //save to file
                if (isDrawingMode && isBuildingSketchMode) {
                    saveToFile();
                }
            }

        } else if (requestCode == ACTIVITY_MIDDLE_POINT_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (point1 != null && point2 != null && point3 == null && point4 == null) {
                    double distance = data.getDoubleExtra(Constant.DISTANCE, -1);
                    ArrayList<Point> pointList = new ArrayList<>();
                    pointList.add(point1);
                    pointList.add(point2);
                    double bearing = TurfMeasurement.bearing(point1, point2);

                    DistanceToLocationCalculator d = new DistanceToLocationCalculator();
                    LatLng latLng = new LatLng();
                    latLng.setLatitude(point1.latitude());
                    latLng.setLongitude(point1.longitude());
                    LatLng l = d.calculateLatLangFromDistance(bearing, distance, latLng);
                    lastPointMarked = Point.fromLngLat(l.getLongitude(), l.getLatitude());
                    //LineString lineString = LineString.fromLngLats(pointList);
                    //lastPointMarked = TurfMeasurement.along(lineString, distance*meters2inches, TurfConstants.UNIT_METERS);


                    drawAQLine(lastPointMarked);
                    binding.layoutBuildingDrawingTool.pointMarker.setVisibility(View.GONE);

                } else if (point1 != null && point2 != null && point3 != null && point4 != null) {
                    double distance = data.getDoubleExtra(Constant.DISTANCE, -1);
                    ArrayList<Point> pointList = new ArrayList<>();
                    pointList.add(point3);
                    pointList.add(point4);
                    double bearing = TurfMeasurement.bearing(point3, point4);
                    DistanceToLocationCalculator d = new DistanceToLocationCalculator();
                    LatLng latLng = new LatLng();
                    latLng.setLatitude(point3.latitude());
                    latLng.setLongitude(point3.longitude());
                    LatLng l = d.calculateLatLangFromDistance(bearing, distance, latLng);
                    lastPointMarked = Point.fromLngLat(l.getLongitude(), l.getLatitude());
                    // LineString lineString = LineString.fromLngLats(pointList);
                    //lastPointMarked = TurfMeasurement.along(lineString, distance*meters2inches, TurfConstants.UNIT_METERS);

                    drawAQLine(lastPointMarked);
                    binding.layoutBuildingDrawingTool.pointMarker.setVisibility(View.GONE);
                }
            }

        } else if (requestCode == ACTIVITY_MEASURE_Building_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                DistanceToLocationCalculator d = new DistanceToLocationCalculator();
                double distance = data.getDoubleExtra(Constant.DISTANCE, -1);
                double bearing = data.getDoubleExtra(Constant.BEARING, -1);
                LatLng latLng = new LatLng();
                latLng.setLatitude(lastPointMarked.latitude());
                latLng.setLongitude(lastPointMarked.longitude());
                LatLng l = d.calculateLatLangFromDistance(bearing, distance, latLng);
                drawMeasuredPointForBuilding(l);

                //save to file
                if (isDrawingMode && isBuildingSketchMode) {
                    saveToFile();
                }
            }
        } else if (requestCode == ACTIVITY_LOT_DATA_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                LotMixinContent ltm = data.getParcelableExtra(Constant.DETAILS);
                finish();
                overridePendingTransition(0, 0);
                Intent i = getIntent();
                i.putExtra(Constant.MASTER_FILE, masterFile);
                i.putExtra(Constant.DETAILS, ltm);
                i.putExtra(Constant.MODE, 2);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            }
        } else if (requestCode == ACTIVITY_MEASURE_Building_CORNER_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                CornerCalculator cc = new CornerCalculator();
                int line1 = data.getIntExtra(Constant.LINE1, -1);
                int line2 = data.getIntExtra(Constant.LINE2, -1);
                int side = data.getIntExtra(Constant.TYPE, -1);


                DistanceToLocationCalculator d = new DistanceToLocationCalculator();
                double distance = cc.lineLength(line1, line2);
                double bearing = cc.getBearing(line1, line2, side);
                LatLng latLng = new LatLng();
                latLng.setLatitude(lastPointMarked.latitude());
                latLng.setLongitude(lastPointMarked.longitude());
                LatLng l = d.calculateLatLangFromDistance(bearing, distance, latLng);
                drawMeasuredPointForBuilding(l);
                //save to file
                if (isDrawingMode && isBuildingSketchMode) {
                    saveToFile();
                }
            }
        }
    }



 /*   private void markPoint(RectF rectF,String layerID){
        isAqLineDraw = true;
        this.rectF = rectF;
        List<Feature> features = mapboxMap.queryRenderedFeatures(rectF,layerID);
        for (Feature feature : features){

        }
    }*/

    private void enableSketchMode(RectF rectF) {
        isDrawingMode = true;

        mapboxMap.getStyle().getLayer(ID_IMAGE_LAYER).setProperties(visibility(VISIBLE));


        //startRepeatingTask();
        this.rectF = rectF;
        List<Feature> featureList = mapboxMap.queryRenderedFeatures(rectF, FILL_LAYER_ID);
        for (Feature feature : featureList) {
            try {

                hideShowLandComponents(true);
                hideShowBuildingComponents(true);
                hideShowLotComponents(false);
                hideShowLayers(true);


                double bbox[] = TurfMeasurement.bbox((Polygon) feature.geometry());
                LatLng locationOne = new LatLng(bbox[1], bbox[0]);
                LatLng locationTwo = new LatLng(bbox[3], bbox[2]);

                double distance = TurfMeasurement.distance(Point.fromLngLat(locationOne.getLongitude(), locationOne.getLatitude()), Point.fromLngLat(locationTwo.getLongitude(), locationTwo.getLatitude()));
                Point point = TurfMeasurement.midpoint(Point.fromLngLat(locationOne.getLongitude(), locationOne.getLatitude()), Point.fromLngLat(locationTwo.getLongitude(), locationTwo.getLatitude()));
                List<List<Point>> arList;
                arList = TurfTransformation.circle(
                        point, (distance * 0.75), TurfConstants.UNIT_KILOMETERS).coordinates();
                double bounding[] = TurfMeasurement.bbox(Feature.fromGeometry(Polygon.fromLngLats(arList)));
                lOne = new LatLng(bounding[1], bounding[0]);
                lTwo = new LatLng(bounding[3], bounding[2]);

                lineManager.deleteAll();
                GridManager gridManager = new GridManager();
                List<List<LatLng>> list = gridManager.drawGrid(bounding);
                for (List<LatLng> lats : list) {
                    lineManager.create(new LineOptions()
                            .withDraggable(false)
                            .withLatLngs(lats)
                            .withLineColor("Black")
                    );
                }


                LatLngBounds latLngBounds = new LatLngBounds.Builder()
                        .include(lOne) // Northeast
                        .include(lTwo) // Southwest
                        .build();

                int duration = 5000;

                List<com.google.android.gms.maps.model.LatLng> latLngs = new ArrayList<>();
                for (Point f : this.lot.getLineLayerPointList()) {
                    com.google.android.gms.maps.model.LatLng lt = new com.google.android.gms.maps.model.LatLng(f.latitude(), f.longitude());
                    latLngs.add(lt);
                }

                Double sqMeters = SphericalUtil.computeArea(latLngs);

                double inchtot = (sqMeters * 1550.0031);
                int feet = (int) (inchtot / (144));
                int inches = (int) Math.round(inchtot % (144));
                if (inches == 144) {
                    inches = 0;
                    feet += 1;
                }
                binding.tvLandArea.setText(Html.fromHtml(feet + " ft<sup>2</sup> " + inches + " inch<sup>2</sup>"));

                mapboxMap.easeCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100), duration);
                binding.multipleActionsParentFab.setVisibility(View.GONE);
                //   binding.imgBtnRedo.setVisibility(View.INVISIBLE);
                binding.imgBtnUndo.setVisibility(View.INVISIBLE);

                new Handler().postDelayed(() -> {
                    mapboxMap.setLatLngBoundsForCameraTarget(latLngBounds);
                    binding.layoutDrawingTool.getRoot().setVisibility(View.VISIBLE);
                    setIsEnabledBuildingDraw(true);

                    setIsEnabledBuildingDraw(true);
              /*  if (mapboxMap.getStyle().getLayer(GRID_LAYER) != null) {
                    mapboxMap.getStyle().getLayer(GRID_LAYER).setProperties(visibility(VISIBLE));
                }*/
                    zoomLevel = mapboxMap.getCameraPosition().zoom;
                }, duration);
            } catch (ClassCastException e) {
                undo();
                Intent i = new Intent(MapViewActivity.this, ErrorBoxActivity.class);
                i.putExtra(Constant.NAME, "Invalid Drawing");
                startActivity(i);
            }


        }
    }


    @Override
    public void lineDraw() {
        binding.layoutDrawingTool.btnDraw.setEnabled(true);
        isLineDraw = true;
        isCircleDraw = false;
        isPolygonDraw = false;
        binding.layoutDrawingTool.imgBtnDrawLine.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorSoftYellow), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutDrawingTool.imgBtnDrawPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutDrawingTool.imgBtnDrawCircle.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        Intent i = new Intent(MapViewActivity.this, ConstructionDetailActivity.class);
        startActivityForResult(i, ACTIVITY_LAND_RESULT_CODE);
    }

    @Override
    public void polygonDraw() {
        binding.layoutDrawingTool.btnDraw.setEnabled(true);
        isPolygonDraw = true;
        isCircleDraw = false;
        isLineDraw = false;

        binding.layoutDrawingTool.imgBtnDrawLine.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutDrawingTool.imgBtnDrawPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorSoftYellow), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutDrawingTool.imgBtnDrawCircle.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);


        Intent i = new Intent(MapViewActivity.this, ConstructionDetailActivity.class);
        startActivityForResult(i, ACTIVITY_LAND_RESULT_CODE);
    }

    @Override
    public void circleDraw() {
        binding.layoutDrawingTool.btnDraw.setEnabled(true);
        isCircleDraw = true;
        isLineDraw = false;
        isPolygonDraw = false;

        binding.layoutDrawingTool.imgBtnDrawLine.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutDrawingTool.imgBtnDrawPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutDrawingTool.imgBtnDrawCircle.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorSoftYellow), android.graphics.PorterDuff.Mode.SRC_IN);
        Intent i = new Intent(MapViewActivity.this, ConstructionDetailActivity.class);
        startActivityForResult(i, ACTIVITY_LAND_RESULT_CODE);
    }

    @Override
    public void aqnDraw() {

        if (isLineDraw || isPolygonDraw || isCircleDraw || isRoomPolygonDraw || isFloorPolygonDraw) {
            Toast.makeText(this, "Save previous construct before draw new one", Toast.LENGTH_LONG).show();
        } else if (isAqLineDraw) {
            buildingSaveDrawing();
            isAqLineDraw = false;
        } else {
            floorDrawingList = buildingFloors.get(currentFloor).getFloorDrawingList();
            binding.layoutBuildingDrawingTool.btnDrawBuilding.setEnabled(true);
            isAqLineDraw = true;

            aqCircleSource = initAQBuildingCircleSource(mapboxMap.getStyle(), "CircleAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
            aqLineSource = initAQBuildingLineSource(mapboxMap.getStyle(), "LineAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
            aqFillSource = initAQBuildingFillSource(mapboxMap.getStyle(), "FillAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
            initAQBuildingFillLayer(mapboxMap.getStyle(), "LayerFillAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size(), "FillAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
            initAQBuildingLineLayer(mapboxMap.getStyle(), "LayerLineAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size(), "LineAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size(), "LayerFillAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
            initAQBuildingCircleLayer(mapboxMap.getStyle(), "LayerCircleAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size(), "CircleAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size(), "LayerLineAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());

            point1 = null;
            point2 = null;
            point3 = null;
            point4 = null;
            tmpPoint1 = null;
            tmpPoint2 = null;
            tmpPoint3 = null;
            tmpPoint4 = null;

            aqCircleLayerFeatureList.clear();
            aqFillLayerPointList.clear();
            aqLineLayerPointList.clear();
            binding.layoutBuildingDrawingTool.imgAqDrawing.setEnabled(false);

        }
    }


    private GeoJsonSource initDrawingCircleSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection circleFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource circleGeoJsonSource = new GeoJsonSource(id, circleFeatureCollection);
        loadedMapStyle.addSource(circleGeoJsonSource);
        return circleGeoJsonSource;
    }

    private GeoJsonSource initDrawingLineSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection lineFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource lineGeoJsonSource = new GeoJsonSource(id, lineFeatureCollection);
        loadedMapStyle.addSource(lineGeoJsonSource);
        return lineGeoJsonSource;
    }

    private void initDrawingCircleLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String bottomLayer) {
        CircleLayer circleLayer = new CircleLayer(id,
                sourceId);
        circleLayer.setProperties(
                circleRadius(5f),
                circleColor(Color.parseColor("#00b09b"))
        );
        if (bottomLayer.equals("")) {
            loadedMapStyle.addLayerAbove(circleLayer, FILL_LAYER_ID);
        } else {
            loadedMapStyle.addLayerAbove(circleLayer, bottomLayer);
        }
    }

    private void initDrawingLineLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String bottomLayer) {
        LineLayer lineLayer = new LineLayer(id,
                sourceId);
        lineLayer.setProperties(
                lineColor(Color.parseColor("#96c93d")),
                lineWidth(5f)
        );
        if (bottomLayer.equals("")) {
            loadedMapStyle.addLayerAbove(lineLayer, FILL_LAYER_ID);
        } else {
            loadedMapStyle.addLayerAbove(lineLayer, bottomLayer);
        }
    }


    private GeoJsonSource initDrawingFillSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection fillFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource fillGeoJsonSource = new GeoJsonSource(id, fillFeatureCollection);
        loadedMapStyle.addSource(fillGeoJsonSource);
        return fillGeoJsonSource;
    }


    private void initDrawingFillLayer(@NonNull Style loadedMapStyle, String id, String sourceId) {
        FillLayer fillLayer = new FillLayer(id,
                sourceId);
        fillLayer.setProperties(
                fillOpacity(.6f),
                fillColor(Color.parseColor("#f7eb9e"))
        );
        loadedMapStyle.addLayerAbove(fillLayer, FILL_LAYER_ID);
    }

    @Override
    public void markPoint() {
        binding.layoutDrawingTool.btnSaveDraw.setEnabled(true);
        binding.imgBtnLandUndo.setEnabled(true);

        // Use the map click location to create a Point object
        Point mapTargetPoint = Point.fromLngLat(mapboxMap.getCameraPosition().target.getLongitude(),
                mapboxMap.getCameraPosition().target.getLatitude());

        if (isLineDraw) {

// Make note of the first map click location so that it can be used to create a closed polygon later on
            if (circleLayerDrawingFeatureList.size() == 0) {
                firstPointOfPolygon = mapTargetPoint;
            }

            lastPointMarked = mapTargetPoint;

// Add the click point to the circle layer and update the display of the circle layer data
            circleLayerDrawingFeatureList.add(Feature.fromGeometry(mapTargetPoint));

            if (circleLineSource != null) {
                circleLineSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerDrawingFeatureList));
            }


// Add the click point to the line layer and update the display of the line layer data
            if (circleLayerDrawingFeatureList.size() < 3) {
                lineLayerDrawingPointList.add(mapTargetPoint);
                if (circleLayerDrawingFeatureList.size() > 1) {
                    showBuildingDistance(lineLayerDrawingPointList);
                }
            } else if (circleLayerDrawingFeatureList.size() == 3) {
                lineLayerDrawingPointList.add(mapTargetPoint);
                showBuildingDistance(lineLayerDrawingPointList);
            } else {
                lineLayerDrawingPointList.add(mapTargetPoint);
                showBuildingDistance(lineLayerDrawingPointList);
            }
            if (lineLineSource != null) {
                lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                        {Feature.fromGeometry(LineString.fromLngLats(lineLayerDrawingPointList))}));
            }

        } else if (isPolygonDraw || isRoomPolygonDraw || isFloorPolygonDraw) {
            // Make note of the first map click location so that it can be used to create a closed polygon later on
            if (circleLayerDrawingFeatureList.size() == 0) {
                firstPointOfPolygon = mapTargetPoint;
            }

            lastPointMarked = mapTargetPoint;


// Add the click point to the circle layer and update the display of the circle layer data
            circleLayerDrawingFeatureList.add(Feature.fromGeometry(mapTargetPoint));
            if (circleLineSource != null) {
                circleLineSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerDrawingFeatureList));
            }


// Add the click point to the line layer and update the display of the line layer data
            if (circleLayerDrawingFeatureList.size() < 3) {
                lineLayerDrawingPointList.add(mapTargetPoint);
                if (circleLayerDrawingFeatureList.size() > 1) {
                    showBuildingDistance(lineLayerDrawingPointList);
                }
            } else if (circleLayerDrawingFeatureList.size() == 3) {
                lineLayerDrawingPointList.add(mapTargetPoint);
                showBuildingDistance(lineLayerDrawingPointList);
                lineLayerDrawingPointList.add(firstPointOfPolygon);
                showBuildingDistance(lineLayerDrawingPointList);

            } else {
                lineLayerDrawingPointList.remove(lineLayerDrawingPointList.size() - 1);
                if (isDrawingMode) {
                    buildingDetailManager.delete(labelSymbolBuildingList.get(labelSymbolBuildingList.size() - 1));
                    labelSymbolBuildingList.remove(labelSymbolBuildingList.size() - 1);
                } else if (isBuildingSketchMode) {
                    floorDetailManager.delete(floorSymbols.get(floorSymbols.size() - 1));
                    floorSymbols.remove(floorSymbols.size() - 1);
                }
                lineLayerDrawingPointList.add(mapTargetPoint);
                showBuildingDistance(lineLayerDrawingPointList);
                lineLayerDrawingPointList.add(firstPointOfPolygon);
                showBuildingDistance(lineLayerDrawingPointList);

            }
            if (lineLineSource != null) {
                lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                        {Feature.fromGeometry(LineString.fromLngLats(lineLayerDrawingPointList))}));
            }

// Add the click point to the fill layer and update the display of the fill layer data
            if (circleLayerDrawingFeatureList.size() < 3) {
                fillLayerDrawingPointList.add(mapTargetPoint);
            } else if (circleLayerDrawingFeatureList.size() == 3) {
                fillLayerDrawingPointList.add(mapTargetPoint);
                fillLayerDrawingPointList.add(firstPointOfPolygon);
            } else {
                fillLayerDrawingPointList.remove(fillLayerDrawingPointList.size() - 1);
                fillLayerDrawingPointList.add(mapTargetPoint);
                fillLayerDrawingPointList.add(firstPointOfPolygon);
            }
            listOfList = new ArrayList<>();
            listOfList.add(fillLayerDrawingPointList);
            List<Feature> finalFeatureList = new ArrayList<>();
            finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
            FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
            if (fillLineSource != null) {
                fillLineSource.setGeoJson(newFeatureCollection);
            }


        } else if (isCircleDraw) {
            lastPointMarked = mapTargetPoint;
            if (circleLayerDrawingFeatureList.size() == 0) {
                firstPointOfPolygon = mapTargetPoint;
                circleLayerDrawingFeatureList.add(Feature.fromGeometry(mapTargetPoint));
                circleLineSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerDrawingFeatureList));
            } else {
                radius = (float) MapCalculations.calculateDistance(firstPointOfPolygon, mapTargetPoint);
                listOfList = new ArrayList<>();


                listOfList = TurfTransformation.circle(
                        Point.fromLngLat(firstPointOfPolygon.longitude(), firstPointOfPolygon.latitude()), radius, 64, "meters").coordinates();
                List<Feature> features = new ArrayList<>();
                features.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                circlePointList = features;
                fillLineSource.setGeoJson(TurfTransformation.circle(
                        Point.fromLngLat(firstPointOfPolygon.longitude(), firstPointOfPolygon.latitude()), radius, 64, "meters"));
            }
        } else if (isAqLineDraw) {
            // drawAQLineBuilding(mapTargetPoint);
        }
        //save to File
        saveToFile();
    }

    private PolygonOptions generatePerimeter(LatLng centerCoordinates, double radiusInKilometers, int numberOfSides) {
        List<LatLng> positions = new ArrayList<>();
        double distanceX = radiusInKilometers / (111.319 * Math.cos(centerCoordinates.getLatitude() * Math.PI / 180));
        double distanceY = radiusInKilometers / 110.574;

        double slice = (2 * Math.PI) / numberOfSides;

        double theta;
        double x;
        double y;
        LatLng position;
        for (int i = 0; i < numberOfSides; ++i) {
            theta = i * slice;
            x = distanceX * Math.cos(theta);
            y = distanceY * Math.sin(theta);

            position = new LatLng(centerCoordinates.getLatitude() + y,
                    centerCoordinates.getLongitude() + x);
            positions.add(position);
        }
        return new PolygonOptions()
                .addAll(positions)
                .fillColor(Color.BLUE)
                .alpha(0.4f);
    }

    @Override
    public void markMeasuredPoint() {
        if (isCircleDraw | isLineDraw | isPolygonDraw || isRoomPolygonDraw || isFloorPolygonDraw) {
            binding.layoutDrawingTool.btnSaveDraw.setEnabled(true);
            binding.imgBtnLandUndo.setEnabled(true);

            Point mapTargetPoint = Point.fromLngLat(mapboxMap.getCameraPosition().target.getLongitude(),
                    mapboxMap.getCameraPosition().target.getLatitude());

// Make note of the first map click location so that it can be used to create a closed polygon later on
            if (circleLayerDrawingFeatureList.size() == 0) {
                firstPointOfPolygon = mapTargetPoint;
                lineLayerDrawingPointList.add(mapTargetPoint);
                lotLastPointMarked = mapTargetPoint;
                fillLayerDrawingPointList.add(mapTargetPoint);
                lastPointMarked = mapTargetPoint;
            }


// Add the click point to the circle layer and update the display of the circle layer data
            if (circleLayerDrawingFeatureList.size() == 0) {
                circleLayerDrawingFeatureList.add(Feature.fromGeometry(mapTargetPoint));
                if (circleLineSource != null) {
                    circleLineSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerDrawingFeatureList));

                    saveToFile();
                }
            } else {
                Intent i = new Intent(MapViewActivity.this, MeasuredLineDrawingActivity.class);
                startActivityForResult(i, ACTIVITY_MEASURE_Building_RESULT_CODE);
            }
        } else if (isAqLineDraw) {
        /*    binding.layoutBuildingDrawingTool.btnSaveBuildingDraw.setEnabled(true);
            binding.imgBtnLandUndoBuilding.setEnabled(true);

               *//* Point mapTargetPoint = Point.fromLngLat(mapboxMap.getCameraPosition().target.getLongitude(),
                        mapboxMap.getCameraPosition().target.getLatitude());*//*

// Make note of the first map click location so that it can be used to create a closed polygon later on7
               *//* if (aqCircleLayerFeatureList.size() == 0) {
                    firstPointOfPolygon = mapTargetPoint;
                    aqLineLayerPointList.add(mapTargetPoint);
                    lotLastPointMarked = mapTargetPoint;
                    aqFillLayerPointList.add(mapTargetPoint);
                    lastPointMarked = mapTargetPoint;
                }*//*


// Add the click point to the circle layer and update the display of the circle layer data
            if (aqCircleLayerFeatureList.size() <= 1) {
                Intent i = new Intent(MapViewActivity.this, MeasuredLineDrawingActivity.class);
                startActivityForResult(i, ACTIVITY_MEASURE_Building_RESULT_CODE);
            }*/

        }
    }

    @Override
    public void markCornedPoint() {
        if (isPolygonDraw | isLineDraw | isCircleDraw || isRoomPolygonDraw || isFloorPolygonDraw) {
            binding.layoutDrawingTool.btnSaveDraw.setEnabled(true);
            binding.imgBtnLandUndo.setEnabled(true);


            Point mapTargetPoint = Point.fromLngLat(mapboxMap.getCameraPosition().target.getLongitude(),
                    mapboxMap.getCameraPosition().target.getLatitude());

// Make note of the first map click location so that it can be used to create a closed polygon later on
            if (circleLayerDrawingFeatureList.size() == 0) {
                firstPointOfPolygon = mapTargetPoint;
                lineLayerDrawingPointList.add(mapTargetPoint);
                lotLastPointMarked = mapTargetPoint;
                fillLayerDrawingPointList.add(mapTargetPoint);
                lastPointMarked = mapTargetPoint;

            }


// Add the click point to the circle layer and update the display of the circle layer data
            if (circleLayerDrawingFeatureList.size() == 0) {
                circleLayerDrawingFeatureList.add(Feature.fromGeometry(mapTargetPoint));
                if (circleLineSource != null) {
                    circleLineSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerDrawingFeatureList));

                    saveToFile();
                }
            } else {
                Intent i = new Intent(MapViewActivity.this, MeasuredCornerDrawingActivity.class);
                startActivityForResult(i, ACTIVITY_MEASURE_Building_CORNER_RESULT_CODE);
            }
        }
    }


    @Override
    public void saveDrawing() {

        binding.layoutDrawingTool.imgBtnDrawLine.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutDrawingTool.imgBtnDrawPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.layoutDrawingTool.imgBtnDrawCircle.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);


        saveToFile();
        Toast.makeText(this, "Drawing saved...", Toast.LENGTH_SHORT).show();
        // Intent i = new Intent(MapViewActivity.this, ConstructionDetailActivity.class);
        // startActivityForResult(i, ACTIVITY_LAND_RESULT_CODE);

    }

    @Override
    public void landDrawingModeBackPressed() {
        saveToFile();
        isDrawingMode = false;
        mapboxMap.getStyle().getLayer(ID_IMAGE_LAYER).setProperties(visibility(NONE));


        //   stopRepeatingTask();
        isBuildingSketchMode = false;
        mapboxMap.setLatLngBoundsForCameraTarget(RESTRICTED_BOUNDS_AREA);
        mapboxMap.animateCamera(
                CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                        .zoom(16)
                        .build()), 4000);

        /*if (mapboxMap.getStyle().getLayer(GRID_LAYER) != null) {
            mapboxMap.getStyle().getLayer(GRID_LAYER).setProperties(visibility(NONE));
        }*/
        isDrawingMode = false;
        isLineDraw = false;
        isPolygonDraw = false;
        isCircleDraw = false;
        zoomLevel = -1;
        binding.layoutDrawingTool.getRoot().setVisibility(View.GONE);
        setIsEnabledBuildingDraw(false);
        hideShowLayers(false);


        binding.imgBtnUndo.setVisibility(View.VISIBLE);
        binding.imgBtnRedo.setVisibility(View.VISIBLE);
        binding.multipleActionsParentFab.setVisibility(View.VISIBLE);
    }


    private void enableBuildingDrawingTool(RectF rectF, String layerId, Construction building) {
        saveToFile();
        binding.multipleActionsParentFab.setVisibility(View.GONE);
        binding.imgBtnUndo.setVisibility(View.INVISIBLE);
        //binding.imgBtnRedo.setVisibility(View.INVISIBLE);
        isBuildingSketchMode = true;

        isLineDraw = false;
        isPolygonDraw = false;
        isCircleDraw = false;
        isFloorPolygonDraw = false;
        isRoomPolygonDraw = false;
        isAQPolygonDraw = false;

        binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingAQPolygon.setVisibility(View.INVISIBLE);
        binding.layoutBuildingDrawingTool.imgbtnDrawBuildingFloorPolygon.setVisibility(View.INVISIBLE);
        binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingRoomPolygon.setVisibility(View.INVISIBLE);
        binding.layoutBuildingDrawingTool.imgAqDrawing.setVisibility(View.VISIBLE);
        binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingPolygon.setVisibility(View.VISIBLE);


        binding.imgBtnBuildingBack.setVisibility(View.VISIBLE);
        binding.imgBtnLandUndoBuilding.setVisibility(View.VISIBLE);

        hideShowLandComponents(true);
        hideShowLayers(true);
        hideShowBuildingComponents(false);
        hideShowLotComponents(true);

        isDrawingMode = false;
        this.building = building;
        floorDrawingList = new ArrayList<>();
        binding.layoutDrawingTool.getRoot().setVisibility(View.GONE);
        setIsEnabledBuildingDraw(false);

        binding.layoutBuildingDrawingTool.getRoot().setVisibility(View.VISIBLE);
        binding.cLBuildingFloor.setVisibility(View.VISIBLE);
        List<Feature> featureList = mapboxMap.queryRenderedFeatures(rectF, layerId);

        if (building.getDrawingType().equals(DrawingType.POLYGON.name())) {
            dimLayerStyle(building.getCircleLayerID());
            dimLayerStyle(building.getFillLayerID());
            dimLayerStyle(building.getLineLayerID());
        } else if (building.getDrawingType().equals(DrawingType.LINE.name())) {
            dimLayerStyle(building.getCircleLayerID());
            dimLayerStyle(building.getLineLayerID());
        } else if (building.getDrawingType().equals(DrawingType.CIRCLE.name())) {
            dimLayerStyle(building.getCircleLayerID());
            dimLayerStyle(building.getFillLayerID());
        }
        layerStyle = layerId;

        for (Feature feature : featureList) {
            double bbox[];
            String drawingType = building.getDrawingType();
            if (building.getBuildingFloorList().size() > 0) {
                buildingFloors = building.getBuildingFloorList();
                floorAdapter.setFloors(buildingFloors);
                currentFloor = 0;
                floorDrawingList = building.getBuildingFloorList().get(currentFloor).getFloorDrawingList();
                buildingLabelManagerMap.put(building.getId(), new ArrayList<>());

                double area = calculateArea(building.getLineLayerPointList());
                double inchtot = (area * 1550.0031);
                int feet = (int) (inchtot / (144));
                int inches = (int) Math.round(inchtot % (144));

                if (inches == 144) {
                    feet += 1;
                    inches = 0;
                }
                binding.layoutBuildingDrawingTool.tvTotalBuildingArea.setText(Html.fromHtml(feet + " ft<sup>2</sup> " + inches + " inch<sup>2</sup>"));


                List<Point> line = new ArrayList<>();
                List<List<Point>> points = new ArrayList<>();

                try {
                    for (FloorDrawing fd : building.getBuildingFloorList().get(0).getFloorDrawingList()) {
                        if ((fd.getDrawingType().equals(DrawingType.AQLINE.name())) || (fd.getDrawingType().equals(DrawingType.AQ_POLYGON.name()))) {
                            line = new ArrayList<>(fd.getLineLayerAQPointList());
                            points.add(fd.getLineLayerAQPointList());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                double aqArea = calculateTotalAquiredArea(points);

                if (aqArea != -1) {
                    double inchtotAQ = (aqArea * 1550.0031);
                    int feetAQ = (int) (inchtotAQ / (144));
                    int inchesAQ = (int) Math.round(inchtotAQ % (144));
                    if (inchesAQ == 144) {
                        feetAQ += 1;
                        inchesAQ = 0;
                    }
                    binding.layoutBuildingDrawingTool.tvAcquiredArea.setText(Html.fromHtml(feetAQ + " ft<sup>2</sup> " + inchesAQ + " inch<sup>2</sup>"));
                } else {
                    binding.layoutBuildingDrawingTool.tvAcquiredArea.setText(Html.fromHtml("0 ft<sup>2</sup> 0 inch<sup>2</sup>"));

                }


                floorDrawing.setFloors(new ArrayList<>(floorDrawingList));


                for (FloorDrawing floorDrawing : floorDrawingList) {

                    if (floorDrawing.getCircleLayerID() != null) {
                        if (mapboxMap.getStyle().getLayer(floorDrawing.getCircleLayerID()) != null) {
                            mapboxMap.getStyle().getLayer(floorDrawing.getCircleLayerID()).setProperties(visibility(VISIBLE));
                        }
                    }
                    if (floorDrawing.getFillLayerID() != null) {
                        if (mapboxMap.getStyle().getLayer(floorDrawing.getFillLayerID()) != null) {
                            mapboxMap.getStyle().getLayer(floorDrawing.getFillLayerID()).setProperties(visibility(VISIBLE));
                        }
                    }
                    if (floorDrawing.getLineLayerID() != null) {
                        if (mapboxMap.getStyle().getLayer(floorDrawing.getLineLayerID()) != null) {
                            mapboxMap.getStyle().getLayer(floorDrawing.getLineLayerID()).setProperties(visibility(VISIBLE));
                        }
                    }


                    if (floorDrawing.getAqCircleLayerID() != null) {
                        if (mapboxMap.getStyle().getLayer(floorDrawing.getAqCircleLayerID()) != null) {
                            mapboxMap.getStyle().getLayer(floorDrawing.getAqCircleLayerID()).setProperties(visibility(VISIBLE));
                        }
                    }
                    if (floorDrawing.getAqFillLayerID() != null) {
                        if (mapboxMap.getStyle().getLayer(floorDrawing.getAqFillLayerID()) != null) {
                            mapboxMap.getStyle().getLayer(floorDrawing.getAqFillLayerID()).setProperties(visibility(VISIBLE));
                        }
                    }
                    if (floorDrawing.getLineLayerID() != null) {
                        if (mapboxMap.getStyle().getLayer(floorDrawing.getLineLayerID()) != null) {
                            mapboxMap.getStyle().getLayer(floorDrawing.getLineLayerID()).setProperties(visibility(VISIBLE));
                        }
                    }
                }
            } else {
                BuildingFloor floor = new BuildingFloor();
                floor.setFloorNo("G");
                ArrayList<BuildingFloor> floors = new ArrayList<>();
                floors.add(floor);
                floor.setuFloors(1);
                floor.setgFloors(0);
                buildingFloors = floors;
                floorAdapter.setFloors(floors);
                floorDrawingList = new ArrayList<>();
                currentFloor = 0;
                List<BuildingFloor> bf = new ArrayList<>();
                bf.add(floor);
                building.setBuildingFloorList(bf);

                this.building = building;

                floorDrawing.setFloors(new ArrayList<>());

                double area = calculateArea(building.getLineLayerPointList());
                double inchtot = (area * 1550.0031);
                int feet = (int) (inchtot / (144));
                int inches = (int) Math.round(inchtot % (144));
                if (inches == 144) {
                    feet += 1;
                    inches = 0;
                }
                binding.layoutBuildingDrawingTool.tvTotalBuildingArea.setText(Html.fromHtml(feet + " ft<sup>2</sup> " + inches + " inch<sup>2</sup>"));



                List<List<Point>> points = new ArrayList<>();

                try {
                    for (FloorDrawing fd : building.getBuildingFloorList().get(0).getFloorDrawingList()) {
                        if (fd.getDrawingType().equals(DrawingType.AQ_POLYGON.name())) {
                          points.add(fd.getLineLayerAQPointList());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                double aqArea = calculateTotalAquiredArea(points);
                if (aqArea != -1) {
                    double inchtotAQ = (aqArea * 1550.0031);
                    int feetAQ = (int) (inchtotAQ / (144));
                    int inchesAQ = (int) Math.round(inchtotAQ % (144));
                    if (inchesAQ == 144) {
                        inchesAQ = 0;
                        feetAQ += 1;
                    }
                    binding.layoutBuildingDrawingTool.tvAcquiredArea.setText(Html.fromHtml(feetAQ + " ft<sup>2</sup> " + inchesAQ + " inch<sup>2</sup>"));
                } else {
                    binding.layoutBuildingDrawingTool.tvAcquiredArea.setText(Html.fromHtml("0 ft<sup>2</sup> 0 inch<sup>2</sup>"));

                }

                //    List<SymbolManager>  sm =buildingLabelManagerMap.get(building.getId());

            }

            if (drawingType.equals(DrawingType.POLYGON.name()) || drawingType.equals(DrawingType.CIRCLE.name())) {
                bbox = TurfMeasurement.bbox((Polygon) feature.geometry());
            } else {
                bbox = TurfMeasurement.bbox((LineString) feature.geometry());
            }

            LatLng locationOne = new LatLng(bbox[1], bbox[0]);
            LatLng locationTwo = new LatLng(bbox[3], bbox[2]);


            LatLngBounds latLngBounds = new LatLngBounds.Builder()
                    .include(locationOne) // Northeast
                    .include(locationTwo) // Southwest
                    .build();

            int duration = 3000;
            mapboxMap.easeCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100), duration);


            new Handler().postDelayed(() -> {
                mapboxMap.setLatLngBoundsForCameraTarget(latLngBounds);
                binding.layoutDrawingTool.getRoot().setVisibility(View.GONE);
            }, duration);

            binding.layoutDrawingTool.getRoot().setVisibility(View.GONE);


        }
    }

    @Override
    public void buildingLineDraw() {
        if (isCircleDraw || isPolygonDraw || isRoomPolygonDraw || isFloorPolygonDraw || isAQPolygonDraw) {
            Toast.makeText(this, "Save previous construct before draw new one", Toast.LENGTH_LONG).show();
        } else if (isLineDraw) {
            buildingSaveDrawing();
            isLineDraw = false;
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingLine.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        } else {
            floorDrawingList = buildingFloors.get(currentFloor).getFloorDrawingList();
            binding.layoutBuildingDrawingTool.btnDrawBuilding.setEnabled(true);
            isLineDraw = true;
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingLine.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorSoftYellow), android.graphics.PorterDuff.Mode.SRC_IN);
            circleLayerDrawingFeatureList.clear();
            lineLayerDrawingPointList.clear();
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingLine.setEnabled(false);
            circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), "CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            lineLineSource = initBuildingLineSource(mapboxMap.getStyle(), "LineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            initBuildingLineLayer(mapboxMap.getStyle(), "LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "LineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "");
            initBuildingCircleLayer(mapboxMap.getStyle(), "LayerCircleBuilding" + building.getId() + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
        }
    }

    @Override
    public void buildingPolygonDraw() {
        if (isCircleDraw || isLineDraw || isRoomPolygonDraw || isFloorPolygonDraw || isAQPolygonDraw) {
            Toast.makeText(this, "Save previous construct before draw new one", Toast.LENGTH_LONG).show();
        } else if (isPolygonDraw) {
            buildingSaveDrawing();
            isPolygonDraw = false;
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);

        } else {
            floorDrawingList = buildingFloors.get(currentFloor).getFloorDrawingList();
            binding.layoutBuildingDrawingTool.btnDrawBuilding.setEnabled(true);
            isPolygonDraw = true;
            circleLayerDrawingFeatureList.clear();
            lineLayerDrawingPointList.clear();
            fillLayerDrawingPointList.clear();
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorSoftYellow), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingPolygon.setEnabled(false);
            circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), "CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            lineLineSource = initBuildingLineSource(mapboxMap.getStyle(), "LineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            fillLineSource = initBuildingFillSource(mapboxMap.getStyle(), "FillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            initBuildingFillLayer(mapboxMap.getStyle(), "LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "FillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), FILL_LAYER_ID);
            initBuildingLineLayer(mapboxMap.getStyle(), "LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "LineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            initBuildingCircleLayer(mapboxMap.getStyle(), "LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
        }
    }

    @Override
    public void buildingCircleDraw() {
        if (isLineDraw || isPolygonDraw || isRoomPolygonDraw || isFloorPolygonDraw || isAQPolygonDraw) {
            Toast.makeText(this, "Save previous construct before draw new one", Toast.LENGTH_LONG).show();
        } else if (isCircleDraw) {
            buildingSaveDrawing();
            isCircleDraw = false;
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingCircle.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);

        } else {
            floorDrawingList = buildingFloors.get(currentFloor).getFloorDrawingList();
            binding.layoutBuildingDrawingTool.btnDrawBuilding.setEnabled(true);
            isCircleDraw = true;
            circleLayerDrawingFeatureList.clear();
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingCircle.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorSoftYellow), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingCircle.setEnabled(false);
            circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), "CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            initBuildingCircleLayer(mapboxMap.getStyle(), "LayerCircleBuilding" + building.getId() + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "");
            fillLineSource = initBuildingFillSource(mapboxMap.getStyle(), "FillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            initBuildingFillLayer(mapboxMap.getStyle(), "LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "FillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), FILL_LAYER_ID);


        }
    }


    @Override
    public void buildingSaveDrawing() {
        double totFloor = 0;
        double totAq = 0;
        int uFloors = 0;
        int gFloors = 0;


        if (isLineDraw || isCircleDraw || isPolygonDraw || isAqLineDraw || isRoomPolygonDraw || isFloorPolygonDraw || isAQPolygonDraw) {

            FloorDrawing drawing = new FloorDrawing();
            floorDrawingList = buildingFloors.get(currentFloor).getFloorDrawingList();
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingLine.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingCircle.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingRoomPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingFloorPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);

            if (isLineDraw) {
                drawing.setDrawingType(DrawingType.LINE.name());
                drawing.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                drawing.setLineLayerPointList(new ArrayList<>(lineLayerDrawingPointList));
                drawing.setLineLayerID("LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setCircleLayerID("LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setLineSourceID("LineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setCircleSourceID("CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                isLineDraw = false;
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingLine.setEnabled(true);

            } else if (isCircleDraw) {
                drawing.setDrawingType(DrawingType.CIRCLE.name());
                drawing.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                drawing.setFillLayerPointList(new ArrayList<>(fillLayerDrawingPointList));
                drawing.setFillLayerID("LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setCircleLayerID("LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setFillSourceID("FillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setCircleSourceID("CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                isCircleDraw = false;
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingCircle.setEnabled(true);

            } else if (isPolygonDraw) {
                drawing.setDrawingType(DrawingType.POLYGON.name());
                drawing.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                drawing.setLineLayerPointList(new ArrayList<>(lineLayerDrawingPointList));
                drawing.setFillLayerPointList(new ArrayList<>(fillLayerDrawingPointList));
                drawing.setLineLayerID("LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setCircleLayerID("LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setFillLayerID("LayerFillBuilding" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
                drawing.setFillSourceID("FillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setCircleSourceID("CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setLineSourceID("LineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                isPolygonDraw = false;
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingPolygon.setEnabled(true);
            } else if (isAqLineDraw) {
                isAqLineDraw = false;
                binding.layoutDrawingTool.imgAq.setEnabled(true);
                binding.layoutBuildingDrawingTool.imgAqDrawing.setEnabled(true);

                binding.layoutDrawingTool.btnSaveDraw.setEnabled(false);
                drawing.setAqCircleSourceID("AQCircle" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setAqLineSourceID("AQLine" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setAqFillSourceID("AQFill" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setAqCircleLayerID("LayerCircleAQLine" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setAqLineLayerID("LayerLineAQLine" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setAqFillLayerID("LayerFillAQLine" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setDrawingType(DrawingType.AQLINE.name());
                drawing.setCircleLayerAQFeatureList(new ArrayList<>(aqCircleLayerFeatureList));
                drawing.setLineLayerAQPointList(new ArrayList<>(aqLineLayerPointList));
                drawing.setFillLayerAQPointList(new ArrayList<>(aqFillLayerPointList));



             /*   double area = calculateArea(drawing.getLineLayerAQPointList());
                double inchtotAQ = (area * 1550.0031);
                int feetAQ = (int) (inchtotAQ / (144));
                int inchesAQ = (int) Math.round(inchtotAQ % (144));


                point1 = null;
                point2 = null;
                point3 = null;
                point4 = null;


                if (inchesAQ == 144) {
                    inchesAQ = 0;
                    feetAQ += 0;
                }

                binding.layoutBuildingDrawingTool.tvAcquiredArea.setText(Html.fromHtml(feetAQ + " ft<sup>2</sup> " + inchesAQ + " inch<sup>2</sup>"));*/
                aqCircleLayerFeatureList = new ArrayList<>();
                aqLineLayerPointList = new ArrayList<>();
                aqFillLayerPointList = new ArrayList<>();
            } else if (isRoomPolygonDraw) {
                drawing.setDrawingType(DrawingType.BROOM_POLYGON.name());
                drawing.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                drawing.setLineLayerPointList(new ArrayList<>(lineLayerDrawingPointList));
                drawing.setFillLayerPointList(new ArrayList<>(fillLayerDrawingPointList));
                drawing.setLineLayerID("LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setCircleLayerID("LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setFillLayerID("LayerFillBuilding" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
                drawing.setFillSourceID("FillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setCircleSourceID("CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setLineSourceID("LineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                isRoomPolygonDraw = false;
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingRoomPolygon.setEnabled(true);

            } else if (isFloorPolygonDraw) {
                drawing.setDrawingType(DrawingType.ZFLOOR_POLYGON.name());
                drawing.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                drawing.setLineLayerPointList(new ArrayList<>(lineLayerDrawingPointList));
                drawing.setFillLayerPointList(new ArrayList<>(fillLayerDrawingPointList));
                drawing.setLineLayerID("LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setCircleLayerID("LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setFillLayerID("LayerFillBuilding" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
                drawing.setFillSourceID("FillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setCircleSourceID("CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setLineSourceID("LineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                isFloorPolygonDraw = false;
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingFloorPolygon.setEnabled(true);

            } else if (isAQPolygonDraw) {
                drawing.setAqCircleSourceID("AQCircle" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setAqLineSourceID("AQLine" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setAqFillSourceID("AQFill" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setAqCircleLayerID("LayerCircleAQLine" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setAqLineLayerID("LayerLineAQLine" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setAqFillLayerID("LayerFillAQLine" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
                drawing.setDrawingType(DrawingType.AQLINE.name());
                drawing.setCircleLayerAQFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                drawing.setLineLayerAQPointList(new ArrayList<>(lineLayerDrawingPointList));
                drawing.setFillLayerAQPointList(new ArrayList<>(fillLayerDrawingPointList));
                isAQPolygonDraw = false;
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingAQPolygon.setEnabled(true);

/*
                double area = calculateArea(drawing.getLineLayerAQPointList());
                double inchtotAQ = (area * 1550.0031);
                int feetAQ = (int) (inchtotAQ / (144));
                int inchesAQ = (int) Math.round(inchtotAQ % (144));


                if (inchesAQ == 144) {
                    inchesAQ = 0;
                    feetAQ += 1;
                }

                binding.layoutBuildingDrawingTool.tvAcquiredArea.setText(Html.fromHtml(feetAQ + " ft<sup>2</sup> " + inchesAQ + " inch<sup>2</sup>"));*/
            }

            floorDrawingList.add(drawing);
            buildingFloors.get(currentFloor).setFloorDrawingList(floorDrawingList);
            List<io.xiges.ximapper.model.Symbol> symbols = new ArrayList<>();
            for (Symbol s : floorSymbols) {
                io.xiges.ximapper.model.Symbol symbol = new io.xiges.ximapper.model.Symbol();
                symbol.setLatLng(s.getLatLng());
                symbol.setName(s.getTextField());
                symbols.add(symbol);
            }
            double inchtot = 0;
            double inchtotAQ = 0;

            for (FloorDrawing fd : floorDrawingList) {
                if (fd.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name())) {
                    double area = calculateArea(fd.getLineLayerPointList());
                    inchtot += (area * 1550.0031);
                } else if (fd.getDrawingType().equals(DrawingType.AQLINE.name()) || fd.getDrawingType().equals(DrawingType.AQ_POLYGON.name())) {
                    double area = calculateArea(fd.getLineLayerAQPointList());
                    inchtotAQ += (area * 1550.0031);
                }
            }

            int feet = (int) (inchtot / (144));
            int inches = (int) Math.round(inchtot % (144));

            if (inches == 144) {
                inches = 0;
                feet += 1;
            }
            binding.layoutBuildingDrawingTool.tvTotalBuildingArea.setText(Html.fromHtml(feet + " ft<sup>2</sup> " + inches + " inch<sup>2</sup>"));


            int feetAQ = (int) (inchtotAQ / (144));
            int inchesAQ = (int) Math.round(inchtotAQ % (144));


            if (inchesAQ == 144) {
                inchesAQ = 0;
                feetAQ += 1;
            }
            binding.layoutBuildingDrawingTool.tvAcquiredArea.setText(Html.fromHtml(feetAQ + " ft<sup>2</sup> " + inchesAQ + " inch<sup>2</sup>"));


            List<io.xiges.ximapper.model.Symbol> floorSym = new ArrayList<>();
            for (Symbol s : floorLabelSymbols) {
                io.xiges.ximapper.model.Symbol ss = new io.xiges.ximapper.model.Symbol();
                ss.setName(s.getTextField());
                ss.setLatLng(s.getLatLng());
                floorSym.add(ss);
            }

            building.getBuildingFloorList().get(currentFloor).setFloorSymbol(floorSym);


            buildingFloors.get(currentFloor).setSymbols(symbols);
            floorAdapter.setFloors(buildingFloors);
            this.building.setBuildingFloorList(buildingFloors);
            constructions.set(constructions.indexOf(building), this.building);

            DataManager dataManager = new DataManager();
            binding.layoutDrawingTool.btnSaveDraw.setEnabled(false);
            binding.layoutDrawingTool.btnDraw.setEnabled(false);
            lot.setConstructions(constructions);
            //  this.lot.setSymbolList(symbols);


            List<io.xiges.ximapper.model.Symbol> landSymbols = new ArrayList<>();
            for (Symbol s : landLabelSymbols) {
                io.xiges.ximapper.model.Symbol ss = new io.xiges.ximapper.model.Symbol();
                ss.setName(s.getTextField());
                ss.setLatLng(s.getLatLng());
                landSymbols.add(ss);
            }
            this.lot.setFloorLabels(landSymbols);

            try {
                dataManager.writeToFile(lot, masterFile.getMasterFileName());
            } catch (IOException e) {
                e.printStackTrace();
            }


            floorDrawing.setFloors(new ArrayList<>(floorDrawingList));


            binding.layoutBuildingDrawingTool.tvTotalBuildingArea.setText(Html.fromHtml(feet + " ft<sup>2</sup> " + inches + " inch<sup>2</sup>"));
            Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();


        }

        double inchtot = 0;
        double inchtotAQ = 0;


        try {
            if (currentFloor != 0) {

                for (FloorDrawing fd : building.getBuildingFloorList().get(currentFloor).getFloorDrawingList()) {
                    if (fd.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name())) {
                        double area = calculateArea(fd.getLineLayerPointList());
                        inchtot += (area * 1550.0031);
                    } else if (fd.getDrawingType().equals(DrawingType.AQLINE.name()) || fd.getDrawingType().equals(DrawingType.AQ_POLYGON.name())) {
                        double area = calculateArea(fd.getLineLayerAQPointList());
                        inchtotAQ += (area * 1550.0031);
                    }
                }


            } else {

                double area = calculateArea(this.building.getLineLayerPointList());
                inchtot += (area * 1550.0031);

                for (FloorDrawing fd : building.getBuildingFloorList().get(currentFloor).getFloorDrawingList()) {
                    if (fd.getDrawingType().equals(DrawingType.AQLINE.name()) || fd.getDrawingType().equals(DrawingType.AQ_POLYGON.name())) {
                        double ar = calculateArea(fd.getLineLayerAQPointList());
                        inchtotAQ += (ar * 1550.0031);
                    }
                }

            }


            int feet = (int) (inchtot / (144));
            int inches = (int) Math.round(inchtot % (144));
            if (inches == 144) {
                inches = 0;
                feet += 1;
            }

            binding.layoutBuildingDrawingTool.tvTotalBuildingArea.setText(Html.fromHtml(feet + " ft<sup>2</sup> " + inches + " inch<sup>2</sup>"));


            int feetAQ = (int) (inchtotAQ / (144));
            int inchesAQ = (int) Math.round(inchtotAQ % (144));

            if (inchesAQ == 144) {
                inchesAQ = 0;
                feetAQ += 1;
            }

            binding.layoutBuildingDrawingTool.tvAcquiredArea.setText(Html.fromHtml(feetAQ + " ft<sup>2</sup> " + inchesAQ + " inch<sup>2</sup>"));

        } catch (Exception e) {
        }


        for (BuildingFloor bd : building.getBuildingFloorList()) {
            if (bd.getFloorNo().equals("G")) {
                double area = calculateArea(building.getLineLayerPointList());
                double inchTot = (area * 1550.0031);
                totFloor += inchTot;
            }
            uFloors += bd.getuFloors();
            gFloors += bd.getgFloors();

            for (FloorDrawing fd : bd.getFloorDrawingList()) {

                if (fd.getDrawingType().equals(DrawingType.AQ_POLYGON.name()) || fd.getDrawingType().equals(DrawingType.AQLINE.name())) {
                    double area = calculateArea(fd.getLineLayerAQPointList());
                    double inchTot = (area * 1550.0031);
                    totAq += inchTot;
                } else if (fd.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name())) {
                    double area = calculateArea(fd.getLineLayerPointList());
                    double inchTot = (area * 1550.0031);
                    totFloor += inchTot;
                }
            }
        }

        Building bd = new Building();
        bd.setNo(building.getId());
        bd.setName(building.getName());

        long ft = Math.round(totFloor / 144);
        long inch = Math.round(totFloor % 144);

        if (inch == 144) {
            inch = 0;
        }
        bd.setTotalFloorArea(totFloor + "");
        bd.setMinusFloors(gFloors);
        bd.setFloors(uFloors);


        saveBuildingDataToCR(bd);


    }


    @Override
    public void buildingDrawBackPressed() {
        isDrawingMode = true;
        this.rectF = rectF;
        isBuildingSketchMode = false;
        hideShowLayers(true);

        buildingSaveDrawing();

      /*  List<String> layers = new ArrayList<>();

        for (Layer s : mapboxMap.getStyle().getLayers()) {
            if (s.getId().contains("mapbox-android-line-layer")) {
                layers.add(s.getId());
            }
        }*/
        //   layers.add(FILL_LAYER_ID);


       /* List<Feature> featureList = mapboxMap.queryRenderedFeatures(rectF, layers.toArray(new String[0]));
        for (Feature feature : featureList) {
*/

           /* double bbox[] = TurfMeasurement.bbox((LineString) feature.geometry());
            LatLng locationOne = new LatLng(bbox[1], bbox[0]);
            LatLng locationTwo = new LatLng(bbox[3], bbox[2]);

            double distance = TurfMeasurement.distance(Point.fromLngLat(locationOne.getLongitude(), locationOne.getLatitude()), Point.fromLngLat(locationTwo.getLongitude(), locationTwo.getLatitude()));
            Point point = TurfMeasurement.midpoint(Point.fromLngLat(locationOne.getLongitude(), locationOne.getLatitude()), Point.fromLngLat(locationTwo.getLongitude(), locationTwo.getLatitude()));
            List<List<Point>> arList;
            arList = TurfTransformation.circle(
                    point, (distance*0.5), TurfConstants.UNIT_KILOMETERS).coordinates();
            double bounding[] = TurfMeasurement.bbox(Feature.fromGeometry(Polygon.fromLngLats(arList)));
            LatLng lOne = new LatLng(bounding[1], bounding[0]);
            LatLng lTwo = new LatLng(bounding[3], bounding[2]);
*/
        LatLngBounds latLngBounds = new LatLngBounds.Builder()
                .include(lOne) // Northeast
                .include(lTwo) // Southwest
                .build();

        int duration = 3000;

        if (building.getDrawingType().equals(DrawingType.POLYGON.name())) {
            brightLayerStyle(building.getCircleLayerID());
            brightLayerStyle(building.getFillLayerID());
            brightLayerStyle(building.getLineLayerID());
        } else if (building.getDrawingType().equals(DrawingType.LINE.name())) {
            brightLayerStyle(building.getCircleLayerID());
            brightLayerStyle(building.getLineLayerID());
        } else if (building.getDrawingType().equals(DrawingType.CIRCLE.name())) {
            brightLayerStyle(building.getCircleLayerID());
            brightLayerStyle(building.getFillLayerID());
        }

        mapboxMap.easeCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100), duration);
        binding.imgBtnBuildingBack.setVisibility(View.VISIBLE);

        binding.cLBuildingFloor.setVisibility(View.GONE);
        binding.imgBtnBuildingBack.setVisibility(View.GONE);
        binding.imgBtnLandUndoBuilding.setVisibility(View.GONE);
        binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingAQPolygon.setVisibility(View.INVISIBLE);
        binding.layoutBuildingDrawingTool.imgbtnDrawBuildingFloorPolygon.setVisibility(View.INVISIBLE);
        binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingRoomPolygon.setVisibility(View.INVISIBLE);
        binding.imgBtnUndo.setVisibility(View.VISIBLE);
        binding.imgBtnRedo.setVisibility(View.VISIBLE);
        binding.imgBtnBack.setVisibility(View.VISIBLE);
        binding.layoutBuildingDrawingTool.getRoot().setVisibility(View.GONE);

        floorDrawingList = building.getBuildingFloorList().get(currentFloor).getFloorDrawingList();

        for (int i = 0; i < floorDrawingList.size(); i++) {

            if (mapboxMap.getStyle().getLayer("LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                mapboxMap.getStyle().getLayer("LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(NONE));
            }
            if (mapboxMap.getStyle().getLayer("LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                mapboxMap.getStyle().getLayer("LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(NONE));
            }
            if (mapboxMap.getStyle().getLayer("LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                mapboxMap.getStyle().getLayer("LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(NONE));
            }


            if (mapboxMap.getStyle().getLayer("LayerCircleAQLine" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                mapboxMap.getStyle().getLayer("LayerCircleAQLine" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(NONE));
            }
            if (mapboxMap.getStyle().getLayer("LayerFillAQLine" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                mapboxMap.getStyle().getLayer("LayerFillAQLine" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(NONE));
            }
            if (mapboxMap.getStyle().getLayer("LayerLineAQLine" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                mapboxMap.getStyle().getLayer("LayerLineAQLine" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(NONE));
            }
        }

        new Handler().postDelayed(() -> {
            mapboxMap.setLatLngBoundsForCameraTarget(latLngBounds);
            binding.layoutDrawingTool.getRoot().setVisibility(View.VISIBLE);
            setIsEnabledBuildingDraw(true);

        }, duration);

    }

    @Override
    public void addFloor() {
        int size = buildingFloors.size();
        if (size == 0) {
            BuildingFloor floor = new BuildingFloor();
            floor.setFloorNo("G");
            floor.setuFloors(1);
            floor.setgFloors(0);
            floor.setFloorDrawingList(new ArrayList<>());
            buildingFloors.add(floor);
            floorAdapter.setFloors(buildingFloors);
            building.setBuildingFloorList(buildingFloors);
        } else {
            Intent i = new Intent(MapViewActivity.this, FloorAddActivity.class);
            startActivityForResult(i, ACTIVITY_FLOOR_ADD_RESULT_CODE);
        }

    }

    @Override
    public void undoLand() {
        if (isAqLineDraw) {
            binding.imgBtnRedo.setEnabled(true);
            if (aqCircleLayerFeatureList.size() == 0) {
                binding.imgBtnLandUndo.setEnabled(false);
                tmpPoint1 = point1;
                tmpPoint2 = point2;
                tmpPoint3 = point3;
                tmpPoint4 = point4;
                point1 = null;
                point2 = null;
                point3 = null;
                point4 = null;

                tmpAqCircleLayerFeatureList = new ArrayList<>();
                tmpAqFillLayerPointList = new ArrayList<>();
                tmpAqLineLayerPointList = new ArrayList<>();

            } else if (aqCircleLayerFeatureList.size() == 1) {
                //     binding.layoutDrawingTool.imgBtnLandRedo.setEnabled(true);

                //  circleTempLayerList = new ArrayList<>(circleLayerDrawingFeatureList);

                tmpAqCircleLayerFeatureList = new ArrayList<>(aqCircleLayerFeatureList);
                tmpAqFillLayerPointList = new ArrayList<>();
                tmpAqLineLayerPointList = new ArrayList<>();
                aqCircleLayerFeatureList.remove(aqCircleLayerFeatureList.size() - 1);

                //  lineLayerTempPointList = new ArrayList<>(lineLayerDrawingPointList);
                aqLineLayerPointList.clear();
                tmpPoint1 = point1;
                tmpPoint2 = point2;
                tmpPoint3 = point3;
                tmpPoint4 = point4;
                point1 = null;
                point2 = null;
                point3 = null;
                point4 = null;

                //fillLayerTempPointList = new ArrayList<>(fillLayerDrawingPointList);
                //  aqFillLayerPointList.clear();

            } else if (aqCircleLayerFeatureList.size() == 2) {
                //    binding.layoutDrawingTool.imgBtnLandRedo.setEnabled(true);


                tmpAqCircleLayerFeatureList = new ArrayList<>(aqCircleLayerFeatureList);
                tmpAqFillLayerPointList = new ArrayList<>();
                tmpAqLineLayerPointList = new ArrayList<>(aqLineLayerPointList);
                //  circleTempLayerList = new ArrayList<>(circleLayerFeatureList);
                aqCircleLayerFeatureList.remove(aqCircleLayerFeatureList.size() - 1);

                // lineLayerTempPointList = new ArrayList<>(lineLayerDrawingPointList);

                //aqLineLayerPointList.remove(aqLineLayerPointList.size() - 1);
                aqLineLayerPointList.remove(aqLineLayerPointList.size() - 1);

                lastPointMarked = aqLineLayerPointList.get(aqLineLayerPointList.size() - 1);
                aqFillLayerPointList.clear();
                tmpPoint1 = point1;
                tmpPoint2 = point2;
                tmpPoint3 = point3;
                tmpPoint4 = point4;
                point3 = null;
                point4 = null;
                //  fillLayerTempPointList = new ArrayList<>(fillLayerDrawingPointList);
                //aqFillLayerPointList.remove(aqFillLayerPointList.size() - 1);


            } else if (aqCircleLayerFeatureList.size() == 3) {
                tmpPoint1 = point1;
                tmpPoint2 = point2;
                tmpPoint3 = point3;
                tmpPoint4 = point4;
                tmpAqCircleLayerFeatureList = new ArrayList<>(aqCircleLayerFeatureList);
                tmpAqFillLayerPointList = new ArrayList<>(aqFillLayerPointList);
                tmpAqLineLayerPointList = new ArrayList<>(aqLineLayerPointList);
                // binding.layoutDrawingTool.imgBtnLandRedo.setEnabled(true);

                //   circleTempLayerList = new ArrayList<>(circleLayerDrawingFeatureList);
                aqCircleLayerFeatureList.remove(aqCircleLayerFeatureList.size() - 1);

                //  lineLayerTempPointList = new ArrayList<>(lineLayerDrawingPointList);
                aqLineLayerPointList.remove(aqLineLayerPointList.size() - 1);
                aqLineLayerPointList.remove(aqLineLayerPointList.size() - 1);

                //      aqLineLayerPointList.remove(aqLineLayerPointList.size() - 1);
                lastPointMarked = aqLineLayerPointList.get(aqLineLayerPointList.size() - 1);


                //     fillLayerTempPointList = new ArrayList<>(fillLayerPointList);
                aqFillLayerPointList.remove(aqFillLayerPointList.size() - 1);
                aqFillLayerPointList.remove(aqFillLayerPointList.size() - 1);

            } else {
                tmpPoint1 = point1;
                tmpPoint2 = point2;
                tmpPoint3 = point3;
                tmpPoint4 = point4;
                //        binding.layoutDrawingTool.imgBtnLandRedo.setEnabled(true);
                tmpAqCircleLayerFeatureList = new ArrayList<>(aqCircleLayerFeatureList);
                tmpAqFillLayerPointList = new ArrayList<>(aqFillLayerPointList);
                tmpAqLineLayerPointList = new ArrayList<>(aqLineLayerPointList);
                //   circleTempLayerList = new ArrayList<>(circleLayerDrawingFeatureList);
                aqCircleLayerFeatureList.remove(aqCircleLayerFeatureList.size() - 1);

                //      lineLayerTempPointList = new ArrayList<>(lineLayerDrawingPointList);
                aqLineLayerPointList.remove(aqLineLayerPointList.size() - 1);
                aqLineLayerPointList.remove(aqLineLayerPointList.size() - 1);
                aqLineLayerPointList.add(firstPointOfPolygon);

                //  aqLineLayerPointList.remove(aqLineLayerPointList.size() - 1);
                lastPointMarked = aqLineLayerPointList.get(aqLineLayerPointList.size() - 1);
                //aqLineLayerPointList.add(firstPointOfPolygon);

                //       fillLayerTempPointList = new ArrayList<>(lineLayerDrawingPointList);
                aqFillLayerPointList.remove(aqFillLayerPointList.size() - 1);
                aqFillLayerPointList.remove(aqFillLayerPointList.size() - 1);
                aqFillLayerPointList.add(firstPointOfPolygon);

            }

            if (aqCircleSource != null) {
                aqCircleSource.setGeoJson(FeatureCollection.fromFeatures(aqCircleLayerFeatureList));
            }

            if (aqLineSource != null) {
                aqLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                        {Feature.fromGeometry(LineString.fromLngLats(aqLineLayerPointList))}));
            }

            listOfList = new ArrayList<>();
            listOfList.add(aqFillLayerPointList);
            List<Feature> finalFeatureList = new ArrayList<>();
            finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
            FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
            if (aqFillSource != null) {
                aqFillSource.setGeoJson(newFeatureCollection);
            }
        } else {
            circleTempLayerList = new ArrayList<>(circleLayerDrawingFeatureList);
            fillLayerTempPointList = new ArrayList<>(fillLayerDrawingPointList);
            lineLayerTempPointList = new ArrayList<>(lineLayerDrawingPointList);
            binding.imgBtnRedo.setEnabled(true);

            if (circleLayerDrawingFeatureList.size() == 0) {
                binding.imgBtnLandUndo.setEnabled(false);
            } else if (circleLayerDrawingFeatureList.size() == 1) {
                //     binding.layoutDrawingTool.imgBtnLandRedo.setEnabled(true);

                //  circleTempLayerList = new ArrayList<>(circleLayerDrawingFeatureList);

                circleLayerDrawingFeatureList.clear();
                // circleLayerDrawingFeatureList.remove(circleLayerDrawingFeatureList.size() - 1);

                if (isLineDraw || isPolygonDraw || isFloorPolygonDraw || isRoomPolygonDraw || isAQPolygonDraw) {
                    lastPointMarked = lineLayerDrawingPointList.get(lineLayerDrawingPointList.size() - 1);
                    binding.imgBtnRedo.setEnabled(true);

                }

                //  lineLayerTempPointList = new ArrayList<>(lineLayerDrawingPointList);
                lineLayerDrawingPointList.clear();

                //fillLayerTempPointList = new ArrayList<>(fillLayerDrawingPointList);
                fillLayerDrawingPointList.clear();

            } else if (circleLayerDrawingFeatureList.size() == 2) {
                //    binding.layoutDrawingTool.imgBtnLandRedo.setEnabled(true);

                //  circleTempLayerList = new ArrayList<>(circleLayerFeatureList);
                circleLayerDrawingFeatureList.remove(circleLayerDrawingFeatureList.size() - 1);

                // lineLayerTempPointListm = new ArrayList<>(lineLayerDrawingPointList);

                if (isLineDraw || isPolygonDraw || isFloorPolygonDraw || isRoomPolygonDraw || isAQPolygonDraw) {
                    lastPointMarked = lineLayerDrawingPointList.get(lineLayerDrawingPointList.size() - 1);
                    lineLayerDrawingPointList.remove(lineLayerDrawingPointList.size() - 1);
                    binding.imgBtnRedo.setEnabled(true);

                }
                //  fillLayerTempPointList = new ArrayList<>(fillLayerDrawingPointList);
                fillLayerDrawingPointList.clear();


                if (isDrawingMode) {

                    Symbol s = labelSymbolBuildingList.get(labelSymbolBuildingList.size() - 1);
                    buildingDetailManager.delete(s);
                    labelSymbolBuildingList.remove(labelSymbolBuildingList.size() - 1);

                    try {
                        Symbol s1 = labelSymbolBuildingList.get(labelSymbolBuildingList.size() - 1);
                        buildingDetailManager.delete(s1);
                        labelSymbolBuildingList.remove(labelSymbolBuildingList.size() - 1);
                    } catch (Exception e) {

                    }

                } else if (isBuildingSketchMode) {
                    Symbol s = floorSymbols.get(floorSymbols.size() - 1);
                    floorDetailManager.delete(s);
                    floorSymbols.remove(floorSymbols.size() - 1);
                }
                lastPointMarked = lineLayerDrawingPointList.get(lineLayerDrawingPointList.size() - 1);


            } else if (circleLayerDrawingFeatureList.size() == 3) {
                // binding.layoutDrawingTool.imgBtnLandRedo.setEnabled(true);

                //   circleTempLayerList = new ArrayList<>(circleLayerDrawingFeatureList);
                circleLayerDrawingFeatureList.remove(circleLayerDrawingFeatureList.size() - 1);


                if (isPolygonDraw || isFloorPolygonDraw || isRoomPolygonDraw || isAQPolygonDraw) {
                    //    lineLayerTempPointList = new ArrayList<>(lineLayerDrawingPointList);
                    lineLayerDrawingPointList.remove(lineLayerDrawingPointList.size() - 1);
                    lineLayerDrawingPointList.remove(lineLayerDrawingPointList.size() - 1);
                    lastPointMarked = lineLayerDrawingPointList.get(lineLayerDrawingPointList.size() - 1);
                } else if (isLineDraw) {
                    lineLayerDrawingPointList.remove(lineLayerDrawingPointList.size() - 1);
                    lastPointMarked = lineLayerDrawingPointList.get(lineLayerDrawingPointList.size() - 1);
                    binding.imgBtnRedo.setEnabled(true);

                }


                if (isPolygonDraw || isCircleDraw || isFloorPolygonDraw || isRoomPolygonDraw || isAQPolygonDraw) {
                    //     fillLayerTempPointList = new ArrayList<>(fillLayerPointList);
                    fillLayerDrawingPointList.remove(fillLayerDrawingPointList.size() - 1);
                    fillLayerDrawingPointList.remove(fillLayerDrawingPointList.size() - 1);
                }


                if (isDrawingMode) {
                    if (isPolygonDraw || isFloorPolygonDraw || isRoomPolygonDraw || isAQPolygonDraw) {

                        Symbol s = labelSymbolBuildingList.get(labelSymbolBuildingList.size() - 1);
                        buildingDetailManager.delete(s);
                        labelSymbolBuildingList.remove(labelSymbolBuildingList.size() - 1);
                        Symbol s1 = labelSymbolBuildingList.get(labelSymbolBuildingList.size() - 1);
                        buildingDetailManager.delete(s1);
                        labelSymbolBuildingList.remove(labelSymbolBuildingList.size() - 1);
                        showBuildingDistance(lineLayerDrawingPointList);

                    } else if (isLineDraw) {
                        Symbol s = labelSymbolBuildingList.get(labelSymbolBuildingList.size() - 1);
                        buildingDetailManager.delete(s);
                        labelSymbolBuildingList.remove(labelSymbolBuildingList.size() - 1);
                    }
                } else if (isBuildingSketchMode) {
                    if (isPolygonDraw || isFloorPolygonDraw || isRoomPolygonDraw || isAQPolygonDraw) {
                        Symbol s = floorSymbols.get(floorSymbols.size() - 1);
                        floorDetailManager.delete(s);
                        floorSymbols.remove(floorSymbols.size() - 1);
                        Symbol s1 = floorSymbols.get(floorSymbols.size() - 1);
                        floorDetailManager.delete(s1);
                        floorSymbols.remove(floorSymbols.size() - 1);
                        showBuildingDistance(lineLayerDrawingPointList);

                    } else if (isLineDraw) {
                        Symbol s = floorSymbols.get(floorSymbols.size() - 1);
                        floorDetailManager.delete(s);
                        floorSymbols.remove(floorSymbols.size() - 1);
                        binding.imgBtnRedo.setEnabled(true);

                    }
                }


                lastPointMarked = lineLayerDrawingPointList.get(lineLayerDrawingPointList.size() - 1);

            } else {
                //        binding.layoutDrawingTool.imgBtnLandRedo.setEnabled(true);

                //   circleTempLayerList = new ArrayList<>(circleLayerDrawingFeatureList);
                circleLayerDrawingFeatureList.remove(circleLayerDrawingFeatureList.size() - 1);

                if (isPolygonDraw || isFloorPolygonDraw || isRoomPolygonDraw || isAQPolygonDraw) {
                    //      lineLayerTempPointList = new ArrayList<>(lineLayerDrawingPointList);
                    lineLayerDrawingPointList.remove(lineLayerDrawingPointList.size() - 1);
                    lineLayerDrawingPointList.remove(lineLayerDrawingPointList.size() - 1);
                    lastPointMarked = lineLayerDrawingPointList.get(lineLayerDrawingPointList.size() - 1);
                    lineLayerDrawingPointList.add(firstPointOfPolygon);
                } else if (isLineDraw) {
                    lineLayerDrawingPointList.remove(lineLayerDrawingPointList.size() - 1);
                    lastPointMarked = lineLayerDrawingPointList.get(lineLayerDrawingPointList.size() - 1);
                }


                if (isCircleDraw || isPolygonDraw || isFloorPolygonDraw || isRoomPolygonDraw || isAQPolygonDraw) {

                    //       fillLayerTempPointList = new ArrayList<>(lineLayerDrawingPointList);
                    fillLayerDrawingPointList.remove(fillLayerDrawingPointList.size() - 1);
                    fillLayerDrawingPointList.remove(fillLayerDrawingPointList.size() - 1);
                    fillLayerDrawingPointList.add(firstPointOfPolygon);
                }

                if (isDrawingMode) {
                    if (isPolygonDraw || isFloorPolygonDraw || isRoomPolygonDraw || isAQPolygonDraw) {

                        Symbol s = labelSymbolBuildingList.get(labelSymbolBuildingList.size() - 1);
                        buildingDetailManager.delete(s);
                        labelSymbolBuildingList.remove(labelSymbolBuildingList.size() - 1);
                        Symbol s1 = labelSymbolBuildingList.get(labelSymbolBuildingList.size() - 1);
                        buildingDetailManager.delete(s1);
                        labelSymbolBuildingList.remove(labelSymbolBuildingList.size() - 1);
                        showBuildingDistance(lineLayerDrawingPointList);

                    } else if (isLineDraw) {
                        Symbol s = labelSymbolBuildingList.get(labelSymbolBuildingList.size() - 1);
                        buildingDetailManager.delete(s);
                        labelSymbolBuildingList.remove(labelSymbolBuildingList.size() - 1);

                    }
                } else if (isBuildingSketchMode) {
                    if (isPolygonDraw || isFloorPolygonDraw || isRoomPolygonDraw || isAQPolygonDraw) {
                        Symbol s = floorSymbols.get(floorSymbols.size() - 1);
                        floorDetailManager.delete(s);
                        floorSymbols.remove(floorSymbols.size() - 1);
                        Symbol s1 = floorSymbols.get(floorSymbols.size() - 1);
                        floorDetailManager.delete(s1);
                        floorSymbols.remove(floorSymbols.size() - 1);
                        showBuildingDistance(lineLayerDrawingPointList);

                    } else if (isLineDraw) {
                        Symbol s = floorSymbols.get(floorSymbols.size() - 1);
                        floorDetailManager.delete(s);
                        floorSymbols.remove(floorSymbols.size() - 1);

                    }
                }

            }

            if (circleLineSource != null) {
                circleLineSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerDrawingFeatureList));
            }

            if (lineLineSource != null) {
                lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                        {Feature.fromGeometry(LineString.fromLngLats(lineLayerDrawingPointList))}));
            }

            listOfList = new ArrayList<>();
            listOfList.add(fillLayerDrawingPointList);
            List<Feature> finalFeatureList = new ArrayList<>();
            finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
            FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
            if (fillLineSource != null) {
                fillLineSource.setGeoJson(newFeatureCollection);
            }
        }
    }

    @Override
    public void redoLand() {
        if (isAqLineDraw) {
            point1 = tmpPoint1;
            point2 = tmpPoint2;
            point3 = tmpPoint3;
            point4 = tmpPoint4;
            binding.imgBtnLandUndo.setEnabled(true);
            aqCircleLayerFeatureList = new ArrayList<>(tmpAqCircleLayerFeatureList);
            aqFillLayerPointList = new ArrayList<>(tmpAqFillLayerPointList);
            aqLineLayerPointList = new ArrayList<>(tmpAqLineLayerPointList);

            tmpPoint1 = null;
            tmpPoint2 = null;
            tmpPoint3 = null;
            tmpPoint4 = null;

            tmpAqCircleLayerFeatureList = new ArrayList<>();
            tmpAqFillLayerPointList = new ArrayList<>();
            tmpAqLineLayerPointList = new ArrayList<>();

            if (aqCircleSource != null) {
                aqCircleSource.setGeoJson(FeatureCollection.fromFeatures(aqCircleLayerFeatureList));
            }

            if (aqLineSource != null) {
                aqLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                        {Feature.fromGeometry(LineString.fromLngLats(aqLineLayerPointList))}));
            }

            listOfList = new ArrayList<>();
            listOfList.add(aqFillLayerPointList);
            List<Feature> finalFeatureList = new ArrayList<>();
            finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
            FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
            if (aqFillSource != null) {
                aqFillSource.setGeoJson(newFeatureCollection);
            }
            binding.imgBtnRedo.setEnabled(false);

        } else if (!isDrawingMode && !isBuildingSketchMode) {
            try {

                circleLayerFeatureList = new ArrayList<>(circleTempLayerList);
                fillLayerPointList = new ArrayList<>(fillLayerTempPointList);
                lineLayerPointList = new ArrayList<>(lineLayerTempPointList);


                if (circleSource != null) {
                    circleSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList));
                }

                if (lineSource != null) {
                    lineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                            {Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList))}));
                }

                listOfList = new ArrayList<>();
                listOfList.add(fillLayerPointList);
                List<Feature> finalFeatureList = new ArrayList<>();
                finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
                if (fillSource != null) {
                    fillSource.setGeoJson(newFeatureCollection);
                }

                if (lineLayerDrawingPointList.size() > 2) {
                    List<Point> last = new ArrayList<>(lineLayerDrawingPointList);
                    last.remove(last.size() - 1);
                    showBuildingDistance(last);
                }
                showBuildingDistance(lineLayerPointList);


                fillLayerTempPointList = null;
                circleTempLayerList = null;
                lineLayerTempPointList = null;
                binding.imgBtnRedo.setEnabled(false);
            } catch (NullPointerException e) {
            }
        } else if (isDrawingMode || isPolygonDraw || isFloorPolygonDraw || isRoomPolygonDraw || isAQPolygonDraw) {
            try {

                circleLayerDrawingFeatureList = new ArrayList<>(circleTempLayerList);
                fillLayerDrawingPointList = new ArrayList<>(fillLayerTempPointList);
                lineLayerDrawingPointList = new ArrayList<>(lineLayerTempPointList);


                if (circleLineSource != null) {
                    circleLineSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerDrawingFeatureList));
                }

                if (lineLineSource != null) {
                    lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                            {Feature.fromGeometry(LineString.fromLngLats(lineLayerDrawingPointList))}));
                }

                listOfList = new ArrayList<>();
                listOfList.add(fillLayerDrawingPointList);
                List<Feature> finalFeatureList = new ArrayList<>();
                finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
                if (fillLineSource != null) {
                    fillLineSource.setGeoJson(newFeatureCollection);
                }

                if (lineLayerDrawingPointList.size() > 2) {
                    List<Point> last = new ArrayList<>(lineLayerDrawingPointList);
                    last.remove(last.size() - 1);
                    showBuildingDistance(last);
                }
                showBuildingDistance(lineLayerDrawingPointList);


                fillLayerTempPointList = null;
                circleTempLayerList = null;
                lineLayerTempPointList = null;
                binding.imgBtnRedo.setEnabled(false);

                if (isBuildingSketchMode && isDrawingMode) {
                    saveToFile();
                }
            } catch (Exception e) {
            }
        } else if (isLineDraw) {
            try {

                circleLayerDrawingFeatureList = new ArrayList<>(circleTempLayerList);
                lineLayerDrawingPointList = new ArrayList<>(lineLayerTempPointList);


                if (circleLineSource != null) {
                    circleLineSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerDrawingFeatureList));
                }

                if (lineLineSource != null) {
                    lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                            {Feature.fromGeometry(LineString.fromLngLats(lineLayerDrawingPointList))}));
                }

                if (lineLayerDrawingPointList.size() > 2) {
                    List<Point> last = new ArrayList<>(lineLayerDrawingPointList);
                    last.remove(last.size() - 1);
                    showBuildingDistance(last);
                }
                showBuildingDistance(lineLayerDrawingPointList);


                fillLayerTempPointList = null;
                circleTempLayerList = null;
                lineLayerTempPointList = null;
                binding.imgBtnRedo.setEnabled(false);

                if (isBuildingSketchMode && isDrawingMode) {
                    saveToFile();
                }

            } catch (NullPointerException e) {
            }
        }
    }

    @Override
    public void openGallery() {
        Intent i = new Intent(MapViewActivity.this, ImageGalleryActivity.class);
        i.putExtra(Constant.LOT, lot.getLotId());
        i.putExtra(Constant.MASTER_FILE, masterFile.getMasterFileId());
        startActivity(i);
    }

    @Override
    public void openCamera() {
        cameraIntent();
    }

    @Override
    public void buildingRoomDrawing() {
        if (isCircleDraw || isLineDraw || isPolygonDraw || isFloorPolygonDraw || isAQPolygonDraw) {
            Toast.makeText(this, "Save previous construct before draw new one", Toast.LENGTH_LONG).show();

        } else if (isRoomPolygonDraw) {
            buildingSaveDrawing();
            isRoomPolygonDraw = false;
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingRoomPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);

        } else {
            floorDrawingList = buildingFloors.get(currentFloor).getFloorDrawingList();
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingRoomPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorSoftYellow), android.graphics.PorterDuff.Mode.SRC_IN);

            binding.layoutBuildingDrawingTool.btnDrawBuilding.setEnabled(true);
            isRoomPolygonDraw = true;
            circleLayerDrawingFeatureList.clear();
            lineLayerDrawingPointList.clear();
            fillLayerDrawingPointList.clear();
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingRoomPolygon.setEnabled(false);
            circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), "CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            lineLineSource = initBuildingLineSource(mapboxMap.getStyle(), "LineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            fillLineSource = initBuildingFillSource(mapboxMap.getStyle(), "FillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            initBuildingFillLayer(mapboxMap.getStyle(), "LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "FillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), FILL_LAYER_ID);
            initBuildingLineLayer(mapboxMap.getStyle(), "LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "LineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            initBuildingCircleLayer(mapboxMap.getStyle(), "LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
        }
    }

    @Override
    public void buildingFloorDrawing() {
        if (isCircleDraw || isLineDraw || isPolygonDraw || isRoomPolygonDraw || isAQPolygonDraw) {
            Toast.makeText(this, "Save previous construct before draw new one", Toast.LENGTH_LONG).show();
        } else if (isFloorPolygonDraw) {
            buildingSaveDrawing();
            isFloorPolygonDraw = false;
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingFloorPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);

        } else {
            floorDrawingList = buildingFloors.get(currentFloor).getFloorDrawingList();
            binding.layoutBuildingDrawingTool.btnDrawBuilding.setEnabled(true);
            isFloorPolygonDraw = true;
            circleLayerDrawingFeatureList.clear();
            lineLayerDrawingPointList.clear();
            fillLayerDrawingPointList.clear();
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingFloorPolygon.setColorFilter(ContextCompat.getColor(MapViewActivity.this, R.color.colorSoftYellow), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.layoutBuildingDrawingTool.imgbtnDrawBuildingFloorPolygon.setEnabled(false);
            circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), "CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            lineLineSource = initBuildingLineSource(mapboxMap.getStyle(), "LineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            fillLineSource = initBuildingFillSource(mapboxMap.getStyle(), "FillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            initBuildingFloorFillLayer(mapboxMap.getStyle(), "LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "FillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "");
            initBuildingFloorLineLayer(mapboxMap.getStyle(), "LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "LineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
            initBuildingFloorCircleLayer(mapboxMap.getStyle(), "LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "CircleBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size(), "LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + this.floorDrawingList.size());
        }
    }

    @Override
    public void buildingAQDrawing() {
        if (isCircleDraw || isLineDraw || isPolygonDraw || isRoomPolygonDraw || isFloorPolygonDraw) {
            Toast.makeText(this, "Save previous construct before draw new one", Toast.LENGTH_LONG).show();
        } else if (isAQPolygonDraw) {
            buildingSaveDrawing();
            isAQPolygonDraw = false;
        } else {
            floorDrawingList = buildingFloors.get(currentFloor).getFloorDrawingList();
            binding.layoutBuildingDrawingTool.btnDrawBuilding.setEnabled(true);
            //  isAQPolygonDraw = true;
            circleLayerDrawingFeatureList.clear();
            lineLayerDrawingPointList.clear();
            fillLayerDrawingPointList.clear();
            circleLineSource = initAQBuildingCircleSource(mapboxMap.getStyle(), "CircleAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
            lineLineSource = initAQBuildingLineSource(mapboxMap.getStyle(), "LineAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
            fillLineSource = initAQBuildingFillSource(mapboxMap.getStyle(), "FillAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
            initAQBuildingFillLayer(mapboxMap.getStyle(), "LayerFillAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size(), "FillAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
            initAQBuildingLineLayer(mapboxMap.getStyle(), "LayerLineAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size(), "LineAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size(), "LayerFillAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
            initAQBuildingCircleLayer(mapboxMap.getStyle(), "LayerCircleAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size(), "CircleAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size(), "LayerLineAQLine" + building.getId() + "" + currentFloor + "" + floorDrawingList.size());
        }
    }

    @Override
    public void middlePointMarker() {
        if (point1 != null && point2 != null) {
            Intent i = new Intent(MapViewActivity.this, PointMarkerActivity.class);
            startActivityForResult(i, ACTIVITY_MIDDLE_POINT_RESULT_CODE);
        }
    }

    @Override
    public void copyLayer() {
        buildingSaveDrawing();
        FloorCopyDetails current = new FloorCopyDetails();
        ArrayList<FloorCopyDetails> details = new ArrayList<>();


        for (int i = 0; i < buildingFloors.size(); i++) {
            if (i == currentFloor) {

                current.setIndex(i);
                if (currentFloor == 0) {
                    current.setFloor(true);
                }
                current.setFloorId(buildingFloors.get(i).getFloorNo());
                for (FloorDrawing floor : buildingFloors.get(i).getFloorDrawingList()) {
                    if (!current.isRoom()) {
                        if ((floor.getDrawingType().equals(DrawingType.BROOM_POLYGON.name())) || (floor.getDrawingType().equals(DrawingType.LINE.name())) || (floor.getDrawingType().equals(DrawingType.CIRCLE.name())) || (floor.getDrawingType().equals(DrawingType.POLYGON.name()))) {
                            current.setRoom(true);
                        }
                    }

                    if (!current.isFloor()) {
                        if (floor.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name())) {
                            current.setFloor(true);
                        }
                    }

                    if (!current.isAqLine()) {
                        if (floor.getDrawingType().equals(DrawingType.AQLINE.name())) {
                            current.setAqLine(true);
                        } else if (floor.getDrawingType().equals(DrawingType.AQ_POLYGON.name())) {
                            current.setAqLine(true);
                        }
                    }
                }
            } else {
                FloorCopyDetails other = new FloorCopyDetails();
                other.setIndex(i);
                other.setFloorId(buildingFloors.get(i).getFloorNo());
                if (other.getIndex() == 0) {
                    other.setFloor(true);
                }
                for (FloorDrawing floor : buildingFloors.get(i).getFloorDrawingList()) {

                    if (!other.isRoom()) {
                        if ((floor.getDrawingType().equals(DrawingType.BROOM_POLYGON.name())) || (floor.getDrawingType().equals(DrawingType.LINE.name())) || (floor.getDrawingType().equals(DrawingType.CIRCLE.name())) || (floor.getDrawingType().equals(DrawingType.POLYGON.name()))) {
                            other.setRoom(true);
                        }
                    }

                    if (!other.isFloor()) {
                        if (floor.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name())) {
                            other.setFloor(true);
                        }
                    }

                    if (!other.isAqLine()) {
                        if (floor.getDrawingType().equals(DrawingType.AQLINE.name())) {
                            other.setAqLine(true);
                        } else if (floor.getDrawingType().equals(DrawingType.AQ_POLYGON.name())) {
                            other.setAqLine(true);
                        }
                    }
                }
                details.add(other);
            }
        }

        Intent i = new Intent(MapViewActivity.this, CopyLayerActivity.class);
        i.putExtra(Constant.FLOOR_Label, current);
        i.putParcelableArrayListExtra(Constant.DETAILS, details);
        startActivityForResult(i, ACTIVITY_COPY_LAYER_RESULT_CODE);
    }


    private GeoJsonSource initBuildingCircleSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection circleFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource circleGeoJsonSource = new GeoJsonSource(id, circleFeatureCollection);
        loadedMapStyle.addSource(circleGeoJsonSource);
        return circleGeoJsonSource;
    }


    private GeoJsonSource initBuildingLineSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection lineFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource lineGeoJsonSource = new GeoJsonSource(id, lineFeatureCollection);
        loadedMapStyle.addSource(lineGeoJsonSource);
        return lineGeoJsonSource;
    }

    private void initBuildingCircleLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String bottomLayer) {
        CircleLayer circleLayer = new CircleLayer(id,
                sourceId);
        circleLayer.setProperties(
                circleRadius(3f),
                circleColor(Color.parseColor("#fc91fd"))
        );
        if (bottomLayer.equals("")) {
            loadedMapStyle.addLayerAbove(circleLayer, FILL_LAYER_ID);
        } else {
            loadedMapStyle.addLayerAbove(circleLayer, bottomLayer);
        }
    }

    private void initBuildingLineLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String bottomLayer) {
        LineLayer lineLayer = new LineLayer(id,
                sourceId);
        lineLayer.setProperties(
                lineColor(Color.parseColor("#6892ec")),
                lineWidth(5f)
        );
        if (bottomLayer.equals("")) {
            loadedMapStyle.addLayerAbove(lineLayer, FILL_LAYER_ID);
        } else {
            loadedMapStyle.addLayerAbove(lineLayer, bottomLayer);
        }
    }


    private GeoJsonSource initBuildingFillSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection fillFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource fillGeoJsonSource = new GeoJsonSource(id, fillFeatureCollection);
        loadedMapStyle.addSource(fillGeoJsonSource);
        return fillGeoJsonSource;
    }


    private void initBuildingFillLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String layer) {
        FillLayer fillLayer = new FillLayer(id,
                sourceId);
        fillLayer.setProperties(
                fillOpacity(.6f),
                fillColor(Color.parseColor("#fc91fd"))

        );

        loadedMapStyle.addLayer(fillLayer);

    }


    private GeoJsonSource initAQBuildingCircleSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection circleFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource circleGeoJsonSource = new GeoJsonSource(id, circleFeatureCollection);
        loadedMapStyle.addSource(circleGeoJsonSource);
        return circleGeoJsonSource;
    }


    private GeoJsonSource initAQBuildingLineSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection lineFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource lineGeoJsonSource = new GeoJsonSource(id, lineFeatureCollection);
        loadedMapStyle.addSource(lineGeoJsonSource);
        return lineGeoJsonSource;
    }

    private void initAQBuildingCircleLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String bottomLayer) {
        CircleLayer circleLayer = new CircleLayer(id,
                sourceId);
        circleLayer.setProperties(
                circleRadius(5f),
                circleColor(Color.parseColor("#b72424"))
        );
        if (bottomLayer.equals("")) {
            loadedMapStyle.addLayerAbove(circleLayer, FILL_LAYER_ID);
        } else {
            loadedMapStyle.addLayerAbove(circleLayer, bottomLayer);
        }
    }

    private void initAQBuildingLineLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String bottomLayer) {
        LineLayer lineLayer = new LineLayer(id,
                sourceId);
        lineLayer.setProperties(
                lineColor(Color.parseColor("#b72424")),
                lineWidth(5f)
        );
        if (bottomLayer.equals("")) {
            loadedMapStyle.addLayerAbove(lineLayer, FILL_LAYER_ID);
        } else {
            loadedMapStyle.addLayerAbove(lineLayer, bottomLayer);
        }
    }


    private GeoJsonSource initAQBuildingFillSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection fillFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource fillGeoJsonSource = new GeoJsonSource(id, fillFeatureCollection);
        loadedMapStyle.addSource(fillGeoJsonSource);
        return fillGeoJsonSource;
    }


    private void initAQBuildingFillLayer(@NonNull Style loadedMapStyle, String id, String sourceId) {
        FillLayer fillLayer = new FillLayer(id,
                sourceId);
        fillLayer.setProperties(
                fillOpacity(.6f),
                fillColor(Color.parseColor("#b72424"))
        );
        loadedMapStyle.addLayer(fillLayer);
    }


    private GeoJsonSource initBuildingBottomCircleSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection circleFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource circleGeoJsonSource = new GeoJsonSource(id, circleFeatureCollection);
        loadedMapStyle.addSource(circleGeoJsonSource);
        return circleGeoJsonSource;
    }


    private GeoJsonSource initBuildingBottomLineSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection lineFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource lineGeoJsonSource = new GeoJsonSource(id, lineFeatureCollection);
        loadedMapStyle.addSource(lineGeoJsonSource);
        return lineGeoJsonSource;
    }


    private void initBuildingBottomCircleLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String bottomLayer) {
        CircleLayer circleLayer = new CircleLayer(id,
                sourceId);
        circleLayer.setProperties(
                circleRadius(3f),
                circleOpacity(0.4f),
                circleColor(Color.parseColor("#696969"))
        );
        if (bottomLayer.equals("")) {
            loadedMapStyle.addLayerAbove(circleLayer, FILL_LAYER_ID);
        } else {
            loadedMapStyle.addLayerAbove(circleLayer, bottomLayer);
        }
    }

    private void initBuildingBottomLineLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String bottomLayer) {
        LineLayer lineLayer = new LineLayer(id,
                sourceId);
        lineLayer.setProperties(
                lineColor(Color.parseColor("#696969")),
                lineWidth(5f),
                lineOpacity(0.4f)
        );
        if (bottomLayer.equals("")) {
            loadedMapStyle.addLayerAbove(lineLayer, FILL_LAYER_ID);
        } else {
            loadedMapStyle.addLayerAbove(lineLayer, bottomLayer);
        }
    }


    private void initBuildingFloorCircleLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String bottomLayer) {
        CircleLayer circleLayer = new CircleLayer(id,
                sourceId);
        circleLayer.setProperties(
                circleRadius(7f),
                circleColor(Color.parseColor("#56a832"))
        );
        if (bottomLayer.equals("")) {
            loadedMapStyle.addLayerAbove(circleLayer, FILL_LAYER_ID);
        } else {
            loadedMapStyle.addLayerAbove(circleLayer, CIRCLE_LAYER_ID);
        }
    }

    private void initBuildingFloorLineLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String bottomLayer) {
        LineLayer lineLayer = new LineLayer(id,
                sourceId);
        lineLayer.setProperties(
                lineColor(Color.parseColor("#9F640B")),
                lineWidth(5f),
                lineOpacity(0.4f)
        );
        if (bottomLayer.equals("")) {
            loadedMapStyle.addLayerAbove(lineLayer, FILL_LAYER_ID);
        } else {
            loadedMapStyle.addLayerAbove(lineLayer, bottomLayer);
        }
    }

    private void initBuildingFloorFillLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String layer) {
        FillLayer fillLayer = new FillLayer(id,
                sourceId);
        fillLayer.setProperties(
                fillOpacity(.6f),
                fillColor(Color.parseColor("#F2BE6E"))
        );
        if (layer.equals("")) {
            loadedMapStyle.addLayer(fillLayer);
        } else {
            loadedMapStyle.addLayerAbove(fillLayer, FILL_LAYER_ID);

        }
    }


    private GeoJsonSource initBuildingBottomFillSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection fillFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource fillGeoJsonSource = new GeoJsonSource(id, fillFeatureCollection);
        loadedMapStyle.addSource(fillGeoJsonSource);
        return fillGeoJsonSource;
    }


    private void initBuildingBottomFillLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String bottomLayer) {
        FillLayer fillLayer = new FillLayer(id,
                sourceId);

        fillLayer.setProperties(
                fillColor(Color.parseColor("#FF5733"))
        );
        if (bottomLayer.equals("")) {
            loadedMapStyle.addLayerAbove(fillLayer, FILL_LAYER_ID);
        } else {
            loadedMapStyle.addLayerAbove(fillLayer, bottomLayer);
        }
    }


    private GeoJsonSource initAQBuildingBottomCircleSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection circleFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource circleGeoJsonSource = new GeoJsonSource(id, circleFeatureCollection);
        loadedMapStyle.addSource(circleGeoJsonSource);
        return circleGeoJsonSource;
    }


    private GeoJsonSource initAQBuildingBottomLineSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection lineFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource lineGeoJsonSource = new GeoJsonSource(id, lineFeatureCollection);
        loadedMapStyle.addSource(lineGeoJsonSource);
        return lineGeoJsonSource;
    }

    private void initAQBuildingBottomCircleLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String bottomLayer) {
        CircleLayer circleLayer = new CircleLayer(id,
                sourceId);
        circleLayer.setProperties(
                circleRadius(5f),
                circleOpacity(0.4f),
                circleColor(Color.parseColor("#b72424"))
        );
        if (bottomLayer.equals("")) {
            loadedMapStyle.addLayerAbove(circleLayer, FILL_LAYER_ID);
        } else {
            loadedMapStyle.addLayerAbove(circleLayer, bottomLayer);
        }
    }

    private void initAQBuildingBottomLineLayer(@NonNull Style loadedMapStyle, String id, String sourceId, String bottomLayer) {
        LineLayer lineLayer = new LineLayer(id,
                sourceId);
        lineLayer.setProperties(
                lineColor(Color.parseColor("#b72424")),
                lineWidth(5f),
                lineOpacity(0.4f)

        );
        if (bottomLayer.equals("")) {
            loadedMapStyle.addLayerAbove(lineLayer, FILL_LAYER_ID);
        } else {
            loadedMapStyle.addLayerAbove(lineLayer, bottomLayer);
        }
    }


    private GeoJsonSource initAQBuildingBottomFillSource(@NonNull Style loadedMapStyle, String id) {
        FeatureCollection fillFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource fillGeoJsonSource = new GeoJsonSource(id, fillFeatureCollection);
        loadedMapStyle.addSource(fillGeoJsonSource);
        return fillGeoJsonSource;
    }


    private void initAQBuildingBottomFillLayer(@NonNull Style loadedMapStyle, String id, String sourceId) {
        FillLayer fillLayer = new FillLayer(id,
                sourceId);
        fillLayer.setProperties(
                fillOpacity(.2f),
                fillColor(Color.parseColor("#b72424"))
        );
        loadedMapStyle.addLayerAbove(fillLayer, FILL_LAYER_ID);
    }


    public void onFloorSelected(BuildingFloor buildingFloor, int adapterPosition, BuildingFloor bottomBuildingFloor, List<BuildingFloor> floors) {


        if (adapterPosition != currentFloor) {
            buildingSaveDrawing();
            floorDrawingList = building.getBuildingFloorList().get(currentFloor).getFloorDrawingList();

            for (int i = 0; i < floorDrawingList.size(); i++) {

                if (mapboxMap.getStyle().getLayer("LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                    mapboxMap.getStyle().getLayer("LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(NONE));
                }
                if (mapboxMap.getStyle().getLayer("LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                    mapboxMap.getStyle().getLayer("LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(NONE));
                }
                if (mapboxMap.getStyle().getLayer("LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                    mapboxMap.getStyle().getLayer("LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(NONE));
                }


                if (mapboxMap.getStyle().getLayer("LayerCircleAQLine" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                    mapboxMap.getStyle().getLayer("LayerCircleAQLine" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(NONE));
                }
                if (mapboxMap.getStyle().getLayer("LayerFillAQLine" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                    mapboxMap.getStyle().getLayer("LayerFillAQLine" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(NONE));
                }
                if (mapboxMap.getStyle().getLayer("LayerLineAQLine" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                    mapboxMap.getStyle().getLayer("LayerLineAQLine" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(NONE));
                }
            }


            if (currentFloor > 0) {
                int bottomId = currentFloor - 1;

                for (int i = 0; i < floors.get(bottomId).getFloorDrawingList().size(); i++) {

                    if (mapboxMap.getStyle().getLayer(floors.get(bottomId).getFloorDrawingList().get(i).getCircleLayerID() + Constant.SHADOW) != null) {
                        mapboxMap.getStyle().getLayer(floors.get(bottomId).getFloorDrawingList().get(i).getCircleLayerID() + Constant.SHADOW).setProperties(visibility(NONE));
                    }
                    if (mapboxMap.getStyle().getLayer(floors.get(bottomId).getFloorDrawingList().get(i).getFillLayerID() + Constant.SHADOW) != null) {
                        mapboxMap.getStyle().getLayer(floors.get(bottomId).getFloorDrawingList().get(i).getFillLayerID() + Constant.SHADOW).setProperties(visibility(NONE));

                    }
                    if (mapboxMap.getStyle().getLayer(floors.get(bottomId).getFloorDrawingList().get(i).getLineLayerID() + Constant.SHADOW) != null) {
                        mapboxMap.getStyle().getLayer(floors.get(bottomId).getFloorDrawingList().get(i).getLineLayerID() + Constant.SHADOW).setProperties(visibility(NONE));

                    }

                }
            }


            floorDetailManager.deleteAll();
            floorSymbols.clear();
            floorDrawingList = new ArrayList<>();
            floorDrawingList = buildingFloor.getFloorDrawingList();

            isFloorPolygonDraw = false;
            isRoomPolygonDraw = false;
            isAQPolygonDraw = false;
            this.currentFloor = adapterPosition;

            List<io.xiges.ximapper.model.Symbol> symbols = buildingFloor.getSymbols();
            SymbolController sc = new SymbolController();
            List<Symbol> sList = new ArrayList<>();
            for (io.xiges.ximapper.model.Symbol s : symbols) {
                Symbol sy = sc.addFloorSymbol(s, floorDetailManager);
                sList.add(sy);
            }

            floorSymbols = sList;
            floorDetailManager.update(floorSymbols);
            double inchtot = 0;
            double inchtotAQ = 0;


            for (int i = 0; i < floorDrawingList.size(); i++) {

                if (mapboxMap.getStyle().getLayer("LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                    mapboxMap.getStyle().getLayer("LayerCircleBuilding" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(VISIBLE));
                }
                if (mapboxMap.getStyle().getLayer("LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                    mapboxMap.getStyle().getLayer("LayerFillBuilding" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(VISIBLE));
                }
                if (mapboxMap.getStyle().getLayer("LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                    mapboxMap.getStyle().getLayer("LayerLineBuilding" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(VISIBLE));
                }

                if (mapboxMap.getStyle().getLayer("LayerCircleAQLine" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                    mapboxMap.getStyle().getLayer("LayerCircleAQLine" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(VISIBLE));
                }
                if (mapboxMap.getStyle().getLayer("LayerFillAQLine" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                    mapboxMap.getStyle().getLayer("LayerFillAQLine" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(VISIBLE));
                }
                if (mapboxMap.getStyle().getLayer("LayerLineAQLine" + building.getId() + "" + this.currentFloor + "" + i) != null) {
                    mapboxMap.getStyle().getLayer("LayerLineAQLine" + building.getId() + "" + this.currentFloor + "" + i).setProperties(visibility(VISIBLE));
                }


            }


            floorDrawing.setFloors(new ArrayList<>(floorDrawingList));


            if (currentFloor != 0) {
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingAQPolygon.setVisibility(View.INVISIBLE);
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingFloorPolygon.setVisibility(View.VISIBLE);
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingRoomPolygon.setVisibility(View.VISIBLE);
                binding.layoutBuildingDrawingTool.imgAqDrawing.setVisibility(View.INVISIBLE);
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingPolygon.setVisibility(View.INVISIBLE);

                List<List<Point>> pp = new ArrayList<>();
                for (FloorDrawing fd : buildingFloor.getFloorDrawingList()) {
                    if (fd.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name())) {
                        double area = calculateArea(fd.getLineLayerPointList());
                        inchtot += (area * 1550.0031);
                    } else if (fd.getDrawingType().equals(DrawingType.AQLINE.name()) || fd.getDrawingType().equals(DrawingType.AQ_POLYGON.name())) {

                        pp.add(fd.getLineLayerAQPointList());

                    }
                }

                double area = calculateTotalAquiredArea(pp);
                inchtotAQ = (area * 1550.0031);

                mapboxMap.setLatLngBoundsForCameraTarget(RESTRICTED_BOUNDS_AREA);

            } else {
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingAQPolygon.setVisibility(View.INVISIBLE);
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingFloorPolygon.setVisibility(View.INVISIBLE);
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingRoomPolygon.setVisibility(View.INVISIBLE);
                binding.layoutBuildingDrawingTool.imgAqDrawing.setVisibility(View.VISIBLE);
                binding.layoutBuildingDrawingTool.imgbtnDrawBuildingBuildingPolygon.setVisibility(View.VISIBLE);


                double area = calculateArea(this.building.getLineLayerPointList());
                inchtot += (area * 1550.0031);
                List<List<Point>> points = new ArrayList<>();

                for (FloorDrawing fd : buildingFloor.getFloorDrawingList()) {
                    if (fd.getDrawingType().equals(DrawingType.AQLINE.name()) || fd.getDrawingType().equals(DrawingType.AQ_POLYGON.name())) {
                        points.add(fd.getLineLayerAQPointList());
                    }
                }

                double ar = calculateTotalAquiredArea(points);
                inchtotAQ = (ar * 1550.0031);

            }
            long feet = (long) (inchtot / 144);
            long inches = Math.round(inchtot % 144);

            if (inches == 144) {
                inches = 0;
                feet += 1;
            }

            binding.layoutBuildingDrawingTool.tvTotalBuildingArea.setText(Html.fromHtml(feet + " ft<sup>2</sup> " + inches + " inch<sup>2</sup>"));


            long feetAQ = (long) (inchtotAQ / 144);
            long inchesAQ = Math.round(inchtotAQ % 144);

            if (inchesAQ == 144) {
                inchesAQ = 0;
                feetAQ += 1;
            }

            binding.layoutBuildingDrawingTool.tvAcquiredArea.setText(Html.fromHtml(feetAQ + " ft<sup>2</sup> " + inchesAQ + " inch<sup>2</sup>"));

            if (currentFloor != 0 && bottomBuildingFloor != null) {
                initBuildingShadowLayer(bottomBuildingFloor.getFloorDrawingList(), building.getFillLayerID());
            }

        }
    }

    private void copyLayers(FloorCopyDetails current, FloorCopyDetails copyFrom, FloorCopyDetails copy) {

        floorDrawingList = building.getBuildingFloorList().get(current.getIndex()).getFloorDrawingList();


        int size = building.getBuildingFloorList().get(current.getIndex()).getFloorDrawingList().size();
        for (int i = 0; i < floorDrawingList.size(); i++) {


            if (copy.isFloor() && (floorDrawingList.get(i).getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name()))) {
                if (mapboxMap.getStyle().getLayer("LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + i) != null) {
                    mapboxMap.getStyle().removeLayer("LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + i);
                }
                if (mapboxMap.getStyle().getLayer("LayerFillBuilding" + building.getId() + "" + current.getIndex() + "" + i) != null) {
                    mapboxMap.getStyle().removeLayer("LayerFillBuilding" + building.getId() + "" + current.getIndex() + "" + i);
                }
                if (mapboxMap.getStyle().getLayer("LayerLineBuilding" + building.getId() + "" + current.getIndex() + "" + i) != null) {
                    mapboxMap.getStyle().removeLayer("LayerLineBuilding" + building.getId() + "" + current.getIndex() + "" + i);
                }


                if (mapboxMap.getStyle().getSource("CircleBuilding" + building.getId() + "" + current.getIndex() + "" + i) != null) {
                    mapboxMap.getStyle().removeSource("CircleBuilding" + building.getId() + "" + current.getIndex() + "" + i);
                }
                if (mapboxMap.getStyle().getSource("FillBuilding" + building.getId() + "" + current.getIndex() + "" + i) != null) {
                    mapboxMap.getStyle().removeSource("FillBuilding" + building.getId() + "" + current.getIndex() + "" + i);
                }
                if (mapboxMap.getStyle().getSource("LineBuilding" + building.getId() + "" + current.getIndex() + "" + i) != null) {
                    mapboxMap.getStyle().removeSource("LineBuilding" + building.getId() + "" + current.getIndex() + "" + i);
                }

                floorDrawingList.get(i).setCircleFillPoints(new ArrayList<>());
                floorDrawingList.get(i).setLineLayerPointList(new ArrayList<>());
                floorDrawingList.get(i).setCircleLayerFeatureList(new ArrayList<>());
                floorDrawingList.get(i).setLineLayerPointList(new ArrayList<>());
                floorDrawingList.get(i).setFillLayerPointList(new ArrayList<>());
                floorDrawingList.get(i).setDrawingType(DrawingType.CNULL.name());

            }


            if (copy.isAqLine() && ((floorDrawingList.get(i).getDrawingType().equals(DrawingType.AQLINE.name())) || (floorDrawingList.get(i).getDrawingType().equals(DrawingType.AQ_POLYGON.name())))) {
                if (mapboxMap.getStyle().getLayer("LayerCircleAQLine" + building.getId() + "" + current.getIndex() + "" + i) != null) {
                    mapboxMap.getStyle().removeLayer("LayerCircleAQLine" + building.getId() + "" + current.getIndex() + "" + i);
                }
                if (mapboxMap.getStyle().getLayer("LayerFillAQLine" + building.getId() + "" + current.getIndex() + "" + i) != null) {
                    mapboxMap.getStyle().removeLayer("LayerFillAQLine" + building.getId() + "" + current.getIndex() + "" + i);
                }
                if (mapboxMap.getStyle().getLayer("LayerLineAQLine" + building.getId() + "" + current.getIndex() + "" + i) != null) {
                    mapboxMap.getStyle().removeLayer("LayerLineAQLine" + building.getId() + "" + current.getIndex() + "" + i);
                }

                if (mapboxMap.getStyle().getSource("CircleAQLine" + building.getId() + "" + current.getIndex() + "" + i) != null) {
                    mapboxMap.getStyle().removeSource("CircleAQLine" + building.getId() + "" + current.getIndex() + "" + i);
                }
                if (mapboxMap.getStyle().getSource("FillAQLine" + building.getId() + "" + current.getIndex() + "" + i) != null) {
                    mapboxMap.getStyle().removeSource("FillAQLine" + building.getId() + "" + current.getIndex() + "" + i);
                }
                if (mapboxMap.getStyle().getSource("LineAQLine" + building.getId() + "" + current.getIndex() + "" + i) != null) {
                    mapboxMap.getStyle().removeSource("LineAQLine" + building.getId() + "" + current.getIndex() + "" + i);
                }

                floorDrawingList.get(i).setCircleLayerAQFeatureList(new ArrayList<>());
                floorDrawingList.get(i).setFillLayerAQPointList(new ArrayList<>());
                floorDrawingList.get(i).setLineLayerAQPointList(new ArrayList<>());
                floorDrawingList.get(i).setDrawingType(DrawingType.CNULL.name());

            }
        }

        //  building.getBuildingFloorList().get(current.getIndex()).setFloorDrawingList(new ArrayList<>(floorDrawingList));

        floorDetailManager.deleteAll();
        floorSymbols.clear();


        List<FloorDrawing> floorDrawings = new ArrayList<>(building.getBuildingFloorList().get(copyFrom.getIndex()).getFloorDrawingList());

        Collections.sort(floorDrawings, (o1, o2) -> o2.getDrawingType().compareTo(o1.getDrawingType()));


        double aqAr = 0;
        double floorAr = 0;

        for (FloorDrawing floorDrawing : floorDrawings) {
            if (copy.isRoom()) {
                if (floorDrawing.getDrawingType().equals(DrawingType.POLYGON.name()) || floorDrawing.getDrawingType().equals(DrawingType.BROOM_POLYGON.name())) {
                    FloorDrawing drawing = new FloorDrawing();
                    circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), "CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    lineLineSource = initBuildingLineSource(mapboxMap.getStyle(), "LineBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    fillLineSource = initBuildingFillSource(mapboxMap.getStyle(), "FillBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    initBuildingCircleLayer(mapboxMap.getStyle(), "LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + size, "CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size, building.getFillLayerID());
                    initBuildingLineLayer(mapboxMap.getStyle(), "LayerLineBuilding" + building.getId() + "" + current.getIndex() + "" + size, "LineBuilding" + building.getId() + "" + current.getIndex() + "" + size, "LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    initBuildingFillLayer(mapboxMap.getStyle(), "LayerFillBuilding" + building.getId() + "" + current.getIndex() + "" + size, "FillBuilding" + building.getId() + "" + current.getIndex() + "" + size, "LayerLineBuilding" + building.getId() + "" + current.getIndex() + "" + size);


                    listOfList = new ArrayList<>();
                    listOfList.add(new ArrayList<>(floorDrawing.getFillLayerPointList()));
                    List<Feature> features = new ArrayList<>();
                    features.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                    FeatureCollection featureCollection = FeatureCollection.fromFeatures(features);
                    if (fillLineSource != null) {
                        fillLineSource.setGeoJson(featureCollection);
                    }


                    if (lineLineSource != null) {
                        lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                {Feature.fromGeometry(LineString.fromLngLats(floorDrawing.getLineLayerPointList()))}));
                    }

                    if (circleLineSource != null) {
                        circleLineSource.setGeoJson(FeatureCollection.fromFeatures(floorDrawing.getCircleLayerFeatureList()));
                    }


                    drawing.setDrawingType(DrawingType.BROOM_POLYGON.name());


                    drawing.setDrawingType(DrawingType.BROOM_POLYGON.name());
                    drawing.setCircleLayerFeatureList(new ArrayList<>(floorDrawing.getCircleLayerFeatureList()));
                    drawing.setLineLayerPointList(new ArrayList<>(floorDrawing.getLineLayerPointList()));
                    drawing.setFillLayerPointList(new ArrayList<>(floorDrawing.getFillLayerPointList()));
                    drawing.setLineLayerID("LayerLineBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setCircleLayerID("LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setFillLayerID("LayerFillBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setFillSourceID("FillBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setCircleSourceID("CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setLineSourceID("LineBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    floorDrawingList.add(drawing);

                    size++;

                } else if (floorDrawing.getDrawingType().equals(DrawingType.LINE.name())) {
                    FloorDrawing drawing = new FloorDrawing();
                    circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), "CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    lineLineSource = initBuildingLineSource(mapboxMap.getStyle(), "LineBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    initBuildingLineLayer(mapboxMap.getStyle(), "LayerLineBuilding" + building.getId() + "" + current.getIndex() + "" + size, "LineBuilding" + building.getId() + "" + current.getIndex() + "" + size, "");
                    initBuildingCircleLayer(mapboxMap.getStyle(), "LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + size, "CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size, "LayerLineBuilding" + building.getId() + "" + current.getIndex() + "" + size);


                    if (circleLineSource != null) {
                        circleLineSource.setGeoJson(FeatureCollection.fromFeatures(floorDrawing.getCircleLayerFeatureList()));
                    }

                    if (lineLineSource != null) {
                        lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                {Feature.fromGeometry(LineString.fromLngLats(floorDrawing.getLineLayerPointList()))}));
                    }


                    drawing.setDrawingType(DrawingType.LINE.name());
                    drawing.setCircleLayerFeatureList(new ArrayList<>(floorDrawing.getCircleLayerFeatureList()));
                    drawing.setLineLayerPointList(new ArrayList<>(floorDrawing.getLineLayerPointList()));
                    drawing.setLineLayerID("LayerLineBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setCircleLayerID("LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setLineSourceID("LineBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setCircleSourceID("CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);

                    floorDrawingList.add(drawing);
                    size++;

                } else if (floorDrawing.getDrawingType().equals(DrawingType.CIRCLE.name())) {
                    FloorDrawing drawing = new FloorDrawing();
                    circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), "CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    initBuildingCircleLayer(mapboxMap.getStyle(), "LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + size, "CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size, "");
                    fillLineSource = initBuildingFillSource(mapboxMap.getStyle(), "FillBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    initBuildingFillLayer(mapboxMap.getStyle(), "LayerFillBuilding" + building.getId() + "" + current.getIndex() + "" + size, "FillBuilding" + building.getId() + "" + current.getIndex() + "" + size, FILL_LAYER_ID);



                            /*if (fillLineSource != null) {
                                fillLineSource.setGeoJson(TurfTransformation.circle(
                                        floorDrawing.getCircle().getMidPoint(), floorDrawing.getCircle().getRadius(), floorDrawing.getCircle().getUnit()));
                            }*/

                    listOfList = new ArrayList<>();
                    listOfList.add(floorDrawing.getFillLayerPointList());
                    List<Feature> features = new ArrayList<>();
                    features.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                    FeatureCollection featureCollection = FeatureCollection.fromFeatures(features);
                    if (fillLineSource != null) {
                        fillLineSource.setGeoJson(featureCollection);
                    }


                    if (circleLineSource != null) {
                        circleLineSource.setGeoJson(FeatureCollection.fromFeatures(floorDrawing.getCircleLayerFeatureList()));
                    }

                    drawing.setDrawingType(DrawingType.CIRCLE.name());
                    drawing.setCircleLayerFeatureList(new ArrayList<>(floorDrawing.getCircleLayerFeatureList()));
                    drawing.setFillLayerPointList(new ArrayList<>(floorDrawing.getFillLayerPointList()));
                    drawing.setFillLayerID("LayerFillBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setCircleLayerID("LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setFillSourceID("FillBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setCircleSourceID("CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    floorDrawingList.add(drawing);

                    size++;
                }
            }
            if (copy.isAqLine()) {
                if (floorDrawing.getDrawingType().equals(DrawingType.AQLINE.name())) {
                    FloorDrawing drawing = new FloorDrawing();
                    aqCircleSource = initAQBuildingCircleSource(mapboxMap.getStyle(), "CircleAQLine" + building.getId() + "" + current.getIndex() + "" + size);
                    aqLineSource = initAQBuildingLineSource(mapboxMap.getStyle(), "LineAQLine" + building.getId() + "" + current.getIndex() + "" + size);
                    aqFillSource = initAQBuildingFillSource(mapboxMap.getStyle(), "FillAQLine" + building.getId() + "" + current.getIndex() + "" + size);
                    initAQBuildingFillLayer(mapboxMap.getStyle(), "LayerFillAQLine" + building.getId() + "" + current.getIndex() + "" + size, "FillAQLine" + building.getId() + "" + current.getIndex() + "" + size);
                    initAQBuildingLineLayer(mapboxMap.getStyle(), "LayerLineAQLine" + building.getId() + "" + current.getIndex() + "" + size, "LineAQLine" + building.getId() + "" + current.getIndex() + "" + size, "LayerFillAQLine" + building.getId() + "" + current.getIndex() + "" + size);
                    initAQBuildingCircleLayer(mapboxMap.getStyle(), "LayerCircleAQLine" + building.getId() + "" + current.getIndex() + "" + size, "CircleAQLine" + building.getId() + "" + current.getIndex() + "" + size, "LayerLineAQLine" + building.getId() + "" + current.getIndex() + "" + size);


                    if (aqCircleSource != null) {
                        aqCircleSource.setGeoJson(FeatureCollection.fromFeatures(floorDrawing.getCircleLayerAQFeatureList()));
                    }

                    if (aqLineSource != null) {
                        aqLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                {Feature.fromGeometry(LineString.fromLngLats(floorDrawing.getLineLayerAQPointList()))}));
                    }

                    listOfList = new ArrayList<>();
                    listOfList.add(floorDrawing.getFillLayerAQPointList());
                    List<Feature> arrayList = new ArrayList<>();
                    arrayList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                    FeatureCollection collection = FeatureCollection.fromFeatures(arrayList);
                    if (aqFillSource != null) {
                        aqFillSource.setGeoJson(collection);
                    }

                    drawing.setAqCircleSourceID("AQCircle" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setAqLineSourceID("AQLine" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setAqFillSourceID("AQFill" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setAqCircleLayerID("LayerCircleAQLine" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setAqLineLayerID("LayerLineAQLine" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setAqFillLayerID("LayerFillAQLine" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setDrawingType(DrawingType.AQLINE.name());
                    drawing.setCircleLayerAQFeatureList(new ArrayList<>(floorDrawing.getCircleLayerAQFeatureList()));
                    drawing.setLineLayerAQPointList(new ArrayList<>(floorDrawing.getLineLayerAQPointList()));
                    drawing.setFillLayerAQPointList(new ArrayList<>(floorDrawing.getFillLayerAQPointList()));

                    double area = calculateArea(drawing.getLineLayerAQPointList());
                    double inchtotAQ = (area * 1550.0031);


                    point1 = null;
                    point2 = null;
                    point3 = null;
                    point4 = null;


                    aqAr += inchtotAQ;


                    aqCircleLayerFeatureList = new ArrayList<>();
                    aqLineLayerPointList = new ArrayList<>();
                    aqFillLayerPointList = new ArrayList<>();
                    floorDrawingList.add(drawing);

                    size++;
                }

            }

            if (copy.isFloor()) {
                if (floorDrawing.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name())) {
                    FloorDrawing drawing = new FloorDrawing();
                    circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), "CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    lineLineSource = initBuildingLineSource(mapboxMap.getStyle(), "LineBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    fillLineSource = initBuildingFillSource(mapboxMap.getStyle(), "FillBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    initBuildingFloorCircleLayer(mapboxMap.getStyle(), "LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + size, "CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size, building.getFillLayerID());
                    initBuildingFloorLineLayer(mapboxMap.getStyle(), "LayerLineBuilding" + building.getId() + "" + current.getIndex() + "" + size, "LineBuilding" + building.getId() + "" + current.getIndex() + "" + size, "LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    initBuildingFloorFillLayer(mapboxMap.getStyle(), "LayerFillBuilding" + building.getId() + "" + current.getIndex() + "" + size, "FillBuilding" + building.getId() + "" + current.getIndex() + "" + size, "");


                    if (copyFrom.getIndex() != 0) {
                        listOfList = new ArrayList<>();
                        listOfList.add(new ArrayList<>(floorDrawing.getFillLayerPointList()));
                        List<Feature> features = new ArrayList<>();
                        features.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                        FeatureCollection featureCollection = FeatureCollection.fromFeatures(features);
                        if (fillLineSource != null) {
                            fillLineSource.setGeoJson(featureCollection);
                        }


                        if (lineLineSource != null) {
                            lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                    {Feature.fromGeometry(LineString.fromLngLats(floorDrawing.getLineLayerPointList()))}));
                        }


                        if (circleLineSource != null) {
                            circleLineSource.setGeoJson(FeatureCollection.fromFeatures(floorDrawing.getCircleLayerFeatureList()));
                        }
                    }

                    drawing.setDrawingType(DrawingType.ZFLOOR_POLYGON.name());
                    drawing.setCircleLayerFeatureList(new ArrayList<>(floorDrawing.getCircleLayerFeatureList()));
                    drawing.setLineLayerPointList(new ArrayList<>(floorDrawing.getLineLayerPointList()));
                    drawing.setFillLayerPointList(new ArrayList<>(floorDrawing.getFillLayerPointList()));
                    drawing.setLineLayerID("LayerLineBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setCircleLayerID("LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setFillLayerID("LayerFillBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setFillSourceID("FillBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setCircleSourceID("CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    drawing.setLineSourceID("LineBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                    floorDrawingList.add(drawing);

                    size++;

                }
            }

        }

        long feetAQ = (long) (aqAr / 144);
        long inchesAQ = Math.round(aqAr % 144);
        if (inchesAQ == 144) {
            inchesAQ = 0;
            feetAQ += 1;
        }
        binding.layoutBuildingDrawingTool.tvAcquiredArea.setText(Html.fromHtml(feetAQ + " ft<sup>2</sup> " + inchesAQ + " inch<sup>2</sup>"));

        if (copy.isFloor()) {
            if (copyFrom.getIndex() == 0) {
                FloorDrawing drawing = new FloorDrawing();
                circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), "CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                lineLineSource = initBuildingLineSource(mapboxMap.getStyle(), "LineBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                fillLineSource = initBuildingFillSource(mapboxMap.getStyle(), "FillBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                initBuildingFloorCircleLayer(mapboxMap.getStyle(), "LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + size, "CircleBuilding" + building.getId() + "" + current.getIndex() + "" + size, building.getFillLayerID());
                initBuildingFloorLineLayer(mapboxMap.getStyle(), "LayerLineBuilding" + building.getId() + "" + current.getIndex() + "" + size, "LineBuilding" + building.getId() + "" + current.getIndex() + "" + size, "LayerCircleBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                initBuildingFloorFillLayer(mapboxMap.getStyle(), "LayerFillBuilding" + building.getId() + "" + current.getIndex() + "" + size, "FillBuilding" + building.getId() + "" + current.getIndex() + "" + size, "fill");


                listOfList = new ArrayList<>();
                listOfList.add(new ArrayList<>(building.getFillLayerPointList()));
                List<Feature> features = new ArrayList<>();
                features.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                FeatureCollection featureCollection = FeatureCollection.fromFeatures(features);
                if (fillLineSource != null) {
                    fillLineSource.setGeoJson(featureCollection);
                }


                if (lineLineSource != null) {
                    lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                            {Feature.fromGeometry(LineString.fromLngLats(building.getLineLayerPointList()))}));
                }


                if (circleLineSource != null) {
                    circleLineSource.setGeoJson(FeatureCollection.fromFeatures(building.getCircleLayerFeatureList()));
                }


                drawing.setDrawingType(DrawingType.ZFLOOR_POLYGON.name());
                drawing.setCircleLayerFeatureList(new ArrayList<>(building.getCircleLayerFeatureList()));
                drawing.setLineLayerPointList(new ArrayList<>(building.getLineLayerPointList()));
                drawing.setFillLayerPointList(new ArrayList<>(building.getFillLayerPointList()));
                drawing.setLineLayerID("LayerLineBuilding" + building.getId() + "" + building.getId() + "" + current.getIndex() + "" + size);
                drawing.setCircleLayerID("LayerCircleBuilding" + building.getId() + "" + building.getId() + "" + current.getIndex() + "" + size);
                drawing.setFillLayerID("LayerFillBuilding" + building.getId() + "" + current.getIndex() + "" + size);
                drawing.setFillSourceID("FillBuilding" + building.getId() + "" + building.getId() + "" + current.getIndex() + "" + size);
                drawing.setCircleSourceID("CircleBuilding" + building.getId() + "" + building.getId() + "" + current.getIndex() + "" + size);
                drawing.setLineSourceID("LineBuilding" + building.getId() + "" + building.getId() + "" + current.getIndex() + "" + size);
                floorDrawingList.add(drawing);

                size++;

            }
        }
        for (FloorDrawing f : floorDrawingList) {
            showBuildingLineDistances(f.getLineLayerAQPointList());
            showBuildingLineDistances(f.getLineLayerPointList());

        }

        List<io.xiges.ximapper.model.Symbol> symbols = new ArrayList<>();
        for (Symbol s : floorSymbols) {
            io.xiges.ximapper.model.Symbol symbol = new io.xiges.ximapper.model.Symbol();
            symbol.setLatLng(s.getLatLng());
            symbol.setName(s.getTextField());
            symbols.add(symbol);
        }

        buildingFloors.get(currentFloor).setSymbols(symbols);
        buildingFloors.get(currentFloor).setFloorDrawingList(new ArrayList<>(floorDrawingList));
        this.building.setBuildingFloorList(new ArrayList<>(buildingFloors));
        constructions.set(constructions.indexOf(building), this.building);

        buildingFloors.get(currentFloor).setAqArea(aqAr);
        buildingFloors.get(currentFloor).setFloorArea(floorAr);

        DataManager dataManager = new DataManager();
        binding.layoutDrawingTool.btnSaveDraw.setEnabled(false);
        binding.layoutDrawingTool.btnDraw.setEnabled(false);
        lot.setConstructions(constructions);
        this.lot.setSymbolList(symbols);


        try {
            dataManager.writeToFile(lot, masterFile.getMasterFileName());
        } catch (IOException e) {
            e.printStackTrace();
        }
        floorDrawing.setFloors(new ArrayList<>(floorDrawingList));


        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
    }

    private void showDistance() {
        Point p = TurfMeasurement.midpoint(lineLayerPointList.get(lineLayerPointList.size() - 1), lineLayerPointList.get(lineLayerPointList.size() - 2));
        double distance = MapCalculations.calculateDistance(lineLayerPointList.get(lineLayerPointList.size() - 1), lineLayerPointList.get(lineLayerPointList.size() - 2));
        double inchtot = Math.round(distance / meters2inches);
        int feet = (int) inchtot / 12;
        int inches = (int) inchtot % 12;

        if (inches == 12) {
            inches = 0;
        }

        Symbol symbol = lotLabelManager.create(new SymbolOptions()
                .withGeometry(p)
                .withDraggable(false)
                .withTextSize(11f)
                .withTextColor("#087735")
                .withTextField(feet + " ft " + inches + "\"")
        );
        symbolList.add(symbol);

    }

    private void showBuildingDistance(List<Point> line) {
        Point p = TurfMeasurement.midpoint(line.get(line.size() - 1), line.get(line.size() - 2));
        double distance = MapCalculations.calculateDistance(line.get(line.size() - 1), line.get(line.size() - 2));
        double inchtot = Math.round(distance / meters2inches);


        int feet = (int) inchtot / 12;
        int inches = (int) inchtot % 12;

        if (inches == 12) {
            inches = 0;
        }
        if (isDrawingMode) {
            Symbol symbol = buildingDetailManager.create(new SymbolOptions()
                    .withGeometry(p)
                    .withDraggable(false)
                    .withTextSize(11f)
                    .withTextColor("#581845")
                    .withTextField(feet + " ft " + inches + "\"")
            );
            labelSymbolBuildingList.add(symbol);
        } else if (isBuildingSketchMode) {
            Symbol symbol = floorDetailManager.create(new SymbolOptions()
                    .withGeometry(p)
                    .withDraggable(false)
                    .withTextSize(11f)
                    .withTextColor("#581845")
                    .withTextField(feet + " ft " + inches + "\"")
            );
            floorSymbols.add(symbol);
        }

    }


    private void showBuildingLineDistances(List<Point> line) {
        if (line.size() > 1) {
            for (int i = 0; i < line.size() - 1; i++) {
                Point p = TurfMeasurement.midpoint(line.get(i), line.get(i + 1));
                double distance = MapCalculations.calculateDistance(line.get(i), line.get(i + 1));
                double inchtot = Math.round(distance / meters2inches);
                int feet = (int) inchtot / 12;
                int inches = (int) inchtot % 12;


                Symbol symbol = floorDetailManager.create(new SymbolOptions()
                        .withGeometry(p)
                        .withDraggable(false)
                        .withTextSize(11f)
                        .withTextColor("#581845")
                        .withTextField(feet + " ft " + inches + "\"")
                );
                floorSymbols.add(symbol);
            }
        }

    }

    private void drawMeasuredPolygon(LatLng markedLatLng) {

        Point point = Point.fromLngLat(markedLatLng.getLongitude(), markedLatLng.getLatitude());
        lastPointMarked = point;
        // Make note of the first map click location so that it can be used to create a closed polygon later on
        if (circleLayerFeatureList.size() == 0) {
            firstPointOfPolygon = point;
        } else if (circleLayerFeatureList.size() == 1) {
            binding.imgBtnUndo.setEnabled(true);
        }

        lineLayerTempPointList = null;
        fillLayerTempPointList = null;
        circleTempLayerList = null;

// Add the click point to the circle layer and update the display of the circle layer data
        circleLayerFeatureList.add(Feature.fromGeometry(point));
        if (circleSource != null) {
            circleSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList));
        }

        // Add the click point to the line layer and update the display of the line layer data
        if (circleLayerFeatureList.size() < 3) {
            lineLayerPointList.add(point);
            lotLastPointMarked = point;
            if (lineLayerPointList.size() >= 2) {
                showDistance();
            }

        } else if (circleLayerFeatureList.size() == 3) {
            lineLayerPointList.add(point);
            showDistance();
            lineLayerPointList.add(firstPointOfPolygon);
            showDistance();
            lotLastPointMarked = point;
        } else {
            lineLayerPointList.remove(lineLayerPointList.size() - 1);
            lotLabelManager.delete(symbolList.get(symbolList.size() - 1));
            lineLayerPointList.add(point);
            showDistance();
            lineLayerPointList.add(firstPointOfPolygon);
            showDistance();
            lotLastPointMarked = point;
        }
        if (lineSource != null) {
            lineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                    {Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList))}));
        }

        // Add the click point to the fill layer and update the display of the fill layer data
        if (circleLayerFeatureList.size() < 3) {
            fillLayerPointList.add(point);
        } else if (circleLayerFeatureList.size() == 3) {
            fillLayerPointList.add(point);
            fillLayerPointList.add(firstPointOfPolygon);
        } else {
            fillLayerPointList.remove(fillLayerPointList.size() - 1);
            fillLayerPointList.add(point);
            fillLayerPointList.add(firstPointOfPolygon);
        }
        listOfList = new ArrayList<>();
        listOfList.add(fillLayerPointList);
        List<Feature> finalFeatureList = new ArrayList<>();
        finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
        FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
        if (fillSource != null) {
            fillSource.setGeoJson(newFeatureCollection);
        }
        LatLng latLng = new LatLng();
        latLng.setLatitude(point.latitude());
        latLng.setLongitude(point.longitude());
        PointF pointf = mapboxMap.getProjection().toScreenLocation(latLng);
        /*rectF = new RectF(pointf.x - 100000, pointf.y - 100000, pointf.x + 100000, pointf.y + 100000);
        List<Feature> featureList = mapboxMap.queryRenderedFeatures(rectF, FILL_LAYER_ID);
        for (Feature feature : featureList) {
            try {
                double bbox[] = TurfMeasurement.bbox((Polygon) feature.geometry());
            } catch (ClassCastException e) {
                undo();
                Intent i = new Intent(MapViewActivity.this, ErrorBoxActivity.class);
                i.putExtra(Constant.NAME, "Invalid Drawing");
                startActivity(i);
            }
        }*/
    }

    private double calculateArea(List<Point> layerList) {
        List<com.google.android.gms.maps.model.LatLng> latLngs = new ArrayList<>();
        for (Point f : layerList) {
            com.google.android.gms.maps.model.LatLng lt = new com.google.android.gms.maps.model.LatLng(f.latitude(), f.longitude());
            latLngs.add(lt);
        }
        double meters = SphericalUtil.computeArea(latLngs);
        return meters;
    }

    private double calculateCircleArea() {
        double circleArea = 3.14 * (radius * radius);
        return circleArea;
    }

    private void drawMeasuredPointForBuilding(LatLng markedLatLng) {
        binding.layoutDrawingTool.btnSaveDraw.setEnabled(true);
        binding.imgBtnLandUndo.setEnabled(true);
        // Use the map click location to create a Point object
        Point mapTargetPoint = Point.fromLngLat(markedLatLng.getLongitude(), markedLatLng.getLatitude());
        lastPointMarked = mapTargetPoint;

        if (isLineDraw) {
// Make note of the first map click location so that it can be used to create a closed polygon later on
            if (circleLayerDrawingFeatureList.size() == 0) {
                firstPointOfPolygon = mapTargetPoint;
            }
            lastPointMarked = mapTargetPoint;

// Add the click point to the circle layer and update the display of the circle layer data
            circleLayerDrawingFeatureList.add(Feature.fromGeometry(mapTargetPoint));

            if (circleLineSource != null) {
                circleLineSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerDrawingFeatureList));
            }

// Add the click point to the line layer and update the display of the line layer data
            if (circleLayerDrawingFeatureList.size() < 3) {
                lineLayerDrawingPointList.add(mapTargetPoint);
                if (circleLayerDrawingFeatureList.size() > 1) {
                    showBuildingDistance(lineLayerDrawingPointList);
                }
            } else if (circleLayerDrawingFeatureList.size() == 3) {
                lineLayerDrawingPointList.add(mapTargetPoint);
                showBuildingDistance(lineLayerDrawingPointList);
            } else {
                lineLayerDrawingPointList.add(mapTargetPoint);
                showBuildingDistance(lineLayerDrawingPointList);
            }
            if (lineLineSource != null) {
                lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                        {Feature.fromGeometry(LineString.fromLngLats(lineLayerDrawingPointList))}));
            }

        } else if (isPolygonDraw || isRoomPolygonDraw || isFloorPolygonDraw || isAQPolygonDraw) {
            // Make note of the first map click location so that it can be used to create a closed polygon later on
            if (circleLayerDrawingFeatureList.size() == 0) {
                firstPointOfPolygon = mapTargetPoint;
            }

            lastPointMarked = mapTargetPoint;


// Add the click point to the circle layer and update the display of the circle layer data
            circleLayerDrawingFeatureList.add(Feature.fromGeometry(mapTargetPoint));
            if (circleLineSource != null) {
                circleLineSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerDrawingFeatureList));
            }


// Add the click point to the line layer and update the display of the line layer data
            if (circleLayerDrawingFeatureList.size() < 3) {
                lineLayerDrawingPointList.add(mapTargetPoint);
                if (circleLayerDrawingFeatureList.size() > 1) {
                    showBuildingDistance(lineLayerDrawingPointList);
                }
            } else if (circleLayerDrawingFeatureList.size() == 3) {
                lineLayerDrawingPointList.add(mapTargetPoint);
                showBuildingDistance(lineLayerDrawingPointList);
                lineLayerDrawingPointList.add(firstPointOfPolygon);
                showBuildingDistance(lineLayerDrawingPointList);

            } else {
                lineLayerDrawingPointList.remove(lineLayerDrawingPointList.size() - 1);
                if (isDrawingMode) {
                    buildingDetailManager.delete(labelSymbolBuildingList.get(labelSymbolBuildingList.size() - 1));
                    labelSymbolBuildingList.remove(labelSymbolBuildingList.size() - 1);
                } else if (isBuildingSketchMode) {
                    floorDetailManager.delete(floorSymbols.get(floorSymbols.size() - 1));
                    floorSymbols.remove(floorSymbols.size() - 1);
                }

                lineLayerDrawingPointList.add(mapTargetPoint);
                showBuildingDistance(lineLayerDrawingPointList);
                lineLayerDrawingPointList.add(firstPointOfPolygon);
                showBuildingDistance(lineLayerDrawingPointList);

            }
            if (lineLineSource != null) {
                lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                        {Feature.fromGeometry(LineString.fromLngLats(lineLayerDrawingPointList))}));
            }

// Add the click point to the fill layer and update the display of the fill layer data
            if (circleLayerDrawingFeatureList.size() < 3) {
                fillLayerDrawingPointList.add(mapTargetPoint);
            } else if (circleLayerDrawingFeatureList.size() == 3) {
                fillLayerDrawingPointList.add(mapTargetPoint);
                fillLayerDrawingPointList.add(firstPointOfPolygon);
            } else {
                fillLayerDrawingPointList.remove(fillLayerDrawingPointList.size() - 1);
                fillLayerDrawingPointList.add(mapTargetPoint);
                fillLayerDrawingPointList.add(firstPointOfPolygon);
            }
            listOfList = new ArrayList<>();
            listOfList.add(fillLayerDrawingPointList);
            List<Feature> finalFeatureList = new ArrayList<>();
            finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
            FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
            if (fillLineSource != null) {
                fillLineSource.setGeoJson(newFeatureCollection);
            }
        } else if (isCircleDraw) {
            lastPointMarked = mapTargetPoint;
            if (circleLayerDrawingFeatureList.size() == 0) {
                firstPointOfPolygon = mapTargetPoint;
                circleLayerDrawingFeatureList.add(Feature.fromGeometry(mapTargetPoint));
                circleLineSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerDrawingFeatureList));
            } else {
                radius = (float) MapCalculations.calculateDistance(firstPointOfPolygon, mapTargetPoint);
                listOfList = new ArrayList<>();
                listOfList = TurfTransformation.circle(
                        Point.fromLngLat(firstPointOfPolygon.longitude(), firstPointOfPolygon.latitude()), radius, 64, "meters").coordinates();
                List<Feature> features = new ArrayList<>();
                features.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                circlePointList = features;
                fillLineSource.setGeoJson(TurfTransformation.circle(
                        Point.fromLngLat(firstPointOfPolygon.longitude(), firstPointOfPolygon.latitude()), radius, 64, "meters"));
            }
        } else if (isAqLineDraw) {
            //   drawAQLine(mapTargetPoint);
        }
    }

    private void drawAQLine(Point mapTargetPoint) {
        // Make note of the first map click location so that it can be used to create a closed polygon later on
        circleManager.deleteAll();
        if (aqCircleLayerFeatureList.size() == 0) {
            firstPointOfPolygon = mapTargetPoint;
        }

        lastPointMarked = mapTargetPoint;


// Add the click point to the circle layer and update the display of the circle layer data
        aqCircleLayerFeatureList.add(Feature.fromGeometry(mapTargetPoint));
        if (aqCircleSource != null) {
            aqCircleSource.setGeoJson(FeatureCollection.fromFeatures(aqCircleLayerFeatureList));
        }

// Add the click point to the line layer and update the display of the line layer data
        if (aqCircleLayerFeatureList.size() < 3) {
            aqLineLayerPointList.add(mapTargetPoint);
            if (aqCircleLayerFeatureList.size() > 1) {
                //    showBuildingDistance(aqLineLayerPointList);
            }
        } else if (aqCircleLayerFeatureList.size() == 3) {
            aqLineLayerPointList.add(mapTargetPoint);
            //  showBuildingDistance(aqLineLayerPointList);
            aqLineLayerPointList.add(firstPointOfPolygon);
            //   showBuildingDistance(aqLineLayerPointList);

        } else {
            aqLineLayerPointList.remove(aqLineLayerPointList.size() - 1);
//            buildingDetailManager.delete(labelSymbolBuildingList.get(labelSymbolBuildingList.size() - 1));
            aqLineLayerPointList.add(mapTargetPoint);
            //showBuildingDistance(aqLineLayerPointList);
            aqLineLayerPointList.add(firstPointOfPolygon);
            //  showBuildingDistance(aqLineLayerPointList);

        }
        if (aqLineSource != null) {
            aqLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                    {Feature.fromGeometry(LineString.fromLngLats(aqLineLayerPointList))}));
        }


// Add the click point to the fill layer and update the display of the fill layer data
        if (aqCircleLayerFeatureList.size() < 3) {
            aqFillLayerPointList.add(mapTargetPoint);
        } else if (aqCircleLayerFeatureList.size() == 3) {
            aqFillLayerPointList.add(mapTargetPoint);
            aqFillLayerPointList.add(firstPointOfPolygon);
        } else {
            aqFillLayerPointList.remove(aqFillLayerPointList.size() - 1);
            aqFillLayerPointList.add(mapTargetPoint);
            aqFillLayerPointList.add(firstPointOfPolygon);
        }
        listOfList = new ArrayList<>();
        listOfList.add(aqFillLayerPointList);
        List<Feature> finalFeatureList = new ArrayList<>();
        finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
        FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
        if (aqFillSource != null) {
            aqFillSource.setGeoJson(newFeatureCollection);
        }
    }

    private double calculateAquiredArea(List<Feature> features, List<Point> list) {
        if (aqCircleLayerFeatureList.size() >= 3) {
            List<List<Point>> ll = new ArrayList<>();
            ll.add(aqFillLayerPointList);
            List<Feature> finalFeatureList = new ArrayList<>();
            finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(ll)));
            FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
            FeatureCollection featureList = FeatureCollection.fromFeatures(features);
            FeatureCollection ff = TurfJoins.pointsWithinPolygon(featureList, newFeatureCollection);

            if (features.size() == ff.features().size()) {
                return calculateArea(list);
            } else {
                return 0;
            }
        }
        return -1;
    }

    private double calculateTotalAquiredArea(List<List<Point>> list) {
        double total = 0;

        for (List<Point> points : list) {
            total += calculateArea(points);

       }
        return total;
    }


    private void drawAQLineBuilding(Point mapTargetPoint) {
        // Make note of the first map click location so that it can be used to create a closed polygon later on
        if (aqCircleLayerFeatureList.size() == 0) {
            firstPointOfPolygon = mapTargetPoint;
        }

        lastPointMarked = mapTargetPoint;


// Add the click point to the circle layer and update the display of the circle layer data
        aqCircleLayerFeatureList.add(Feature.fromGeometry(mapTargetPoint));
        if (aqCircleSource != null) {
            aqCircleSource.setGeoJson(FeatureCollection.fromFeatures(aqCircleLayerFeatureList));
        }

// Add the click point to the line layer and update the display of the line layer data
        if (aqCircleLayerFeatureList.size() < 3) {
            aqLineLayerPointList.add(mapTargetPoint);
            if (aqCircleLayerFeatureList.size() > 1) {
                //    showBuildingDistance(aqLineLayerPointList);
            }
        } else if (aqCircleLayerFeatureList.size() == 3) {
            aqLineLayerPointList.add(mapTargetPoint);
            //  showBuildingDistance(aqLineLayerPointList);
            //    aqLineLayerPointList.add(firstPointOfPolygon);
            //   showBuildingDistance(aqLineLayerPointList);

        } else {
            //     aqLineLayerPointList.remove(aqLineLayerPointList.size() - 1);
//            buildingDetailManager.delete(labelSymbolBuildingList.get(labelSymbolBuildingList.size() - 1));
            aqLineLayerPointList.add(mapTargetPoint);
            //showBuildingDistance(aqLineLayerPointList);
            //      aqLineLayerPointList.add(firstPointOfPolygon);
            //  showBuildingDistance(aqLineLayerPointList);

        }
        if (aqLineSource != null) {
            aqLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                    {Feature.fromGeometry(LineString.fromLngLats(aqLineLayerPointList))}));
        }
    }

    private void drawAcqLineSelect(Feature feature) {
        Log.e("Point ", feature.geometry().toJson());
        Point p = (Point) feature.geometry();
        if (point1 == null) {
            point1 = p;
            //lastPointMarked = p;
            //circleManager.deleteAll();
            circleManager.create(new CircleOptions()
                    .withCircleColor("Yellow")
                    .withCircleRadius(7f)
                    .withGeometry(p)
            );
            binding.layoutBuildingDrawingTool.pointMarker.setVisibility(View.GONE);
        } else if (point2 == null) {
            point2 = p;
            //lastPointMarked = p;
            circleManager.deleteAll();
            circleManager.create(new CircleOptions()
                    .withCircleColor("Yellow")
                    .withCircleRadius(7f)
                    .withGeometry(p)
            );
            binding.layoutBuildingDrawingTool.pointMarker.setVisibility(View.VISIBLE);
            if (point1 != null) {
                Intent i = new Intent(MapViewActivity.this, PointMarkerActivity.class);
                startActivityForResult(i, ACTIVITY_MIDDLE_POINT_RESULT_CODE);
            }

        } else if (point3 == null) {
            point3 = p;
            //lastPointMarked = p;
            //circleManager.deleteAll();
            circleManager.create(new CircleOptions()
                    .withCircleColor("Yellow")
                    .withCircleRadius(7f)
                    .withGeometry(p)
            );
            binding.layoutBuildingDrawingTool.pointMarker.setVisibility(View.GONE);
        } else if (point4 == null) {
            point4 = p;
            //lastPointMarked = p;
            circleManager.deleteAll();
            circleManager.create(new CircleOptions()
                    .withCircleColor("Yellow")
                    .withCircleRadius(7f)
                    .withGeometry(p)
            );
            binding.layoutBuildingDrawingTool.pointMarker.setVisibility(View.VISIBLE);
            if (point3 != null) {
                Intent i = new Intent(MapViewActivity.this, PointMarkerActivity.class);
                startActivityForResult(i, ACTIVITY_MIDDLE_POINT_RESULT_CODE);
            }
        } else {
            circleManager.deleteAll();
            binding.layoutBuildingDrawingTool.pointMarker.setVisibility(View.GONE);
            drawAQLine(p);
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public void onListItemClicked(int adapterPosition, String name) {
        if (isBuildingSketchMode) {

        } else if (isDrawingMode) {
            Intent i = new Intent(MapViewActivity.this, CustomLabelAddActivity.class);
            i.putExtra(Constant.TYPE, 1);
            i.putExtra(Constant.MODE, 2);
            i.putExtra(Constant.DETAILS, name);
            i.putExtra(Constant.ID, adapterPosition);
            startActivityForResult(i, ACTIVITY_LABEL_RESULT_CODE);
        }
    }

    /**
     * Since we expect from the results of the offline merge callback to interact with the hosting activity,
     * we need to ensure that we are not interacting with a destroyed activity.
     */
    private class MergeCallback implements OfflineManager.MergeOfflineRegionsCallback {

        OfflineManager.MergeOfflineRegionsCallback activityCallback;

        MergeCallback(OfflineManager.MergeOfflineRegionsCallback callback) {
            activityCallback = callback;
        }


        @Override
        public void onMerge(OfflineRegion[] offlineRegions) {
            activityCallback.onMerge(offlineRegions);
        }

        @Override
        public void onError(String error) {
            activityCallback.onError(error);
        }

        public void onActivityDestroy() {
            activityCallback = null;
        }
    }

    private void copyAsset() {
        // copy db asset to internal memory
        new FileUtils.CopyFileFromAssetsTask(this, onFileCopiedListener).execute(TEST_DB_FILE_NAME, FileSource.getResourcesCachePath(this));
    }

    private void mergeDb() {
        OfflineManager.getInstance(this).mergeOfflineRegions(
                FileSource.getResourcesCachePath(this) + "/" + TEST_DB_FILE_NAME, mergeCallback
        );
    }

    private void cameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = ImageUtil.createImageFile(MapViewActivity.this, lotName);
                file = photoFile;
            } catch (IOException ex) {
                ex.printStackTrace();
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "io.xiges.ximapper.fileprovider",
                        photoFile);


                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAPTURE_IMAGE);
            }
        }
    }

    private void dimLayerStyle(String layerId) {
        mapboxMap.getStyle().getLayer(layerId).setProperties(fillOpacity(.3f),
                lineOpacity(.4f)
        );
    }

    private void brightLayerStyle(String layerId) {
        mapboxMap.getStyle().getLayer(layerId).setProperties(fillOpacity(.6f),
                lineOpacity(1f)
        );
    }

    private void loadDataLayer(LotMixinContent ltm) {

        Lot lt = new LotDataHandler().getLotData(ltm);
        this.lot = lt;

        circleLayerFeatureList = new ArrayList<>(lt.getCircleLayerFeatureList());
        fillLayerPointList = new ArrayList<>(lt.getFillLayerPointList());
        lineLayerPointList = new ArrayList<>(lt.getLineLayerPointList());


        if (circleSource != null) {
            circleSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList));
        }

        if (lineSource != null) {
            lineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                    {Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList))}));
        }

        listOfList = new ArrayList<>();
        listOfList.add(fillLayerPointList);
        List<Feature> finalFeatureList = new ArrayList<>();
        finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
        FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
        if (fillSource != null) {
            fillSource.setGeoJson(newFeatureCollection);
        }


        for (io.xiges.ximapper.model.Symbol s : lt.getSymbolList()) {
            Point point = Point.fromLngLat(s.getLatLng().getLongitude(), s.getLatLng().getLatitude());
            Symbol symbol = lotLabelManager.create(new SymbolOptions()
                    .withGeometry(point)
                    .withDraggable(false)
                    .withTextSize(11f)
                    .withTextHaloColor("#581845")
                    .withIconHaloWidth(1F)
                    .withTextColor("#581845")
                    .withTextField(s.getName())
            );
            symbolList.add(symbol);
        }


        for (io.xiges.ximapper.model.Symbol s : lt.getFloorLabels()) {
            Point point = Point.fromLngLat(s.getLatLng().getLongitude(), s.getLatLng().getLatitude());
            Symbol symbol = lotLabelManager.create(new SymbolOptions()
                    .withGeometry(point)
                    .withDraggable(false)
                    .withTextSize(11f)
                    .withTextHaloColor("#581845")
                    .withIconHaloWidth(1F)
                    .withTextColor("#581845")
                    .withTextField(s.getName())
            );
            symbolList.add(symbol);
        }


        for (io.xiges.ximapper.model.Symbol s : lt.getFloorLabels()) {
            Point point = Point.fromLngLat(s.getLatLng().getLongitude(), s.getLatLng().getLatitude());
            Symbol symbol = landSymbolManager.create(new SymbolOptions()
                    .withGeometry(point)
                    .withDraggable(false)
                    .withTextSize(16f)
                    .withTextColor("PURPLE")
                    .withTextField(s.getName())
            );

            landLabelSymbols.add(symbol);
            labelListAdapter.setLabel(landLabelSymbols);
        }


        isSaved = true;
        constructions = lt.getConstructions();

        for (Construction c : lt.getConstructions()) {
            if (c.getType().equals(Construction_Type.Other.name())) {
                Construction other_construction = c;
                if (other_construction.getDrawingType().equals(DrawingType.POLYGON.name())) {
                    fillLineSource = initDrawingFillSource(mapboxMap.getStyle(), other_construction.getFillSourceID());
                    initDrawingFillLayer(mapboxMap.getStyle(), other_construction.getFillLayerID(), other_construction.getFillSourceID());
                    lineLineSource = initDrawingLineSource(mapboxMap.getStyle(), other_construction.getLineSourceID());
                    initDrawingLineLayer(mapboxMap.getStyle(), other_construction.getLineLayerID(), other_construction.getLineSourceID(), other_construction.getFillLayerID());
                    circleLineSource = initDrawingCircleSource(mapboxMap.getStyle(), other_construction.getCircleSourceID());
                    initDrawingCircleLayer(mapboxMap.getStyle(), other_construction.getCircleLayerID(), other_construction.getCircleSourceID(), other_construction.getLineLayerID());

                    if (circleLineSource != null) {
                        circleLineSource.setGeoJson(FeatureCollection.fromFeatures(other_construction.getCircleLayerFeatureList()));
                    }

                    if (lineLineSource != null) {
                        lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                {Feature.fromGeometry(LineString.fromLngLats(other_construction.getLineLayerPointList()))}));
                    }

                    listOfList = new ArrayList<>();
                    listOfList.add(other_construction.getFillLayerPointList());
                    List<Feature> features = new ArrayList<>();
                    features.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                    FeatureCollection featureCollection = FeatureCollection.fromFeatures(features);
                    if (fillLineSource != null) {
                        fillLineSource.setGeoJson(featureCollection);
                    }

                } else if (other_construction.getDrawingType().equals(DrawingType.LINE.name())) {
                    lineLineSource = initDrawingLineSource(mapboxMap.getStyle(), other_construction.getLineSourceID());
                    initDrawingLineLayer(mapboxMap.getStyle(), other_construction.getLineLayerID(), other_construction.getLineSourceID(), "");
                    circleLineSource = initDrawingCircleSource(mapboxMap.getStyle(), other_construction.getCircleSourceID());
                    initDrawingCircleLayer(mapboxMap.getStyle(), other_construction.getCircleLayerID(), other_construction.getCircleSourceID(), other_construction.getLineLayerID());

                    if (circleLineSource != null) {
                        circleLineSource.setGeoJson(FeatureCollection.fromFeatures(other_construction.getCircleLayerFeatureList()));
                    }

                    if (lineLineSource != null) {
                        lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                {Feature.fromGeometry(LineString.fromLngLats(other_construction.getLineLayerPointList()))}));
                    }


                } else if (other_construction.getDrawingType().equals(DrawingType.CIRCLE.name())) {
                    fillLineSource = initDrawingFillSource(mapboxMap.getStyle(), other_construction.getFillSourceID());
                    initDrawingFillLayer(mapboxMap.getStyle(), other_construction.getFillLayerID(), other_construction.getFillSourceID());
                    circleLineSource = initDrawingCircleSource(mapboxMap.getStyle(), other_construction.getCircleSourceID());
                    initDrawingCircleLayer(mapboxMap.getStyle(), other_construction.getCircleLayerID(), other_construction.getCircleSourceID(), "");
                    if (fillLineSource != null) {
                        fillLineSource.setGeoJson(TurfTransformation.circle(
                                other_construction.getCircle().getMidPoint(), other_construction.getCircle().getRadius(), 64, other_construction.getCircle().getUnit()));
                    }

                    if (circleLineSource != null) {
                        circleLineSource.setGeoJson(FeatureCollection.fromFeatures(other_construction.getCircleLayerFeatureList()));
                    }


                }


            } else if (c.getType().equals(Construction_Type.Building.name())) {
                Construction building = c;
                if (building.getDrawingType().equals(DrawingType.POLYGON.name())) {
                    fillLineSource = initDrawingFillSource(mapboxMap.getStyle(), building.getFillSourceID());
                    initDrawingFillLayer(mapboxMap.getStyle(), building.getFillLayerID(), building.getFillSourceID());
                    lineLineSource = initDrawingLineSource(mapboxMap.getStyle(), building.getLineSourceID());
                    initDrawingLineLayer(mapboxMap.getStyle(), building.getLineLayerID(), building.getLineSourceID(), building.getFillLayerID());
                    circleLineSource = initDrawingCircleSource(mapboxMap.getStyle(), building.getCircleSourceID());
                    initDrawingCircleLayer(mapboxMap.getStyle(), building.getCircleLayerID(), building.getCircleSourceID(), building.getLineLayerID());

                    if (circleLineSource != null) {
                        circleLineSource.setGeoJson(FeatureCollection.fromFeatures(building.getCircleLayerFeatureList()));
                    }

                    if (lineLineSource != null) {
                        lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                {Feature.fromGeometry(LineString.fromLngLats(building.getLineLayerPointList()))}));
                    }

                    listOfList = new ArrayList<>();
                    listOfList.add(building.getFillLayerPointList());
                    List<Feature> features = new ArrayList<>();
                    features.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                    FeatureCollection featureCollection = FeatureCollection.fromFeatures(features);
                    if (fillLineSource != null) {
                        fillLineSource.setGeoJson(featureCollection);
                    }

                    building.toString();

                } else if (building.getDrawingType().equals(DrawingType.LINE.name())) {
                    lineLineSource = initDrawingLineSource(mapboxMap.getStyle(), building.getLineSourceID());
                    initDrawingLineLayer(mapboxMap.getStyle(), building.getLineLayerID(), building.getLineSourceID(), "");
                    circleLineSource = initDrawingCircleSource(mapboxMap.getStyle(), building.getCircleSourceID());
                    initDrawingCircleLayer(mapboxMap.getStyle(), building.getCircleLayerID(), building.getCircleSourceID(), building.getLineLayerID());

                    if (circleLineSource != null) {
                        circleLineSource.setGeoJson(FeatureCollection.fromFeatures(building.getCircleLayerFeatureList()));
                    }

                    if (lineLineSource != null) {
                        lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                {Feature.fromGeometry(LineString.fromLngLats(building.getLineLayerPointList()))}));
                    }


                } else if (building.getDrawingType().equals(DrawingType.CIRCLE.name())) {
                    fillLineSource = initDrawingFillSource(mapboxMap.getStyle(), building.getFillSourceID());
                    initDrawingFillLayer(mapboxMap.getStyle(), building.getFillLayerID(), building.getFillSourceID());
                    circleLineSource = initDrawingCircleSource(mapboxMap.getStyle(), building.getCircleSourceID());
                    initDrawingCircleLayer(mapboxMap.getStyle(), building.getCircleLayerID(), building.getCircleSourceID(), "");
                    if (fillLineSource != null) {
                        fillLineSource.setGeoJson(TurfTransformation.circle(
                                building.getCircle().getMidPoint(), building.getCircle().getRadius(), 64, building.getCircle().getUnit()));
                    }

                    if (circleLineSource != null) {
                        circleLineSource.setGeoJson(FeatureCollection.fromFeatures(building.getCircleLayerFeatureList()));
                    }
                }


                for (BuildingFloor buildingFloor : building.getBuildingFloorList()) {
                    List<FloorDrawing> drawing = new ArrayList<>(buildingFloor.getFloorDrawingList());
                    Collections.sort(drawing, (o1, o2) -> o2.getDrawingType().compareTo(o1.getDrawingType()));

                    for (FloorDrawing floorDrawing : drawing) {
                        if (floorDrawing.getDrawingType().equals(DrawingType.POLYGON.name()) || floorDrawing.getDrawingType().equals(DrawingType.BROOM_POLYGON.name())) {
                            circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), floorDrawing.getCircleSourceID());
                            lineLineSource = initBuildingLineSource(mapboxMap.getStyle(), floorDrawing.getLineSourceID());
                            fillLineSource = initBuildingFillSource(mapboxMap.getStyle(), floorDrawing.getFillSourceID());
                            initBuildingFillLayer(mapboxMap.getStyle(), floorDrawing.getFillLayerID(), floorDrawing.getFillSourceID(), FILL_LAYER_ID);
                            initBuildingLineLayer(mapboxMap.getStyle(), floorDrawing.getLineLayerID(), floorDrawing.getLineSourceID(), floorDrawing.getFillLayerID());
                            initBuildingCircleLayer(mapboxMap.getStyle(), floorDrawing.getCircleLayerID(), floorDrawing.getCircleSourceID(), floorDrawing.getLineLayerID());


                            listOfList = new ArrayList<>();
                            listOfList.add(floorDrawing.getFillLayerPointList());
                            List<Feature> features = new ArrayList<>();
                            features.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                            FeatureCollection featureCollection = FeatureCollection.fromFeatures(features);
                            if (fillLineSource != null) {
                                fillLineSource.setGeoJson(featureCollection);
                            }


                            if (lineLineSource != null) {
                                lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                        {Feature.fromGeometry(LineString.fromLngLats(floorDrawing.getLineLayerPointList()))}));
                            }

                            if (circleLineSource != null) {
                                circleLineSource.setGeoJson(FeatureCollection.fromFeatures(floorDrawing.getCircleLayerFeatureList()));
                            }


                        } else if (floorDrawing.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name())) {
                            circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), floorDrawing.getCircleSourceID());
                            lineLineSource = initBuildingLineSource(mapboxMap.getStyle(), floorDrawing.getLineSourceID());
                            fillLineSource = initBuildingFillSource(mapboxMap.getStyle(), floorDrawing.getFillSourceID());
                            initBuildingFloorFillLayer(mapboxMap.getStyle(), floorDrawing.getFillLayerID(), floorDrawing.getFillSourceID(), "");
                            initBuildingFloorLineLayer(mapboxMap.getStyle(), floorDrawing.getLineLayerID(), floorDrawing.getLineSourceID(), floorDrawing.getFillLayerID());
                            initBuildingFloorCircleLayer(mapboxMap.getStyle(), floorDrawing.getCircleLayerID(), floorDrawing.getCircleSourceID(), floorDrawing.getLineLayerID());


                            listOfList = new ArrayList<>();
                            listOfList.add(floorDrawing.getFillLayerPointList());
                            List<Feature> features = new ArrayList<>();
                            features.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                            FeatureCollection featureCollection = FeatureCollection.fromFeatures(features);
                            if (fillLineSource != null) {
                                fillLineSource.setGeoJson(featureCollection);
                            }


                            if (lineLineSource != null) {
                                lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                        {Feature.fromGeometry(LineString.fromLngLats(floorDrawing.getLineLayerPointList()))}));
                            }

                            if (circleLineSource != null) {
                                circleLineSource.setGeoJson(FeatureCollection.fromFeatures(floorDrawing.getCircleLayerFeatureList()));
                            }


                        } else if (floorDrawing.getDrawingType().equals(DrawingType.LINE.name())) {
                            circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), floorDrawing.getCircleSourceID());
                            lineLineSource = initBuildingLineSource(mapboxMap.getStyle(), floorDrawing.getLineSourceID());
                            initBuildingLineLayer(mapboxMap.getStyle(), floorDrawing.getLineLayerID(), floorDrawing.getLineSourceID(), "");
                            initBuildingCircleLayer(mapboxMap.getStyle(), floorDrawing.getCircleLayerID(), floorDrawing.getCircleSourceID(), floorDrawing.getLineLayerID());


                            if (circleLineSource != null) {
                                circleLineSource.setGeoJson(FeatureCollection.fromFeatures(floorDrawing.getCircleLayerFeatureList()));
                            }

                            if (lineLineSource != null) {
                                lineLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                        {Feature.fromGeometry(LineString.fromLngLats(floorDrawing.getLineLayerPointList()))}));
                            }


                        } else if (floorDrawing.getDrawingType().equals(DrawingType.CIRCLE.name())) {
                            circleLineSource = initBuildingCircleSource(mapboxMap.getStyle(), floorDrawing.getCircleSourceID());
                            initBuildingCircleLayer(mapboxMap.getStyle(), floorDrawing.getCircleLayerID(), floorDrawing.getCircleSourceID(), "");
                            fillLineSource = initBuildingFillSource(mapboxMap.getStyle(), floorDrawing.getFillSourceID());
                            initBuildingFillLayer(mapboxMap.getStyle(), floorDrawing.getFillLayerID(), floorDrawing.getFillSourceID(), FILL_LAYER_ID);


                            listOfList = new ArrayList<>();
                            listOfList.add(floorDrawing.getFillLayerPointList());
                            List<Feature> features = new ArrayList<>();
                            features.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                            FeatureCollection featureCollection = FeatureCollection.fromFeatures(features);
                            if (fillLineSource != null) {
                                fillLineSource.setGeoJson(featureCollection);
                            }


                            if (circleLineSource != null) {
                                circleLineSource.setGeoJson(FeatureCollection.fromFeatures(floorDrawing.getCircleLayerFeatureList()));
                            }
                        } else if (floorDrawing.getDrawingType().equals(DrawingType.AQLINE.name())) {
                            aqCircleSource = initAQBuildingCircleSource(mapboxMap.getStyle(), floorDrawing.getAqCircleSourceID());
                            aqLineSource = initAQBuildingLineSource(mapboxMap.getStyle(), floorDrawing.getAqLineSourceID());
                            aqFillSource = initAQBuildingFillSource(mapboxMap.getStyle(), floorDrawing.getAqFillSourceID());
                            initAQBuildingFillLayer(mapboxMap.getStyle(), floorDrawing.getAqFillLayerID(), floorDrawing.getAqFillSourceID());
                            initAQBuildingLineLayer(mapboxMap.getStyle(), floorDrawing.getAqLineLayerID(), floorDrawing.getAqLineSourceID(), floorDrawing.getAqFillLayerID());
                            initAQBuildingCircleLayer(mapboxMap.getStyle(), floorDrawing.getAqCircleLayerID(), floorDrawing.getAqCircleSourceID(), floorDrawing.getAqLineLayerID());


                            if (aqCircleSource != null) {
                                aqCircleSource.setGeoJson(FeatureCollection.fromFeatures(floorDrawing.getCircleLayerAQFeatureList()));
                            }


                            if (aqLineSource != null) {
                                aqLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                        {Feature.fromGeometry(LineString.fromLngLats(floorDrawing.getLineLayerAQPointList()))}));
                            }


                            listOfList = new ArrayList<>();
                            listOfList.add(floorDrawing.getFillLayerAQPointList());
                            List<Feature> arrayList = new ArrayList<>();
                            arrayList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
                            FeatureCollection collection = FeatureCollection.fromFeatures(arrayList);
                            if (aqFillSource != null) {
                                aqFillSource.setGeoJson(collection);
                            }


                        }

                        if (floorDrawing.getCircleLayerID() != null) {
                            if (mapboxMap.getStyle().getLayer(floorDrawing.getCircleLayerID()) != null) {
                                mapboxMap.getStyle().getLayer(floorDrawing.getCircleLayerID()).setProperties(visibility(NONE));
                            }
                        }
                        if (floorDrawing.getFillLayerID() != null) {
                            if (mapboxMap.getStyle().getLayer(floorDrawing.getFillLayerID()) != null) {
                                mapboxMap.getStyle().getLayer(floorDrawing.getFillLayerID()).setProperties(visibility(NONE));
                            }
                        }
                        if (floorDrawing.getLineLayerID() != null) {
                            if (mapboxMap.getStyle().getLayer(floorDrawing.getLineLayerID()) != null) {
                                mapboxMap.getStyle().getLayer(floorDrawing.getLineLayerID()).setProperties(visibility(NONE));
                            }
                        }


                        if (floorDrawing.getAqCircleLayerID() != null) {
                            if (mapboxMap.getStyle().getLayer(floorDrawing.getAqCircleLayerID()) != null) {
                                mapboxMap.getStyle().getLayer(floorDrawing.getAqCircleLayerID()).setProperties(visibility(NONE));
                            }
                        }
                        if (floorDrawing.getAqFillLayerID() != null) {
                            if (mapboxMap.getStyle().getLayer(floorDrawing.getAqFillLayerID()) != null) {
                                mapboxMap.getStyle().getLayer(floorDrawing.getAqFillLayerID()).setProperties(visibility(NONE));
                            }
                        }
                        if (floorDrawing.getAqLineLayerID() != null) {
                            if (mapboxMap.getStyle().getLayer(floorDrawing.getAqLineLayerID()) != null) {
                                mapboxMap.getStyle().getLayer(floorDrawing.getAqLineLayerID()).setProperties(visibility(NONE));
                            }
                        }


                    }


                }


            }

            for (io.xiges.ximapper.model.Symbol s : c.getBuildingLabels()) {
                Point point = Point.fromLngLat(s.getLatLng().getLongitude(), s.getLatLng().getLatitude());
                Symbol symbol = buildingDetailManager.create(new SymbolOptions()
                        .withGeometry(point)
                        .withDraggable(false)
                        .withTextSize(11f)
                        .withTextHaloColor("#581845")
                        .withIconHaloWidth(1F)
                        .withTextColor("#581845")
                        .withTextField(s.getName())
                );
                labelSymbolBuildingList.add(symbol);
            }
        }


    }

    private void hideShowLandComponents(boolean isHide) {
        if (isHide) {
            //  binding.imgBtnRedo.setVisibility(View.INVISIBLE);
            binding.imgBtnUndo.setVisibility(View.INVISIBLE);
        } else {
            binding.imgBtnRedo.setVisibility(View.VISIBLE);
            binding.imgBtnUndo.setVisibility(View.VISIBLE);
        }
    }

    private void hideShowLayers(boolean isHide) {
        if (isHide) {

            if (mapboxMap.getStyle().getLayer(LINE_LAYER_ID) != null) {
                mapboxMap.getStyle().getLayer(LINE_LAYER_ID).setProperties(visibility(NONE));
            }
            if (mapboxMap.getStyle().getLayer(CIRCLE_LAYER_ID) != null) {
                mapboxMap.getStyle().getLayer(CIRCLE_LAYER_ID).setProperties(visibility(NONE));
            }


            if (bundleInfo.isATFile()) {
                binding.swATMap.setChecked(false);
                mapboxMap.getStyle().getLayer(Constant.AT_LAYER).setProperties(visibility(NONE));

            }

            if (bundleInfo.isATFile()) {
                binding.swATDetail.setChecked(false);
                mapboxMap.getStyle().getLayer(Constant.AT_TEXT_LAYER).setProperties(visibility(NONE));

            }

            if (bundleInfo.isPPFile()) {
                binding.swPPMap.setChecked(false);
                mapboxMap.getStyle().getLayer(Constant.PP_LAYER).setProperties(visibility(NONE));

            }

            if (bundleInfo.isPPFile()) {
                binding.swPPDetail.setChecked(false);
                mapboxMap.getStyle().getLayer(Constant.PP_TEXT_LAYER).setProperties(visibility(NONE));
            }


            lotLabelManager.delete(symbolList);


        } else {

            if (mapboxMap.getStyle().getLayer(LINE_LAYER_ID) != null) {
                mapboxMap.getStyle().getLayer(LINE_LAYER_ID).setProperties(visibility(VISIBLE));
            }
            if (mapboxMap.getStyle().getLayer(CIRCLE_LAYER_ID) != null) {
                mapboxMap.getStyle().getLayer(CIRCLE_LAYER_ID).setProperties(visibility(VISIBLE));
            }


            if (bundleInfo.isATFile()) {
                binding.swATMap.setChecked(true);
                mapboxMap.getStyle().getLayer(Constant.AT_LAYER).setProperties(visibility(VISIBLE));

            }

            if (bundleInfo.isATFile()) {
                binding.swATDetail.setChecked(true);
                mapboxMap.getStyle().getLayer(Constant.AT_TEXT_LAYER).setProperties(visibility(VISIBLE));

            }

            if (bundleInfo.isPPFile()) {
                binding.swPPMap.setChecked(true);
                mapboxMap.getStyle().getLayer(Constant.PP_LAYER).setProperties(visibility(VISIBLE));

            }

            if (bundleInfo.isPPFile()) {
                binding.swPPDetail.setChecked(true);
                mapboxMap.getStyle().getLayer(Constant.PP_TEXT_LAYER).setProperties(visibility(VISIBLE));
            }

            lotLabelManager.update(symbolList);


        }
    }


    private void hideShowLotComponents(boolean isHide) {
        if (isHide) {
            binding.imgBtnBack.setVisibility(View.INVISIBLE);
            binding.imgBtnLandUndo.setVisibility(View.INVISIBLE);
        } else {
            binding.imgBtnBack.setVisibility(View.VISIBLE);
            binding.imgBtnLandUndo.setVisibility(View.VISIBLE);
        }
    }

    private void hideShowBuildingComponents(boolean isHide) {
       /* if (isHide){
            binding.imgBtnBack.setVisibility(View.INVISIBLE);
            binding.imgBtnLandUndo.setVisibility(View.INVISIBLE);
        }else {
            binding.imgBtnBack.setVisibility(View.VISIBLE);
            binding.imgBtnLandUndo.setVisibility(View.VISIBLE);
        } */
    }


    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MapViewActivity.this);
// Add the buttons
        builder.setPositiveButton(getResources().getString(R.string.ad_button_ok), (dialog, id) -> logOut());
        builder.setNegativeButton(getResources().getString(R.string.ad_button_cancel), (dialog, id) -> dialog.cancel());

        builder.setMessage(getResources().getString(R.string.ad_message_logout));
        builder.setTitle(getResources().getString(R.string.ad_title_logout));

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void logOut() {
        new SharedPreferencesHandler().logoutClearSharedPref(MapViewActivity.this);
        Intent i = new Intent(MapViewActivity.this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    private void initBuildingShadowLayer(List<FloorDrawing> buildingFloorList, String layer) {
        for (FloorDrawing floorDrawing : buildingFloorList) {
            if ((floorDrawing.getDrawingType().equals(DrawingType.POLYGON.name())) || (floorDrawing.getDrawingType().equals(DrawingType.ZFLOOR_POLYGON.name())) || (floorDrawing.getDrawingType().equals(DrawingType.BROOM_POLYGON.name()))) {

                if (mapboxMap.getStyle().getLayer(floorDrawing.getCircleLayerID() + Constant.SHADOW) != null) {
                    mapboxMap.getStyle().getLayer(floorDrawing.getCircleLayerID() + Constant.SHADOW).setProperties(visibility(VISIBLE));
                } else {
                    circleShadowSource = initBuildingBottomCircleSource(mapboxMap.getStyle(), floorDrawing.getCircleSourceID() + Constant.SHADOW);
                    initBuildingBottomCircleLayer(mapboxMap.getStyle(), floorDrawing.getCircleLayerID() + Constant.SHADOW, floorDrawing.getCircleSourceID() + Constant.SHADOW, layer);
                    if (circleShadowSource != null) {
                        circleShadowSource.setGeoJson(FeatureCollection.fromFeatures(floorDrawing.getCircleLayerFeatureList()));

                    }
                }


                if (mapboxMap.getStyle().getLayer(floorDrawing.getLineLayerID() + Constant.SHADOW) != null) {
                    mapboxMap.getStyle().getLayer(floorDrawing.getLineLayerID() + Constant.SHADOW).setProperties(visibility(VISIBLE));
                } else {
                    lineShadowSource = initBuildingBottomLineSource(mapboxMap.getStyle(), floorDrawing.getLineSourceID() + Constant.SHADOW);
                    initBuildingBottomLineLayer(mapboxMap.getStyle(), floorDrawing.getLineLayerID() + Constant.SHADOW, floorDrawing.getLineSourceID() + Constant.SHADOW, floorDrawing.getCircleLayerID() + Constant.SHADOW);
                    if (lineShadowSource != null) {
                        lineShadowSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                {Feature.fromGeometry(LineString.fromLngLats(floorDrawing.getLineLayerPointList()))}));
                    }

                }


            } else if (floorDrawing.getDrawingType().equals(DrawingType.LINE.name())) {

                if (mapboxMap.getStyle().getLayer(floorDrawing.getCircleLayerID() + Constant.SHADOW) != null) {
                    mapboxMap.getStyle().getLayer(floorDrawing.getCircleLayerID() + Constant.SHADOW).setProperties(visibility(VISIBLE));
                } else {
                    circleShadowSource = initBuildingBottomCircleSource(mapboxMap.getStyle(), floorDrawing.getCircleSourceID() + Constant.SHADOW);
                    initBuildingBottomCircleLayer(mapboxMap.getStyle(), floorDrawing.getCircleLayerID() + Constant.SHADOW, floorDrawing.getCircleSourceID() + Constant.SHADOW, layer);
                    if (circleShadowSource != null) {
                        circleShadowSource.setGeoJson(FeatureCollection.fromFeatures(floorDrawing.getCircleLayerFeatureList()));

                    }
                }


                if (mapboxMap.getStyle().getLayer(floorDrawing.getLineLayerID() + Constant.SHADOW) != null) {
                    mapboxMap.getStyle().getLayer(floorDrawing.getLineLayerID() + Constant.SHADOW).setProperties(visibility(VISIBLE));
                } else {
                    lineShadowSource = initBuildingBottomLineSource(mapboxMap.getStyle(), floorDrawing.getLineSourceID() + Constant.SHADOW);
                    initBuildingBottomLineLayer(mapboxMap.getStyle(), floorDrawing.getLineLayerID() + Constant.SHADOW, floorDrawing.getLineSourceID() + Constant.SHADOW, floorDrawing.getCircleLayerID() + Constant.SHADOW);
                    if (lineShadowSource != null) {
                        lineShadowSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                                {Feature.fromGeometry(LineString.fromLngLats(floorDrawing.getLineLayerPointList()))}));
                    }

                }

            }

        }
    }

    private void saveCRData(LotMixinContent lotMixinContent, CR conditionalReport, List<Marker> marker) {

        Output output = new Output();
        output.setConditional_report(conditionalReport);
        output.setMarkers(marker);
        output.setCircleLayerFeatureList(lotMixinContent.getCircleLayerFeatureList());
        output.setConstructions(lotMixinContent.getConstructions());
        output.setFillLayerPointList(lotMixinContent.getFillLayerPointList());
        output.setFloorLabels(lotMixinContent.getFloorLabels());
        output.setLotId(lotMixinContent.getLotId());
        output.setLotName(lotMixinContent.getLotName());
        output.setFloorLabels(lotMixinContent.getFloorLabels());
        output.setFillLayerPointList(lotMixinContent.getFillLayerPointList());
        output.setLineLayerPointList(lotMixinContent.getLineLayerPointList());
        output.setSymbolList(lotMixinContent.getSymbolList());

        DataMasterFileDetails mf = new DataMasterFileDetails();
        mf.setMasterFileId(masterFile.getMasterFileId() + "");

        DataSave dataSave = new DataSave();
        dataSave.setBuilding(output);
        dataSave.setMasterFileDetails(mf);


        try {
            File folder = new File(Environment.getExternalStorageDirectory() +
                    File.separator + "Test" + File.separator + "Land" + File.separator);

            if (!folder.exists()) {
                folder.mkdirs();
            }

            Date date = new Date();
            long time = date.getTime();

            Writer writer = new FileWriter(Environment.getExternalStorageDirectory() +
                    File.separator + "Test" + File.separator + "Land" + File.separator + "sample.json");
            Gson gson = new GsonBuilder().create();
            gson.toJson(dataSave, writer);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (new NetworkStatus().isConnected(MapViewActivity.this)) {

            apiInterface.saveCRData("Bearer " + prefs.getString(Constant.ACCESS_TOKEN, ""), dataSave).enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {

                    if (response.isSuccessful()) {
                        showErrorMessage("Saved Successfully");

                    } else if (response.code() == 401) {
                        new RefreshToken().refreshAccessToken(MapViewActivity.this, new OnAPICallback() {
                            @Override
                            public void onSuccess() {
                                saveCRData(lotMixinContent, conditionalReport, marker);
                            }

                            @Override
                            public void onFailed(String message) {
                                showErrorMessage(message);
                            }
                        });
                    } else {

                        ErrorBody errorBody = ErrorUtils.parseAPIError(response);
                        showErrorMessage(errorBody.getErrorMsg());
                    }

                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    showErrorMessage("Failed  when sending CR data");

                }
            });


        } else {
            showErrorMessage("No internet connection");
        }

    }

    private void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

  /*  Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {
            saveToFile();
            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }
    };*/

  /*  void startRepeatingTask() {
        mHandlerTask.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mHandlerTask);
    }
*/

    private void saveToFile() {
        try {
            saveConstruction();
            dataManager.writeToFile(this.lot, masterFile.getMasterFileName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveConstruction() {

        if (!isBuildingSketchMode) {
            if (constructions.size() > 0) {
                if (!currentStructure.equals("")) {
                    Construction construction = new Construction();
                    int index = 0;
                    for (int i = 0; i < constructions.size(); i++) {
                        try {
                            if (constructions.get(i).getId().equals(currentStructure)) {
                                index = i;
                                construction = constructions.get(i);
                                break;
                            }
                        } catch (Exception e) {
                        }
                    }

                    if (isLineDraw) {
                        construction.setDrawingType(DrawingType.LINE.name());
                        construction.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                        construction.setLineLayerPointList(new ArrayList<>(lineLayerDrawingPointList));
                        construction.setLineLayerID("LayerLine" + constructions.size());
                        construction.setCircleLayerID("LayerCircle" + constructions.size());
                        construction.setLineSourceID("Line" + constructions.size());
                        construction.setCircleSourceID("Circle" + constructions.size());
                        binding.layoutDrawingTool.imgBtnDrawLine.setEnabled(true);

                    } else if (isCircleDraw) {
                        construction.setDrawingType(DrawingType.CIRCLE.name());
                        construction.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                        Circle circle = new Circle();
                        circle.setUnit("meters");
                        circle.setRadius(radius);
                        circle.setCircleFillPoints(new ArrayList<>(circlePointList));
                        circle.setMidPoint(firstPointOfPolygon);
                        construction.setCircle(circle);
                        construction.setFillLayerID("LayerFill" + constructions.size());
                        construction.setCircleLayerID("LayerCircle" + constructions.size());
                        construction.setFillSourceID("Fill" + constructions.size());
                        construction.setCircleSourceID("Circle" + constructions.size());
                        binding.layoutDrawingTool.imgBtnDrawCircle.setEnabled(true);

                    } else if (isPolygonDraw) {
                        construction.setDrawingType(DrawingType.POLYGON.name());
                        construction.setCircleLayerFeatureList(new ArrayList<>(circleLayerDrawingFeatureList));
                        construction.setLineLayerPointList(new ArrayList<>(lineLayerDrawingPointList));
                        construction.setFillLayerPointList(new ArrayList<>(fillLayerDrawingPointList));
                        construction.setLineLayerID("LayerLine" + constructions.size());
                        construction.setCircleLayerID("LayerCircle" + constructions.size());
                        construction.setFillLayerID("LayerFill" + constructions.size());
                        construction.setFillSourceID("Fill" + constructions.size());
                        construction.setCircleSourceID("Circle" + constructions.size());
                        construction.setLineSourceID("Line" + constructions.size());
                        binding.layoutDrawingTool.imgBtnDrawLine.setEnabled(true);
                    }

                    List<io.xiges.ximapper.model.Symbol> consSymbol = new ArrayList<>();
                    for (Symbol symbol : labelSymbolBuildingList) {
                        io.xiges.ximapper.model.Symbol s = new io.xiges.ximapper.model.Symbol();
                        s.setLatLng(symbol.getLatLng());
                        s.setName(symbol.getTextField());
                        consSymbol.add(s);
                    }
                    construction.setBuildingLabels(consSymbol);

                    constructions.set(index, construction);


                    List<io.xiges.ximapper.model.Symbol> symbols = new ArrayList<>();
                    for (Symbol symbol : symbolList) {
                        io.xiges.ximapper.model.Symbol s = new io.xiges.ximapper.model.Symbol();
                        s.setLatLng(symbol.getLatLng());
                        s.setName(symbol.getTextField());
                        symbols.add(s);
                    }
                    this.lot.setSymbolList(symbols);
                    this.lot.setConstructions(constructions);
                    List<io.xiges.ximapper.model.Symbol> landSymbols = new ArrayList<>();
                    for (Symbol s : landLabelSymbols) {
                        io.xiges.ximapper.model.Symbol ss = new io.xiges.ximapper.model.Symbol();
                        ss.setName(s.getTextField());
                        ss.setLatLng(s.getLatLng());
                        landSymbols.add(ss);
                    }
                    this.lot.setFloorLabels(landSymbols);
                }
            }
        }
    }


    private void setIsEnabledBuildingDraw(boolean b) {
        if (b) {
            binding.layoutDrawingTool.imgBtnDrawCircle.setEnabled(true);
            binding.layoutDrawingTool.imgBtnDrawLine.setEnabled(true);
            binding.layoutDrawingTool.imgCamera.setEnabled(true);
            binding.layoutDrawingTool.imgBtnDrawPolygon.setEnabled(true);
            binding.layoutDrawingTool.imgGallery.setEnabled(true);
        } else {
            binding.layoutDrawingTool.imgBtnDrawCircle.setEnabled(false);
            binding.layoutDrawingTool.imgBtnDrawLine.setEnabled(false);
            binding.layoutDrawingTool.imgCamera.setEnabled(false);
            binding.layoutDrawingTool.imgBtnDrawPolygon.setEnabled(false);
            binding.layoutDrawingTool.imgGallery.setEnabled(false);

        }
    }

    public void removeBuilding(Building b) {
        for (Construction construction : constructions) {
            if (construction.getId().equals(b.getNo())) {
                if (mapboxMap.getStyle().getLayer(construction.getCircleLayerID()) != null) {
                    mapboxMap.getStyle().removeLayer(construction.getCircleLayerID());
                }
                if (mapboxMap.getStyle().getLayer(construction.getLineLayerID()) != null) {
                    mapboxMap.getStyle().removeLayer(construction.getLineLayerID());
                }
                if (mapboxMap.getStyle().getLayer(construction.getFillLayerID()) != null) {
                    mapboxMap.getStyle().removeLayer(construction.getFillLayerID());
                }

                if (mapboxMap.getStyle().getSource(construction.getCircleSourceID()) != null) {
                    mapboxMap.getStyle().removeSource(construction.getCircleSourceID());
                }


                if (mapboxMap.getStyle().getSource(construction.getLineSourceID()) != null) {
                    mapboxMap.getStyle().removeSource(construction.getLineSourceID());
                }

                if (mapboxMap.getStyle().getSource(construction.getFillSourceID()) != null) {
                    mapboxMap.getStyle().removeSource(construction.getFillSourceID());
                }


                for (BuildingFloor bf : construction.getBuildingFloorList()) {
                    for (FloorDrawing fd : bf.getFloorDrawingList()) {
                        if (mapboxMap.getStyle().getLayer(fd.getCircleLayerID()) != null) {
                            mapboxMap.getStyle().removeLayer(fd.getCircleLayerID());
                        }
                        if (mapboxMap.getStyle().getLayer(fd.getLineLayerID()) != null) {
                            mapboxMap.getStyle().removeLayer(fd.getLineLayerID());
                        }
                        if (mapboxMap.getStyle().getLayer(fd.getFillLayerID()) != null) {
                            mapboxMap.getStyle().removeLayer(fd.getFillLayerID());
                        }

                        if (mapboxMap.getStyle().getSource(fd.getCircleSourceID()) != null) {
                            mapboxMap.getStyle().removeSource(fd.getCircleSourceID());
                        }


                        if (mapboxMap.getStyle().getSource(fd.getLineSourceID()) != null) {
                            mapboxMap.getStyle().removeSource(fd.getLineSourceID());
                        }

                        if (mapboxMap.getStyle().getSource(fd.getFillSourceID()) != null) {
                            mapboxMap.getStyle().removeSource(fd.getFillSourceID());
                        }

                        if (mapboxMap.getStyle().getLayer(fd.getAqCircleLayerID()) != null) {
                            mapboxMap.getStyle().removeLayer(fd.getAqCircleLayerID());
                        }
                        if (mapboxMap.getStyle().getLayer(fd.getAqLineLayerID()) != null) {
                            mapboxMap.getStyle().removeLayer(fd.getAqLineLayerID());
                        }
                        if (mapboxMap.getStyle().getLayer(fd.getAqFillLayerID()) != null) {
                            mapboxMap.getStyle().removeLayer(fd.getAqFillLayerID());
                        }

                        if (mapboxMap.getStyle().getSource(fd.getAqCircleSourceID()) != null) {
                            mapboxMap.getStyle().removeSource(fd.getAqCircleSourceID());
                        }


                        if (mapboxMap.getStyle().getSource(fd.getAqLineSourceID()) != null) {
                            mapboxMap.getStyle().removeSource(fd.getAqLineSourceID());
                        }

                        if (mapboxMap.getStyle().getSource(fd.getAqFillSourceID()) != null) {
                            mapboxMap.getStyle().removeSource(fd.getAqFillSourceID());
                        }

                    }
                }
                constructions.remove(construction);
            }
        }
    }

    private void saveBuildingDataToCR(Building b) {

        CR conditionalReport = new CR();
        DataManager dataManager = new DataManager();
        try {
            conditionalReport = dataManager.getCRData(masterFile.getMasterFileRefNumber().replaceAll("/", ""), lot.getLotId());
        } catch (IOException e) {
            e.printStackTrace();
        }

        BuildingDetails buildingDetails = conditionalReport.getBuildingDetails();
        List<Building> buildings = new ArrayList<>(buildingDetails.getBuildings());
        int i = 0;
        for (Building building : buildings) {
            if (building.getNo().equals(b.getNo())) {
                building.setFloors(b.getFloors());
                building.setMinusFloors(b.getMinusFloors());
                building.setTotalFloorArea(b.getTotalFloorArea());
                buildings.set(i, building);
                break;
            }
            i++;
        }

        buildingDetails.setBuildings(new ArrayList<>(buildings));
        conditionalReport.setBuildingDetails(buildingDetails);
        try {
            dataManager.saveCRData(conditionalReport, masterFile.getMasterFileRefNumber().replaceAll("/", ""), lot.getLotId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
