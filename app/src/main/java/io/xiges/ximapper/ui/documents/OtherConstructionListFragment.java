package io.xiges.ximapper.ui.documents;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.io.IOException;
import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.adapter.OtherConstructionDetailAdapter;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.FragmentOtherConstructionListBinding;
import io.xiges.ximapper.model.cr.CR;
import io.xiges.ximapper.model.cr.OtherConstruction;

public class OtherConstructionListFragment extends Fragment implements RecyclerViewClickListener{
    private FragmentOtherConstructionListBinding binding;
    private final int REQUEST_CODE = 324;
    private final int RESULT_CODE = 747;
    private int position = -1;
    private Context c =null;
    private OtherConstructionDetailAdapter ocAdapter;

    public OtherConstructionListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_other_construction_list, container, false);
        View view = binding.getRoot();

        return view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        binding.btnAddOtherConstructionDetails.setOnClickListener(v->{
            Intent i = new Intent(getActivity(),OtherConstructionCRDetailActivity.class);
            i.putExtra(Constant.MODE,true);
            startActivityForResult(i,REQUEST_CODE);
        });

        ocAdapter = new OtherConstructionDetailAdapter(((CRDocumentActivity)getActivity()).getConditionalReport().getOtherConstructions(),c,this);

        binding.rcOtherConstructionDetails.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rcOtherConstructionDetails.setAdapter(ocAdapter);

        CR cr =((CRDocumentActivity)getActivity()).getConditionalReport();
        ocAdapter.setDetails(cr.getOtherConstructions());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(binding.rcOtherConstructionDetails.getContext(),
                LinearLayout.VERTICAL);
        binding.rcOtherConstructionDetails.addItemDecoration(dividerItemDecoration);


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels / 6;

        /*SwipeHelper swipeHelper = new SwipeHelper(getActivity(), binding.rcOtherConstructionDetails, width) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new SwipeHelper.UnderlayButton(getActivity(),
                        getResources().getString(R.string.tv_label_delete),
                        0,
                        ContextCompat.getColor(getActivity(), R.color.colorRed),
                        pos -> {
                            position = pos;
                            Intent i = new Intent(getActivity(), ConfirmationDialogActivity.class);
                            i.putExtra(Constant.NAME, "Are you sure you want to delete this item?");
                            startActivityForResult(i, RESULT_CODE);
                        }
                ));
            }
        };

        swipeHelper.attachSwipe();*/

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                boolean isUpdate = data.getBooleanExtra(Constant.TYPE, false);
                OtherConstruction crOtherConstruction = data.getParcelableExtra(Constant.DETAILS);
                if (!isUpdate) {
                    try {
                        ((CRDocumentActivity) getActivity()).saveOtherConinfo(crOtherConstruction);
                        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        ocAdapter.setDetails(cr.getOtherConstructions());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        CR cr =((CRDocumentActivity)getActivity()).getConditionalReport();
                        List<OtherConstruction> details = cr.getOtherConstructions();
                        details.set(position,crOtherConstruction);
                        ((CRDocumentActivity)getActivity()).updateOtherConinfo(details);
                        cr =((CRDocumentActivity)getActivity()).getConditionalReport();
                        ocAdapter.setDetails(cr.getOtherConstructions());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }else if (requestCode == RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
               try {
                    CR cr =((CRDocumentActivity)getActivity()).getConditionalReport();
                    List<OtherConstruction> details = cr.getOtherConstructions();
                    details.remove(position);
                    ((CRDocumentActivity)getActivity()).updateOtherConinfo(details);
                    cr =((CRDocumentActivity)getActivity()).getConditionalReport();
                    ocAdapter.setDetails(cr.getOtherConstructions());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onItemClick(int position) {
        Intent i = new Intent(getActivity(),OtherConstructionCRDetailActivity.class);
        i.putExtra(Constant.MODE,false);
        this.position= position;
        i.putExtra(Constant.DETAILS,((CRDocumentActivity)getActivity()).getConditionalReport().getOtherConstructions().get(position));
        startActivityForResult(i,REQUEST_CODE);
    }

    @Override
    public void onLongItemClick(int position) {

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        c = context;

    }
}
