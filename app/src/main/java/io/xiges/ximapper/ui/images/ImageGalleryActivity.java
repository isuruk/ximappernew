package io.xiges.ximapper.ui.images;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.xiges.ximapper.R;
import io.xiges.ximapper.adapter.ImageGalleryViewAdapter;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.databinding.ActivityImageGallartBinding;
import io.xiges.ximapper.model.OnAPICallback;
import io.xiges.ximapper.model.UploadImageSave;
import io.xiges.ximapper.model.api.ImageUploadResponse;
import io.xiges.ximapper.network.ApiInterface;
import io.xiges.ximapper.ui.alertboxes.ConfirmationDialogActivity;
import io.xiges.ximapper.util.NetworkStatus;
import io.xiges.ximapper.util.RefreshToken;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImageGalleryActivity extends AppCompatActivity {

    String lotName = "Test";
    ArrayList<String> files;
    private ImageGalleryViewAdapter adapter;
    private ActivityImageGallartBinding binding;
    private final int RESULT_CODE = 834;
    String image = "";
    int position = -1;
    int mfid = 0;
    String lotid = "";

    @Inject
    ApiInterface apiInterface;


    @Inject
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_image_gallart);

        ((XimapperApplication) getApplication()).getAppComponent().inject(this);


        mfid = getIntent().getIntExtra(Constant.MASTER_FILE,-1);
        lotid = getIntent().getStringExtra(Constant.LOT);

        files = new ArrayList<>();

        binding.rcImageGallary.setLayoutManager(new GridLayoutManager(this, calculateNoOfColumns(300)));

        adapter = new ImageGalleryViewAdapter(files, ImageGalleryActivity.this);
        binding.rcImageGallary.setAdapter(adapter);


        File file = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES + "" + File.separator + "" + lotName);
        if (file != null) {
            for (File f : file.listFiles()) {
                if (f.isFile()) {
                    String name = f.getName();
                    String path = f.getPath();
                    files.add(path);
                }
            }
        }

        adapter.setImages(files);


        binding.imgUpload.setOnClickListener(v -> {
            fileUpload();
        });
    }

    private int calculateNoOfColumns(float columnWidthDp) {
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (screenWidthDp / columnWidthDp + 0.5); // +0.5 for correct rounding to int.
        return noOfColumns;
    }

    public void onImageSelected(String s, int adapterPosition) {
        Intent fullImageIntent = new Intent(ImageGalleryActivity.this, ImageViewActivity.class);
        fullImageIntent.putExtra(Constant.IMAGE, s);
        startActivity(fullImageIntent);
    }

    public boolean onImageLongClicked(String s, int adapterPosition) {
        image = s;
        position = adapterPosition;
        Intent i = new Intent(ImageGalleryActivity.this, ConfirmationDialogActivity.class);
        i.putExtra(Constant.NAME, "Are you sure you want to delete this image?");
        startActivityForResult(i, RESULT_CODE);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                File file = new File(image);
                boolean deleted = file.delete();
                if (deleted) {
                    files.remove(position);
                    adapter.setImages(files);
                }
            }
        }
    }


    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, File f) {
        RequestBody requestFile =
                RequestBody.create(
                        f,
                        MediaType.parse("image/*")
                );

        return MultipartBody.Part.createFormData(partName, f.getName(), requestFile);
    }

    private void fileUpload() {
        List<MultipartBody.Part> parts = new ArrayList<>();


        File file = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES + "" + File.separator + "" + lotName);
        if (file != null) {
            for (File f : file.listFiles()) {
                if (f.isFile()) {
                    parts.add(prepareFilePart("photo", f));
                }
            }
        }

        uploadImages(parts);
    }

    private void uploadImages(List<MultipartBody.Part> parts) {
        if (new NetworkStatus().isConnected(ImageGalleryActivity.this)) {

            apiInterface.uploadImages("Bearer " + prefs.getString(Constant.ACCESS_TOKEN, ""), mfid+"", lotid, parts).enqueue(new Callback<ImageUploadResponse>() {

                @Override
                public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                    if (response.isSuccessful()) {
                        UploadImageSave uploadImageSave = new UploadImageSave();
                        uploadImageSave.setFiles(response.body().getFiles());
                        uploadImageSave.setLotNo(Integer.parseInt(lotid));
                        uploadImageSave.setMasterFile(mfid);
                        saveUpload(uploadImageSave);
                    } else if (response.code() == 401) {
                        new RefreshToken().refreshAccessToken(ImageGalleryActivity.this, new OnAPICallback() {
                            @Override
                            public void onSuccess() {
                                //recall method
                                uploadImages(parts);
                            }

                            @Override
                            public void onFailed(String message) {
                                showErrorMessage(message);
                            }
                        });
                    }

                }

                @Override
                public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                    showErrorMessage("Failed when uploading images");

                }
            });


        } else {
            showErrorMessage("No internet connection");
        }
    }

    private void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public SharedPreferences getPrefs() {
        return prefs;
    }


    private void saveUpload(UploadImageSave uploadImageSave) {
        if (new NetworkStatus().isConnected(ImageGalleryActivity.this)) {

            apiInterface.uploadImagesSave("Bearer " + prefs.getString(Constant.ACCESS_TOKEN, ""), uploadImageSave).enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    if (response.isSuccessful()) {
                        showErrorMessage("Success");
                    } else if (response.code() == 401) {
                        new RefreshToken().refreshAccessToken(ImageGalleryActivity.this, new OnAPICallback() {
                            @Override
                            public void onSuccess() {
                                //recall method
                                saveUpload(uploadImageSave);
                            }

                            @Override
                            public void onFailed(String message) {
                                showErrorMessage(message);
                            }
                        });
                    }

                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    showErrorMessage("Failed when saving images");

                }
            });


        } else {
            showErrorMessage("No internet connection");
        }
    }
}
