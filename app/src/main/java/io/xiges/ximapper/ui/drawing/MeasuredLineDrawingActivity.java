package io.xiges.ximapper.ui.drawing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityMessueredLineDrawingBinding;

public class MeasuredLineDrawingActivity extends AppCompatActivity {

    private ActivityMessueredLineDrawingBinding binding;

    boolean up,left,right,down;
    
    @Override
    protected void onCreate(Bundle savedInleftanceleftate) {
        super.onCreate(savedInleftanceleftate);        
        binding = DataBindingUtil.setContentView(this, R.layout.activity_messuered_line_drawing);


        changeToDefault();


        binding.imgUpArrow.setOnClickListener(v->{
            up = true;
            left = false;
            right= false;
            down = false;
            binding.imgUpArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgLeftArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgRightArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgDownArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        });
        binding.imgLeftArrow.setOnClickListener(v->{
            up = false;
            left = true;
            right= false;
            down = false;
            binding.imgUpArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgLeftArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgRightArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgDownArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        });
        binding.imgRightArrow.setOnClickListener(v->{
            up = false;
            left = false;
            right= true;
            down = false;
            binding.imgUpArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgLeftArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgRightArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgDownArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        });
        binding.imgDownArrow.setOnClickListener(v->{
            up = false;
            left = false;
            right= false;
            down = true;
            binding.imgUpArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgLeftArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgRightArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgDownArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        });


        binding.btnOKLine.setOnClickListener(v->{
            String ft = binding.edtFeetLine.getText().toString();
            String in = binding.edtInchLine.getText().toString();

            boolean a= false,b = false;


            if(ft==null || ft.isEmpty()){
                ft = "0";
            }

            if(in==null || in.isEmpty()){
                in = "0";
            }

            double feet = Double.valueOf(ft);
            double inch = Double.valueOf(in);

            if(inch<12){
                a = true;
            }
            double totInch =  (feet * 12) + inch;
            if(a & (up | left | right | down)) {

                double brng = 0;

                if (up){
                    brng = 0;
                }else if(left){
                    brng = 270;
                }else if(right){
                    brng = 90;
                }else if(down){
                    brng = 180;
                }



                Intent returnIntent = new Intent();
                returnIntent.putExtra(Constant.DISTANCE, totInch);
                returnIntent.putExtra(Constant.BEARING, brng);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }else {
                Toast.makeText(this,"Invalid Values", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void changeToDefault(){
        binding.imgUpArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.imgLeftArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.imgRightArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.imgDownArrow.setColorFilter(ContextCompat.getColor(MeasuredLineDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
    }
}
