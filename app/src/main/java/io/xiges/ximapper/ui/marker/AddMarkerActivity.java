package io.xiges.ximapper.ui.marker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityAddMarkerBinding;
import io.xiges.ximapper.model.Marker;
import io.xiges.ximapper.model.Marker_Type;

public class AddMarkerActivity extends AppCompatActivity {
    private ActivityAddMarkerBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_marker);

        Marker m = getIntent().getParcelableExtra(Constant.MARKER);
        int mode = getIntent().getIntExtra(Constant.MODE,-1);

        if(mode==1){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
         //   binding.tvMarkerName.setEnabled(false);
            binding.cmbLS.setEnabled(false);
            binding.cmbPV.setEnabled(false);
            binding.cmbRE.setEnabled(false);
            binding.cmbBR.setEnabled(false);
            // binding.tvMarkerName.setText(m.getName());
                if(m.getType().equals(Marker_Type.LS.name())){
                    binding.cmbLS.setChecked(true);
                }else if(m.getType().equals(Marker_Type.PV.name())){
                    binding.cmbPV.setChecked(true);
                }else if(m.getType().equals(Marker_Type.RE.name())){
                    binding.cmbRE.setChecked(true);
                }else if(m.getType().equals(Marker_Type.BR.name())) {
                    binding.cmbBR.setChecked(true);
                }


                }else if(mode == 2){

        }else if (mode == 3){
            //binding.tvMarkerName.setText(m.getName());
                if(m.getType().equals(Marker_Type.LS.name())){
                    binding.cmbLS.setChecked(true);
                }else if(m.getType().equals(Marker_Type.PV.name())){
                    binding.cmbPV.setChecked(true);
                }else if(m.getType().equals(Marker_Type.RE.name())){
                    binding.cmbRE.setChecked(true);
                }
        }




        binding.btnOK.setOnClickListener(v -> {

            if(mode != 1) {
             //   String name = binding.tvMarkerName.getText().toString();
                boolean ls = binding.cmbLS.isChecked();
                boolean re = binding.cmbRE.isChecked();
                boolean pv = binding.cmbPV.isChecked();
                boolean br = binding.cmbBR.isChecked();

                if (ls || re || pv || br) {
                  //  m.setName(name);
                    //List<String> marker_types = new ArrayList<>();

                    if (ls) {
                        m.setType(Marker_Type.LS.name());
                    }
                    if (re) {
                        m.setType(Marker_Type.RE.name());
                    }
                    if (pv) {
                        m.setType(Marker_Type.PV.name());
                    }
                    if (br) {
                        m.setType(Marker_Type.BR.name());
                    }

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(Constant.MARKER, m);
                    returnIntent.putExtra(Constant.MODE,mode);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Toast.makeText(AddMarkerActivity.this, "All fields required .", Toast.LENGTH_LONG).show();
                }
            }else {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }

        });
    }

}
