package io.xiges.ximapper.ui.documents;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.databinding.ActivityOwnerDetailCrBinding;
import io.xiges.ximapper.model.api.master_data.MasterData;
import io.xiges.ximapper.model.cr.OwnerDetail;

public class OwnerDetailCRActivity extends AppCompatActivity {
    private ActivityOwnerDetailCrBinding binding;
    private List<String> typeList;
    private int type = -1;
    HashMap<Integer, Integer> occuperTypeMap;
    @Inject
    SharedPreferences prefs;
    private boolean isUpdate = false;
    int year;
    int month;
    int day;
    private List<String> buildingList;
    String buildingNo = "";

    private DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_owner_detail_cr);
        ((XimapperApplication) getApplication()).getAppComponent().inject(this);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);
        int screenHeight = (int) (metrics.heightPixels * 0.80);

        typeList = new ArrayList<>();

        getWindow().setLayout(screenWidth, screenHeight);

        Gson gson = new Gson();
        String json = prefs.getString(Constant.MASTER_DATA, "");
        MasterData masterData = gson.fromJson(json, MasterData.class);


        OwnerDetail ownerDetails = getIntent().getParcelableExtra(Constant.DETAILS);
        boolean mode = getIntent().getBooleanExtra(Constant.MODE, false);

        String noArray[] = getIntent().getStringArrayExtra(Constant.ID);

        occuperTypeMap = new HashMap<>();
        for (int i = 0; i < masterData.getBuilding().getOwnersAndOccupierDetails().getOccupierType().size(); i++) {
            occuperTypeMap.put(i, masterData.getBuilding().getOwnersAndOccupierDetails().getOccupierType().get(i).getId());
            typeList.add(masterData.getBuilding().getOwnersAndOccupierDetails().getOccupierType().get(i).getDescription());
        }


        binding.edtOccuperType.setItem(typeList);


        binding.swSignmode.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked){
                binding.scroll.setEnableScrolling(false);
            }else{
                binding.scroll.setEnableScrolling(true);
            }
        });


        buildingList = Arrays.asList(noArray);

        binding.edtBuildingNo.setItem(buildingList);

        changeMode(mode);

        if (ownerDetails != null) {
            binding.setData(ownerDetails);

            if (ownerDetails.getOccupierType() > 0) {
                if (ownerDetails.getOccupierType() != -1) {
                    int i = 0;
                    for (Map.Entry<Integer, Integer> entry : occuperTypeMap.entrySet()) {
                        if (entry.getValue() == ownerDetails.getOccupierType()) {
                            binding.edtOccuperType.setSelection(i);

                        }
                        i++;
                    }
                }
            }

            if (buildingNo.equals("") || buildingNo.isEmpty()) {
                binding.edtBuildingNo.setSelection(buildingList.indexOf(ownerDetails.getBuildingNo()));
            }
            byte[] decodedString = Base64.decode(ownerDetails.getSign(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            binding.imgSign.setImageBitmap(decodedByte);
        }


        initSpinner();

        binding.btnEditClear.setOnClickListener(v -> {
            binding.signPad.clear();
            binding.imgSign.setVisibility(View.INVISIBLE);
            binding.signPad.setVisibility(View.VISIBLE);

        });

        binding.editDate.setOnClickListener(view -> {
            hideKeyboard(this, view);
            createDialog().show();
        });


        if (TextUtils.isEmpty(binding.editDate.getText().toString()) || binding.editDate.getText().toString().equals("")) {
            Calendar cal = Calendar.getInstance();
            year = cal.get(Calendar.YEAR);
            month = cal.get(Calendar.MONTH);
            day = cal.get(Calendar.DAY_OF_MONTH);
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sf.format(cal.getTime());
            binding.editDate.setText((date));
        } else {
            String string = binding.editDate.getText().toString();
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = sf.parse(string);
                cal.setTime(date);
                year = cal.get(Calendar.YEAR);
                month = cal.get(Calendar.MONTH);
                day = cal.get(Calendar.DAY_OF_MONTH);
            } catch (ParseException e) {
                year = cal.get(Calendar.YEAR);
                month = cal.get(Calendar.MONTH);
                day = cal.get(Calendar.DAY_OF_MONTH);
                String date = sf.format(cal.getTime());
                binding.editDate.setText(date);
            }

        }

        datePickerDialog = new DatePickerDialog(this, datePickerListener, year, month, day);


        binding.btnUpdate.setOnClickListener(v -> {
            isUpdate = true;
            changeMode(true);
        });

        binding.btnCancel.setOnClickListener(v -> finish());

        binding.btnSave.setOnClickListener(v -> {
            OwnerDetail crOwnerDetails = new OwnerDetail();
            crOwnerDetails.setOwnerName(binding.edtName.getText().toString());
            crOwnerDetails.setOwnerNic(binding.edtOwnerNIC.getText().toString());
            crOwnerDetails.setOccupierId(binding.edtONIC.getText().toString());
            crOwnerDetails.setOccupierName(binding.edtOccupentName.getText().toString());
            crOwnerDetails.setOccupierType(type);
            try {
                if (type == -1) {
                    type = occuperTypeMap.get(binding.edtOccuperType.getSelectedItemPosition());
                }
            }catch (Exception e){}

            try {
                if (buildingNo.equals("")) {
                    buildingNo = buildingList.get(binding.edtBuildingNo.getSelectedItemPosition());
                }
            }catch (Exception e){}


            crOwnerDetails.setTenantDetails(binding.edtTenDetails.getText().toString());
            crOwnerDetails.setPeriod(binding.edtPeriod.getText().toString());
            crOwnerDetails.setRent(binding.edtRent.getText().toString());
            crOwnerDetails.setTerm(binding.edtTerm.getText().toString());
            crOwnerDetails.setDate(binding.editDate.getText().toString());
            crOwnerDetails.setRemark(binding.edtRemark.getText().toString());
            crOwnerDetails.setBuildingNo(buildingNo);
            Bitmap drawing = binding.signPad.getBitmap();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            drawing.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            crOwnerDetails.setSign(Base64.encodeToString(byteArray, Base64.DEFAULT));

            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constant.DETAILS, crOwnerDetails);
            returnIntent.putExtra(Constant.TYPE, isUpdate);
            setResult(Activity.RESULT_OK, returnIntent);
            Toast.makeText(this, "Saved..", Toast.LENGTH_SHORT).show();
            finish();
        });


    }

    private void changeMode(boolean mode) {
        if (mode) {
            binding.edtName.setEnabled(true);
            binding.edtOwnerNIC.setEnabled(true);
            binding.edtONIC.setEnabled(true);
            binding.edtOccupentName.setEnabled(true);
            binding.edtOccuperType.setEnabled(true);
            binding.edtTenDetails.setEnabled(true);
            binding.editDate.setEnabled(true);
            binding.edtBuildingNo.setEnabled(true);
            binding.edtPeriod.setEnabled(true);
            binding.edtRent.setEnabled(true);
            binding.edtTerm.setEnabled(true);
            binding.edtRemark.setEnabled(true);
            binding.btnSave.setVisibility(View.VISIBLE);
            binding.btnUpdate.setVisibility(View.GONE);
            binding.signPad.setVisibility(View.VISIBLE);
            binding.btnEditClear.setVisibility(View.VISIBLE);
            binding.imgSign.setVisibility(View.GONE);

        } else {
            binding.edtName.setEnabled(false);
            binding.edtOwnerNIC.setEnabled(false);
            binding.edtONIC.setEnabled(false);
            binding.edtOccupentName.setEnabled(false);
            binding.edtBuildingNo.setEnabled(false);
            binding.edtOccuperType.setEnabled(false);
            binding.edtTenDetails.setEnabled(false);
            binding.editDate.setEnabled(false);
            binding.edtPeriod.setEnabled(false);
            binding.edtRent.setEnabled(false);
            binding.edtTerm.setEnabled(false);
            binding.edtRemark.setEnabled(false);
            binding.signPad.setVisibility(View.INVISIBLE);
            binding.imgSign.setVisibility(View.VISIBLE);
            binding.btnEditClear.setVisibility(View.GONE);
            binding.btnSave.setVisibility(View.GONE);
            binding.btnUpdate.setVisibility(View.VISIBLE);
        }
    }

    private void initSpinner() {

        binding.edtOccuperType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                type = occuperTypeMap.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        binding.edtBuildingNo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position != -1) {
                    buildingNo = buildingList.get(position);
                } else {
                    buildingNo = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    protected Dialog createDialog() {
        datePickerDialog.updateDate(year, month, day);
        return datePickerDialog;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            day = selectedDay;
            month = selectedMonth;
            year = selectedYear;
            Calendar cal = Calendar.getInstance();
            cal.set(year, month, day);
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sf.format(cal.getTime());
            binding.editDate.setText((date));
        }
    };


    private void hideKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


}
