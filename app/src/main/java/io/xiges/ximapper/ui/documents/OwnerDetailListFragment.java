package io.xiges.ximapper.ui.documents;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.adapter.OwnerDetailAdapter;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.FragmentOwnerDetailListBinding;
import io.xiges.ximapper.model.cr.Building;
import io.xiges.ximapper.model.cr.CR;
import io.xiges.ximapper.model.cr.OwnerDetail;
import io.xiges.ximapper.ui.alertboxes.ConfirmationDialogActivity;
import io.xiges.ximapper.util.SwipeHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class OwnerDetailListFragment extends Fragment implements RecyclerViewClickListener {

    private FragmentOwnerDetailListBinding binding;
    private final int REQUEST_CODE = 236;
    private final int RESULT_CODE = 789;
    private int position = -1;
    private Context c = null;
    private OwnerDetailAdapter ownerDetailAdapter;
    private List<OwnerDetail> ownerDetails;

    public OwnerDetailListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_owner_detail_list, container, false);
        View view = binding.getRoot();

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                boolean isUpdate = data.getBooleanExtra(Constant.TYPE, false);
                OwnerDetail crOwnerDetails = data.getParcelableExtra(Constant.DETAILS);
               if (!isUpdate) {

                    try {
                        ((CRDocumentActivity) getActivity()).saveOwnerinfo(crOwnerDetails);
                        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        ownerDetails.add(crOwnerDetails);
                        ownerDetailAdapter.setDetails(ownerDetails);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else {
                /*    try {
                        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        for (Building b : cr.getBuildingDetails().getBuildings()) {
                            if (crOwnerDetails.getBuildingNo().equals(b.getNo()) || crOwnerDetails.getBuildingNo().equals(b.getBuildingId() + "")) {
                                b.getOwnerDetails();
                            }
                        }
                        List<OwnerDetail> details = cr.getOwnerDetails();
                        details.set(position,crOwnerDetails);
                        ((CRDocumentActivity) getActivity()).updateOwnerConinfo(details);
                        cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        ownerDetailAdapter.setDetails(cr.getOwnerDetails());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                }
            }
        } else if (requestCode == RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
               /* try {
                    CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                    List<OwnerDetail> details = cr.getOwnerDetails();
                    details.remove(position);
                    ((CRDocumentActivity) getActivity()).updateOwnerConinfo(details);
                    cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                    ownerDetailAdapter.setDetails(cr.getOwnerDetails());

                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        binding.btnAddOwnerDetails.setOnClickListener(v -> {
            Intent i = new Intent(getActivity(), OwnerDetailCRActivity.class);
            i.putExtra(Constant.MODE, true);
            i.putExtra(Constant.ID,((CRDocumentActivity) getActivity()).getBuildingNo().toArray(new String[0]));
            startActivityForResult(i, REQUEST_CODE);
        });

        ownerDetailAdapter = new OwnerDetailAdapter(ownerDetails, c, this);


        binding.rcOwnerDetails.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rcOwnerDetails.setAdapter(ownerDetailAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(binding.rcOwnerDetails.getContext(),
                LinearLayout.VERTICAL);
        binding.rcOwnerDetails.addItemDecoration(dividerItemDecoration);
        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
        for (Building b : cr.getBuildingDetails().getBuildings()) {
            if (b.getOwnerDetails().size() > 0) {
                ownerDetails.addAll(b.getOwnerDetails());
            }
        }

        ownerDetailAdapter.setDetails(ownerDetails);



        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels / 6;

        SwipeHelper swipeHelper = new SwipeHelper(getActivity(), binding.rcOwnerDetails, width) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new SwipeHelper.UnderlayButton(getActivity(),
                        getResources().getString(R.string.tv_label_delete),
                        0,
                        ContextCompat.getColor(getActivity(), R.color.colorRed),
                        pos -> {
                            position = pos;
                            Intent i = new Intent(getActivity(), ConfirmationDialogActivity.class);
                            i.putExtra(Constant.NAME, "Are you sure you want to delete this item?");
                            startActivityForResult(i, RESULT_CODE);
                        }
                ));
            }
        };

        swipeHelper.attachSwipe();

    }

    @Override
    public void onItemClick(int position) {
        Intent i = new Intent(getActivity(), OwnerDetailCRActivity.class);
        i.putExtra(Constant.MODE, false);
        this.position = position;
       // i.putExtra(Constant.DETAILS, ((CRDocumentActivity) getActivity()).getConditionalReport().getOwnerDetails().get(position));
       // i.putExtra(Constant.ID,((CRDocumentActivity) getActivity()).getBuildingNo().toArray(new String[0]));
        startActivityForResult(i, REQUEST_CODE);
    }

    @Override
    public void onLongItemClick(int position) {

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        c = context;

    }


}
