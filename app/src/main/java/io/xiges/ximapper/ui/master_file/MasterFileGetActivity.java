package io.xiges.ximapper.ui.master_file;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.xiges.ximapper.BaseActivity;
import io.xiges.ximapper.BuildConfig;
import io.xiges.ximapper.R;
import io.xiges.ximapper.adapter.MasterFileListAdapter;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.dagger.AppComponent;
import io.xiges.ximapper.databinding.ActivityMasterFileGetBinding;
import io.xiges.ximapper.model.BundleInfo;
import io.xiges.ximapper.model.OnAPICallback;
import io.xiges.ximapper.model.api.MasterFileInfoResponse;
import io.xiges.ximapper.model.api.MasterFileListResponse;
import io.xiges.ximapper.network.ApiInterface;
import io.xiges.ximapper.room.AppDatabase;
import io.xiges.ximapper.room.MasterFileEntity;
import io.xiges.ximapper.ui.login.LoginActivity;
import io.xiges.ximapper.ui.map.MapViewActivity;
import io.xiges.ximapper.util.MasterFileManager;
import io.xiges.ximapper.util.NetworkStatus;
import io.xiges.ximapper.util.RefreshToken;
import io.xiges.ximapper.util.SharedPreferencesHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MasterFileGetActivity extends BaseActivity<MasterFileGetPresenter> implements MasterFileGetView, MasterFileListAdapter.ImportClickedClickedListener, SwipeRefreshLayout.OnRefreshListener {

    private ActivityMasterFileGetBinding binding;
    private MasterFileListAdapter adapter;
    private List<MasterFileListResponse> masterFiles;

    @Inject
    ApiInterface apiInterface;

    @Inject
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_master_file_get);

        ((XimapperApplication) getApplication()).getAppComponent().inject(this);

        binding.swipeContainer.setOnRefreshListener(this);


// Setting timeout globally for the download network requests:
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setReadTimeout(30_000)
                .setDatabaseEnabled(true)
                .setConnectTimeout(30_000)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);

        askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, 101);

        masterFiles = new ArrayList<>();

        binding.rcMasterFiles.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(binding.rcMasterFiles.getContext(),
                LinearLayout.VERTICAL);
        binding.rcMasterFiles.addItemDecoration(dividerItemDecoration);
        adapter = new MasterFileListAdapter(masterFiles, MasterFileGetActivity.this, this,apiInterface);
        binding.rcMasterFiles.setAdapter(adapter);


        getMasterFiles();


        try {
            PackageInfo pInfo = MasterFileGetActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName + " | "+ BuildConfig.BuildTime;
            binding.tvDebug.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        binding.btnMFSearch.setOnClickListener(v->{
            String search = binding.edtMFSearch.getText().toString();
            List<MasterFileListResponse> mfs =new ArrayList<>();
            for (MasterFileListResponse response :masterFiles){
                if(search.equals("") || search.isEmpty()){
                    adapter.setMasterFiles(masterFiles);
                    break;
                }else {

                    if (response.getFileNumber().equals(search)) {
                        mfs.add(response);
                    }else if(response.getMasterFileRefNumber().equals(search)){
                        mfs.add(response);
                    }else if(response.getAtNumber().equals(search)) {
                        mfs.add(response);
                    }else if(response.getMasterFileName().equals(search)) {
                        mfs.add(response);
                    }


                }

            }
            adapter.setMasterFiles(mfs);
        });

        binding.imgLogout.setOnClickListener(v-> showAlertDialog());

    }

    public void addMasterFile(MasterFileListResponse masterFile) {
        MasterFileEntity masterFileEntity = new MasterFileEntity();
        masterFileEntity.setMid(masterFile.getMasterFileId());
        masterFileEntity.setMasterFileId(masterFile.getMasterFileRefNumber());
        masterFileEntity.setVersion(masterFile.getChecksum());
        new addMasterFileAsyncTask(AppDatabase.getAppDatabase(this)).execute(masterFileEntity);
    }


    private void validateFiles(){
        new getAllReceiptAsyncTask(AppDatabase.getAppDatabase(this),this).execute();
    }

    @Override
    protected void initDependencies(AppComponent appComponent) {
        appComponent.inject(this);
    }


    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(MasterFileGetActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MasterFileGetActivity.this, permission)) {
                ActivityCompat.requestPermissions(MasterFileGetActivity.this, new String[]{permission}, requestCode);

            } else {
                ActivityCompat.requestPermissions(MasterFileGetActivity.this, new String[]{permission}, requestCode);
            }
        } else if (ContextCompat.checkSelfPermission(MasterFileGetActivity.this, permission) == PackageManager.PERMISSION_DENIED) {
            Toast.makeText(getApplicationContext(), "Permission was denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {

            if (requestCode == 101)
                Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onImportClicked(MasterFileListResponse masterFile) {
       MasterFileManager  mf = new MasterFileManager();
       BundleInfo bi = mf.getMasterFileData(masterFile.getMasterFileRefNumber());

        Intent i = new Intent(MasterFileGetActivity.this,MapViewActivity.class);
        i.putExtra(Constant.MASTER_FILE,masterFile);
        i.putExtra(Constant.BUNDLE,bi);
        i.putExtra(Constant.MODE,1);
        startActivity(i);
       // finish();
    }

    @Override
    public void validateMasterFiles(MasterFileListResponse masterFile) {
        for (MasterFileListResponse mf : masterFiles){
            if(mf.getMasterFileRefNumber().equals(masterFile.getMasterFileRefNumber()) && mf.getChecksum().equals(masterFile.getChecksum())){
                 MasterFileListResponse m = masterFiles.get(masterFiles.indexOf(mf));
                 m.setAvailability(true);
                 masterFiles.set(masterFiles.indexOf(mf),m);
                 setAdapterFiles();
            }
        }
    }

    @Override
    public void setAdapterFiles() {
        adapter.setMasterFiles(masterFiles);
    }

    @Override
    public void onRefresh() {
        binding.swipeContainer.setRefreshing(true);
        binding.edtMFSearch.setText("");
        getMasterFiles();
    }


    private static class addMasterFileAsyncTask extends AsyncTask<MasterFileEntity, Void, Long> {

        private AppDatabase db;

        addMasterFileAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }


        @Override
        protected Long doInBackground(MasterFileEntity... masterFileEntities) {
            try{
                return db.masterFileDao().insert(masterFileEntities[0]);

            }catch (SQLiteConstraintException w){
                return -1L;
            }


        }

        @Override
        protected void onPostExecute(Long id) {
            super.onPostExecute(id);
            if (id==-1L){
            }
        }
    }

    private static class deleteMasterFileAsyncTask extends AsyncTask<MasterFileEntity, Void, Void> {

        private AppDatabase db;

        deleteMasterFileAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }


        @Override
        protected Void doInBackground(MasterFileEntity... masterFileEntities) {
            db.masterFileDao().delete(masterFileEntities[0]);
            return null;
        }


    }

    private void getMasterFiles() {
        if (new NetworkStatus().isConnected(MasterFileGetActivity.this)) {

                apiInterface.getMasterFile("Bearer "+prefs.getString(Constant.ACCESS_TOKEN, ""),"Doma").enqueue(new Callback<List<MasterFileListResponse>>() {

                    @Override
                    public void onResponse(Call<List<MasterFileListResponse>> call, Response<List<MasterFileListResponse>> response) {
                        if (response.isSuccessful()) {

                            List<MasterFileListResponse> masterFileListResponses = response.body();
                            checkMasterFileInfo();
                            masterFiles = masterFileListResponses;
                            adapter.setMasterFiles(masterFiles);
                            new SharedPreferencesHandler().saveMasterFileSharedPreferences(getApplicationContext(), masterFileListResponses);
                            validateFiles();
                            binding.swipeContainer.setRefreshing(false);



                        } else if(response.code()==401){
                            new RefreshToken().refreshAccessToken(MasterFileGetActivity.this, new OnAPICallback() {
                                @Override
                                public void onSuccess() {
                                    //recall method
                                    getMasterFiles();
                                }

                                @Override
                                public void onFailed(String message) {
                                    showErrorMessage(message);
                                }
                            });
                        }else{

                            showErrorMessage("Failed to get Master file info ");
                        }

                    }

                    @Override
                    public void onFailure(Call<List<MasterFileListResponse>> call, Throwable t) {

                        showErrorMessage("Failed to get Master file info ");

                    }
                });


        } else {
            Gson gson = new Gson();
            String json = prefs.getString(Constant.MASTER_FILE, "");
            Type masterListType = new TypeToken<ArrayList<MasterFileListResponse>>(){}.getType();
            List<MasterFileListResponse> masterData= gson.fromJson(json, masterListType);
            masterFiles = masterData;
            adapter.setMasterFiles(masterFiles);
            validateFiles();
            binding.swipeContainer.setRefreshing(false);
            showErrorMessage("No internet connection");

        }

    }

    private void getMasterFileInfo(int mfid) {
        if (new NetworkStatus().isConnected(MasterFileGetActivity.this)) {

            apiInterface.getMasterFileInfo("Bearer "+prefs.getString(Constant.ACCESS_TOKEN, ""),mfid).enqueue(new Callback<MasterFileInfoResponse>() {

                @Override
                public void onResponse(Call<MasterFileInfoResponse> call, Response<MasterFileInfoResponse> response) {
                    if (response.isSuccessful()) {


                        MasterFileInfoResponse masterFileInfoResponse = response.body();



                    } else if(response.code()==401){
                        showErrorMessage("Unauthorized");
                    }

                }

                @Override
                public void onFailure(Call<MasterFileInfoResponse> call, Throwable t) {
                    showErrorMessage("Something went wrong ");


                }
            });


        } else {
            showErrorMessage("No internet connection");
        }

    }


    private void checkMasterFileInfo() {
        if (new NetworkStatus().isConnected(MasterFileGetActivity.this)) {

            apiInterface.checkmasterdata("Bearer "+prefs.getString(Constant.ACCESS_TOKEN, "")).enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {



                    } else if(response.code()==401){
                        showErrorMessage("Unauthorized");
                    }

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    showErrorMessage("Failed to get Master file info ");
                }
            });


        } else {
            showErrorMessage("No internet connection");
        }

    }


    private static class getAllReceiptAsyncTask extends AsyncTask<Void,Void, List<MasterFileEntity>>{

        private AppDatabase db;
        private MasterFileGetView view;

        getAllReceiptAsyncTask(AppDatabase appDatabase,MasterFileGetView view) {
            db = appDatabase;
            this.view = view;
        }

        @Override
        protected List<MasterFileEntity> doInBackground(Void... voids) {
            return db.masterFileDao().getAll();
        }

        @Override
        protected void onPostExecute(List<MasterFileEntity> masterFileEntities) {
            super.onPostExecute(masterFileEntities);
            if(masterFileEntities!=null) {
                for (MasterFileEntity masterFileEntity : masterFileEntities) {
                    MasterFileListResponse masterFile = new MasterFileListResponse();
                    masterFile.setChecksum(masterFileEntity.getVersion());
                    masterFile.setMasterFileRefNumber(masterFileEntity.getMasterFileId());
                    this.view.validateMasterFiles(masterFile);
                }
                this.view.setAdapterFiles();
            }
        }
    }
    private void showErrorMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }


    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MasterFileGetActivity.this);
// Add the buttons
        builder.setPositiveButton(getResources().getString(R.string.ad_button_ok), (dialog, id) -> logOut());
        builder.setNegativeButton(getResources().getString(R.string.ad_button_cancel), (dialog, id) -> dialog.cancel());

        builder.setMessage(getResources().getString(R.string.ad_message_logout));
        builder.setTitle(getResources().getString(R.string.ad_title_logout));

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }



    private void logOut() {
        new SharedPreferencesHandler().logoutClearSharedPref(MasterFileGetActivity.this);
        Intent i = new Intent(MasterFileGetActivity.this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }



    public SharedPreferences getPrefs() {
        return prefs;
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}


