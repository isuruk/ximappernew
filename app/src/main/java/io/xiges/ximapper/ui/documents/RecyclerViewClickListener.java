package io.xiges.ximapper.ui.documents;

public interface RecyclerViewClickListener {
    void onItemClick(int position);
    void onLongItemClick(int position);

}
