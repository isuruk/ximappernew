package io.xiges.ximapper.ui.documents;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivitySignPadBinding;
import io.xiges.ximapper.model.cr.AcquisitionOfficerDetails;
import io.xiges.ximapper.model.cr.ChiefValuerRepresentationDetails;
import io.xiges.ximapper.model.cr.GramaNiladhariDetails;
import io.xiges.ximapper.model.cr.OccupierDetails;


public class SignPadActivity extends AppCompatActivity {
    private ActivitySignPadBinding binding;

    int year;
    int month;
    int day;

    private DatePickerDialog datePickerDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_pad);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);
        int screenHeight = (int) (metrics.heightPixels * 0.80);

        getWindow().setLayout(screenWidth, screenHeight);

        int mode = getIntent().getIntExtra(Constant.MODE,-1);
        int type = getIntent().getIntExtra(Constant.TYPE,-1);


        binding.switch2.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked){
                binding.scroll.setEnableScrolling(false);
            }else{
                binding.scroll.setEnableScrolling(true);
            }
        });


        String name = "";
        String d = "";
        String desc = "";
        String sign = "";




        binding.editDate.setOnClickListener(view -> {
            hideKeyboard(this, view);
            createDialog().show();
        });


        if (TextUtils.isEmpty(binding.editDate.getText().toString()) || binding.editDate.getText().toString().equals("")) {
            Calendar cal = Calendar.getInstance();
            year = cal.get(Calendar.YEAR);
            month = cal.get(Calendar.MONTH);
            day = cal.get(Calendar.DAY_OF_MONTH);
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sf.format(cal.getTime());
            binding.editDate.setText((date));
        } else {
            String string = binding.editDate.getText().toString();
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = sf.parse(string);
                cal.setTime(date);
                year = cal.get(Calendar.YEAR);
                month = cal.get(Calendar.MONTH);
                day = cal.get(Calendar.DAY_OF_MONTH);
            } catch (ParseException e) {
                year = cal.get(Calendar.YEAR);
                month = cal.get(Calendar.MONTH);
                day = cal.get(Calendar.DAY_OF_MONTH);
                String date = sf.format(cal.getTime());
                binding.editDate.setText(date);
            }

        }

        datePickerDialog = new DatePickerDialog(this, datePickerListener, year, month, day);

        if(type==1){
            AcquisitionOfficerDetails acquisitionOfficerDetails = getIntent().getParcelableExtra(Constant.DETAILS);
            if(mode != 1){
                name = acquisitionOfficerDetails.getName();
                d = acquisitionOfficerDetails.getDate();
                desc = acquisitionOfficerDetails.getDesignation();
                sign = acquisitionOfficerDetails.getSign();
            }
            binding.textView48.setVisibility(View.VISIBLE);
            binding.editDesignation.setVisibility(View.VISIBLE);
            binding.editDesignation.setText(desc);
        }else if(type==2){
            GramaNiladhariDetails niladhariDetails  = getIntent().getParcelableExtra(Constant.DETAILS);
            if(mode != 1){
                name = niladhariDetails.getName();
                d = niladhariDetails.getDate();
                sign = niladhariDetails.getSign();
            }
        }else if(type==3){
            ChiefValuerRepresentationDetails chiefValuerRepresentationDetails  = getIntent().getParcelableExtra(Constant.DETAILS);
            if(mode != 1){
                name = chiefValuerRepresentationDetails.getName();
                d = chiefValuerRepresentationDetails.getDate();
                sign = chiefValuerRepresentationDetails.getSign();
            }
        }else if(type==4){
            OccupierDetails occupierDetails  = getIntent().getParcelableExtra(Constant.DETAILS);
            if(mode != 1){
                name = occupierDetails.getName();
                d = occupierDetails.getDate();
                sign = occupierDetails.getSign();
            }
        }



        if(mode == 1 ){
            //binding.signPad.clear();
            binding.imgSign.setVisibility(View.INVISIBLE);
            binding.signPad.setVisibility(View.VISIBLE);

        }else {
            binding.signPad.setVisibility(View.INVISIBLE);
            binding.imgSign.setVisibility(View.VISIBLE);


            byte[] decodedString = Base64.decode(sign, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            binding.imgSign.setImageBitmap(decodedByte);
            binding.editName.setText(name);
            binding.editDate.setText(d);



            Bitmap emptyBitmap = Bitmap.createBitmap(decodedByte.getWidth(), decodedByte.getHeight(), decodedByte.getConfig());
            if (decodedByte.sameAs(emptyBitmap)) {
                binding.imgSign.setVisibility(View.INVISIBLE);
                binding.signPad.setVisibility(View.VISIBLE);
            }

        }


        binding.btnSignOk.setOnClickListener(v->{
            Bitmap drawing = binding.signPad.getBitmap();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            drawing.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            String sign_ = Base64.encodeToString(byteArray, Base64.DEFAULT);
            String name_ = (binding.editName.getText().toString());
            String date_ = (binding.editDate.getText().toString());
            String desc_ = (binding.editDesignation.getText().toString());

            Intent returnIntent = new Intent();

            if(type==1){
                AcquisitionOfficerDetails acquisitionOfficerDetails = new AcquisitionOfficerDetails();
                acquisitionOfficerDetails.setName(name_);
                acquisitionOfficerDetails.setDate(date_);
                acquisitionOfficerDetails.setDesignation(desc_);
                acquisitionOfficerDetails.setSign(sign_);
                returnIntent.putExtra(Constant.DETAILS, acquisitionOfficerDetails);

            }else if(type==2){
                GramaNiladhariDetails niladhariDetails  = new GramaNiladhariDetails();
                niladhariDetails.setName(name_);
                niladhariDetails.setDate(date_);
                niladhariDetails.setSign(sign_);
                returnIntent.putExtra(Constant.DETAILS, niladhariDetails);
            }else if (type == 3){
                ChiefValuerRepresentationDetails chiefValuerRepresentationDetails = new ChiefValuerRepresentationDetails();
                chiefValuerRepresentationDetails.setName(name_);
                chiefValuerRepresentationDetails.setDate(date_);
                chiefValuerRepresentationDetails.setSign(sign_);
                returnIntent.putExtra(Constant.DETAILS, chiefValuerRepresentationDetails);
            }else if(type == 4){
                OccupierDetails occupierDetails = new OccupierDetails();
                occupierDetails.setName(name_);
                occupierDetails.setDate(date_);
                occupierDetails.setSign(sign_);
                returnIntent.putExtra(Constant.DETAILS, occupierDetails);
            }
            returnIntent.putExtra(Constant.TYPE, type);
            setResult(Activity.RESULT_OK, returnIntent);
            Toast.makeText(this,"Saved..",Toast.LENGTH_SHORT).show();
            finish();
        });

        binding.btnEditClear.setOnClickListener(v->{
            binding.signPad.clear();
            binding.imgSign.setVisibility(View.INVISIBLE);
            binding.signPad.setVisibility(View.VISIBLE);

        });
    }
    protected Dialog createDialog() {
        datePickerDialog.updateDate(year, month, day);
        return datePickerDialog;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            day = selectedDay;
            month = selectedMonth;
            year = selectedYear;
            Calendar cal = Calendar.getInstance();
            cal.set(year, month, day);
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sf.format(cal.getTime());
            binding.editDate.setText((date));
        }
    };


    private void hideKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
