package io.xiges.ximapper.ui.images;

import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.github.chrisbanes.photoview.PhotoView;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;

public class ImageViewActivity extends AppCompatActivity {

    String image = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);

        image = getIntent().getStringExtra(Constant.IMAGE);

        Uri uri = Uri.parse(image);

        PhotoView photoView = findViewById(R.id.photo_view);
        photoView.setImageURI(uri);
    }
}
