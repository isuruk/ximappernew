package io.xiges.ximapper.ui.documents;

import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.xiges.ximapper.BuildConfig;
import io.xiges.ximapper.R;
import io.xiges.ximapper.adapter.DocumentFragmentAdapter;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.databinding.ActivityCrdocumentBinding;
import io.xiges.ximapper.model.api.master_data.MasterData;
import io.xiges.ximapper.model.cr.Access;
import io.xiges.ximapper.model.cr.AcquisitionOfficerDetails;
import io.xiges.ximapper.model.cr.Building;
import io.xiges.ximapper.model.cr.BuildingDetails;
import io.xiges.ximapper.model.cr.CR;
import io.xiges.ximapper.model.cr.ChiefValuerRepresentationDetails;
import io.xiges.ximapper.model.cr.General;
import io.xiges.ximapper.model.cr.GramaNiladhariDetails;
import io.xiges.ximapper.model.cr.OccupierDetails;
import io.xiges.ximapper.model.cr.OtherConstruction;
import io.xiges.ximapper.model.cr.OwnerDetail;
import io.xiges.ximapper.model.cr.ProposedRate;
import io.xiges.ximapper.model.cr.RepresentativeDetail;
import io.xiges.ximapper.model.cr.Signatures;
import io.xiges.ximapper.model.masterfile.MasterFileData;
import io.xiges.ximapper.util.DataManager;

public class CRDocumentActivity extends AppCompatActivity {

    private ActivityCrdocumentBinding binding;
    private CR conditionalReport;
    private DataManager dataManager = new DataManager();
    private String landName = "Test";
    private String lotName = "Test";
    private MasterFileData masterFile = new MasterFileData();


    @Inject
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_crdocument);
        ((XimapperApplication) getApplication()).getAppComponent().inject(this);

        conditionalReport = new CR();

        landName = getIntent().getStringExtra(Constant.MASTER_FILE);
        lotName = getIntent().getStringExtra(Constant.LOT);

        this.masterFile = getIntent().getParcelableExtra(Constant.MASTER_DATA);

        try {
            PackageInfo pInfo = CRDocumentActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName + " | "+ BuildConfig.BuildTime;
            binding.tvDebug.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        binding.vpActivityProgress.setAdapter(new DocumentFragmentAdapter(getSupportFragmentManager(),
                CRDocumentActivity.this));
        binding.slidingTabs.setupWithViewPager(binding.vpActivityProgress);

        try {
            conditionalReport = dataManager.getCRData(landName, lotName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        binding.btnBack.setOnClickListener(v -> {
            finish();
        });
    }

    public MasterFileData getMasterFile() {
        return masterFile;
    }

    public MasterData getMasterData() {
        Gson gson = new Gson();
        String json = prefs.getString(Constant.MASTER_DATA, "");
        MasterData masterData = gson.fromJson(json, MasterData.class);
        return masterData;
    }


    public void saveLandInfo(General general, Access access) throws IOException {
        conditionalReport.setAccess(access);
        conditionalReport.setGeneral(general);
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }

    public void saveBuildinginfo(BuildingDetails buildingDetails) throws IOException {
        conditionalReport.setBuildingDetails(buildingDetails);
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }


    public void updateOtherConinfo(List<OtherConstruction> otherConstructions) throws IOException {
        conditionalReport.setOtherConstructions(otherConstructions);
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }


    public void updateOwnerConinfo(List<OwnerDetail> ownerDetails,String bNo) throws IOException {
        BuildingDetails bd = conditionalReport.getBuildingDetails();
        int i =0;
        for (Building b : bd.getBuildings()){
            if (bNo.equals(b.getNo()) || bNo.equals(b.getBuildingId()+"")){
                bd.getBuildings().get(i).setOwnerDetails(ownerDetails);
                updateBuildinginfo(bd);
                break;
            }
            i++;
        }
    }

    public void saveOwnerinfo(OwnerDetail ownerDetails) throws IOException {
        BuildingDetails bd = conditionalReport.getBuildingDetails();
        int i =0;
        for (Building b : bd.getBuildings()){
            if (ownerDetails.getBuildingNo().equals(b.getNo()) || ownerDetails.getBuildingNo().equals(b.getBuildingId()+"")){
                List<OwnerDetail>  details =  bd.getBuildings().get(i).getOwnerDetails();
                details.add(ownerDetails);
                bd.getBuildings().get(i).setOwnerDetails(details);
                updateBuildinginfo(bd);
                break;
            }
            i++;
        }
    }

    public void saveOtherConinfo(OtherConstruction otherConstructions) throws IOException {
        List<OtherConstruction> otherConstructionList = conditionalReport.getOtherConstructions();
        otherConstructionList.add(otherConstructions);
        conditionalReport.setOtherConstructions(otherConstructionList);
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }

    public void updateChiefValuerRepresentationDetails(ChiefValuerRepresentationDetails chiefValuerRepresentationDetails) throws IOException {
        Signatures signatures = conditionalReport.getSignatures();
        signatures.setChiefValuerRepresentationDetails(chiefValuerRepresentationDetails);
        conditionalReport.setSignatures(signatures);
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }

    public void updateGramaNiladhariDetails(GramaNiladhariDetails gramaNiladhariDetails) throws IOException {
        Signatures signatures = conditionalReport.getSignatures();
        signatures.setGramaNiladhariDetails(gramaNiladhariDetails);
        conditionalReport.setSignatures(signatures);
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }


    public void updateRepresentativeDetails(List<RepresentativeDetail> representativeDetail) throws IOException {
        Signatures signatures = conditionalReport.getSignatures();
        signatures.setRepresentativeDetails(representativeDetail);
        conditionalReport.setSignatures(signatures);
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }


    public void updateAcquisitionOfficerDetails(AcquisitionOfficerDetails acquisitionOfficerDetails) throws IOException {
        Signatures signatures = conditionalReport.getSignatures();
        signatures.setAcquisitionOfficerDetails(acquisitionOfficerDetails);
        conditionalReport.setSignatures(signatures);
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }

    public void updateOccupierDetails(OccupierDetails occupierDetails) throws IOException {
        Signatures signatures = conditionalReport.getSignatures();
        signatures.setOccupierDetails(occupierDetails);
        conditionalReport.setSignatures(signatures);
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }

    public void updateBuildinginfo(BuildingDetails building) throws IOException {
        conditionalReport.setBuildingDetails(building);
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }

    public void savePRinfo(ProposedRate pr) throws IOException {
        List<ProposedRate> prs = conditionalReport.getAccess().getProposedRates();
        prs.add(pr);
        Access access = conditionalReport.getAccess();
        access.setProposedRates(prs);
        conditionalReport.setAccess(access);
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }

    public void updatePRinfo(List<ProposedRate> proposedRates) throws IOException {
        conditionalReport.getAccess().setProposedRates(proposedRates);
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }
    public List<String> getBuildingNo() {
        List<Building> buildings = conditionalReport.getBuildingDetails().getBuildings();
        List<String> nos = new ArrayList<>();
        for (Building b : buildings) {
            nos.add(b.getNo());
        }
        return nos;
    }


    public CR getConditionalReport() {
        try {
            conditionalReport = dataManager.getCRData(landName, lotName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return conditionalReport;
    }

    public void setConditionalReport(CR conditionalReport) {
        this.conditionalReport = conditionalReport;
    }

    public String getLandName() {
        return landName;
    }

    public void setLandName(String landName) {
        this.landName = landName;
    }

    public String getLotName() {
        return lotName;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

    public void removeBuildingFromMap(Building building) {

    }

}
