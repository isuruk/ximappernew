package io.xiges.ximapper.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.FirebaseApp;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import java.io.File;

import javax.inject.Inject;

import io.xiges.ximapper.BuildConfig;
import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.ui.login.LoginActivity;
import io.xiges.ximapper.ui.master_file.MasterFileGetActivity;

public class MainActivity extends AppCompatActivity {

    public static String ip = "";

    @Inject
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.SplashMain);
        setContentView(R.layout.activity_main);
        ((XimapperApplication) getApplication()).getAppComponent().inject(MainActivity.this);

        FirebaseApp.initializeApp(getApplicationContext());
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);

        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "Maps");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }

        ip = prefs.getString(Constant.CONFIG,"");
        if(ip.equals("")){
            ip = BuildConfig.API_URL;
        }

        Log.e("SAD",prefs.getString(Constant.ACCESS_TOKEN, ""));

        if (!prefs.getString(Constant.ACCESS_TOKEN, "").equals("") && !prefs.getString(Constant.REFRESH_TOKEN, "").equals("")) {
            Intent i = new Intent(MainActivity.this, MasterFileGetActivity.class);
            startActivity(i);
            finish();
        }else {
            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
    }
}
