package io.xiges.ximapper.ui.menu;

public interface MenuView {
    void openSketchTool();
    void openCR();
}
