package io.xiges.ximapper.ui.drawing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityMeasuredCornerDrawingBinding;

public class MeasuredCornerDrawingActivity extends AppCompatActivity {

    private ActivityMeasuredCornerDrawingBinding binding;
    private boolean ft, st, tt, fot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_measured_corner_drawing);


        changeToDefault();

        binding.imgFT.setOnClickListener(v -> {
            ft = true;
            st = false;
            tt = false;
            fot = false;
            binding.imgFT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgST.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgTT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgFoT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        });
        binding.imgST.setOnClickListener(v -> {
            ft = false;
            st = true;
            tt = false;
            fot = false;
            binding.imgFT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgST.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgTT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgFoT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        });
        binding.imgTT.setOnClickListener(v -> {
            ft = false;
            st = false;
            tt = true;
            fot = false;
            binding.imgFT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgST.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgTT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgFoT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        });
        binding.imgFoT.setOnClickListener(v -> {
            ft = false;
            st = false;
            tt = false;
            fot = true;
            binding.imgFT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgST.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgTT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgFoT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        });


        binding.btnOKCurve.setOnClickListener(v -> {
            String f1 = binding.edtFeetLineCorner.getText().toString();
            String in1 = binding.edtInchLineCorner.getText().toString();
            String f2 = binding.edtFeetLineCornerLine2.getText().toString();
            String in2 = binding.edtInchLineCornerLine2.getText().toString();


            boolean a = false;


            double feet1 = 0.0;
            try {
                feet1 = Double.valueOf(f1);
            } catch (NumberFormatException e) {
                feet1 = 0.0;
            }

            double inch1 = 0.0;
            try {
                inch1 = Double.valueOf(in1);
            } catch (NumberFormatException e) {
                inch1 = 0.0;
            }

            double feet2 = 0.0;

            try {
                feet2 = Double.valueOf(f2);
            } catch (NumberFormatException e) {
                feet2 = 0.0;
            }
            double inch2 = 0.0;

            try {
                inch2 = Double.valueOf(in2);
            } catch (NumberFormatException e) {
                inch2 = 0.0;
            }
            if (inch1 < 12 && inch2 < 12) {
                a = true;
            }

            int angle = 0;

            int totInch1 = (int) ((feet1 * 12) + inch1);
            int totInch2 = (int) ((feet2 * 12) + inch2);


            if (a & (ft | st | tt | fot)) {

                if (ft) {
                    angle = 1;
                } else if (st) {
                    angle = 2;
                } else if (tt) {
                    angle = 3;
                } else if (fot) {
                    angle = 4;
                }

                Intent returnIntent = new Intent();
                returnIntent.putExtra(Constant.LINE1, totInch1);
                returnIntent.putExtra(Constant.LINE2, totInch2);
                returnIntent.putExtra(Constant.TYPE, angle);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            } else {
                Toast.makeText(this, "Invalid Values", Toast.LENGTH_LONG).show();
            }
        });


    }

    private void changeToDefault() {
        binding.imgFT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.imgST.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.imgTT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.imgFoT.setColorFilter(ContextCompat.getColor(MeasuredCornerDrawingActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
    }

}
