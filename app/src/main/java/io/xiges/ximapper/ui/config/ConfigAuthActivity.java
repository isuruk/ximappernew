package io.xiges.ximapper.ui.config;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import io.xiges.ximapper.R;
import io.xiges.ximapper.databinding.ActivityConfigAuthBinding;
import io.xiges.ximapper.ui.login.LoginActivity;
import io.xiges.ximapper.ui.master_file.MasterFileGetActivity;

public class ConfigAuthActivity extends AppCompatActivity {

    private ActivityConfigAuthBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_config_auth);

        binding.btnSubmit.setOnClickListener(v->{
            String text = binding.edtPassword.getText().toString();
            if(text.equals("Xiges-Epic")){
                Intent i = new Intent(ConfigAuthActivity.this, ConfigActivity.class);
                startActivity(i);
                finish();
            }else {
                Toast.makeText(getApplicationContext(),"Invalid password",Toast.LENGTH_LONG).show();
            }
        });

    }
}
