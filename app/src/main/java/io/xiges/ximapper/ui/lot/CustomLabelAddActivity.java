package io.xiges.ximapper.ui.lot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityCustomLabelAddBinding;

public class CustomLabelAddActivity extends AppCompatActivity {

    private ActivityCustomLabelAddBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_custom_label_add);

        int type = getIntent().getIntExtra(Constant.TYPE,-1);
        int mode = getIntent().getIntExtra(Constant.MODE,-1);

        String value = "";
        int p = -1;
        if (mode == 2){
             value = getIntent().getStringExtra(Constant.DETAILS);
             p = getIntent().getIntExtra(Constant.ID,-1);
            binding.edtLabel.setText(value);
        }
        int finalP = p;
        binding.btnLabelAdd.setOnClickListener(v->{
            String label = binding.edtLabel.getText().toString();
            if((!label.equals("") || !label.isEmpty()) ) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra(Constant.NAME,label);
                returnIntent.putExtra(Constant.ID, finalP);
                returnIntent.putExtra(Constant.MODE,mode);
                returnIntent.putExtra(Constant.TYPE,1);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
            });
    }
}
