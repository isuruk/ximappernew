package io.xiges.ximapper.ui.helpers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;

import io.xiges.ximapper.R;
import io.xiges.ximapper.adapter.MultiSelectSpinnerAdapter;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityMultiSelectUiBinding;
import io.xiges.ximapper.model.MultiSelectSpinner;

public class MultiSelectUIActivity extends AppCompatActivity {
    private ActivityMultiSelectUiBinding binding;
    private MultiSelectSpinnerAdapter adapter;
    private String type = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_multi_select_ui);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.60);
        int screenHeight = (int) (metrics.heightPixels * 0.80);

        getWindow().setLayout(screenWidth, screenHeight);

        ArrayList<MultiSelectSpinner> spinnerData = getIntent().getParcelableArrayListExtra(Constant.DETAILS);
        type = getIntent().getStringExtra(Constant.TYPE);

        adapter = new MultiSelectSpinnerAdapter(spinnerData, this);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);
        adapter.setDetails(spinnerData);

        binding.btnOkay.setOnClickListener(v->{
            Intent returnIntent = new Intent();
            returnIntent.putParcelableArrayListExtra(Constant.DETAILS, adapter.getSpinner());
            returnIntent.putExtra(Constant.TYPE, type);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        });

    }
}
