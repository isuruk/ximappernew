package io.xiges.ximapper.ui.login;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import javax.inject.Inject;

import io.xiges.ximapper.BaseActivity;
import io.xiges.ximapper.BuildConfig;
import io.xiges.ximapper.R;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.dagger.AppComponent;
import io.xiges.ximapper.databinding.ActivityLoginBinding;
import io.xiges.ximapper.model.APIError;
import io.xiges.ximapper.model.api.LoginResponse;
import io.xiges.ximapper.network.ApiInterface;
import io.xiges.ximapper.ui.config.ConfigAuthActivity;
import io.xiges.ximapper.ui.master_file.MasterFileGetActivity;
import io.xiges.ximapper.util.ErrorUtils;
import io.xiges.ximapper.util.NetworkStatus;
import io.xiges.ximapper.util.SharedPreferencesHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity<LoginPresenter> implements LoginView {

    private ActivityLoginBinding binding;

    @Inject
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        ((XimapperApplication) getApplication()).getAppComponent().inject(this);
        binding.setHandler(this);

        binding.imageView2.setLongClickable(true);
        binding.imageView2.setOnLongClickListener(v->{
            Intent i = new Intent(LoginActivity.this, ConfigAuthActivity.class);
            startActivity(i);
            return true;
        });

        try {
            PackageInfo pInfo = LoginActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName + " | "+ BuildConfig.BuildTime;
            binding.tvVersion.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void initDependencies(AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    public void onLoginPressed() {
        binding.edtUserName.getText().toString();
        String password = binding.edtPassword.getText().toString();
        String userName = binding.edtUserName.getText().toString();


        login(userName, password);

    }

    private boolean isValidate(String userName, String password) {

        if (TextUtils.isEmpty(userName)) {
            return false;
        }


        if (TextUtils.isEmpty(password)) {
            return false;
        }

        return true;

    }

    private void login(String userName, String password) {
     /*BundleInfo bi = new BundleInfo();
        MasterFileListResponse masterFile = new MasterFileListResponse();
        Intent i = new Intent(LoginActivity.this, MapViewActivity.class);
        i.putExtra(Constant.MASTER_FILE, masterFile);
        i.putExtra(Constant.BUNDLE, bi);
        i.putExtra(Constant.MODE, 1);
        startActivity(i);
      */  if (new NetworkStatus().isConnected(LoginActivity.this)) {

                if (isValidate(userName, password)) {
                    String credentials = "doma:domapass";
                    final String basic =
                            "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    apiInterface.login(basic, userName, password, "password").enqueue(new Callback<LoginResponse>() {

                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            if (response.isSuccessful()) {

                                String accessToken = response.body().getAccess_token();
                                String refreshToken = response.body().getRefresh_token();
                                new SharedPreferencesHandler().saveIntoSharedPreferences(getApplicationContext(), accessToken, refreshToken);
                          //      new SharedPreferencesHandler().saveMasterSharedPreferences(getApplicationContext(), response.body());


                              /*  apiInterface.getMasterData("Bearer " + accessToken, "dama").enqueue(new Callback<MasterData>() {

                                    @Override
                                    public void onResponse(Call<MasterData> call, Response<MasterData> response) {
                                        if (response.isSuccessful()) {



                                        } else if (response.code() == 401) {
                                            showErrorMessage("Unauthorized");
                                        } else if (response.code() == 400) {
                                            showErrorMessage("Invalid Credentials");
                                        }else{
                                            Log.e("zxc",response.code()+"");
                                        APIError errorBody = ErrorUtils.parseError(response);
                                        showErrorMessage(errorBody.getError_description());
                                    }

                                    }

                                    @Override
                                    public void onFailure(Call<MasterData> call, Throwable t) {
                                        showErrorMessage("Error while logging");
                                    }
                                });*/

                                Intent i = new Intent(LoginActivity.this, MasterFileGetActivity.class);
                                startActivity(i);
                                finish();

                            } else if (response.code() == 401) {
                                showErrorMessage("Unauthorized");
                            } else if (response.code() == 400) {
                                showErrorMessage("Invalid Credentials");
                            }else{
                                Log.e("asd",response.code()+"");
                                APIError errorBody = ErrorUtils.parseError(response);
                                    showErrorMessage(errorBody.getError_description());
                                }

                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            showErrorMessage("Error while logging");
                        }
                    });
                } else {
                    showErrorMessage(getMessage(userName, password));
                }

        } else {
            showErrorMessage("No internet connection");
        }

    }

    private String getMessage(String userName, String password) {
        if (TextUtils.isEmpty(userName)) {
            return "Empty User Name";
        }


        if (TextUtils.isEmpty(password)) {
            return "Empty Password";
        }

        return "Something went wrong";
    }


    private void showErrorMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }
}
