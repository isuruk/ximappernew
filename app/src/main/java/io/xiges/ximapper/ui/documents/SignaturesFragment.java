package io.xiges.ximapper.ui.documents;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.adapter.RepresentitveSignDetailAdapter;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.FragmentSignaturesBinding;
import io.xiges.ximapper.model.cr.AcquisitionOfficerDetails;
import io.xiges.ximapper.model.cr.CR;
import io.xiges.ximapper.model.cr.ChiefValuerRepresentationDetails;
import io.xiges.ximapper.model.cr.GramaNiladhariDetails;
import io.xiges.ximapper.model.cr.OccupierDetails;
import io.xiges.ximapper.model.cr.RepresentativeDetail;


public class SignaturesFragment extends Fragment {

    private FragmentSignaturesBinding binding;
    private final int RESULT_CODE = 956;
    private final int RESULT_CODE_RP = 6458;

    private RepresentitveSignDetailAdapter adapter;
    private List<RepresentativeDetail> rep = new ArrayList<>();

    public SignaturesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_signatures, container, false);
        View view = binding.getRoot();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
        adapter = new RepresentitveSignDetailAdapter(rep, getActivity());

        rep = new ArrayList<>(cr.getSignatures().getRepresentativeDetails());

        binding.rcRepresentative.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rcRepresentative.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(binding.rcRepresentative.getContext(),
                LinearLayout.VERTICAL);
        binding.rcRepresentative.addItemDecoration(dividerItemDecoration);
        adapter.setDetails(rep);


        if (cr.getSignatures().getAcquisitionOfficerDetails() != null) {
            if (cr.getSignatures().getAcquisitionOfficerDetails().getSign() != null) {
                byte[] decodedString = Base64.decode(cr.getSignatures().getAcquisitionOfficerDetails().getSign(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                binding.acquiringOfficer.setImageBitmap(decodedByte);
            }
        }

        if (cr.getSignatures().getGramaNiladhariDetails() != null) {
            if (cr.getSignatures().getGramaNiladhariDetails().getSign() != null) {
                byte[] decodedString = Base64.decode(cr.getSignatures().getGramaNiladhariDetails().getSign(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                binding.padGramasewaka.setImageBitmap(decodedByte);
            }
        }

        if (cr.getSignatures().getChiefValuerRepresentationDetails() != null) {
            if (cr.getSignatures().getChiefValuerRepresentationDetails().getSign() != null) {
                byte[] decodedString = Base64.decode(cr.getSignatures().getChiefValuerRepresentationDetails().getSign(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                binding.padCheifAq.setImageBitmap(decodedByte);
            }
        }
        if (cr.getSignatures().getOccupierDetails() != null) {
            if (cr.getSignatures().getOccupierDetails().getSign() != null) {
                byte[] decodedString = Base64.decode(cr.getSignatures().getOccupierDetails().getSign(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                binding.padOccupier.setImageBitmap(decodedByte);
            }
        }

        binding.btnRp.setOnClickListener(v->{
            Intent intent = new Intent(getActivity(), RPSignPadActivity.class);
            intent.putExtra(Constant.MODE, 1);
            startActivityForResult(intent, RESULT_CODE_RP);
        });

        binding.acquiringOfficer.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SignPadActivity.class);
            CR c = ((CRDocumentActivity) getActivity()).getConditionalReport();

            if (c.getSignatures().getAcquisitionOfficerDetails() != null) {
                intent.putExtra(Constant.DETAILS, c.getSignatures().getAcquisitionOfficerDetails());
                intent.putExtra(Constant.MODE, 2);
            }else{
                intent.putExtra(Constant.MODE, 1);
            }
            intent.putExtra(Constant.TYPE, 1);
            startActivityForResult(intent, RESULT_CODE);
        });


        binding.padGramasewaka.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SignPadActivity.class);
            CR c = ((CRDocumentActivity) getActivity()).getConditionalReport();

            if (c.getSignatures().getGramaNiladhariDetails() != null) {
                intent.putExtra(Constant.DETAILS, c.getSignatures().getGramaNiladhariDetails());
                intent.putExtra(Constant.MODE, 2);
            }else {
                intent.putExtra(Constant.MODE, 1);
            }
            intent.putExtra(Constant.TYPE, 2);
            startActivityForResult(intent, RESULT_CODE);
        });


        binding.padCheifAq.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SignPadActivity.class);
            CR c = ((CRDocumentActivity) getActivity()).getConditionalReport();

            if (c.getSignatures().getChiefValuerRepresentationDetails() != null) {
                intent.putExtra(Constant.DETAILS, c.getSignatures().getChiefValuerRepresentationDetails());
                intent.putExtra(Constant.MODE, 2);
            }else {
                intent.putExtra(Constant.MODE, 1);
            }
             intent.putExtra(Constant.TYPE, 3);
            startActivityForResult(intent, RESULT_CODE);
        });


        binding.padOccupier.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SignPadActivity.class);
            CR c = ((CRDocumentActivity) getActivity()).getConditionalReport();
            if (c.getSignatures().getOccupierDetails() != null) {
                intent.putExtra(Constant.DETAILS, c.getSignatures().getOccupierDetails());
                intent.putExtra(Constant.MODE, 2);
            }else {
                intent.putExtra(Constant.MODE, 1);
            }
            intent.putExtra(Constant.TYPE, 4);
            startActivityForResult(intent, RESULT_CODE);
        });


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                int type = data.getIntExtra(Constant.TYPE, -1);

                if (type == 1) {
                    AcquisitionOfficerDetails acquisitionOfficerDetails = data.getParcelableExtra(Constant.DETAILS);
                    try {
                        ((CRDocumentActivity) getActivity()).updateAcquisitionOfficerDetails(acquisitionOfficerDetails);
                        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        if (cr.getSignatures().getAcquisitionOfficerDetails() != null) {
                            if (cr.getSignatures().getAcquisitionOfficerDetails().getSign() != null) {
                                byte[] decodedString = Base64.decode(cr.getSignatures().getAcquisitionOfficerDetails().getSign(), Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                binding.acquiringOfficer.setImageBitmap(decodedByte);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } else if (type == 2) {
                    GramaNiladhariDetails gramaNiladhariDetails = data.getParcelableExtra(Constant.DETAILS);
                    try {
                        ((CRDocumentActivity) getActivity()).updateGramaNiladhariDetails(gramaNiladhariDetails);
                        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        if (cr.getSignatures().getGramaNiladhariDetails() != null) {
                            if (cr.getSignatures().getGramaNiladhariDetails().getSign() != null) {
                                byte[] decodedString = Base64.decode(cr.getSignatures().getGramaNiladhariDetails().getSign(), Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                binding.padGramasewaka.setImageBitmap(decodedByte);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (type == 3) {
                    ChiefValuerRepresentationDetails chiefValuerRepresentationDetails = data.getParcelableExtra(Constant.DETAILS);
                    try {
                        ((CRDocumentActivity) getActivity()).updateChiefValuerRepresentationDetails(chiefValuerRepresentationDetails);
                        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        if (cr.getSignatures().getChiefValuerRepresentationDetails() != null) {
                            if (cr.getSignatures().getChiefValuerRepresentationDetails().getSign() != null) {
                                byte[] decodedString = Base64.decode(cr.getSignatures().getChiefValuerRepresentationDetails().getSign(), Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                binding.padCheifAq.setImageBitmap(decodedByte);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (type == 4) {
                    OccupierDetails occupierDetails = data.getParcelableExtra(Constant.DETAILS);
                    try {
                        ((CRDocumentActivity) getActivity()).updateOccupierDetails(occupierDetails);
                        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        if (cr.getSignatures().getOccupierDetails() != null) {
                            if (cr.getSignatures().getOccupierDetails().getSign() != null) {
                                byte[] decodedString = Base64.decode(cr.getSignatures().getOccupierDetails().getSign(), Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                binding.padOccupier.setImageBitmap(decodedByte);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                }
            } else if (requestCode == RESULT_CODE_RP) {
                if (resultCode == Activity.RESULT_OK) {
                    RepresentativeDetail representativeDetail = data.getParcelableExtra(Constant.DETAILS);
                    rep.add(representativeDetail);
                    try {
                        ((CRDocumentActivity) getActivity()).updateRepresentativeDetails(rep);
                        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                       adapter.setDetails(new ArrayList<>(rep));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

    }
}
