package io.xiges.ximapper.ui.documents;

public interface SignatureView {
    void onAqClearPressed();
    void onAqSavePressed();
    void onGramaClearPressed();
    void onGramaSavePressed();
}
