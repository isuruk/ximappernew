package io.xiges.ximapper.ui.marker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityMarkerMenuBinding;
import io.xiges.ximapper.model.Marker;
import io.xiges.ximapper.model.Marker_Type;
import io.xiges.ximapper.model.api.MasterFileListResponse;
import io.xiges.ximapper.ui.map.MapViewActivity;

public class MarkerMenuActivity extends AppCompatActivity {

    private ActivityMarkerMenuBinding binding;
    private int mode = 0;
    String lotId = "";
    MasterFileListResponse masterFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_marker_menu);

        Marker m = getIntent().getParcelableExtra(Constant.MARKER);
        mode = getIntent().getIntExtra(Constant.MODE,-1);
        lotId = getIntent().getStringExtra(Constant.LOT);
        masterFile = getIntent().getParcelableExtra(Constant.MASTER_FILE);

        boolean isLS = false;
        boolean isPV = false;
        boolean isRE = false;
        boolean isBR = false;

        try {
            if (m.getType().equals(Marker_Type.LS.name())) {
                isLS = true;
            } else if (m.getType().equals(Marker_Type.PV.name())) {
                isPV = true;
            } else if (m.getType().equals(Marker_Type.RE.name())) {
                isRE = true;
            } else if (m.getType().equals(Marker_Type.BR.name())) {
                isBR = true;
            }
        }catch (Exception e){
            Toast.makeText(this,"Invalid marker",Toast.LENGTH_LONG).show();
        }


        binding.btnMarkerInfo.setOnClickListener(v -> {
            mode = -2;
            Intent i = new Intent(MarkerMenuActivity.this, AddMarkerActivity.class);
            i.putExtra(Constant.MARKER,m);
            i.putExtra(Constant.MODE,1);
            startActivityForResult(i,MapViewActivity.ACTIVITY_MARKER_MENU_RESULT_CODE);
            binding.getRoot().setVisibility(View.GONE);

        });

        binding.btnMarkerDataUpdate.setOnClickListener(v -> {
            mode = -2;
            Intent i = new Intent(MarkerMenuActivity.this, AddMarkerActivity.class);
            i.putExtra(Constant.MARKER,m);
            i.putExtra(Constant.MODE,3);
            startActivityForResult(i,MapViewActivity.ACTIVITY_MARKER_MENU_RESULT_CODE);
            binding.getRoot().setVisibility(View.GONE);

        });



        if(!isLS){
            binding.btnLSData.setVisibility(View.GONE);
        }

        if(!isPV){
            binding.btnPVData.setVisibility(View.GONE);
        }

        if(!isRE){
            binding.btnREData.setVisibility(View.GONE);
        }

        if(!isBR){
            binding.btnBuildingData.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mode==-2){
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MapViewActivity.ACTIVITY_MARKER_MENU_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                mode = data.getIntExtra(Constant.MODE,-1);
                if(mode == -1 || mode == -2){
                    finish();
                }else{
                    Marker m = data.getParcelableExtra(Constant.MARKER);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(Constant.MARKER, m);
                    returnIntent.putExtra(Constant.MODE,mode);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            }
        }
    }
}
