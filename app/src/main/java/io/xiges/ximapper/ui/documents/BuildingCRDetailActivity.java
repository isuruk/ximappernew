package io.xiges.ximapper.ui.documents;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.xiges.ximapper.R;
import io.xiges.ximapper.adapter.OwnerDetailAdapter;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.databinding.ActivityBuildingCrdetailBinding;
import io.xiges.ximapper.model.MultiSelectSpinner;
import io.xiges.ximapper.model.api.master_data.Ceiling;
import io.xiges.ximapper.model.api.master_data.FinishersAndServicesBathroomAndToilet;
import io.xiges.ximapper.model.api.master_data.FinishersAndServicesFloorFinisher;
import io.xiges.ximapper.model.api.master_data.FinishersAndServicesService;
import io.xiges.ximapper.model.api.master_data.FinishersAndServicesWallFinisher;
import io.xiges.ximapper.model.api.master_data.FixtureAndFittingDoor;
import io.xiges.ximapper.model.api.master_data.FixtureAndFittingDoorsBathroomAndToiletFitting;
import io.xiges.ximapper.model.api.master_data.FixtureAndFittingDoorsHandRail;
import io.xiges.ximapper.model.api.master_data.FixtureAndFittingDoorsOther;
import io.xiges.ximapper.model.api.master_data.FixtureAndFittingDoorsPantryCupbord;
import io.xiges.ximapper.model.api.master_data.FixtureAndFittingWindow;
import io.xiges.ximapper.model.api.master_data.FixtureAndFittingWindowProtection;
import io.xiges.ximapper.model.api.master_data.FloorStructure;
import io.xiges.ximapper.model.api.master_data.FoundationStructure;
import io.xiges.ximapper.model.api.master_data.MasterData;
import io.xiges.ximapper.model.api.master_data.RoofFinisher;
import io.xiges.ximapper.model.api.master_data.RoofFrame;
import io.xiges.ximapper.model.api.master_data.RoofMaterial;
import io.xiges.ximapper.model.api.master_data.WallStructure;
import io.xiges.ximapper.model.building_details.BuildingDetailsPlaceholders;
import io.xiges.ximapper.model.cr.Building;
import io.xiges.ximapper.model.cr.BuildingDetails;
import io.xiges.ximapper.model.cr.CR;
import io.xiges.ximapper.model.cr.FinishersAndServices;
import io.xiges.ximapper.model.cr.FixtureAndFitting;
import io.xiges.ximapper.model.cr.OwnerDetail;
import io.xiges.ximapper.ui.alertboxes.ConfirmationDialogActivity;
import io.xiges.ximapper.ui.helpers.MultiSelectUIActivity;
import io.xiges.ximapper.util.DataManager;

public class BuildingCRDetailActivity extends AppCompatActivity implements RecyclerViewClickListener {

    private ActivityBuildingCrdetailBinding binding;
    private boolean isUpdate = false;
    private CR conditionalReport;

    private List<Integer> roofFrameItems = new ArrayList<>();
    private List<Integer> ceilingItems = new ArrayList<>();
    private List<Integer> roofMaterialItems = new ArrayList<>();
    private List<Integer> roofFinisherItems = new ArrayList<>();

    private List<Integer> foundationStructureItems = new ArrayList<>();
    private List<Integer> wallStructureItems = new ArrayList<>();
    private List<Integer> floorStructureItems = new ArrayList<>();

    private List<Integer> fixtureAndFittingDoorItems = new ArrayList<>();
    private List<Integer> fixtureAndFittingWindowsItems = new ArrayList<>();
    private List<Integer> fixtureAndFittingDoorsHandRailItems = new ArrayList<>();
    private List<Integer> fixtureAndFittingDoorsBathroomAndToiletFittingItems = new ArrayList<>();
    private List<Integer> fixtureAndFittingDoorsPantryCupbordItems = new ArrayList<>();
    private List<Integer> fixtureAndFittingWindowProtectionItems = new ArrayList<>();
    private List<Integer> fixtureAndFittingDoorsOtherItems = new ArrayList<>();


    private List<Integer> finishersAndServicesWallFinisherItems = new ArrayList<>();
    private List<Integer> finishersAndServicesFloorFinisherItems = new ArrayList<>();
    private List<Integer> finishersAndServicesBathroomAndToiletItems = new ArrayList<>();
    private List<Integer> finishersAndServicesServicesItems = new ArrayList<>();


    private final int REQUEST_CODE = 324;
    private final int REQUEST_CODE_MSS = 554;
    private final int RESULT_CODE = 747;
    private int position = -1;
    private int p = -1;
    private OwnerDetailAdapter ownerDetailAdapter;
    private List<OwnerDetail> ownerDetails;


    ArrayList<MultiSelectSpinner> foundationStructureList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> floorStructureList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> wallStructureList = new ArrayList<>();

    ArrayList<MultiSelectSpinner> roofMaterialList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> roofFinisherList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> roofFrameList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> ceilingList = new ArrayList<>();

    ArrayList<MultiSelectSpinner> fixtureAndFittingDoorList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> fixtureAndFittingWindowsList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> fixtureAndFittingDoorsHandRailList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> fixtureAndFittingDoorsBathroomAndToiletFittingsList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> fixtureAndFittingDoorsPantryCupbordList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> fixtureAndFittingWindowProtectionList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> fixtureAndFittingDoorsOtherList = new ArrayList<>();

    ArrayList<MultiSelectSpinner> finishersAndServicesWallFinisherList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> finishersAndServicesFloorFinisherList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> finishersAndServicesBathroomAndToiletList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> finishersAndServicesServicesList = new ArrayList<>();


    private DataManager dataManager = new DataManager();
    private String landName = "Test";
    private String lotName = "Test";
    private Building building;
    @Inject
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_building_crdetail);
        BuildingDetailsPlaceholders bdp = new BuildingDetailsPlaceholders();
        binding.setPlaceholder(bdp);


        ((XimapperApplication) getApplication()).getAppComponent().inject(this);

        building = getIntent().getParcelableExtra(Constant.DETAILS);

        landName = getIntent().getStringExtra(Constant.MASTER_FILE);
        lotName = getIntent().getStringExtra(Constant.LOT);

        p = getIntent().getIntExtra(Constant.CONFIG, -1);


        try {
            conditionalReport = dataManager.getCRData(landName, lotName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);
        int screenHeight = (int) (metrics.heightPixels * 0.80);

        getWindow().setLayout(screenWidth, screenHeight);

        boolean mode = getIntent().getBooleanExtra(Constant.MODE, false);

        Gson gson = new Gson();
        String json = prefs.getString(Constant.MASTER_DATA, "");
        MasterData masterData = gson.fromJson(json, MasterData.class);

        String[] buildingCategoriesArray = new String[masterData.getBuilding().getBuildingDetails().getBuildingCategory().size()];
        HashMap<Integer, Integer> buildingCategoriesMap = new HashMap<>();
        for (int i = 0; i < masterData.getBuilding().getBuildingDetails().getBuildingCategory().size(); i++) {
            buildingCategoriesMap.put(i, masterData.getBuilding().getBuildingDetails().getBuildingCategory().get(i).getId());
            buildingCategoriesArray[i] = masterData.getBuilding().getBuildingDetails().getBuildingCategory().get(i).getDescription();
        }


        String[] aqPotionArray = new String[masterData.getBuilding().getBuildingDetails().getBuildingAcquiredPortion().size()];
        HashMap<Integer, Integer> aqPotionMap = new HashMap<>();
        for (int i = 0; i < masterData.getBuilding().getBuildingDetails().getBuildingAcquiredPortion().size(); i++) {
            aqPotionMap.put(i, masterData.getBuilding().getBuildingDetails().getBuildingAcquiredPortion().get(i).getId());
            aqPotionArray[i] = masterData.getBuilding().getBuildingDetails().getBuildingAcquiredPortion().get(i).getDescription();
        }

        String[] conditionArray = new String[masterData.getBuilding().getBuildingDetails().getTypeOfCondition().size()];
        HashMap<Integer, Integer> conditionMap = new HashMap<>();
        for (int i = 0; i < masterData.getBuilding().getBuildingDetails().getTypeOfCondition().size(); i++) {
            conditionMap.put(i, masterData.getBuilding().getBuildingDetails().getTypeOfCondition().get(i).getId());
            conditionArray[i] = masterData.getBuilding().getBuildingDetails().getTypeOfCondition().get(i).getDescription();
        }


        String[] buildingClassArray = new String[masterData.getBuilding().getBuildingDetails().getBuildingClasss().size()];
        HashMap<Integer, Integer> buildingClassMap = new HashMap<>();
        for (int i = 0; i < masterData.getBuilding().getBuildingDetails().getBuildingClasss().size(); i++) {
            buildingClassMap.put(i, masterData.getBuilding().getBuildingDetails().getBuildingClasss().get(i).getId());
            buildingClassArray[i] = masterData.getBuilding().getBuildingDetails().getBuildingClasss().get(i).getBuildingClass();
        }

        String[] buildingNatureArray = new String[masterData.getBuilding().getBuildingDetails().getBuildingNatureOfConstruction().size()];
        HashMap<Integer, Integer> buildingNatureMap = new HashMap<>();
        for (int i = 0; i < masterData.getBuilding().getBuildingDetails().getBuildingNatureOfConstruction().size(); i++) {
            buildingNatureMap.put(i, masterData.getBuilding().getBuildingDetails().getBuildingNatureOfConstruction().get(i).getId());
            buildingNatureArray[i] = masterData.getBuilding().getBuildingDetails().getBuildingNatureOfConstruction().get(i).getDescription();
        }


        binding.edtFixtureAndFittingDoorsHandRail.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, fixtureAndFittingDoorsHandRailList);
            i.putExtra(Constant.TYPE, "FixtureAndFittingDoorsHandRail");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });

        ArrayAdapter<String> buildingCategoriesAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, buildingCategoriesArray);
        buildingCategoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.edtBuildingCategory.setAdapter(buildingCategoriesAdapter);

        ArrayAdapter<String> aqPotionAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, aqPotionArray);
        aqPotionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.edtAqPotion.setAdapter(aqPotionAdapter);

        ArrayAdapter<String> conditionAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, conditionArray);
        conditionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.editCondition.setAdapter(conditionAdapter);

        ArrayAdapter<String> buildingClassAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, buildingClassArray);
        buildingClassAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.edtBuildingClass.setAdapter(buildingClassAdapter);

        ArrayAdapter<String> buildingNatureAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, buildingNatureArray);
        buildingNatureAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.edtNatureOfBuilding.setAdapter(buildingNatureAdapter);


        for (FoundationStructure item : masterData.getBuilding().getStructure().getFoundationStructure()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            foundationStructureList.add(mss);
        }

        binding.edtFoundationStructure.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, foundationStructureList);
            i.putExtra(Constant.TYPE, "FoundationStructure");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (FloorStructure item : masterData.getBuilding().getStructure().getFloorStructure()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            floorStructureList.add(mss);
        }

        binding.edtFloorStructure.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, floorStructureList);
            i.putExtra(Constant.TYPE, "FloorStructure");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (WallStructure item : masterData.getBuilding().getStructure().getWallStructure()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            wallStructureList.add(mss);
        }

        binding.edtWallStructure.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, wallStructureList);
            i.putExtra(Constant.TYPE, "WallStructure");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });

        for (RoofMaterial item : masterData.getBuilding().getRoof().getRoofMaterial()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            roofMaterialList.add(mss);
        }

        binding.edtRoofRoofMaterial.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, roofMaterialList);
            i.putExtra(Constant.TYPE, "RoofRoofMaterial");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (RoofFinisher item : masterData.getBuilding().getRoof().getRoofFinisher()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            roofFinisherList.add(mss);
        }

        binding.edtRoofFinisher.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, roofFinisherList);
            i.putExtra(Constant.TYPE, "RoofFinisher");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (RoofFrame item : masterData.getBuilding().getRoof().getRoofFrame()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            roofFrameList.add(mss);
        }

        binding.edtRoofFrame.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, roofFrameList);
            i.putExtra(Constant.TYPE, "RoofFrame");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (Ceiling item : masterData.getBuilding().getRoof().getCeiling()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            ceilingList.add(mss);
        }

        binding.edtCelling.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, ceilingList);
            i.putExtra(Constant.TYPE, "Celling");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (FixtureAndFittingDoor item : masterData.getBuilding().getFixtureAndFitting().getFixtureAndFittingDoor()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            fixtureAndFittingDoorList.add(mss);
        }

        binding.edtFixtureAndFittingDoor.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, fixtureAndFittingDoorList);
            i.putExtra(Constant.TYPE, "FixtureAndFittingDoor");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (FixtureAndFittingWindow item : masterData.getBuilding().getFixtureAndFitting().getFixtureAndFittingWindows()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            fixtureAndFittingWindowsList.add(mss);
        }


        binding.edtFixtureAndFittingWindows.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, fixtureAndFittingWindowsList);
            i.putExtra(Constant.TYPE, "FixtureAndFittingWindows");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (FixtureAndFittingDoorsBathroomAndToiletFitting item : masterData.getBuilding().getFixtureAndFitting().getFixtureAndFittingDoorsBathroomAndToiletFittings()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            fixtureAndFittingDoorsBathroomAndToiletFittingsList.add(mss);
        }

        binding.edtFixtureAndFittingDoorsBathroomAndToiletFittings.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, fixtureAndFittingDoorsBathroomAndToiletFittingsList);
            i.putExtra(Constant.TYPE, "FixtureAndFittingDoorsBathroomAndToiletFittings");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (FixtureAndFittingDoorsPantryCupbord item : masterData.getBuilding().getFixtureAndFitting().getFixtureAndFittingDoorsPantryCupbord()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            fixtureAndFittingDoorsPantryCupbordList.add(mss);
        }

        binding.edtFixtureAndFittingDoorsPantryCupbord.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, fixtureAndFittingDoorsPantryCupbordList);
            i.putExtra(Constant.TYPE, "FixtureAndFittingDoorsPantryCupbord");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (FixtureAndFittingWindowProtection item : masterData.getBuilding().getFixtureAndFitting().getFixtureAndFittingWindowProtection()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            fixtureAndFittingWindowProtectionList.add(mss);
        }

        binding.edtFixtureAndFittingWindowProtection.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, fixtureAndFittingWindowProtectionList);
            i.putExtra(Constant.TYPE, "FixtureAndFittingWindowProtection");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (FixtureAndFittingDoorsOther item : masterData.getBuilding().getFixtureAndFitting().getFixtureAndFittingDoorsOther()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            fixtureAndFittingDoorsOtherList.add(mss);
        }

        binding.edtFixtureAndFittingDoorsOther.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, fixtureAndFittingDoorsOtherList);
            i.putExtra(Constant.TYPE, "FixtureAndFittingDoorsOther");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        ownerDetails = new ArrayList<>();
        ownerDetailAdapter = new OwnerDetailAdapter(ownerDetails, getApplicationContext(), this);


        binding.rcOwnerDetails.setLayoutManager(new LinearLayoutManager(this));
        binding.rcOwnerDetails.setAdapter(ownerDetailAdapter);


        ownerDetails = new ArrayList<>(building.getOwnerDetails());

        ownerDetailAdapter.setDetails(ownerDetails);


        binding.rcOwnerDetails.setNestedScrollingEnabled(false);


        for (FinishersAndServicesWallFinisher item : masterData.getBuilding().getFinishersAndServices().getFinishersAndServicesWallFinisher()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            finishersAndServicesWallFinisherList.add(mss);
        }

        binding.edtFinishersAndServicesWallFinisher.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, finishersAndServicesWallFinisherList);
            i.putExtra(Constant.TYPE, "FinishersAndServicesWallFinisher");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (FinishersAndServicesFloorFinisher item : masterData.getBuilding().getFinishersAndServices().getFinishersAndServicesFloorFinisher()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            finishersAndServicesFloorFinisherList.add(mss);
        }

        binding.edtFinishersAndServicesFloorFinisher.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, finishersAndServicesFloorFinisherList);
            i.putExtra(Constant.TYPE, "FinishersAndServicesFloorFinisher");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (FinishersAndServicesBathroomAndToilet item : masterData.getBuilding().getFinishersAndServices().getFinishersAndServicesBathroomAndToilet()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            finishersAndServicesBathroomAndToiletList.add(mss);
        }

        binding.edtFinishersAndServicesBathroomAndToilet.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, finishersAndServicesBathroomAndToiletList);
            i.putExtra(Constant.TYPE, "FinishersAndServicesBathroomAndToilet");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        for (FinishersAndServicesService item : masterData.getBuilding().getFinishersAndServices().getFinishersAndServicesServices()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            finishersAndServicesServicesList.add(mss);
        }

        binding.edtFinishersAndServicesServices.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, finishersAndServicesServicesList);
            i.putExtra(Constant.TYPE, "FinishersAndServicesServices");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });


        changeMode(mode);

        BuildingDetailsPlaceholders bd = new BuildingDetailsPlaceholders();

        binding.btnAddOwnerDetails.setOnClickListener(v -> {
            Intent i = new Intent(BuildingCRDetailActivity.this, BuildingOwnerDetailsActivity.class);
            i.putExtra(Constant.MODE, true);
            startActivityForResult(i, REQUEST_CODE);
        });

        if (building != null) {

            foundationStructureList = new ArrayList<>();
            floorStructureList = new ArrayList<>();
            wallStructureList = new ArrayList<>();

            roofMaterialList = new ArrayList<>();
            roofFinisherList = new ArrayList<>();
            roofFrameList = new ArrayList<>();
            ceilingList = new ArrayList<>();

            fixtureAndFittingDoorList = new ArrayList<>();
            fixtureAndFittingWindowsList = new ArrayList<>();
            fixtureAndFittingDoorsHandRailList = new ArrayList<>();
            fixtureAndFittingDoorsBathroomAndToiletFittingsList = new ArrayList<>();
            fixtureAndFittingDoorsPantryCupbordList = new ArrayList<>();
            fixtureAndFittingWindowProtectionList = new ArrayList<>();
            fixtureAndFittingDoorsOtherList = new ArrayList<>();

            finishersAndServicesWallFinisherList = new ArrayList<>();
            finishersAndServicesFloorFinisherList = new ArrayList<>();
            finishersAndServicesBathroomAndToiletList = new ArrayList<>();
            finishersAndServicesServicesList = new ArrayList<>();

            binding.setBuilding(building);
            binding.edtBuildingNo.setText(building.getNo() + "");
            binding.edtBuildingName.setText(building.getName() + "");

            fixtureAndFittingDoorsHandRailList = new ArrayList<>();
            for (FixtureAndFittingDoorsHandRail item : masterData.getBuilding().getFixtureAndFitting().getFixtureAndFittingDoorsHandRail()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getFixtureAndFitting().getFixtureAndFittingDoorsHandRail().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtFixtureAndFittingDoorsHandRail.setText("Multiple Items Selected");
                } else {
                    mss.setIschecked(false);
                }
                fixtureAndFittingDoorsHandRailList.add(mss);
            }


            for (RoofFinisher item : masterData.getBuilding().getRoof().getRoofFinisher()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getRoof().getRoofFinisher().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtRoofRoofMaterial.setText("Multiple Items Selected");
                } else {
                    mss.setIschecked(false);
                }
                roofFinisherList.add(mss);
            }


            for (RoofMaterial item : masterData.getBuilding().getRoof().getRoofMaterial()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                mss.setIschecked(false);
                if (building.getRoof().getRoofMaterial().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtRoofRoofMaterial.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                roofMaterialList.add(mss);
            }


            for (WallStructure item : masterData.getBuilding().getStructure().getWallStructure()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getStructure().getWallStructure().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtWallStructure.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                wallStructureList.add(mss);
            }


            for (FloorStructure item : masterData.getBuilding().getStructure().getFloorStructure()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getFloorStructure().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtFloorStructure.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                floorStructureList.add(mss);
            }

            foundationStructureList = new ArrayList<>();
            for (FoundationStructure item : masterData.getBuilding().getStructure().getFoundationStructure()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getStructure().getFoundationStructure().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtFoundationStructure.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                foundationStructureList.add(mss);
            }


            for (FinishersAndServicesService item : masterData.getBuilding().getFinishersAndServices().getFinishersAndServicesServices()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getFinishersAndServices().getFinishersAndServicesServices().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtFinishersAndServicesServices.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                finishersAndServicesServicesList.add(mss);
            }


            for (FinishersAndServicesBathroomAndToilet item : masterData.getBuilding().getFinishersAndServices().getFinishersAndServicesBathroomAndToilet()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getFinishersAndServices().getFinishersAndServicesBathroomAndToilet().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtFinishersAndServicesBathroomAndToilet.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                finishersAndServicesBathroomAndToiletList.add(mss);
            }

            for (FinishersAndServicesFloorFinisher item : masterData.getBuilding().getFinishersAndServices().getFinishersAndServicesFloorFinisher()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getFinishersAndServices().getFinishersAndServicesFloorFinisher().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtFinishersAndServicesFloorFinisher.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                finishersAndServicesFloorFinisherList.add(mss);
            }

            for (FinishersAndServicesWallFinisher item : masterData.getBuilding().getFinishersAndServices().getFinishersAndServicesWallFinisher()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getFinishersAndServices().getFinishersAndServicesWallFinisher().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtFinishersAndServicesWallFinisher.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                finishersAndServicesWallFinisherList.add(mss);
            }

            for (FixtureAndFittingDoorsOther item : masterData.getBuilding().getFixtureAndFitting().getFixtureAndFittingDoorsOther()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getFixtureAndFitting().getFixtureAndFittingDoorsOther().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtFixtureAndFittingDoor.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                fixtureAndFittingDoorsOtherList.add(mss);
            }


            for (FixtureAndFittingWindowProtection item : masterData.getBuilding().getFixtureAndFitting().getFixtureAndFittingWindowProtection()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getFixtureAndFitting().getFixtureAndFittingWindowProtection().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtFixtureAndFittingWindowProtection.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                fixtureAndFittingWindowProtectionList.add(mss);
            }


            for (FixtureAndFittingDoorsPantryCupbord item : masterData.getBuilding().getFixtureAndFitting().getFixtureAndFittingDoorsPantryCupbord()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getFixtureAndFitting().getFixtureAndFittingDoorsPantryCupbord().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtFixtureAndFittingDoorsPantryCupbord.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                fixtureAndFittingDoorsPantryCupbordList.add(mss);
            }

            for (FixtureAndFittingDoorsBathroomAndToiletFitting item : masterData.getBuilding().getFixtureAndFitting().getFixtureAndFittingDoorsBathroomAndToiletFittings()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getFixtureAndFitting().getFixtureAndFittingDoorsBathroomAndToiletFittings().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtFixtureAndFittingDoorsBathroomAndToiletFittings.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                fixtureAndFittingDoorsBathroomAndToiletFittingsList.add(mss);
            }

            for (FixtureAndFittingWindow item : masterData.getBuilding().getFixtureAndFitting().getFixtureAndFittingWindows()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getFixtureAndFitting().getFixtureAndFittingWindows().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtFixtureAndFittingWindows.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                fixtureAndFittingWindowsList.add(mss);
            }

            for (FixtureAndFittingDoor item : masterData.getBuilding().getFixtureAndFitting().getFixtureAndFittingDoor()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                mss.setIschecked(false);
                if (building.getFixtureAndFitting().getFixtureAndFittingDoor().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtFixtureAndFittingDoor.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                fixtureAndFittingDoorList.add(mss);
            }


            for (Ceiling item : masterData.getBuilding().getRoof().getCeiling()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getRoof().getCeiling().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtCelling.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                ceilingList.add(mss);
            }

            for (RoofFrame item : masterData.getBuilding().getRoof().getRoofFrame()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (building.getRoof().getRoofFrame().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtRoofFrame.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                roofFrameList.add(mss);
            }

            if (building.getBuildingCategory() >= 0) {
                for (Map.Entry e : buildingCategoriesMap.entrySet()) {
                    if (((int) e.getValue()) == building.getBuildingCategory()) {
                        binding.edtBuildingCategory.setSelection((int) e.getKey());
                        break;
                    }
                }

            }

            if (building.getAcquiredPotion() >= 0) {


                for (Map.Entry e : aqPotionMap.entrySet()) {
                    if (((int) e.getValue()) == building.getAcquiredPotion()) {
                        binding.edtAqPotion.setSelection((int) e.getKey());
                        break;
                    }
                }

            }

            if (building.getTypeOfCondition() >= 0) {
                for (Map.Entry e : conditionMap.entrySet()) {
                    if (((int) e.getValue()) == building.getTypeOfCondition()) {
                        binding.editCondition.setSelection((int) e.getKey());
                        break;
                    }
                }
            }


            if (building.getBuildingClass() >= 0) {
                for (Map.Entry e : buildingClassMap.entrySet()) {
                    if (((int) e.getValue()) == building.getBuildingClass()) {
                        binding.edtBuildingClass.setSelection((int) e.getKey());
                        break;
                    }
                }
            }

            if (building.getBuildingNatureOfConstruction() >= 0) {
                for (Map.Entry e : buildingNatureMap.entrySet()) {
                    if (((int) e.getValue()) == building.getBuildingNatureOfConstruction()) {
                        binding.edtNatureOfBuilding.setSelection((int) e.getKey());
                        break;
                    }
                }
            }

        }

        binding.setPlaceholder(bd);

        binding.btnUpdate.setOnClickListener(v -> {
            changeMode(true);
            isUpdate = true;
        });
        //   DisplayMetrics displayMetrics = new DisplayMetrics();
        //     BuildingCRDetailActivity.this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // int width = displayMetrics.widthPixels / 6;
/*
        SwipeHelper swipeHelper = new SwipeHelper(this, binding.rcOwnerDetails, width) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new SwipeHelper.UnderlayButton(getApplicationContext(),
                        "Delete",
                        0,
                        ContextCompat.getColor(getApplicationContext(), R.color.colorRed),
                        pos -> {
                            position = pos;
                            Intent i = new Intent(BuildingCRDetailActivity.this, ConfirmationDialogActivity.class);
                            i.putExtra(Constant.NAME, "Are you sure you want to delete this item?");
                            startActivityForResult(i, RESULT_CODE);
                        }
                ));
            }
        };

        swipeHelper.attachSwipe();*/


        binding.btnCancel.setOnClickListener(v -> finish());

        binding.btnSaveBbuttonDetails.setOnClickListener(v -> {
            Building building = new Building();
            building.setNo(binding.edtBuildingNo.getText().toString());
            building.setName(binding.edtBuildingName.getText().toString());
            building.setName(binding.edtBuildingName.getText().toString());

            try {
                int catId = buildingCategoriesMap.get(binding.edtBuildingCategory.getSelectedItemPosition());
                building.setBuildingCategory(catId);
            } catch (Exception e) {
                building.setBuildingCategory(-1);
            }
            try {

                int conditionId = conditionMap.get(binding.editCondition.getSelectedItemPosition());
                building.setTypeOfCondition(conditionId);
            } catch (Exception e) {
                building.setTypeOfCondition(-1);
            }
            try {

                int potionId = aqPotionMap.get(binding.edtAqPotion.getSelectedItemPosition());
                building.setAcquiredPotion(potionId);
            } catch (Exception e) {
                building.setAcquiredPotion(-1);
            }
            try {

                int classId = buildingClassMap.get(binding.edtBuildingClass.getSelectedItemPosition());
                building.setBuildingClass(classId);
            } catch (Exception e) {
                building.setBuildingClass(-1);
            }

            String floors = binding.edtFloors.getText().toString();
            try {

                int natureId = buildingNatureMap.get(binding.edtNatureOfBuilding.getSelectedItemPosition());
                building.setBuildingNatureOfConstruction(natureId);
            } catch (Exception e) {
                building.setBuildingNatureOfConstruction(-1);
            }
            String lifePr = binding.edtExpectedLife.getText().toString();

            building.setExpectedLifePeriod(lifePr);

            building.setType(binding.edtBuildingType.getText().toString());
            building.setAge(binding.edtAge.getText().toString());
            building.setTotalFloorArea(binding.edtTotalFloorArea.getText().toString());

            String minusFloors = binding.edtMinusFloors.getText().toString();

            try {
                building.setFloors(Integer.parseInt(floors));
            } catch (NumberFormatException e) {
                Toast.makeText(this, "Invalid Floors", Toast.LENGTH_LONG).show();
            }

            try {
                building.setMinusFloors(Integer.parseInt(minusFloors));
            } catch (NumberFormatException e) {
                Toast.makeText(this, "Invalid Floors", Toast.LENGTH_LONG).show();
            }


            io.xiges.ximapper.model.cr.Roof roofData = new io.xiges.ximapper.model.cr.Roof();
            roofData.setCeiling(ceilingItems);
            roofData.setRoofFinisher(roofFinisherItems);
            roofData.setRoofFrame(roofFrameItems);
            roofData.setRoofMaterial(roofMaterialItems);

            building.setRoof(roofData);

            io.xiges.ximapper.model.cr.Structure structureData = new io.xiges.ximapper.model.cr.Structure();
            structureData.setFoundationStructure(foundationStructureItems);
            structureData.setWallStructure(wallStructureItems);

            building.setStructure(structureData);



            for (MultiSelectSpinner ms : fixtureAndFittingDoorsHandRailList) {
                if (ms.isIschecked()) {
                    fixtureAndFittingDoorsHandRailItems.add(ms.getId());
                }
            }

            for (MultiSelectSpinner ms : foundationStructureList) {
                if (ms.isIschecked()) {
                    foundationStructureItems.add(ms.getId());
                }
            }


            for (MultiSelectSpinner ms : floorStructureList) {
                if (ms.isIschecked()) {
                    floorStructureItems.add(ms.getId());
                }
            }


            for (MultiSelectSpinner ms : wallStructureList) {
                if (ms.isIschecked()) {
                    wallStructureItems.add(ms.getId());
                }
            }

            for (MultiSelectSpinner ms : roofMaterialList) {
                if (ms.isIschecked()) {
                    roofMaterialItems.add(ms.getId());
                }
            }
            for (MultiSelectSpinner ms : roofFinisherList) {
                if (ms.isIschecked()) {
                    roofFinisherItems.add(ms.getId());
                }
            }

            for (MultiSelectSpinner ms : roofFrameList) {
                if (ms.isIschecked()) {
                    roofFrameItems.add(ms.getId());
                }
            }

            for (MultiSelectSpinner ms : ceilingList) {
                if (ms.isIschecked()) {
                    ceilingItems.add(ms.getId());
                }
            }

            for (MultiSelectSpinner ms : fixtureAndFittingDoorList) {
                if (ms.isIschecked()) {
                    fixtureAndFittingDoorItems.add(ms.getId());
                }
            }

            for (MultiSelectSpinner ms : fixtureAndFittingWindowsList) {
                if (ms.isIschecked()) {
                    fixtureAndFittingWindowsItems.add(ms.getId());
                }
            }

            for (MultiSelectSpinner ms : fixtureAndFittingDoorsBathroomAndToiletFittingsList) {
                if (ms.isIschecked()) {
                    fixtureAndFittingDoorsBathroomAndToiletFittingItems.add(ms.getId());
                }
            }


            for (MultiSelectSpinner ms : fixtureAndFittingDoorsPantryCupbordList) {
                if (ms.isIschecked()) {
                    fixtureAndFittingDoorsPantryCupbordItems.add(ms.getId());
                }
            }

            for (MultiSelectSpinner ms : fixtureAndFittingWindowProtectionList) {
                if (ms.isIschecked()) {
                    fixtureAndFittingWindowProtectionItems.add(ms.getId());
                }
            }

            for (MultiSelectSpinner ms : fixtureAndFittingDoorsOtherList) {
                if (ms.isIschecked()) {
                    fixtureAndFittingDoorsOtherItems.add(ms.getId());
                }
            }


            for (MultiSelectSpinner ms : finishersAndServicesWallFinisherList) {
                if (ms.isIschecked()) {
                    finishersAndServicesWallFinisherItems.add(ms.getId());
                }
            }

            for (MultiSelectSpinner ms : finishersAndServicesFloorFinisherList) {
                if (ms.isIschecked()) {
                    finishersAndServicesFloorFinisherItems.add(ms.getId());
                }
            }

            for (MultiSelectSpinner ms : finishersAndServicesBathroomAndToiletList) {
                if (ms.isIschecked()) {
                    finishersAndServicesBathroomAndToiletItems.add(ms.getId());
                }
            }

            for (MultiSelectSpinner ms : finishersAndServicesServicesList) {
                if (ms.isIschecked()) {
                    finishersAndServicesServicesItems.add(ms.getId());
                }
            }


            io.xiges.ximapper.model.cr.FixtureAndFitting fixtureAndFitting = new FixtureAndFitting();
            fixtureAndFitting.setFixtureAndFittingDoor(fixtureAndFittingDoorItems);
            fixtureAndFitting.setFixtureAndFittingDoorsBathroomAndToiletFittings(fixtureAndFittingDoorsBathroomAndToiletFittingItems);
            fixtureAndFitting.setFixtureAndFittingDoorsHandRail(fixtureAndFittingDoorsHandRailItems);
            fixtureAndFitting.setFixtureAndFittingDoorsOther(fixtureAndFittingDoorsOtherItems);
            fixtureAndFitting.setFixtureAndFittingDoorsPantryCupbord(fixtureAndFittingDoorsPantryCupbordItems);
            fixtureAndFitting.setFixtureAndFittingWindowProtection(fixtureAndFittingWindowProtectionItems);
            fixtureAndFitting.setFixtureAndFittingWindows(fixtureAndFittingWindowsItems);
            building.setFixtureAndFitting(fixtureAndFitting);



            building.setFloorStructure(new ArrayList<>(floorStructureItems));


            io.xiges.ximapper.model.cr.FinishersAndServices finishersAndServices = new FinishersAndServices();
            finishersAndServices.setFinishersAndServicesBathroomAndToilet(finishersAndServicesBathroomAndToiletItems);
            finishersAndServices.setFinishersAndServicesFloorFinisher(finishersAndServicesFloorFinisherItems);
            finishersAndServices.setFinishersAndServicesServices(finishersAndServicesServicesItems);
            finishersAndServices.setFinishersAndServicesWallFinisher(finishersAndServicesWallFinisherItems);
            building.setFinishersAndServices(finishersAndServices);


            building.setOwnerDetails(new ArrayList<>(ownerDetails));

            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constant.DETAILS, building);
            returnIntent.putExtra(Constant.TYPE, isUpdate);
            setResult(Activity.RESULT_OK, returnIntent);
            Toast.makeText(this, "Saved..", Toast.LENGTH_SHORT).show();
            finish();

        });
    }


    private void changeMode(boolean editable) {
        if (editable) {
            binding.edtFloors.setEnabled(true);
            binding.editCondition.setEnabled(true);
            binding.edtAqPotion.setEnabled(true);
            binding.edtBuildingCategory.setEnabled(true);
            binding.edtBuildingNo.setEnabled(false);
            binding.edtBuildingName.setEnabled(false);

            binding.edtBuildingClass.setEnabled(true);
            binding.edtNatureOfBuilding.setEnabled(true);

            binding.rcOwnerDetails.setEnabled(true);

            binding.edtRoofRoofMaterial.setEnabled(true);
            binding.edtRoofFrame.setEnabled(true);
            binding.edtRoofFinisher.setEnabled(true);
            binding.edtCelling.setEnabled(true);

            binding.edtFloorStructure.setEnabled(true);
            binding.edtFoundationStructure.setEnabled(true);
            binding.edtWallStructure.setEnabled(true);
            binding.edtExpectedLife.setEnabled(true);
            binding.edtMinusFloors.setEnabled(true);

            binding.edtTotalFloorArea.setEnabled(true);

            binding.edtFixtureAndFittingDoor.setEnabled(true);
            binding.edtFixtureAndFittingDoorsBathroomAndToiletFittings.setEnabled(true);
            binding.edtFixtureAndFittingDoorsHandRail.setEnabled(true);
            binding.edtFixtureAndFittingDoorsPantryCupbord.setEnabled(true);
            binding.edtFixtureAndFittingWindows.setEnabled(true);
            binding.edtFixtureAndFittingWindowProtection.setEnabled(true);
            binding.edtFixtureAndFittingDoorsOther.setEnabled(true);


            binding.edtFinishersAndServicesWallFinisher.setEnabled(true);
            binding.edtFinishersAndServicesFloorFinisher.setEnabled(true);
            binding.edtFinishersAndServicesBathroomAndToilet.setEnabled(true);
            binding.edtFinishersAndServicesServices.setEnabled(true);


            binding.btnSaveBbuttonDetails.setVisibility(View.VISIBLE);
            binding.btnUpdate.setVisibility(View.GONE);
            binding.btnAddOwnerDetails.setVisibility(View.VISIBLE);

        } else {
            binding.edtFloors.setEnabled(false);
            binding.editCondition.setEnabled(false);
            binding.edtAqPotion.setEnabled(false);
            binding.edtBuildingCategory.setEnabled(false);
            binding.edtBuildingNo.setEnabled(false);
            binding.edtBuildingName.setEnabled(false);
            binding.btnSaveBbuttonDetails.setVisibility(View.GONE);
            binding.btnUpdate.setVisibility(View.VISIBLE);
            binding.edtExpectedLife.setEnabled(false);
            binding.edtMinusFloors.setEnabled(false);

            binding.rcOwnerDetails.setEnabled(false);

            binding.edtTotalFloorArea.setEnabled(false);


            binding.edtBuildingClass.setEnabled(false);
            binding.edtNatureOfBuilding.setEnabled(false);

            binding.btnAddOwnerDetails.setVisibility(View.GONE);

            binding.edtRoofRoofMaterial.setEnabled(false);
            binding.edtRoofFrame.setEnabled(false);
            binding.edtRoofFinisher.setEnabled(false);
            binding.edtCelling.setEnabled(false);

            binding.edtFloorStructure.setEnabled(false);
            binding.edtFoundationStructure.setEnabled(false);
            binding.edtWallStructure.setEnabled(false);


            binding.edtFixtureAndFittingDoor.setEnabled(false);
            binding.edtFixtureAndFittingDoorsBathroomAndToiletFittings.setEnabled(false);
            binding.edtFixtureAndFittingDoorsHandRail.setEnabled(false);
            binding.edtFixtureAndFittingDoorsPantryCupbord.setEnabled(false);
            binding.edtFixtureAndFittingWindows.setEnabled(false);
            binding.edtFixtureAndFittingWindowProtection.setEnabled(false);
            binding.edtFixtureAndFittingDoorsOther.setEnabled(false);


            binding.edtFinishersAndServicesWallFinisher.setEnabled(false);
            binding.edtFinishersAndServicesFloorFinisher.setEnabled(false);
            binding.edtFinishersAndServicesBathroomAndToilet.setEnabled(false);
            binding.edtFinishersAndServicesServices.setEnabled(false);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                boolean isUpdate = data.getBooleanExtra(Constant.TYPE, false);
                OwnerDetail crOwnerDetails = data.getParcelableExtra(Constant.DETAILS);
                if (!isUpdate) {

                    ownerDetails.add(crOwnerDetails);
                    ownerDetailAdapter.setDetails(new ArrayList<>(ownerDetails));

                } else {
                    // CR cr = getConditionalReport();
                    //List<OwnerDetail> details = building.getOwnerDetails();
                    ownerDetails.set(position, crOwnerDetails);
                    //updateOwnerConinfo(details);
                    //conditionalReport = getConditionalReport();
                    ownerDetailAdapter.setDetails(new ArrayList<>(ownerDetails));

                }
            }
        } else if (requestCode == RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                // CR cr = getConditionalReport();
                // List<OwnerDetail> details = building.getOwnerDetails();
                ownerDetails.remove(position);
                // updateOwnerConinfo(details);
                // conditionalReport = getConditionalReport();
                ownerDetailAdapter.setDetails(new ArrayList<>(ownerDetails));

            }
        } else if (requestCode == REQUEST_CODE_MSS) {
            if (resultCode == Activity.RESULT_OK) {
                String type = data.getStringExtra(Constant.TYPE);
                ArrayList<MultiSelectSpinner> ms = new ArrayList<>(data.getParcelableArrayListExtra(Constant.DETAILS));
                if (type.equals("FixtureAndFittingDoorsHandRail")) {
                    fixtureAndFittingDoorsHandRailList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtFixtureAndFittingDoorsHandRail.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtFixtureAndFittingDoorsHandRail.setText("Select Fixture And Fitting Doors HandRail");
                    }
                } else if (type.equals("FoundationStructure")) {
                    foundationStructureList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtFoundationStructure.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtFoundationStructure.setText("Select Foundation Structure");
                    }
                } else if (type.equals("FloorStructure")) {
                    floorStructureList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtFloorStructure.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtFloorStructure.setText("Select Floor Structure");
                    }
                } else if (type.equals("WallStructure")) {
                    wallStructureList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtWallStructure.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtWallStructure.setText("Select Wall Structure");
                    }

                } else if (type.equals("RoofRoofMaterial")) {
                    roofMaterialList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtRoofRoofMaterial.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtRoofRoofMaterial.setText("Select Roof Material");
                    }
                } else if (type.equals("RoofFinisher")) {
                    roofFinisherList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtRoofFinisher.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtRoofFinisher.setText("Select Roof Finisher");
                    }
                } else if (type.equals("RoofFrame")) {
                    roofFrameList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtRoofFrame.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtRoofFrame.setText("Select Roof Frame");
                    }
                } else if (type.equals("Celling")) {
                    ceilingList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtCelling.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtCelling.setText("Select Celling");
                    }
                } else if (type.equals("FixtureAndFittingDoor")) {
                    fixtureAndFittingDoorList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtFixtureAndFittingDoor.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtFixtureAndFittingDoor.setText("Select Fixture And Fitting Door");
                    }
                } else if (type.equals("FixtureAndFittingWindows")) {
                    fixtureAndFittingWindowsList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtFixtureAndFittingWindows.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtFixtureAndFittingWindows.setText("Select Fixture And Fitting Windows");
                    }
                } else if (type.equals("FixtureAndFittingDoorsBathroomAndToiletFittings")) {
                    fixtureAndFittingDoorsBathroomAndToiletFittingsList = ms;

                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtFixtureAndFittingDoorsBathroomAndToiletFittings.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtFixtureAndFittingDoorsBathroomAndToiletFittings.setText("Select Fixture And Fitting Doors Bathroom And Toilet Fittings");
                    }
                } else if (type.equals("FixtureAndFittingDoorsPantryCupbord")) {
                    fixtureAndFittingDoorsPantryCupbordList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtFixtureAndFittingDoorsPantryCupbord.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtFixtureAndFittingDoorsPantryCupbord.setText("Select Fixture And Fitting Doors Pantry Cupboard");
                    }
                } else if (type.equals("FixtureAndFittingWindowProtection")) {
                    fixtureAndFittingWindowProtectionList = ms;

                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtFixtureAndFittingWindowProtection.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtFixtureAndFittingWindowProtection.setText("Select Fixture And Fitting Window Protection");
                    }
                } else if (type.equals("FixtureAndFittingDoorsOther")) {
                    fixtureAndFittingDoorsOtherList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtFixtureAndFittingDoorsOther.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtFixtureAndFittingDoorsOther.setText("Select Fixture And Fitting Doors Other");
                    }
                } else if (type.equals("FinishersAndServicesWallFinisher")) {
                    finishersAndServicesWallFinisherList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtFinishersAndServicesWallFinisher.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtFinishersAndServicesWallFinisher.setText("Select Finishers And Services Wall Finisher");
                    }
                } else if (type.equals("FinishersAndServicesFloorFinisher")) {
                    finishersAndServicesFloorFinisherList = ms;

                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtFinishersAndServicesFloorFinisher.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtFinishersAndServicesFloorFinisher.setText("Select Finishers And Services Floor Finisher");
                    }
                } else if (type.equals("FinishersAndServicesBathroomAndToilet")) {
                    finishersAndServicesBathroomAndToiletList = ms;

                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtFinishersAndServicesBathroomAndToilet.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtFinishersAndServicesBathroomAndToilet.setText("Select Finishers And Services Bathroom And Toilet");
                    }
                } else if (type.equals("FinishersAndServicesServices")) {
                    finishersAndServicesServicesList = ms;

                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtFinishersAndServicesServices.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtFinishersAndServicesServices.setText("Select Finishers And Services Services");
                    }
                }
            }
        }
    }

    private void setList(EditText view, List<String> list) {
        if (list.size() < 0) {
            view.setText("");
        } else {
            String value = ",";
            for (String s : list) {
                value += s;
            }
            view.setText(value.substring(1));
        }
    }

    public void saveOwnerinfo(OwnerDetail ownerDetails) throws IOException {
        List<OwnerDetail> crOwnerDetails = building.getOwnerDetails();
        crOwnerDetails.add(ownerDetails);
        building.setOwnerDetails(crOwnerDetails);
        updateBuildinginfo();
    }

    public void updateOwnerConinfo(List<OwnerDetail> owner) throws IOException {
        building.setOwnerDetails(owner);
        updateBuildinginfo();
    }

    public CR getConditionalReport() {
        return conditionalReport;
    }

    public void updateBuildinginfo() throws IOException {
        BuildingDetails details = conditionalReport.getBuildingDetails();
        details.getBuildings().set(p, building);
        conditionalReport.setBuildingDetails(details);
        ownerDetailAdapter.setDetails(new ArrayList<>(building.getOwnerDetails()));
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }

    public void saveBuildinginfo(BuildingDetails buildingDetails) throws IOException {
        conditionalReport.setBuildingDetails(buildingDetails);
        dataManager.saveCRData(conditionalReport, landName, lotName);
    }


    @Override
    public void onItemClick(int position) {
        if (isUpdate) {
            Intent i = new Intent(BuildingCRDetailActivity.this, BuildingOwnerDetailsActivity.class);
            i.putExtra(Constant.MODE, false);
            this.position = position;
            i.putExtra(Constant.DETAILS, ownerDetails.get(position));
            startActivityForResult(i, REQUEST_CODE);
        }
    }

    @Override
    public void onLongItemClick(int position) {
        if (isUpdate) {
            this.position = position;
            Intent i = new Intent(BuildingCRDetailActivity.this, ConfirmationDialogActivity.class);
            i.putExtra(Constant.NAME, "Are you sure you want to delete this item?");
            startActivityForResult(i, RESULT_CODE);
        }
    }

    private void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


}
