package io.xiges.ximapper.ui.symbol;

import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;

import io.xiges.ximapper.model.Symbol;

public class SymbolController {
    public com.mapbox.mapboxsdk.plugins.annotation.Symbol addFloorSymbol(Symbol symbol, SymbolManager symbolManager){
        Point p =  com.mapbox.geojson.Point.fromLngLat(symbol.getLatLng().getLongitude(), symbol.getLatLng().getLatitude());
        com.mapbox.mapboxsdk.plugins.annotation.Symbol symbol1 = symbolManager.create(new SymbolOptions()
                .withGeometry(p)
                .withDraggable(false)
                .withTextSize(11f)
                .withTextColor("#581845")
                .withTextField(symbol.getName())
        );
        return symbol1;

    }
}
