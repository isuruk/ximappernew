package io.xiges.ximapper.ui.menu;

import android.app.Activity;
import android.content.Intent;
import android.graphics.RectF;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityMenuBinding;
import io.xiges.ximapper.model.masterfile.MasterFileData;
import io.xiges.ximapper.ui.documents.CRDocumentActivity;

public class MenuActivity extends AppCompatActivity implements MenuView{

    private ActivityMenuBinding binding;
    private RectF rectF;
    private String landName = "Test";
    private String lotName = "Test";
    private MasterFileData masterFile = new MasterFileData();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_menu);

        rectF =  getIntent().getParcelableExtra(Constant.LOT);
        landName = getIntent().getStringExtra(Constant.MASTER_FILE);
        lotName= getIntent().getStringExtra(Constant.DETAILS);
        this.masterFile = getIntent().getParcelableExtra(Constant.MASTER_DATA);


        binding.setHandler(this);

    }

    @Override
    public void openSketchTool() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("sketch",true);
        returnIntent.putExtra(Constant.LOT,rectF);
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    @Override
    public void openCR() {
      Intent intent = new Intent(MenuActivity.this, CRDocumentActivity.class);
      intent.putExtra(Constant.MASTER_FILE,landName);
      intent.putExtra(Constant.MASTER_DATA,masterFile);
      intent.putExtra(Constant.LOT,lotName);
        startActivity(intent);
        finish();
    }
}
