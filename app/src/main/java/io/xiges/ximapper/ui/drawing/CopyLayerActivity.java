package io.xiges.ximapper.ui.drawing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityCopyLayerBinding;
import io.xiges.ximapper.model.FloorCopyDetails;

public class CopyLayerActivity extends AppCompatActivity {

    private ActivityCopyLayerBinding binding;
    private List<String> floorList;
    private FloorCopyDetails floor;
    HashMap<Integer, FloorCopyDetails> floorMap;
    private boolean isCopyAQ;
    private boolean isCopyRoom;
    private boolean isCopyFloor;
    FloorCopyDetails detail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_copy_layer);


        List<FloorCopyDetails> details = getIntent().getParcelableArrayListExtra(Constant.DETAILS);
        floorList = new ArrayList<>();
        detail = getIntent().getParcelableExtra(Constant.FLOOR_Label);

        floorMap = new HashMap<>();
        for (int i = 0; i < details.size(); i++) {
            floorMap.put(i,details.get(i));
            floorList.add(details.get(i).getFloorId());
        }


        binding.spFloors.setItem(floorList);

        initSpinner();

        binding.btnCopy.setOnClickListener(v->{
            isCopyAQ = binding.cbAqCopy.isChecked();
            isCopyFloor = binding.cbFloorCopy.isChecked();
            isCopyRoom = binding.cbRoomCopy.isChecked();


            if((isCopyAQ || isCopyFloor || isCopyRoom) && (binding.spFloors.getSelectedItemId()!=-1)){

                FloorCopyDetails copy =new  FloorCopyDetails();
                copy.setFloor(isCopyFloor);
                copy.setAqLine(isCopyAQ);
                copy.setRoom(isCopyRoom);
                Intent returnIntent = new Intent();
                returnIntent.putExtra(Constant.DETAILS, floor);
                returnIntent.putExtra(Constant.FLOOR_Label, detail);
                returnIntent.putExtra(Constant.CONFIG, copy);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }else{
                Toast.makeText(this,"Nothing Selected",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initSpinner() {

        binding.spFloors.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                floor = floorMap.get(position);

                binding.cbAqCopy.setChecked(false);
                binding.cbFloorCopy.setChecked(false);
                binding.cbRoomCopy.setChecked(false);


                if(floor.isAqLine()){
                    binding.cbAqCopy.setEnabled(true);
                }else{
                    binding.cbAqCopy.setEnabled(false);
                }

                if(floor.isFloor()){
                    binding.cbFloorCopy.setEnabled(true);
                }else{
                    binding.cbFloorCopy.setEnabled(false);
                }


                if(floor.isRoom()){
                    binding.cbRoomCopy.setEnabled(true);
                }else{
                    binding.cbRoomCopy.setEnabled(false);
                }

                isCopyAQ = false;
                isCopyFloor = false;
                isCopyRoom = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }
}
