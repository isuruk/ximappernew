package io.xiges.ximapper.ui.alertboxes;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityErrorBoxBinding;

public class ErrorBoxActivity extends AppCompatActivity {

    private ActivityErrorBoxBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_error_box);


        String msg = getIntent().getStringExtra(Constant.NAME);

        binding.tvErrorMessage.setText(msg);

        binding.btnErrorBox.setOnClickListener(v -> finish());

    }

}
