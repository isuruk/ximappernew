package io.xiges.ximapper.ui.lot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityLotSaveBinding;
import io.xiges.ximapper.model.Lot;
import io.xiges.ximapper.model.masterfile.LotNumber;
import io.xiges.ximapper.model.masterfile.MasterFileData;
import io.xiges.ximapper.ui.map.MapViewActivity;

public class LotSaveActivity extends AppCompatActivity {

    ActivityLotSaveBinding binding;
    private List<String> lotList;
    private Map<Integer, LotNumber> lotMap;
    private LotNumber lotNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lot_save);

        lotMap = new HashMap<>();
        lotList = new ArrayList<>();
        MasterFileData masterFileInfoResponse = getIntent().getParcelableExtra(Constant.MASTER_FILE);
        List<LotNumber> lotNumbers = masterFileInfoResponse.getLotNumbers();
        for(LotNumber ln : lotNumbers ){
            lotMap.put(lotList.size(),ln);
            lotList.add(ln.getLotId());
        }

        lotNumber = new LotNumber();
        lotNumber.setLotId("");
        initSpinner();

        if(lotList.size()==0){
            binding.edtLotId.setVisibility(View.GONE);
            binding.edtLotIdEx.setVisibility(View.VISIBLE);
            binding.edtLotName.setEnabled(true);
        }

        binding.btnLotSave.setOnClickListener(v->{
            if(lotList.size()==0){
                String id = binding.edtLotIdEx.getText().toString();
                String name = binding.edtLotName.getText().toString();
                if((!id.equals("") || !id.isEmpty()) && !name.equals("") || !name.isEmpty()){
                    Lot lot = MapViewActivity.lots.get(MapViewActivity.lots.size()-1);

                    lot.setLotId(id);
                    lot.setLotName(name);

                    MapViewActivity.lots.set(MapViewActivity.lots.size()-1,lot);
                    Toast.makeText(LotSaveActivity.this,"Saved",Toast.LENGTH_LONG).show();

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(Constant.NAME,name);
                    returnIntent.putExtra(Constant.ID,id);
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }else {
                    Toast.makeText(LotSaveActivity.this,"All fields required .",Toast.LENGTH_LONG).show();
                }
            }else {
                if (lotNumber != null && (!lotNumber.getLotId().equals("") || !lotNumber.getLotId().isEmpty())) {
                    Lot lot = MapViewActivity.lots.get(MapViewActivity.lots.size() - 1);

                    lot.setLotId(lotNumber.getLotId() + "");
                    lot.setLotName(lotNumber.getId()+"");

                    MapViewActivity.lots.set(MapViewActivity.lots.size() - 1, lot);
                    Toast.makeText(LotSaveActivity.this, "Saved", Toast.LENGTH_LONG).show();

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(Constant.NAME, lot.getLotName());
                    returnIntent.putExtra(Constant.ID, lot.getLotId());
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Toast.makeText(LotSaveActivity.this, "Nothing Selected .", Toast.LENGTH_LONG).show();
                }
            }

        });


    }


    private void initSpinner() {

        binding.edtLotId.setItem(lotList);
        binding.edtLotId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                lotNumber = lotMap.get(position);
               binding.edtLotName.setText(lotNumber.getLotId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }
}
