package io.xiges.ximapper.ui.alertboxes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityConfirmationBinding;

public class ConfirmationDialogActivity extends AppCompatActivity {
    private ActivityConfirmationBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_confirmation);

        String msg = getIntent().getStringExtra(Constant.NAME);
        binding.tvMsg.setText(msg);


        binding.btnCancel.setOnClickListener(v -> finish());

        binding.btnOk.setOnClickListener(v -> {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        });


    }
}
