package io.xiges.ximapper.ui.config;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.webkit.URLUtil;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import javax.inject.Inject;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.databinding.ActivityConfigBinding;
import io.xiges.ximapper.ui.MainActivity;
import io.xiges.ximapper.util.SharedPreferencesHandler;

public class ConfigActivity extends AppCompatActivity {

    @Inject
    SharedPreferences prefs;

    private ActivityConfigBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_config);
        ((XimapperApplication) getApplication()).getAppComponent().inject(this);

        binding.edtIpAddress.setText(prefs.getString(Constant.CONFIG,""));

        binding.btnSave.setOnClickListener(v->{
            String ip = binding.edtIpAddress.getText().toString();
            boolean isValid = URLUtil.isValidUrl( ip);
            if(isValid) {
                new SharedPreferencesHandler().saveConfigSharedPreferences(getApplicationContext(), ip);
                Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_LONG).show();
                MainActivity.ip = ip;
                finish();
            }else {
                Toast.makeText(getApplicationContext(), "Invalid URL", Toast.LENGTH_LONG).show();
            }
        });

    }


}
