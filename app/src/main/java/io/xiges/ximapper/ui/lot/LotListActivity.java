package io.xiges.ximapper.ui.lot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.adapter.LotListAdapter;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityLotListBinding;
import io.xiges.ximapper.model.api.MasterFileListResponse;
import io.xiges.ximapper.model.mixin_clone_model.LotMixinContent;


public class LotListActivity extends AppCompatActivity {

    private ActivityLotListBinding binding;
    private LotListAdapter adapter;
    private List<LotMixinContent> lots;
    private MasterFileListResponse masterFile;
    public static String directory = Environment.getExternalStorageDirectory() +
            File.separator + "Maps" + File.separator + "Land" + File.separator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lot_list);

        lots = new ArrayList<>();

        masterFile = getIntent().getExtras().getParcelable(Constant.MASTER_FILE);

        binding.rcLotList.setLayoutManager(new LinearLayoutManager(this));

        adapter = new LotListAdapter(lots, LotListActivity.this);
        binding.rcLotList.setAdapter(adapter);

        try {
            lots = getLots(masterFile.getMasterFileRefNumber().replaceAll("/", ""));
            adapter.setLots(lots);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void onLotClicked(LotMixinContent lot) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constant.DETAILS, lot);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private File[] getFileList(String masterFileName) {
        String path = directory + masterFileName + File.separator;
        File directory = new File(path);
        File[] files = directory.listFiles();
        return files;
    }

    private File getLatestLotFile(String masterFileName,String lotName) {
        String path = directory + masterFileName;
        File directory = new File(path);
        File[] files = directory.listFiles();

        File f = null;
        if (files.length > 0) {
            for (File file : Arrays.asList(files)) {

                if (!file.isDirectory()) {
                        f = file;
                }
            }
        }
        return f;
    }

    private List<LotMixinContent> getLots(String masterfileName) throws IOException {
        List<LotMixinContent> lots = new ArrayList<>();
        File[] files = getFileList(masterfileName);
        if (files != null) {
            if (files.length > 0) {
                for (File file : Arrays.asList(files)) {
                    if (file.isDirectory()) {
                        File f = getLatestLotFile(masterfileName,file.getName());

                        if(f != null) {
                            Gson gson = new GsonBuilder()
                                    .create();
                            JsonReader reader = new JsonReader(new FileReader(f));
                            LotMixinContent data = gson.fromJson(reader, LotMixinContent.class);
                            lots.add(data);
                        }
                    }
                }
            }
        }
        return lots;

    }


}

