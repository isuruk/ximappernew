package io.xiges.ximapper.ui.map;

public interface MapViewView {
    void lineDraw();
    void polygonDraw();
    void aqnDraw();
    void circleDraw();
    void markPoint();
    void markMeasuredPoint();
    void markCornedPoint();
    void saveDrawing();
    void landDrawingModeBackPressed();
    void buildingLineDraw();
    void buildingPolygonDraw();
    void buildingCircleDraw();
    void buildingSaveDrawing();
    void buildingDrawBackPressed();
    void addFloor();
    void undoLand();
    void redoLand();
    void openGallery();
    void openCamera();
    void buildingRoomDrawing();
    void buildingFloorDrawing();
    void buildingAQDrawing();
    void middlePointMarker();
    void copyLayer();

}
