package io.xiges.ximapper.ui.drawing;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.mapbox.geojson.Point;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityPointMarkerBinding;

public class PointMarkerActivity extends AppCompatActivity {

    ActivityPointMarkerBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_point_marker);

        binding.btnOKLine.setOnClickListener(v->{
            String ft = binding.edtFeetLine.getText().toString();
            String in = binding.edtInchLine.getText().toString();

            boolean a= false,b = false;


            if(ft==null || ft.isEmpty()){
                ft = "0";
            }

            if(in==null || in.isEmpty()){
                in = "0";
            }

            double feet = Double.valueOf(ft);
            double inch = Double.valueOf(in);

            if(inch<12){
                a = true;
            }
            double totInch =  (feet * 12) + inch;
            if(a){

            Intent returnIntent = new Intent();
                returnIntent.putExtra(Constant.DISTANCE, totInch);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }else {
                Toast.makeText(this,"Invalid Values", Toast.LENGTH_LONG).show();
            }
        });

    }

}

