package io.xiges.ximapper.ui.drawing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityFloorAddBinding;

public class FloorAddActivity extends AppCompatActivity {

    boolean up,down;
    ActivityFloorAddBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_floor_add);

        binding.imgUpper.setColorFilter(ContextCompat.getColor(FloorAddActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        binding.imgBase.setColorFilter(ContextCompat.getColor(FloorAddActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);


        binding.imgUpper.setOnClickListener(v->{
            up = true;
            down=false;
            binding.imgUpper.setColorFilter(ContextCompat.getColor(FloorAddActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgBase.setColorFilter(ContextCompat.getColor(FloorAddActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
        });

        binding.imgBase.setOnClickListener(v->{
            up = false;
            down=true;
            binding.imgUpper.setColorFilter(ContextCompat.getColor(FloorAddActivity.this, R.color.half_black), android.graphics.PorterDuff.Mode.SRC_IN);
            binding.imgBase.setColorFilter(ContextCompat.getColor(FloorAddActivity.this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
        });

        binding.btnAdd.setOnClickListener(v->{
            if(validator()){
                String label = binding.editText.getText().toString();
                int floorType = -1 ;
                if(up){
                    floorType = 1;
                }else if(down){
                    floorType = 2;
                }

                Intent returnIntent = new Intent();
                returnIntent.putExtra(Constant.FLOOR_Label, label);
                returnIntent.putExtra(Constant.FLOOR_TYPE, floorType);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }else {
                showErrorMessage(getMessage());
            }
        });

    }


    private void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    private boolean validator(){
        if(TextUtils.isEmpty(binding.editText.getText().toString())) {
            return false;
        }

        if(!up && !down) {
            return false;
        }

        return true;
    }

    private String getMessage(){
        if(TextUtils.isEmpty(binding.editText.getText().toString())) {
            return "Missing Floor Label.";
        }
        if(!up && !down) {
            return "Missing Floor Type.";
        }


        return "";
    }
}
