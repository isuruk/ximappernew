package io.xiges.ximapper.ui.documents;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.adapter.ProposedRatesAdapter;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.FragmentLandInfoBinding;
import io.xiges.ximapper.model.MultiSelectSpinner;
import io.xiges.ximapper.model.api.master_data.AccessCategory;
import io.xiges.ximapper.model.api.master_data.LandUseType;
import io.xiges.ximapper.model.api.master_data.MasterData;
import io.xiges.ximapper.model.cr.Access;
import io.xiges.ximapper.model.cr.Boundaries;
import io.xiges.ximapper.model.cr.CR;
import io.xiges.ximapper.model.cr.General;
import io.xiges.ximapper.model.cr.NameOfVillage;
import io.xiges.ximapper.model.cr.ProposedRate;
import io.xiges.ximapper.model.masterfile.MasterFileData;
import io.xiges.ximapper.model.masterfile.VillageList;
import io.xiges.ximapper.ui.alertboxes.ConfirmationDialogActivity;
import io.xiges.ximapper.ui.helpers.MultiSelectUIActivity;
import io.xiges.ximapper.util.SwipeHelper;

public class LandInfoFragment extends Fragment implements LandInfoView, RecyclerViewClickListener {

    private FragmentLandInfoBinding binding;
    HashMap<Integer, Integer> accessCategoryMap = new HashMap<>();
    HashMap<Integer, Integer> landUseTypeMap = new HashMap<>();
    List<NameOfVillage> villageNameList = new ArrayList<>();
    private ProposedRatesAdapter prAdapter;
    private Context c = null;
    private final int REQUEST_CODE = 478;
    private final int RESULT_CODE = 654;
    private int position = -1;
    private String lotId = "";
    private List<Integer> accesscatItem = new ArrayList<>();
    private List<Integer> landLAndItem = new ArrayList<>();
    private DatePickerDialog datePickerDialog;
    private DatePickerDialog datePickerDialogSection38A;
    private int type = -1;
    private int landUseType;
    int year;
    int month;
    int day;

    int year38;
    int month38;
    int day38;

    private final int REQUEST_CODE_MSS = 87;


    ArrayList<MultiSelectSpinner> villageList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> accessCatList = new ArrayList<>();
    ArrayList<MultiSelectSpinner> landUseList = new ArrayList<>();

    private List<ProposedRate> prs = new ArrayList<>();

    public LandInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_land_info, container, false);
        View view = binding.getRoot();

        return view;
    }


    @Override
    public void onPause() {
        super.onPause();
        OnSave();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
        General general = cr.getGeneral();
        Access access = cr.getAccess();

        MasterData masterData = ((CRDocumentActivity) getActivity()).getMasterData();

        MasterFileData masterFileData = ((CRDocumentActivity) getActivity()).getMasterFile();


        String[] accessCategoryArray = new String[masterData.getLand().getAccessCategory().size()];
        for (int i = 0; i < masterData.getLand().getAccessCategory().size(); i++) {
            accessCategoryMap.put(i, masterData.getLand().getAccessCategory().get(i).getId());
            accessCategoryArray[i] = masterData.getLand().getAccessCategory().get(i).getDescription();
        }


        lotId = ((CRDocumentActivity) getActivity()).getLotName();


        binding.edtLotID.setEnabled(false);
        binding.edtLotID.setText(lotId);

        binding.btnAddPR.setOnClickListener(v -> {
            Intent i = new Intent(getActivity(), PRActivity.class);
            i.putExtra(Constant.MODE, true);
            startActivityForResult(i, REQUEST_CODE);
        });

        String[] landUseTypeArray = new String[masterData.getLand().getLandUseType().size()];
        for (int i = 0; i < masterData.getLand().getLandUseType().size(); i++) {
            landUseTypeMap.put(i, masterData.getLand().getLandUseType().get(i).getId());
            landUseTypeArray[i] = masterData.getLand().getLandUseType().get(i).getDescription();
        }

        for (VillageList item : masterFileData.getVillageList()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getGnId());
            mss.setName(item.getVillageName());
            mss.setIschecked(false);
            villageList.add(mss);
        }

        binding.edtNameOfTheVillage.setOnClickListener(v -> {
            Intent i = new Intent(getActivity(), MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, villageList);
            i.putExtra(Constant.TYPE, "NameOfTheVillage");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });




        prAdapter = new ProposedRatesAdapter(((CRDocumentActivity) getActivity()).getConditionalReport().getAccess().getProposedRates(), c, this);

        binding.rcpr.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rcpr.setAdapter(prAdapter);




        for (AccessCategory item : masterData.getLand().getAccessCategory()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            accessCatList.add(mss);
        }

        binding.edtAccessCategory.setOnClickListener(v -> {
            Intent i = new Intent(getActivity(), MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, accessCatList);
            i.putExtra(Constant.TYPE, "AccessCategory");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });




        for (LandUseType item : masterData.getLand().getLandUseType()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setId(item.getId());
            mss.setName(item.getDescription());
            mss.setIschecked(false);
            landUseList.add(mss);
        }

        binding.edtLandUseType.setOnClickListener(v -> {
            Intent i = new Intent(getActivity(), MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, landUseList);
            i.putExtra(Constant.TYPE, "LandUseType");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });




        if (access != null) {
            binding.setAccess(access);


           accessCatList = new ArrayList<>();
          landUseList = new ArrayList<>();

            for (AccessCategory item : masterData.getLand().getAccessCategory()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (access.getAccessCategory().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtAccessCategory.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                accessCatList.add(mss);
            }

            prs = access.getProposedRates();


            for (LandUseType item : masterData.getLand().getLandUseType()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getId());
                mss.setName(item.getDescription());
                if (access.getLandUse().contains(item.getId())) {
                    mss.setIschecked(true);
                    binding.edtLandUseType.setText("Multiple Items Selected");
                } else {
                    mss.setIschecked(false);
                }
                landUseList.add(mss);
            }

            /*if (access.>0) {
                binding.edtAccessCategory.setSelection(accessCategoryAdapter.getPosition(landInfo.getAccessCategory().getDescription() ));
            }*/
           /* if (access.getL().getDescription() != null) {
                binding.edtLandCategory.setSelection(landCategoryAdapter.getPosition(landInfo.getLandCategory().getDescription() ));
            }
            if (landInfo.getLandSubCategory().getDescription() != null) {
                binding.edtLandSubCategory.setSelection(landSubCategoryAdapter.getPosition(landInfo.getLandSubCategory().getDescription() ));
            }
            if (landInfo.getLandUseType().getDescription() != null) {
                binding.edtLandUseType.setSelection(landUseTypeAdapter.getPosition(landInfo.getLandUseType().getDescription() ));
            }*/


          /*  if (access.getLandUse().size() > 0) {
                int index = -1;
                for (int i : access.getLandUse()) {
                    try {
                        index = landUseTypeMap.get(i);
                    } catch (Exception e) {
                    }
                }
                if (index != -1) {
                    try {
                        binding.edtLandUseType.setSelection(index);
                    } catch (ArrayIndexOutOfBoundsException e) {
                    }
                }
            }*/

        }

        if (general != null) {
            binding.setGeneral(general);
           villageList = new ArrayList<>();

            for (VillageList item : masterFileData.getVillageList()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setId(item.getGnId());
                mss.setName(item.getVillageName());
                mss.setIschecked(false);
                boolean b = false;
                for (NameOfVillage village : general.getNameOfVillage()) {
                    if ((village.getVillageName().equals(item.getVillageName()) && (village.getGnDivision() == item.getGnId()))) {
                        b = true;
                        break;
                    }
                }

                if (b) {
                    mss.setIschecked(true);
                    binding.edtNameOfTheVillage.setText("Multiple Items Selected");

                } else {
                    mss.setIschecked(false);
                }
                villageList.add(mss);
            }

        }

        if (TextUtils.isEmpty(binding.edtDateOfPrepared.getText().toString()) || binding.edtDateOfPrepared.getText().toString().equals("")) {
            Calendar cal = Calendar.getInstance();
            year = cal.get(Calendar.YEAR);
            month = cal.get(Calendar.MONTH);
            day = cal.get(Calendar.DAY_OF_MONTH);
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sf.format(cal.getTime());
            binding.edtDateOfPrepared.setText((date));
        } else {
            String string = binding.edtDateOfPrepared.getText().toString();
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = sf.parse(string);
                cal.setTime(date);
                year = cal.get(Calendar.YEAR);
                month = cal.get(Calendar.MONTH);
                day = cal.get(Calendar.DAY_OF_MONTH);
            } catch (ParseException e) {
                year = cal.get(Calendar.YEAR);
                month = cal.get(Calendar.MONTH);
                day = cal.get(Calendar.DAY_OF_MONTH);
                String date = sf.format(cal.getTime());
                binding.edtDateOfPrepared.setText(date);
            }

        }


        if (TextUtils.isEmpty(binding.edtDateOfSection38A.getText().toString()) || binding.edtDateOfSection38A.getText().toString().equals("")) {
            Calendar cal = Calendar.getInstance();
            year38 = cal.get(Calendar.YEAR);
            month38 = cal.get(Calendar.MONTH);
            day38 = cal.get(Calendar.DAY_OF_MONTH);
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sf.format(cal.getTime());
            binding.edtDateOfSection38A.setText(date);
        } else {
            String string = binding.edtDateOfSection38A.getText().toString();
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = sf.parse(string);
                cal.setTime(date);
                year38 = cal.get(Calendar.YEAR);
                month38 = cal.get(Calendar.MONTH);
                day38 = cal.get(Calendar.DAY_OF_MONTH);
            } catch (ParseException e) {
                year38 = cal.get(Calendar.YEAR);
                month38 = cal.get(Calendar.MONTH);
                day38 = cal.get(Calendar.DAY_OF_MONTH);
                String date = sf.format(cal.getTime());
                binding.edtDateOfSection38A.setText(date);
            }

        }


        datePickerDialog = new DatePickerDialog(c, datePickerListener, year, month, day);

        datePickerDialogSection38A = new DatePickerDialog(c, datePickerListenerSection38A, year38, month38, day38);

        binding.edtDateOfPrepared.setOnClickListener(view -> {
            hideKeyboard(c, view);
            createDialog().show();
        });

        binding.edtDateOfSection38A.setOnClickListener(view -> {
            hideKeyboard(c, view);
            createSection38ADialog().show();
        });


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels / 6;

        SwipeHelper swipeHelper = new SwipeHelper(getActivity(), binding.rcpr, width) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new SwipeHelper.UnderlayButton(getActivity(),
                        getResources().getString(R.string.tv_label_delete),
                        0,
                        ContextCompat.getColor(getActivity(), R.color.colorRed),
                        pos -> {
                            position = pos;
                            Intent i = new Intent(getActivity(), ConfirmationDialogActivity.class);
                            i.putExtra(Constant.NAME, "Are you sure you want to delete this item?");
                            startActivityForResult(i, RESULT_CODE);
                        }
                ));
            }
        };

        swipeHelper.attachSwipe();



        binding.setHandler(this::OnSave);
    }

    public void OnSave() {

        if (validator()) {
            Access access = new Access();



            villageNameList = new ArrayList<>();
            for (MultiSelectSpinner ms : villageList) {
                if (ms.isIschecked()) {
                    NameOfVillage nv = new NameOfVillage();
                    nv.setVillageName(ms.getName());
                    nv.setGnDivision(ms.getId());
                    villageNameList.add(nv);
                }
            }

            accesscatItem = new ArrayList<>();
            for (MultiSelectSpinner ms : accessCatList) {
                if (ms.isIschecked()) {
                    accesscatItem.add(ms.getId());
                }
            }

            landLAndItem = new ArrayList<>();
            for (MultiSelectSpinner ms : landUseList) {
                if (ms.isIschecked()) {
                    landLAndItem.add(ms.getId());
                }
            }



            access.setFrontAge(binding.edtfrontAge.getText().toString());
            access.setAccessDetails(binding.edtAccessDetails.getText().toString());
            access.setDepthOfLength(binding.edtDepthOfLength.getText().toString());
            access.setDescriptionOfLand(binding.edtDescriptionOfLand.getText().toString());
            access.setLevelWithAccess((binding.edtLevelWithAccess.getText().toString()));
            access.setPlantationDetails((binding.edtPlantationDetails.getText().toString()));
            access.setDetailsOfBusiness((binding.edtDetailsofBusiness.getText().toString()));
            access.setAccessCategory(accesscatItem);
            access.setLandUse(landLAndItem);
            access.setProposedRates(prs);

            General general = new General();
            general.setAcquiredExtent(binding.edtAcquiredExtent.getText().toString());
            general.setNameOfVillage(villageNameList);
            general.setAcquisitionName(binding.edtAcquisitionName.getText().toString());
            general.setAssesmentNumber(binding.edtAssessMentNo.getText().toString());
            general.setDateOfPrepared(binding.edtDateOfPrepared.getText().toString());
            general.setDateOfSection38A(binding.edtDateOfSection38A.getText().toString());
            general.setNameOfLand(binding.edtNameOfTheLand.getText().toString());
            general.setRoadName(binding.edtRoadName.getText().toString());
            general.setLandUseDetail(binding.edtLandUseDetail.getText().toString());
            general.setLotId(lotId);
            Boundaries br = new Boundaries();
            br.setBottom(binding.edtBottom.getText().toString());
            br.setTop(binding.edtTop.getText().toString());
            br.setEast(binding.edtEast.getText().toString());
            br.setWest(binding.edtWest.getText().toString());
            br.setNorth(binding.editNorth.getText().toString());
            br.setSouth(binding.edtSouth.getText().toString());


            general.setPlanNumber(binding.edtPlanNumber.getText().toString());
            general.setPrivatePlanNumber(binding.edtPrivatePlanNumber.getText().toString());
            general.setTotalExtent(binding.edtTotalExtent.getText().toString());


            general.setBoundaries(br);



       /* String accessCategoryName = binding.edtAccessCategory.getSelectedItem().toString();
        int accessCategoryId = accessCategoryMap.get(binding.edtAccessCategory.getSelectedItemPosition());
        access.setAccessCategory(new AccessCategory(accessCategoryId,accessCategoryName));*/

    /*    String landCategoryName = binding.edtLandCategory.getSelectedItem().toString();
        int landCategoryId = landCategoryMap.get(binding.edtLandCategory.getSelectedItemPosition());
        access.setLandCategory(new LandCategory(landCategoryId,landCategoryName));

        String landSubCategoryName = binding.edtLandSubCategory.getSelectedItem().toString();
        int landSubCategoryId = landSubCategoryMap.get(binding.edtLandSubCategory.getSelectedItemPosition());
        crLandInfo.setLandSubCategory(new LandSubCategory(landSubCategoryId,landSubCategoryName));
*/

     /*   String landUseTypeName = binding.edtLandUseType.getSelectedItem().toString();
        int landUseTypeId = landUseTypeMap.get(binding.edtLandUseType.getSelectedItemPosition());
        crLandInfo.setLandUseType(new LandUseType(landUseTypeId,landUseTypeName));
*/
            try {
                ((CRDocumentActivity) getActivity()).saveLandInfo(general, access);
                Toast.makeText(c, "Saved..", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            showErrorMessage(getMessage());
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        c = context;

    }

    @Override
    public void onItemClick(int position) {
        Intent i = new Intent(getActivity(), OtherConstructionCRDetailActivity.class);
        i.putExtra(Constant.MODE, false);
        this.position = position;
        i.putExtra(Constant.DETAILS, ((CRDocumentActivity) getActivity()).getConditionalReport().getOtherConstructions().get(position));
        startActivityForResult(i, REQUEST_CODE);
    }

    @Override
    public void onLongItemClick(int position) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                boolean isUpdate = data.getBooleanExtra(Constant.TYPE, false);
                ProposedRate proposedRate = data.getParcelableExtra(Constant.DETAILS);
                if (!isUpdate) {
                    try {
                        ((CRDocumentActivity) getActivity()).savePRinfo(proposedRate);
                        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        prAdapter.setDetails(cr.getAccess().getProposedRates());
                        prs = cr.getAccess().getProposedRates();

                    } catch (Exception e) {
                        showErrorMessage("Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    try {
                        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        List<ProposedRate> details = cr.getAccess().getProposedRates();
                        details.set(position, proposedRate);
                        ((CRDocumentActivity) getActivity()).updatePRinfo(details);
                        cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        prAdapter.setDetails(cr.getAccess().getProposedRates());
                        prs = cr.getAccess().getProposedRates();
                    } catch (Exception e) {
                        showErrorMessage("Something went wrong");
                        e.printStackTrace();
                    }
                }
            }
        } else if (requestCode == RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                    List<ProposedRate> details = cr.getAccess().getProposedRates();
                    details.remove(position);
                    ((CRDocumentActivity) getActivity()).updatePRinfo(details);
                    cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                    prAdapter.setDetails(cr.getAccess().getProposedRates());
                    prs = cr.getAccess().getProposedRates();

                } catch (IOException e) {
                    e.printStackTrace();

                }
            }
        }else if (requestCode == REQUEST_CODE_MSS) {
            if (resultCode == Activity.RESULT_OK) {
                String type = data.getStringExtra(Constant.TYPE);
                ArrayList<MultiSelectSpinner> ms = new ArrayList<>(data.getParcelableArrayListExtra(Constant.DETAILS));
                if (type.equals("NameOfTheVillage")) {
                    villageList = ms;


                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtNameOfTheVillage.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtNameOfTheVillage.setText("Select Village");
                    }
                }else if (type.equals("AccessCategory")){
                    accessCatList = ms;


                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtAccessCategory.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtAccessCategory.setText("Select Access Category");
                    }
                }else if(type.equals("LandUseType")){
                    landUseList = ms;


                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtLandUseType.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtLandUseType.setText("Select Land Use Type");
                    }
                }
            }
        }
    }

    private boolean validator() {
        if (!binding.edtAcquiredExtent.getText().toString().matches("[0-9]*\\.?[0-9]*")) {
            return false;
        }

        if (!binding.edtfrontAge.getText().toString().matches("[0-9]*\\.?[0-9]*")) {
            return false;
        }

        if (!binding.edtDepthOfLength.getText().toString().matches("[0-9]*\\.?[0-9]*")) {
            return false;
        }

        if (!binding.edtTotalExtent.getText().toString().matches("[0-9]*\\.?[0-9]*")) {
            return false;
        }
        return true;
    }

    private String getMessage() {
        if (!binding.edtAcquiredExtent.getText().toString().matches("[0-9]*\\.?[0-9]*")) {
            return "Acquired Extent value is not a number.";
        }
        if (!binding.edtfrontAge.getText().toString().matches("[0-9]*\\.?[0-9]*")) {
            return "Frontage value is not a number.";
        }

        if (!binding.edtDepthOfLength.getText().toString().matches("[0-9]*\\.?[0-9]*")) {
            return "Depth Of Length  value is not a number.";
        }

        if (!binding.edtTotalExtent.getText().toString().matches("[0-9]*\\.?[0-9]*")) {
            return "Total Extent value is not a number.";
        }


        return "";
    }

    private void showErrorMessage(String message) {
        Toast.makeText(c, message, Toast.LENGTH_LONG).show();
    }

    protected Dialog createDialog() {
        datePickerDialog.updateDate(year, month, day);
        return datePickerDialog;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            day = selectedDay;
            month = selectedMonth;
            year = selectedYear;
            Calendar cal = Calendar.getInstance();
            cal.set(year, month, day);
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sf.format(cal.getTime());
            binding.edtDateOfPrepared.setText((date));
        }
    };

    protected Dialog createSection38ADialog() {
        datePickerDialogSection38A.updateDate(year38, month38, day38);
        return datePickerDialogSection38A;
    }


    private DatePickerDialog.OnDateSetListener datePickerListenerSection38A = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            day38 = selectedDay;
            month38 = selectedMonth;
            year38 = selectedYear;
            Calendar cal = Calendar.getInstance();
            cal.set(year38, month38, day38);
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sf.format(cal.getTime());
            binding.edtDateOfSection38A.setText((date));
        }
    };


    private void hideKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


}
