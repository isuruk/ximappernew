package io.xiges.ximapper.ui.login;


import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.databinding.ActivityLogoutConfirmBinding;
import io.xiges.ximapper.util.SharedPreferencesHandler;


public class LogoutConfirmActivity extends AppCompatActivity {

    private ActivityLogoutConfirmBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_logout_confirm);
        ((XimapperApplication) getApplication()).getAppComponent().inject(this);

        binding.btnCancel.setOnClickListener(v-> finish());

        binding.btnLogout.setOnClickListener(v->{
            new SharedPreferencesHandler().logoutClearSharedPref(getApplicationContext());
            finish();
        });
    }
}
