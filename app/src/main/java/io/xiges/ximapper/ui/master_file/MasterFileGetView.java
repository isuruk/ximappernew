package io.xiges.ximapper.ui.master_file;

import io.xiges.ximapper.model.api.MasterFileListResponse;

public interface MasterFileGetView {
    void validateMasterFiles(MasterFileListResponse masterFile);
    void setAdapterFiles();
}
