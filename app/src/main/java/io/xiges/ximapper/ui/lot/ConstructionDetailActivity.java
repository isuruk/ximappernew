package io.xiges.ximapper.ui.lot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.ActivityConstructionDetailBinding;

public class ConstructionDetailActivity extends AppCompatActivity {
    private ActivityConstructionDetailBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_construction_detail);


        binding.btnConstructionSave.setOnClickListener(v->{
                String name = binding.edtName.getText().toString();

                if ((!name.equals("") || !name.isEmpty())) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(Constant.NAME, name);

                    RadioButton rb = findViewById(binding.radioGroup.getCheckedRadioButtonId());
                    int idx = binding.radioGroup.indexOfChild(rb);
                    returnIntent.putExtra(Constant.TYPE, idx);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Toast.makeText(ConstructionDetailActivity.this, "All fields required .", Toast.LENGTH_LONG).show();
                }

        });

    }

    private boolean validator(){
        if(!binding.edtName.getText().toString().matches("[0-9]*\\.?[0-9]*")) {
            return false;
        }


        return true;
    }

    private String getMessage(){
        if(!binding.edtName.getText().toString().matches("[0-9]*\\.?[0-9]*")) {
            return "Construction no is not a number.";
        }


        return "";
    }


    private void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


}
