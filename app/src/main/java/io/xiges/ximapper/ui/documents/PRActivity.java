package io.xiges.ximapper.ui.documents;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.databinding.ActivityPrBinding;
import io.xiges.ximapper.model.api.master_data.LandCategory;
import io.xiges.ximapper.model.api.master_data.LandSubCategory;
import io.xiges.ximapper.model.api.master_data.MasterData;
import io.xiges.ximapper.model.cr.ProposedRate;

public class PRActivity extends AppCompatActivity {


    private ActivityPrBinding binding;
    private boolean isUpdate = false;
    HashMap<Integer, Integer> landCategoryMap = new HashMap<>();
    HashMap<Integer, Integer> landSubCategoryMap = new HashMap<>();
    private List<String> catList;
    private List<String> subCatList;
    private List<LandCategory> landCategories;
    private List<LandSubCategory> landSubCategories;

    private int cat = -1;
    private int subCat = -1;

    @Inject
    SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_pr);

        ((XimapperApplication) getApplication()).getAppComponent().inject(this);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);
        int screenHeight = (int) (metrics.heightPixels * 0.80);

        getWindow().setLayout(screenWidth, screenHeight);

        ProposedRate pr = getIntent().getParcelableExtra(Constant.DETAILS);
        boolean mode = getIntent().getBooleanExtra(Constant.MODE, false);

        catList = new ArrayList<>();

        Gson gson = new Gson();
        String json = prefs.getString(Constant.MASTER_DATA, "");
        MasterData masterData = gson.fromJson(json, MasterData.class);

        for (int i = 0; i < masterData.getLand().getLandCategory().size(); i++) {
            landCategoryMap.put(i, masterData.getLand().getLandCategory().get(i).getId());
            catList.add(masterData.getLand().getLandCategory().get(i).getDescription());
        }
        landCategories = new ArrayList<>(masterData.getLand().getLandCategory());


        binding.edtLandCategory.setItem(catList);

        binding.edtLandCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position != -1) {
                    cat = landCategoryMap.get(position);

                    subCatList = new ArrayList<>();
                    landSubCategories = new ArrayList<>();
                    for (LandSubCategory subCat : masterData.getLand().getLandSubCategory()) {
                        if (subCat.getCategoryId() == (landCategories.get(position).getId())) {
                            landSubCategories.add(subCat);
                        }
                    }

                    for (int i = 0; i < landSubCategories.size(); i++) {
                        landSubCategoryMap.put(i, landSubCategories.get(i).getId());
                        subCatList.add(landSubCategories.get(i).getDescription());
                    }
                    binding.edtLandSubCategory.setItem(subCatList);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        binding.edtLandSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position != -1) {
                    subCat = landSubCategoryMap.get(position);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        if (pr != null) {
            binding.setData(pr);

            if (pr.getLandCategory() > 0) {
                int i = catList.indexOf(pr.getLandCategory());
                if (i != -1) {
                    binding.edtLandCategory.setSelection(i);
                }
            }

            if (pr.getLandSubCategory() > 0) {
                int i = subCatList.indexOf(pr.getLandSubCategory());
                if (i != -1) {
                    binding.edtLandSubCategory.setSelection(i);
                }
            }
        }


        binding.btnUpdate.setOnClickListener(v -> {
            isUpdate = true;
            // changeMode(true);
        });

        binding.btnCancel.setOnClickListener(v -> finish());


        binding.btnSaveBbuttonDetails.setOnClickListener(v -> {
            if (validator()) {
                ProposedRate proposedRate = new ProposedRate();
                proposedRate.setExtent(binding.edtExtent.getText().toString());
                proposedRate.setRemark(binding.edtRemark.getText().toString());
                proposedRate.setLandCategory(cat);
                proposedRate.setLandSubCategory(subCat);
                Intent returnIntent = new Intent();
                returnIntent.putExtra(Constant.DETAILS, proposedRate);
                returnIntent.putExtra(Constant.TYPE, isUpdate);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            } else {
                showErrorMessage(getMessage());
            }
        });

    }

    private boolean validator() {
        if (!binding.edtExtent.getText().toString().matches("[0-9]+")) {
            return false;
        }
        return true;
    }

    private String getMessage() {
        if (!binding.edtExtent.getText().toString().matches("[0-9]+")) {
            return "Extent value is not a number.";
        }
        return "";
    }

    private void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

}
