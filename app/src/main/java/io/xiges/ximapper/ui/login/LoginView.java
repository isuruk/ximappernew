package io.xiges.ximapper.ui.login;

public interface LoginView {
   void onLoginPressed();
}
