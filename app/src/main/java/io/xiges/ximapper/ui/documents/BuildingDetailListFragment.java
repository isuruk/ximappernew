package io.xiges.ximapper.ui.documents;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.xiges.ximapper.R;
import io.xiges.ximapper.adapter.BuildingDetailAdapter;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.databinding.FragmentBuildingDetailListBinding;
import io.xiges.ximapper.model.cr.Building;
import io.xiges.ximapper.model.cr.BuildingDetails;
import io.xiges.ximapper.model.cr.CR;

public class BuildingDetailListFragment extends Fragment implements RecyclerViewClickListener {

    private FragmentBuildingDetailListBinding binding;
    private final int REQUEST_CODE = 535;
    private final int RESULT_CODE = 155;
    private int position = -1;
    private Context c = null;
    private BuildingDetailAdapter adapter;
    private BuildingDetails buildingDetails;
    private String landName = "Test";
    private String lotName = "Test";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_building_detail_list, container, false);
        View view = binding.getRoot();

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                boolean isUpdate = data.getBooleanExtra(Constant.TYPE, false);
              /*  if (!isUpdate) {
                    Building building = data.getParcelableExtra(Constant.DETAILS);
                    try {

                        CR c = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        BuildingDetails buildingDetails = c.getBuildingDetails();
                        if (buildingDetails != null) {
                            List<Building> buildings = buildingDetails.getBuildings();
                            buildings.add(building);
                            buildingDetails.setBuildings(buildings);
                        } else {
                            List<Building> buildings = new ArrayList<>();
                            buildings.add(building);
                            buildingDetails.setBuildings(buildings);

                        }
                        ((CRDocumentActivity) getActivity()).saveBuildinginfo(buildingDetails);
                        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        adapter.setDetails(cr.getBuildingDetails().getBuildings());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {*/
                    Building building = data.getParcelableExtra(Constant.DETAILS);
                    try {
                        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        BuildingDetails details = cr.getBuildingDetails();
                        details.getBuildings().set(position, building);
                        ((CRDocumentActivity) getActivity()).updateBuildinginfo(details);
                        cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                        adapter.setDetails(cr.getBuildingDetails().getBuildings());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
             //   }
            }
        } else if (requestCode == RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                    BuildingDetails details = cr.getBuildingDetails();
                    ((CRDocumentActivity) getActivity()).removeBuildingFromMap(details.getBuildings().get(position));
                    details.getBuildings().remove(position);
                    ((CRDocumentActivity) getActivity()).updateBuildinginfo(details);
                    cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                    adapter.setDetails(cr.getBuildingDetails().getBuildings());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        buildingDetails = new BuildingDetails();

        lotName = ((CRDocumentActivity) getActivity()).getLotName();
        landName = ((CRDocumentActivity) getActivity()).getLandName();

        binding.btnAddBuildingDetails.setOnClickListener(v -> {
            Intent i = new Intent(getActivity(), BuildingCRDetailActivity.class);
            i.putExtra(Constant.MODE, true);
            i.putExtra(Constant.MASTER_FILE,landName);
            i.putExtra(Constant.LOT,lotName);
            startActivityForResult(i, REQUEST_CODE);
        });
        buildingDetails = ((CRDocumentActivity) getActivity()).getConditionalReport().getBuildingDetails();
        binding.edtDescription.setText(buildingDetails.getExplanation());
        adapter = new BuildingDetailAdapter(buildingDetails.getBuildings(), c, this);

        binding.rcBuildingDetails.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rcBuildingDetails.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(binding.rcBuildingDetails.getContext(),
                LinearLayout.VERTICAL);
        binding.rcBuildingDetails.addItemDecoration(dividerItemDecoration);
        adapter.setDetails(buildingDetails.getBuildings());


        binding.btnSaveBuildingDetails.setOnClickListener(v->{
            try {

                CR c = ((CRDocumentActivity) getActivity()).getConditionalReport();
                BuildingDetails buildingDetails = c.getBuildingDetails();
                buildingDetails.setExplanation(binding.edtDescription.getText().toString());
                if (buildingDetails != null) {
                    List<Building> buildings = buildingDetails.getBuildings();
                    buildingDetails.setBuildings(buildings);
                } else {
                    List<Building> buildings = new ArrayList<>();
                    buildingDetails.setBuildings(buildings);
                }
                ((CRDocumentActivity) getActivity()).saveBuildinginfo(buildingDetails);
                CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
                adapter.setDetails(cr.getBuildingDetails().getBuildings());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels / 6;
/*
        SwipeHelper swipeHelper = new SwipeHelper(getActivity(), binding.rcBuildingDetails, width) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new SwipeHelper.UnderlayButton(getActivity(),
                        getResources().getString(R.string.tv_label_delete),
                        0,
                        ContextCompat.getColor(getActivity(), R.color.colorRed),
                        pos -> {
                            position = pos;
                            Intent i = new Intent(getActivity(), ConfirmationDialogActivity.class);
                            i.putExtra(Constant.NAME, "Are you sure you want to delete this item?");
                            startActivityForResult(i, RESULT_CODE);
                        }
                ));
            }
        };

        swipeHelper.attachSwipe();*/

    }

    @Override
    public void onItemClick(int position) {
        Intent i = new Intent(getActivity(), BuildingCRDetailActivity.class);
        i.putExtra(Constant.MODE, false);
        this.position = position;
        CR cr = ((CRDocumentActivity) getActivity()).getConditionalReport();
        i.putExtra(Constant.DETAILS, (cr.getBuildingDetails().getBuildings().get(position)));
        i.putExtra(Constant.MASTER_FILE,landName);
        i.putExtra(Constant.LOT,lotName);
        i.putExtra(Constant.CONFIG,position);
        startActivityForResult(i, REQUEST_CODE);
    }

    @Override
    public void onLongItemClick(int position) {

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        c = context;

    }
}
