package io.xiges.ximapper.ui.documents;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.xiges.ximapper.R;
import io.xiges.ximapper.app.Constant;
import io.xiges.ximapper.app.XimapperApplication;
import io.xiges.ximapper.databinding.ActivityOtherConstructionCrdetailBinding;
import io.xiges.ximapper.model.MultiSelectSpinner;
import io.xiges.ximapper.model.api.master_data.MasterData;
import io.xiges.ximapper.model.api.master_data.OtherConstructionUnit;
import io.xiges.ximapper.model.api.master_data.OtherConstructionsMaterial;
import io.xiges.ximapper.model.api.master_data.OtherConstructionsType;
import io.xiges.ximapper.model.cr.OtherConstruction;
import io.xiges.ximapper.model.cr.OtherConstructionsMaterials;
import io.xiges.ximapper.ui.helpers.MultiSelectUIActivity;

public class OtherConstructionCRDetailActivity extends AppCompatActivity {

    private ActivityOtherConstructionCrdetailBinding binding;
    private boolean isUpdate = false;
    private List<String> unitItems = new ArrayList<>();
    private Map<String, Integer> unitListMap = new HashMap<>();
    private List<String> otherConstructionTypes;
    private List<String> metirials;
    HashMap<Integer, String> otherConstructionTypeMap;
    HashMap<Integer, OtherConstructionsMaterials> metirialMap;
    HashMap<String, List<OtherConstructionsMaterial>> metirialsFiltered;
    private final int REQUEST_CODE_MSS = 145;


    ArrayList<MultiSelectSpinner> unitList = new ArrayList<>();

    OtherConstructionsMaterials material = null;
    int type = -1;
    String code ="";
    String name = "";
    String no = "";

    @Inject
    SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_other_construction_crdetail);
        ((XimapperApplication) getApplication()).getAppComponent().inject(this);


        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);
        int screenHeight = (int) (metrics.heightPixels * 0.80);


        otherConstructionTypes = new ArrayList<>();
        metirials = new ArrayList<>();

        getWindow().setLayout(screenWidth, screenHeight);

        Gson gson = new Gson();
        String json = prefs.getString(Constant.MASTER_DATA, "");
        MasterData masterData = gson.fromJson(json, MasterData.class);


        OtherConstruction otherConstruction = getIntent().getParcelableExtra(Constant.DETAILS);
        boolean mode = getIntent().getBooleanExtra(Constant.MODE, false);


        name =  otherConstruction.getName();

        otherConstructionTypeMap = new HashMap<>();
        for (int i = 0; i < masterData.getOtherConstruction().getOtherConstructionsTypes().size(); i++) {
            otherConstructionTypeMap.put(i, masterData.getOtherConstruction().getOtherConstructionsTypes().get(i).getCode());
            otherConstructionTypes.add(masterData.getOtherConstruction().getOtherConstructionsTypes().get(i).getDescription());
        }

        metirialsFiltered = new HashMap<>();




        String[] unitArray = new String[masterData.getOtherConstruction().getOtherConstructionUnits().size()];
        HashMap<Integer, String> unitMap = new HashMap<>();
        for (int i = 0; i < masterData.getOtherConstruction().getOtherConstructionUnits().size(); i++) {
            unitMap.put(i, masterData.getOtherConstruction().getOtherConstructionUnits().get(i).getCode());
            unitArray[i] = masterData.getOtherConstruction().getOtherConstructionUnits().get(i).getTypeDescription();
        }


        metirialMap = new HashMap<>();
        for (OtherConstructionsType other : masterData.getOtherConstruction().getOtherConstructionsTypes()) {
            List<OtherConstructionsMaterial> materialList = new ArrayList<>();
            for (OtherConstructionsMaterial otherConstructionsMaterial : masterData.getOtherConstruction().getOtherConstructionsMaterials()) {
                if (otherConstructionsMaterial.getOtherConstructionType().equals(other.getCode())) {
                    materialList.add(otherConstructionsMaterial);
                }
            }

            metirialsFiltered.put(other.getCode(), materialList);
        }





        binding.edtMetirial.setItem(metirials);
        binding.edtOtherConstructionType.setItem(otherConstructionTypes);

        binding.edtMetirial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                material = metirialMap.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        binding.edtOtherConstructionType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                type = position;
                metirialMap = new HashMap<>();
                metirials = new ArrayList<>();

                String code = "";
                code = otherConstructionTypeMap.get(type);

                List<OtherConstructionsMaterial> m = new ArrayList<>(metirialsFiltered.get(code));

                for (int i = 0; i < m.size(); i++) {
                    OtherConstructionsMaterials otherConstructionsMaterials = new OtherConstructionsMaterials();
                    otherConstructionsMaterials.setCode(m.get(i).getCode());
                    otherConstructionsMaterials.setOtherConstructionType(m.get(i).getOtherConstructionType());
                    otherConstructionsMaterials.setDescription(m.get(i).getDescription());
                    otherConstructionsMaterials.setUnit(m.get(i).getUnit());
                    metirialMap.put(i, otherConstructionsMaterials);
                    metirials.add(m.get(i).getDescription());
                    Log.e("XX",otherConstructionsMaterials.getUnit()+"");

                }
                binding.edtMetirial.setItem(metirials);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        changeMode(mode);


        for (OtherConstructionUnit item : masterData.getOtherConstruction().getOtherConstructionUnits()) {
            MultiSelectSpinner mss = new MultiSelectSpinner();
            mss.setCode(item.getCode());
            mss.setName(item.getTypeDescription());
            mss.setIschecked(false);
            unitList.add(mss);
        }

        binding.edtUnit.setOnClickListener(v -> {
            Intent i = new Intent(OtherConstructionCRDetailActivity.this, MultiSelectUIActivity.class);
            i.putParcelableArrayListExtra(Constant.DETAILS, unitList);
            i.putExtra(Constant.TYPE, "Unit");
            startActivityForResult(i, REQUEST_CODE_MSS);
        });

        if (otherConstruction != null) {
            binding.setData(otherConstruction);
            code = otherConstruction.getNo();
            unitList = new ArrayList<>();

            if (otherConstruction.getOtherConstructionsType() > 0) {
                int i = otherConstructionTypes.indexOf(otherConstruction.getOtherConstructionsType());
                if (i != -1) {
                    binding.edtOtherConstructionType.setSelection(i);
                }
            }


            if (otherConstruction.getOtherConstructionsType()>=0) {
                binding.edtOtherConstructionType.setSelection(otherConstruction.getOtherConstructionsType());
            }




            if (otherConstruction.getOtherConstructionsMaterials() != null) {
                if (metirialMap.containsValue(otherConstruction.getOtherConstructionsMaterials())) {
                    for (Map.Entry<Integer, OtherConstructionsMaterials> entry : metirialMap.entrySet()) {
                        if (entry.getValue().equals(otherConstruction.getOtherConstructionsMaterials())) {
                            binding.edtMetirial.setSelection(entry.getKey());
                            break;
                        }

                    }
                }
            }


            for (OtherConstructionUnit item : masterData.getOtherConstruction().getOtherConstructionUnits()) {
                MultiSelectSpinner mss = new MultiSelectSpinner();
                mss.setCode(item.getCode());
                mss.setName(item.getTypeDescription());
                if (otherConstruction.getOtherConstructionUnits().contains(item)) {
                    mss.setIschecked(true);
                    binding.edtUnit.setText("Multiple Items Selected");
                } else {
                    mss.setIschecked(false);
                }
                unitList.add(mss);
            }


        }

        binding.btnCancel.setOnClickListener(v -> finish());

        binding.btnUpdate.setOnClickListener(v -> {
            isUpdate = true;
            changeMode(true);
        });


        binding.btnSave.setOnClickListener(v -> {
            if (validator()) {
                OtherConstruction crOtherConstruction = new OtherConstruction();
                int typeId = binding.edtOtherConstructionType.getSelectedItemPosition();
                crOtherConstruction.setOtherConstructionsType(typeId);
                crOtherConstruction.setRemark(binding.edtRemark.getText().toString());
                crOtherConstruction.setExtent(binding.edtExtent.getText().toString());
                crOtherConstruction.setNo(code);
                crOtherConstruction.setName(name);
                for (MultiSelectSpinner ms : unitList) {
                    if (ms.isIschecked()) {
                        unitItems.add(ms.getCode());
                    }
                }

                List<io.xiges.ximapper.model.cr.OtherConstructionUnit> units = new ArrayList<>();
                for (String i : unitItems) {
                    String code = unitMap.get(i);
                    for (OtherConstructionUnit item : masterData.getOtherConstruction().getOtherConstructionUnits()) {
                        if (item.getCode().equals(code)) {
                            io.xiges.ximapper.model.cr.OtherConstructionUnit unit = new io.xiges.ximapper.model.cr.OtherConstructionUnit();
                            unit.setCode(item.getCode());
                            unit.setDescription(item.getTypeDescription());
                            units.add(unit);
                            break;
                        }
                    }
                }
                try {
                    crOtherConstruction.setOtherConstructionUnits(units);
                    OtherConstructionsMaterial om = masterData.getOtherConstruction().getOtherConstructionsMaterials().get(binding.edtOtherConstructionType.getSelectedItemPosition());
                    OtherConstructionsMaterials otherConstructionsMaterials = new OtherConstructionsMaterials();
                    otherConstructionsMaterials.setCode(om.getCode());
                    otherConstructionsMaterials.setDescription(om.getDescription());
                    otherConstructionsMaterials.setOtherConstructionType(om.getOtherConstructionType());
                    otherConstructionsMaterials.setUnit(om.getUnit());
                    crOtherConstruction.setOtherConstructionsMaterials(otherConstructionsMaterials);
                }catch (Exception e){
                    crOtherConstruction.setOtherConstructionsMaterials(new OtherConstructionsMaterials());
                }


                crOtherConstruction.setExtent(binding.edtExtent.getText().toString());
                Intent returnIntent = new Intent();
                returnIntent.putExtra(Constant.DETAILS, crOtherConstruction);
                returnIntent.putExtra(Constant.TYPE, isUpdate);
                setResult(Activity.RESULT_OK, returnIntent);
                Toast.makeText(this, "Saved..", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                showErrorMessage(getMessage());
            }
        });

    }

    private void changeMode(boolean mode) {
        if (mode) {
            binding.edtUnit.setEnabled(true);
            binding.edtOtherConstructionType.setEnabled(true);
            binding.edtExtent.setEnabled(true);
            binding.edtRemark.setEnabled(true);
            binding.edtMetirial.setEnabled(true);
            binding.edtUnit.setEnabled(true);
            binding.btnSave.setVisibility(View.VISIBLE);
            binding.btnUpdate.setVisibility(View.GONE);

        } else {
            binding.edtUnit.setEnabled(false);
            binding.edtOtherConstructionType.setEnabled(false);
            binding.edtUnit.setEnabled(false);
            binding.btnSave.setVisibility(View.GONE);
            binding.btnUpdate.setVisibility(View.VISIBLE);
            binding.edtExtent.setEnabled(false);
            binding.edtRemark.setEnabled(false);
            binding.edtMetirial.setEnabled(false);

        }
    }

    private boolean validator() {
        if (!binding.edtExtent.getText().toString().matches("[0-9]*\\.?[0-9]*")) {
            return false;
        }
        return true;
    }

    private String getMessage() {
        if (!binding.edtExtent.getText().toString().matches("[0-9]*\\.?[0-9]*")) {
            return "Extent value is not a number.";
        }
        return "";
    }

    private void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_MSS) {
            if (resultCode == Activity.RESULT_OK) {
                String type = data.getStringExtra(Constant.TYPE);
                ArrayList<MultiSelectSpinner> ms = new ArrayList<>(data.getParcelableArrayListExtra(Constant.DETAILS));
                if (type.equals("Unit")) {
                    unitList = ms;
                    for (MultiSelectSpinner m : ms) {
                        if (m.isIschecked()) {
                            binding.edtUnit.setText("Multiple Items Selected");
                            break;
                        }
                        binding.edtUnit.setText("Select Unit");
                    }
                }
            }
        }

    }

}
