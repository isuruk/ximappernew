package io.xiges.ximapper.network;

import java.util.List;

import io.xiges.ximapper.model.DataSave;
import io.xiges.ximapper.model.Grid;
import io.xiges.ximapper.model.UploadImageSave;
import io.xiges.ximapper.model.api.ImageUploadResponse;
import io.xiges.ximapper.model.api.LoginResponse;
import io.xiges.ximapper.model.api.MasterFileInfoResponse;
import io.xiges.ximapper.model.api.MasterFileListResponse;
import io.xiges.ximapper.model.api.master_data.MasterData;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("oauth/token")
    Call<LoginResponse> login(@Header("Authorization") String authorization, @Field("username") String userName, @Field("password") String password, @Field("grant_type") String grant_type);

    @FormUrlEncoded
    @POST("oauth/token")
    Call<LoginResponse> refreshToken(@Header("Authorization") String authorization, @Field("refresh_token") String refresh_token, @Field("grant_type") String grant_type);

    @Streaming
    @GET
    Call<ResponseBody> downloadFileWithDynamicUrlSync(@Url String fileUrl);

    @GET("v3/grid-section")
    Call<Grid> getGrid(@Query("key") String key, @Query("bounding-box") String bbox, @Query("format") String format);

    @FormUrlEncoded
    @POST("lamasterfile/list")
    Call<List<MasterFileListResponse>> getMasterFile(@Header("Authorization") String authorization, @Field("Name") String name);


    @FormUrlEncoded
    @POST("lamasterfile/details")
    Call<MasterFileInfoResponse> getMasterFileInfo(@Header("Authorization") String authorization, @Query("mfid") int mfid);


    @FormUrlEncoded
    @POST("masterdata/getmasterdata")
    Call<MasterData> getMasterData(@Header("Authorization") String authorization, @Field("Name") String name);

    @Multipart
    @POST("fileupload")
    Call<ImageUploadResponse> uploadImages(@Header("Authorization") String authorization,
                                           @Header("masterFile") String masterFile,
                                           @Header("lotNo") String lotNo,
                                           @Part List<MultipartBody.Part> files);

    @Headers({"Accept:text/plain"})
    @GET("masterdata/checkmasterdata")
    Call<String> checkmasterdata(@Header("Authorization") String authorization);


    @POST("cr/save")
    Call<Object> saveCRData(@Header("Authorization") String authorization, @Body DataSave output);


    @POST("fileupload/save")
    Call<Object> uploadImagesSave(@Header("Authorization") String authorization,
                                  @Body UploadImageSave uploadImageSave);



}
