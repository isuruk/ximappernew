package io.xiges.ximapper.dagger;

import javax.inject.Singleton;

import dagger.Component;
import io.xiges.ximapper.ui.MainActivity;
import io.xiges.ximapper.ui.config.ConfigActivity;
import io.xiges.ximapper.ui.documents.BuildingCRDetailActivity;
import io.xiges.ximapper.ui.documents.BuildingOwnerDetailsActivity;
import io.xiges.ximapper.ui.documents.CRDocumentActivity;
import io.xiges.ximapper.ui.documents.OtherConstructionCRDetailActivity;
import io.xiges.ximapper.ui.documents.OwnerDetailCRActivity;
import io.xiges.ximapper.ui.documents.PRActivity;
import io.xiges.ximapper.ui.images.ImageGalleryActivity;
import io.xiges.ximapper.ui.login.LoginActivity;
import io.xiges.ximapper.ui.login.LogoutConfirmActivity;
import io.xiges.ximapper.ui.map.MapViewActivity;
import io.xiges.ximapper.ui.master_file.MasterFileGetActivity;
import io.xiges.ximapper.util.RefreshToken;
import io.xiges.ximapper.util.SharedPreferencesHandler;

/**
 * Dagger 2 app component class
 *
 */
@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, PresenterModule.class, DataSourceModule.class,})
public interface AppComponent {
    void inject(LoginActivity target);
    void inject(MasterFileGetActivity target);
    void inject(MapViewActivity target);
    void inject(LogoutConfirmActivity target);
    void inject(SharedPreferencesHandler target);
    void inject(RefreshToken target);
    void inject(MainActivity target);
    void inject(BuildingCRDetailActivity target);
    void inject(OwnerDetailCRActivity target);
    void inject(CRDocumentActivity target);
    void inject(OtherConstructionCRDetailActivity target);
    void inject(ImageGalleryActivity target);
    void inject(ConfigActivity target);
    void inject(PRActivity target);
    void inject(BuildingOwnerDetailsActivity target);

}
