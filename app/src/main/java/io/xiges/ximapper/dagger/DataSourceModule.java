package io.xiges.ximapper.dagger;

import dagger.Module;

/**
 * Dagger 2 data source module class
 * use to inject data source modules
 */
@Module
public class DataSourceModule {


}
