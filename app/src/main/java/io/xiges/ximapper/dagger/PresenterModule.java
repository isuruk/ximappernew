package io.xiges.ximapper.dagger;

import dagger.Module;
import dagger.Provides;
import io.xiges.ximapper.ui.login.LoginPresenter;
import io.xiges.ximapper.ui.map.MapViewPresenter;
import io.xiges.ximapper.ui.master_file.MasterFileGetPresenter;

/**
 * Dagger 2 module for presenter classes
 */
@Module
public class PresenterModule {

    @Provides
    LoginPresenter provideLoginPresenter(){
        return new LoginPresenter();
    }

    @Provides
    MasterFileGetPresenter provideMasterFileGetPresenter(){
        return new MasterFileGetPresenter();
    }


    @Provides
    MapViewPresenter provideMapViewPresenter(){
        return new MapViewPresenter();
    }
}
