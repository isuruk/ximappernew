package io.xiges.ximapper.dagger;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.xiges.ximapper.network.ApiInterface;
import io.xiges.ximapper.util.ServiceGenerator;

/**
 * Dagger 2 API interface
 * This class binds Retrofit 2 ApiInterface
 */
@Module
public class NetworkModule {

    @Provides
    @Singleton
    ApiInterface provideApiInterface(){
        return ServiceGenerator.getClient().create(ApiInterface.class);
    }



}
